﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MyProfile.aspx.vb" Inherits="MyProfile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>My Profile - นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์</title>
	<style type="text/css">
		.style1
		{
			color: #FF0000;
		}
		.column-register
		{
			text-align: left;
		}
		.clear
		{
			clear: both;
		}
	</style>
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {

			$("#txtName").val("<%=objSessionVar.ModelUser.UserName%>");
			$("#txtNickName").val("<%=objSessionVar.ModelUser.NickName%>");
			$("#txtMobile").val("<%=objSessionVar.ModelUser.Mobile%>");

			$("#btnSave").click(function () {

				var UserName = $("#txtName").val();
				if (UserName == "") {
					alert("กรุณาระบุชื่อ-นามสกุลด้วยค่ะ")
					return false;
				}
				var NickName = $("#txtNickName").val();
				if (NickName == "") {
					alert("กรุณาระบุชื่อเล่นด้วยค่ะ")
					return false;
				}
				var Mobile = $("#txtMobile").val();
				if (Mobile == "") {
					alert("กรุณาระบุหมายเลขโทรศัพท์ด้วยค่ะ")
					return false;
				}

				$.ajax({
					type: 'POST',
					url: "ajCheckEditProfile.aspx",
					data: { "UserName": UserName, "NickName": NickName, "Mobile": Mobile },
					success: function (msg) {
						alert(msg);
						if (msg == 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ') {
							window.location.href = "MyAccount.aspx";
						}
					}
				});

			});

		});
            
	</script>
</head>
<body>
	<form id="MyProfileForm" runat="server">
	<div align="left" style="margin: auto; width: 700px;">
		<h1>
			ข้อมูลส่วนตัว</h1>
		<div id="ctl00_BodyCenter_panAccount" style="">
			<div class="column-register">
			</div>
			<div class="column-register">
				<b><font color="#ff6600"><span id="ctl00_BodyCenter_Label1">ข้อมูลบัญชีสมาชิก</span></font></b><br />
			</div>
			<br class="clear" />
			<div class="column-register">
				<b><span id="ctl00_BodyCenter_Label2">e-Mail Address</span></b><br />
			</div>
			<div class="column-register">
				<asp:Label ID="lbEMail" runat="server" Text=""></asp:Label>
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>ชื่อ-นามสกุล</b>
			</div>
			<div class="column-register">
				<input name="txtName" id="txtName" style="width: 200px;" type="text" maxlength="300"
					class="required maxlength" />&nbsp; <span class="style1">*</span>&nbsp;
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>ชื่อเล่น</b>
			</div>
			<div class="column-register">
				<input name="txtNickName" id="txtNickName" style="width: 200px;" type="text" maxlength="300"
					class="required maxlength" />&nbsp; <span class="style1">*</span>&nbsp;
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>เบอร์มือถือ</b>
			</div>
			<div class="column-register">
				<input name="txtMobile" id="txtMobile" style="width: 200px;" type="text" maxlength="10"
					class="required maxlength" />&nbsp; <span class="style1">*</span>&nbsp;
			</div>
			<br class="clear" />
		</div>
		<div class="column-register" style="border-top: solid 1px #999999; padding-top: 20px;">
			<input id="btnSave" type="button" name="btnSave" value="Save" class="btn-face" />
		</div>
		<br class="clear" />
	</div>
	</form>
</body>
</html>
