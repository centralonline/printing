Imports System.Data
Imports EnumTrendyPrint

Partial Class GallerysUpload
    Inherits clsHTTPBase

    Private ThisSearchParams As clsCardSearchParam
    Public ThisPageNo As String = 0
    Public ThisPageSize As Integer = 15
    Private ThisSortBy As eCardSortBy = eCardSortBy.CreateOnDESC
    Private ThisOperation As String
    Private ThisCardType As eCardType = eCardType.Gallery

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ThisSearchParams = New clsCardSearchParam()
        Me.ThisSearchParams.SearchKey = ""
        Me.ThisSearchParams.CardType = ThisCardType
        Me.UcBar1.BarStep = 1
        Me.UcCard1.ThisType = "Upload"

        Me.selectEventHandler()
    End Sub

    Private Sub selectEventHandler()
        '--> !!!Only!!! : Set Value Form Request
        Dim TempOperation As String = Request("Operation")
        If (TempOperation Is Nothing) Then
            TempOperation = "ChangeQueryType"
        End If
        If (Request("SearchKey") IsNot Nothing) Then
            Me.ThisSearchParams.SearchKey = Request("SearchKey")
        End If
        If (Request("SortBy") IsNot Nothing) Then
            Me.ThisSortBy = Request("SortBy")
        End If
        If (Request("PageNo") IsNot Nothing) Then
            Me.ThisPageNo = Request("PageNo")
        End If
        If (Request("PageSize") IsNot Nothing) Then
            Me.ThisPageSize = Request("PageSize")
        End If

        '--> Select From Operation
        Select Case TempOperation
            Case "ChangeQueryType"
                Me.onQueryChange()
            Case "ChangeSortBy"
                Me.ThisPageNo = 0
                Me.onSortByChange()
            Case "ChangePage"
                Me.onPageChange()
            Case "ChangePageSize"
                Me.ThisPageNo = 0
                Me.onPageSizeChange()
        End Select

    End Sub

    Private Sub onPageChange()
        Me.showCard()
    End Sub

    Private Sub onPageSizeChange()
        Me.showCard()
    End Sub

    Private Sub onSortByChange()
        Me.showCard()
    End Sub

    Private Sub onQueryChange()
        Me.showCard()
    End Sub

    Private Sub showCard()
        Dim TempCardEngine As New clsCardEngine

        Dim DSCard As DataSet = TempCardEngine.GetCardBySearch(Me.ThisSearchParams, Me.ThisPageNo, Me.ThisPageSize, Me.ThisSortBy)

        '--> show product group info to text label
        Me.BindCard(DSCard.Tables(0))

        '--> refresh total page
        Me.BindPaging(DSCard.Tables(1))
    End Sub

    Private Sub BindCard(ByVal DTCard As DataTable)
        Me.UcCard1.ThisDTCard = DTCard
        Me.UcCard1.ThisRepeat = 3
    End Sub

    Private Sub BindPaging(ByVal DTVariable As DataTable)
        'Paging Top
        Me.UcPaging1.PageNo = Me.ThisPageNo
        Me.UcPaging1.PageSize = Me.ThisPageSize
        Me.UcPaging1.PGCount = DTVariable.Select("name='CardCount'")(0)("value")

        'Paging Footer
        Me.UcPaging2.PageNo = Me.ThisPageNo
        Me.UcPaging2.PageSize = Me.ThisPageSize
        Me.UcPaging2.PGCount = DTVariable.Select("name='CardCount'")(0)("value")
    End Sub

End Class
