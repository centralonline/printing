﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowOrder.aspx.vb" Inherits="Admin_ShowOrder" %>

<%@ Import Namespace="System.Globalization" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Untitled Page</title>
	<link href="Css/AdminTool.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<form id="form1" runat="server" style="padding: 30px; background-color: #fff; margin: auto;
	width: 1600px">
	<h3 style="width: 1480px;" class="head-column">
		View Order</h3>
	<%If ObjOrder IsNot Nothing AndAlso ObjOrder.Count > 0 Then%>
	<div class="datagrid" style="width: 1500px">
		<table id="tbOrder" class="tableOrder">
			<thead>
				<tr style="background-color: #cfe">
					<th align="center" style="width: 200px">
						เลขที่Order
					</th>
					<th align="center" style="width: 300px">
						วันที่สร้างOrder
					</th>
					<th align="center" style="width: 300px">
						สร้างโดย
					</th>
					<th align="center" style="width: 100px">
						รายละเอียด
					</th>
				</tr>
			</thead>
			<tbody>
				<%		Dim Count As Integer = 0
					For Each ObjectOrder As clsOrder In ObjOrder
				%>
				<tr>
					<td align="center">
						<%=ObjectOrder.OrderID%>
					</td>
					<td align="center">
						<%=ObjectOrder.OrderDate.ToString("G", CultureInfo.CreateSpecificCulture("th-TH"))%>
					</td>
					<td align="center">
						<%=ObjectOrder.ShippingAddr1%>
					</td>
					<td align="center">
						<%If ObjectOrder.Type = "N" Then%>
						<a href="../ConfirmOrderPreview.aspx?User=<%=ObjectOrder.CreateBy%>&Order=<%=ObjectOrder.OrderID%>"
							target="_blank">View Datail</a>
						<%Else%>
						<a href="../ConfirmOrderUploadPreview.aspx?User=<%=ObjectOrder.CreateBy%>&Order=<%=ObjectOrder.OrderID%>"
							target="_blank">View Datail</a>
						<%End If%>
					</td>
				</tr>
				<%
					Count += 1
					  Next%>
			</tbody>
		</table>
	</div>
	<%Else%>
	[No Data]
	<%End If%>
	</form>
</body>
</html>
