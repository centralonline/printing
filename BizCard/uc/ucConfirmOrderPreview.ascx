﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucConfirmOrderPreview.ascx.vb"
	Inherits="uc_ucConfirmOrderPreview" %>
<%@ Register Src="ucHead1.ascx" TagName="ucHead1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Untitled Document</title>
</head>
<body>
	<div align="left">
		<table align="center" border="0">
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="0" style="width: 700px">
						<tr>
							<td style="width: 260px; height: 70px;">
								<asp:Image ID="Image1" runat="server" ImageUrl="../images/logo-trendyprint.png" />
							</td>
							<td style="width: 440px; height: 70px;">
								<p style="font-family: Tahoma; color: #666666; font-size: 11px;">
									<b>บริษัท ซีโอแอล จำกัด (มหาชน)</b><br />
									24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ 10250
									<br />
									โทร : 02-739-5555(120 สาย) แฟ็กซ์ : 02-763-5555(30 สาย)<br />
									Website : www.officemate.co.th, e-Mail : <a href="mailto:printing@officemate.co.th">
										printing@officemate.co.th</a>
								</p>
							</td>
						</tr>
					</table>
					<table width="680" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								<br />
								<strong>เลขที่ใบสั่งทำของท่านคือ </strong>
								<asp:Label ID="lbOrderID" runat="server" Text="lbOrderID" Font-Bold="True"></asp:Label>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								วันที่ทำการสั่ง :
								<asp:Label ID="lbDateOrder" runat="server" Text="lbDateOrder"></asp:Label><br />
							</td>
						</tr>
						<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
							<td bgcolor="#CCCCCC" style="font-family: Tahoma; color: #666666; font-size: 11px">
								<table border="0" cellspacing="1" cellpadding="5" style="width: 700px">
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td width="70px" bgcolor="#FFFFFF">
											รหัสลูกค้า:
										</td>
										<td width="220px" bgcolor="#FFFFFF">
											<span class="hilight">
												<asp:Label ID="lbCustID" runat="server" Text="lbCustID"></asp:Label>
											</span>
										</td>
										<td width="70px" bgcolor="#FFFFFF">
											ชื่อลูกค้า:
										</td>
										<td width="260px" bgcolor="#FFFFFF">
											<asp:Label ID="lbCustName" runat="server" Text="lbCustName"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
											เบอร์มือถือ:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbMobile" runat="server" Text="lbMobile"></asp:Label>
										</td>
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
							<td bgcolor="#FFFFFF" style="font-family: Tahoma; color: #666666; font-size: 11px">
								&nbsp;
							</td>
						</tr>
						<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
							<td bgcolor="#CCCCCC" style="font-family: Tahoma; color: #666666; font-size: 11px">
								<table border="0" cellspacing="1" cellpadding="5" style="width: 700px">
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td width="70px" bgcolor="#FFFFFF">
											สถานที่จัดส่ง:
										</td>
										<td width="220px" bgcolor="#FFFFFF">
											<asp:Label ID="lbAddress1" runat="server" Text="lbAddress1"></asp:Label>
										</td>
										<td width="70px" bgcolor="#FFFFFF">
											ผู้รับสินค้า:
										</td>
										<td width="260px" bgcolor="#FFFFFF">
											<asp:Label ID="lbContactName" runat="server" Text="lbContactName"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbAddress2" runat="server" Text="lbAddress2"></asp:Label>
										</td>
										<td bgcolor="#FFFFFF">
											อีเมล์:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbContactEmail" runat="server" Text="lbContactEmail"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbAddress3" runat="server" Text="lbAddress3"></asp:Label>
										</td>
										<td bgcolor="#FFFFFF">
											เบอร์มือถือ:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbContactMobileNo" runat="server" Text="lbContactMobileNo"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbAddress4" runat="server" Text="lbAddress4"></asp:Label>
										</td>
										<td bgcolor="#FFFFFF">
											เบอร์โทร:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbContactPhoneNo" runat="server" Text="lbContactPhoneNo"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
											เบอร์แฟกซ์:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbContactFaxNo" runat="server" Text="lbContactFaxNo"></asp:Label>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								<br />
								<strong>สรุปรายการที่สั่งทำ:</strong>
							</td>
						</tr>
						<tr>
							<td bgcolor="#CCCCCC" style="font-family: Tahoma; color: #666666; font-size: 11px">
								<table border="0" cellpadding="5" cellspacing="1" width="100%">
									<tr>
										<td bgcolor="#C9E4C9">
											<div align="center">
												<strong>รายละเอียด</strong></div>
										</td>
										<td bgcolor="#C9E4C9">
											<div align="center">
												ราคาต่อหน่วย<br />
												(บาท)</div>
										</td>
										<td bgcolor="#C9E4C9">
											<div align="center">
												จำนวนที่สั่งทำ<br />
												(ใบ)</div>
										</td>
										<td bgcolor="#C9E4C9">
											<div align="center">
												จำนวนเงิน<br />
												(บาท)</div>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											รหัสนามบัตร :
											<asp:Label ID="lbCardID" runat="server" Text="lbCardID"></asp:Label>
										</td>
										<td align="right" bgcolor="#FFFFFF" width="100px">
											<asp:Label ID="lbPrice" runat="server" Text="lbPrice"></asp:Label>
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbNumOrder" runat="server" Text="lbNumOrder"></asp:Label>
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbTotal" runat="server" Text="lbTotal"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#ffffff">
											ขนาดนามบัตร :
											<asp:Label ID="lbSize" runat="server" Text="Label"></asp:Label>
										</td>
										<td bgcolor="#ffffff" colspan="2" style="text-align: right">
										</td>
										<td align="right" bgcolor="#ffffff" style="width: 100px">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#ffffff">
											รหัสกระดาษ :
											<asp:Label ID="lbPaperID" runat="server" Text="lbPaperID"></asp:Label>
										</td>
										<td bgcolor="#ffffff" colspan="2" style="text-align: right">
										</td>
										 <td bgcolor="#ffffff" style="text-align: right">
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											ชนิดกระดาษ :
											<asp:Label ID="lbPaperName" runat="server" Text="lbPaperName"></asp:Label>
										</td>
										<td bgcolor="#ffffff" colspan="2" style="text-align: right">
										</td>
										<td bgcolor="#ffffff" style="text-align: right">
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											บริการตัดมุม :
											<asp:Label ID="lbOptionalDesc2" runat="server" Text="lbOptionalDesc2"></asp:Label>
										</td>
										<td colspan="2" align="right" bgcolor="#FFFFFF">
											ค่าบริการ :
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbOptionalPrice2" runat="server" Text="lbOptionalPrice2"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											บริการเคลือบ :
											<asp:Label ID="lbOptionalDesc1" runat="server" Text="lbOptionalDesc1"></asp:Label>
										</td>
										<td colspan="2" align="right" bgcolor="#FFFFFF">
											ค่าบริการ :
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbOptionalPrice1" runat="server" Text="lbOptionalPrice1"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											การชำระเงิน :
											<asp:Label ID="lbPayMentCode" runat="server" Text="lbPayMentCode"></asp:Label>
										</td>
										<td bgcolor="#ffffff" colspan="2" style="text-align: right; height: 25px;">
											ราคาสุทธิ (ก่อน vat) :
										</td>
										<td align="right" bgcolor="#ffffff" style="width: 100px; height: 25px;">
											<asp:Label ID="lbPriceExcVat" runat="server" Text="lbPriceExcVat"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
										</td>
										<td colspan="2" align="right" bgcolor="#FFFFFF">
											ภาษีมูลค่าเพิ่ม 7% :
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbVat" runat="server" Text="lbVat"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
										</td>
										<td colspan="2" bgcolor="#FFFFFF" align="right">
											ค่าจัดส่ง :
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbDelivery" runat="server" Text="lbDelivery"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="right" bgcolor="#ededed" style="height: 24px;" colspan="3">
											จำนวนเงินรวมทั้งสิ้น :
										</td>
										<td align="right" bgcolor="#ededed" style="height: 24px; width: 100px;">
											<asp:Label ID="lbSum" runat="server" Text="lbSum"></asp:Label>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								<br />
								<strong>รูปแบบนามบัตรทีสั่งทำ:</strong>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								<table border="0" align="center" cellpadding="0" cellspacing="0" style="height: 32px;">
									<tr>
										<td style="width: 14px">
											<img alt="" src="images/uc/b_gallery_top_left.gif" />
										</td>
										<td style="background-image: url(images/uc/b_gallery_top.gif)">
										</td>
										<td style="width: 14px">
											<img alt="" src="images/uc/b_gallery_top_right.gif" />
										</td>
									</tr>
									<tr>
										<td style="width: 14px; background-image: url(images/uc/b_gallery_left.gif)">
										</td>
										<td align="center">
											<asp:Image ID="ImageShow" runat="server" />
										</td>
										<td style="width: 14px; background-image: url(images/uc/b_gallery_right.gif)">
										</td>
									</tr>
									<tr>
										<td style="width: 14px">
											<img alt="" src="images/uc/b_gallery_botomleft.gif" />
										</td>
										<td style="background-image: url(images/uc/b_gallery_bottom.gif);">
										</td>
										<td style="width: 14px">
											<img alt="" src="images/uc/b_gallery_bottom_right.gif" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="font-size: 11px; color: #666666; font-family: Tahoma; height: 90px" valign="top">
								<br />
								<asp:DataList ID="dtTextFields" runat="server" BackColor="White" BorderColor="#336666"
									BorderStyle="Double" BorderWidth="3px" CellPadding="4" ForeColor="Black" GridLines="Both"
									Width="700px">
									<FooterStyle BackColor="White" ForeColor="#333333" />
									<SelectedItemStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
									<ItemTemplate>
										<table width="100%">
											<tr>
												<td style="width: 50px; height: 15px; text-align: left;">
													<asp:Label ID="Label1" runat="server" Text='<%# eval("Seq") %>'></asp:Label>.
												</td>
												<td colspan="1" style="height: 15px; width: 80px; text-align: left;">
													<asp:Label ID="Label3" runat="server" Text='<%# eval("FieldName") %>'></asp:Label>
												</td>
												<td colspan="2" style="height: 15px; text-align: left;">
													<asp:Label ID="Label2" runat="server" Text='<%# eval("TextValue") %>'></asp:Label>
												</td>
											</tr>
										</table>
									</ItemTemplate>
									<ItemStyle BackColor="White" ForeColor="#333333" />
									<HeaderTemplate>
										<table width="100%">
											<tr>
												<td style="width: 50px; height: 15px; text-align: center">
													<asp:Label ID="Label1" runat="server" Text="ลำดับที่"></asp:Label>
												</td>
												<td colspan="1" style="height: 15px; text-align: center; width: 50px;">
													<asp:Label ID="Label4" runat="server" Text="หัวข้อ"></asp:Label>
												</td>
												<td colspan="2" style="height: 15px; text-align: center">
													<asp:Label ID="Label2" runat="server" Text="ข้อมูลที่กรอก"></asp:Label>
												</td>
											</tr>
										</table>
									</HeaderTemplate>
									<HeaderStyle BackColor="#C9E4C9" Font-Bold="True" ForeColor="DimGray" />
								</asp:DataList>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px;" valign="top">
								<strong>บริการเสริมพิเศษ : </strong>
								<asp:Label ID="lbview1" runat="server" Text=""></asp:Label>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px; height: 90px;" valign="top">
								<br />
								<strong>หมายเหตุลูกค้า: </strong>
								<br />
								<asp:TextBox ID="lbDescription" runat="server" ReadOnly="True" TextMode="MultiLine"
									Height="80px" Width="504px"></asp:TextBox>
								<br />
							</td>
						</tr>
						<tr>
							<td style="font-size: 11px; color: #666666; font-family: Tahoma; height: 17px">
								<br />
								<strong>Link&nbsp;ใบ RO:</strong>&nbsp;
								<asp:Label ID="lbLinkRO" runat="server" Text=""></asp:Label>
							</td>
						</tr>
						<tr>
							<td style="font-size: 11px; color: #666666; font-family: Tahoma; height: 17px">
								<br />
								<strong>Link Logo:</strong>&nbsp;
								<asp:Label ID="lbLinkLogo" runat="server" Text=""></asp:Label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
