﻿Imports CoreBaseII
Imports System.Data
Imports System.Collections.Generic

Partial Class Admin_ShowCard
    Inherits clsHTTPBase

    Protected oCard As clsCard = Nothing
    Dim Path As String = Server.MapPath("..")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.imgPreview.ImageUrl = Nothing
        Session("oBitmapZ") = Nothing
        Dim CardID As String = Request("CardID")
        Dim CardEngine As New clsCardEngine
        oCard = CardEngine.GetoCardForAdmin(CardID)
        Session("oBitmapZ") = CardEngine.GeneratePreview(oCard, Path, 1.5)
        Me.imgPreview.ImageUrl = "~/FileSenderZ.aspx"
        Me.imgPreview.Visible = True
    End Sub

End Class
