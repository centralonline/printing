﻿
Partial Class Admin_ajEditCard
    Inherits clsHTTPBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'TBBCCards
        Dim CardID As String = Request("CardID")
        Dim CardName1 As String = Request("CardName1")
        Dim BGImage1 As String = Request("BGImage1")
        Dim BGColor1 As String = Request("BGColor1")
        Dim TemplateImage1 As String = Request("TemplateImage1")
        Dim Width1 As String = Request("Width1")
        Dim Height1 As String = Request("Height1")
        Dim Category1 As String = Request("Category1")

        Dim ObjCard As New clsCard

        With ObjCard
            .CardID = CardID
            .CardName = CardName1
            .BGImage = BGImage1
            .BGColor = BGColor1
            .templateImage = TemplateImage1
            .Width = Width1
            .Height = Height1
            .Category = Category1
        End With

        Dim LibCard As New clsCardEngine
        Dim Complete As Boolean = LibCard.EditCardInfo(ObjCard)

        If Complete = False Then
            Response.Write("Error!!! กรุณาลองใหม่อีกครั้งค่ะ")
        Else
            Response.Write("แก้ไขข้อมูลเรียบร้อยแล้วค่ะ")
        End If

    End Sub
End Class
