Imports System.Drawing.Color
Imports UserEngineTrendyPrint

Partial Class uc_ucPayMents
    Inherits clsUCBase
    Protected ProvinceType As Integer

    Private PaymentCodeValue As String
    Public Property PaymentCode() As String
        Get
            Return PaymentCodeValue
        End Get
        Set(ByVal value As String)
            PaymentCodeValue = value
        End Set
    End Property
    Private PaymentTypeValue As String
    Public Property PaymentType() As String
        Get
            Return PaymentTypeValue
        End Get
        Set(ByVal value As String)
            PaymentTypeValue = value
        End Set
    End Property

    Protected Sub rdBank_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Me.PaymentCode = "Cash"
        Me.PaymentType = "Transfer"

    End Sub
    Protected Sub rdDestination_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Me.PaymentCode = "Cash"
        Me.PaymentType = "Cash on Delivery"

    End Sub

    Protected Sub rdCredit_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Me.PaymentCode = "Cash"
        Me.PaymentType = "Credit Card EDC"

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim User As New UserEngine
        Dim ShipProvince As String = objSessionVar.ModelUser.Shipping.Item(0).ShipProvince
        ProvinceType = User.CheckProvince(ShipProvince)

        If Me.rdBank.Checked = True Then
            Me.PaymentCode = "Cash"
            Me.PaymentType = "Transfer"
        ElseIf Me.rdDestination.Checked = True Then
            Me.PaymentCode = "Cash"
            Me.PaymentType = "Cash on Delivery"
        Else
            Me.PaymentCode = "Cash"
            Me.PaymentType = "Credit Card EDC"
        End If

    End Sub
    Protected Overrides Sub OnInit(ByVal e As EventArgs)

        MyBase.OnInit(e)
        Me.rdBank.Checked = True

    End Sub

End Class
