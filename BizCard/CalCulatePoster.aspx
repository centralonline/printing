﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" CodeFile="CalCulatePoster.aspx.vb"
	Inherits="CalCulatePoster" Title="คำนวณราคา Poster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<script src="js/jquery-1.3.2.js" type="text/javascript"></script>
	<style>
		table
		{
			border-collapse: collapse;
			width: 100%;
		}
		table, td, th
		{
			/*border: 1px solid black;*/
		}
		.head-column
		{
			padding: 10px;
			background: #91c3f4;
			font-size: 100%;
			color: #fff;
			font-weight: bold;
		}
	</style>
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$('#btnCalculate').click(function () {
				var Result = "";
				var count = 0;
				$('input[type=checkbox]').each(function () {

					var price = $(this).attr("price");
					var description = $(this).attr("description");
					var paperID = $(this).attr("paperId");

					if (this.checked) {
						var Qty = $(this).parents("tr").find(".Qty").val();
						if (count === 0) {
							Result += (paperID + "," + description + "," + price + "," + Qty);
						} else {
							Result += (":" + paperID + "," + description + "," + price + "," + Qty);
						}
						count++;
						//alert(Result);
						//console.log(Card);
					}
				});

				var url = '<%=ResolveUrl("~/ajCalCulateCard.aspx")%>';
				$.post(url, { Result: Result },
       		            function (data) {
       		            	$("#show").html(data);
       		            }
                 )
			});



			$('#size').change(function () {
				var selected = $(this).find(':selected').attr("value");
				$('#tablePaper tr').show();
				if (selected == "All") {
					$('#tablePaper tr').show();
				}
				else {
					$('#tablePaper tr').not('.tr_size_' + selected).hide();
				}


			});
		});

	</script>

	<div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<div style="margin: 5px;">
			<div class="head-column">
				คำนวณราคา Poster
			</div><br />
			<div>
				<span>ขนาดกระดาษ</span>
				<select id="size" style="width: 200px;">
					<option value="">กรุณาเลือก</option>
					<%
						For Each size As clsCalCulate In ListSizePoster%>
					<option value="<%=size.Size%>">
						<%=size.Size%></option>
					<%Next%>
					<option value="All">ทั้งหมด</option>
				</select>
			</div>
			<br />
			<table id="tablePaper">
				<% 
					For Each item As clsCalCulate In ListPoster%>
				<tr class="tr_size_<%=item.Size%>">
					<td style="width: 800px;">
						<input class="chkSelect" type="checkbox" name="card" description="<%=item.Description%>"
							price="<%=item.Price%>" paperid="<%=item.PaperID%>" />
						<%=item.Description%>
					</td>
					<td style="width: 100px; text-align: right;">
						<span style="color: Red;">
							<%=item.Price%>
							บาท</span>
					</td>
					<td style="width: 150px; text-align: right;">
						<span style="position: relative;"><span class="arrow"></span>
							<select class="Qty">
								<option value="100">100</option>
								<option value="200">200</option>
								<option value="300">300</option>
								<option value="400">400</option>
								<option value="500">500</option>
								<option value="600">600</option>
								<option value="700">700</option>
								<option value="800">800</option>
								<option value="900">900</option>
								<option value="1000">1000</option>
							</select>
							ใบ </span>
					</td>
					<input type="hidden" name="Type" value="<%=item.Type%>" />
				</tr>
				<%Next%>
			</table>
			<input id="btnCalculate" type="button" name="calculate" value="คำนวณราคา" />
			<div id="show" style="margin-top:30px;">
			</div>
		</div>
	</div>
</asp:Content>
