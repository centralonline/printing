Imports System.Data

Partial Class ucCard
    Inherits clsUCBase

    Private ThisRepeatValue As Integer
    Public Property ThisRepeat() As Integer
        Get
            Return ThisRepeatValue
        End Get
        Set(ByVal value As Integer)
            ThisRepeatValue = value
        End Set
    End Property

    Private ThisDTCardValue As DataTable
    Public Property ThisDTCard() As DataTable
        Get
            Return ThisDTCardValue
        End Get
        Set(ByVal value As DataTable)
            ThisDTCardValue = value
            'Me.dlCard.DataSource = value
            'Me.dlCard.DataBind()
        End Set
    End Property
    Private ThisTypeValue As String
    Public Property ThisType() As String
        Get
            Return ThisTypeValue
        End Get
        Set(ByVal value As String)
            ThisTypeValue = value
        End Set
    End Property

    Private ThisVisibleTxTSizeValue As Boolean
    Public Property ThisVisibleTxTSize() As Boolean
        Get
            Return ThisVisibleTxTSizeValue
        End Get
        Set(ByVal value As Boolean)
            ThisVisibleTxTSizeValue = value
        End Set
    End Property

    Private ThisVisibleTxTOrderIDValue As Boolean
    Public Property ThisVisibleTxTOrderID() As Boolean
        Get
            Return ThisVisibleTxTOrderIDValue
        End Get
        Set(ByVal value As Boolean)
            ThisVisibleTxTOrderIDValue = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.dlCard.RepeatColumns = Me.ThisRepeat
        Me.dlCard.DataSource = Me.ThisDTCard
        Me.dlCard.DataBind()

    End Sub

    Protected Sub dlCard_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlCard.ItemDataBound
        If Me.ThisType = "Upload" Then
            Dim Hylink As HyperLink = e.Item.FindControl("HyperLink1")
            Hylink.NavigateUrl = ""
            CType(e.Item.FindControl("txtOrderID"), Label).Visible = False
        ElseIf Me.ThisType = "Temp" Then
            CType(e.Item.FindControl("txtOrderID"), Label).Visible = False
        Else
            CType(e.Item.FindControl("txtSize"), Label).Visible = ThisVisibleTxTSize
            CType(e.Item.FindControl("txtOrderID"), Label).Visible = ThisVisibleTxTOrderID

        End If
    End Sub
End Class
