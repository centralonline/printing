Imports System.Data

Partial Class FileUploaded
    Inherits clsHTTPBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.BindImageUploaded() ' �� UserID
    End Sub

    Private Sub BindImageUploaded()
        Dim CardEngine As New clsCardEngine
        Dim TempImageFields As DataTable = CardEngine.GetListImageByUser(Me.objSessionVar.ModelUser.UserID)
        Me.UcFileUploaded1.ThisImageFields = TempImageFields
        Me.UcFileUploaded1.ThisRepeat = 5
    End Sub
End Class
