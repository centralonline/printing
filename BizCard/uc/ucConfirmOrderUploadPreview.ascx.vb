Imports System.Collections.Generic
Imports System.Data
Imports UserEngineTrendyPrint

Partial Class uc_ucConfirmOrderUploadPreview
	Inherits clsUCBase

	Private OrderIDValue As String
	Public Property OrderID() As String
		Get
			Return OrderIDValue
		End Get
		Set(ByVal value As String)
			OrderIDValue = value
		End Set
	End Property
	Private CardIDValue As String
	Public Property CardID() As String
		Get
			Return CardIDValue
		End Get
		Set(ByVal value As String)
			CardIDValue = value
		End Set
	End Property
	Private CardSizeValue As String
	Public Property CardSize() As String
		Get
			Return CardSizeValue
		End Get
		Set(ByVal value As String)
			CardSizeValue = value
		End Set
	End Property
	Private LogoUrlValue As String
	Public Property LogoUrl() As String
		Get
			Return LogoUrlValue
		End Get
		Set(ByVal value As String)
			LogoUrlValue = value
		End Set
	End Property
	Private UserIDValue As String
	Public Property UserID() As String
		Get
			Return UserIDValue
		End Get
		Set(ByVal value As String)
			UserIDValue = value
		End Set
	End Property
	Private ImagePathValue As String
	Public Property ImagePath() As String
		Get
			ImagePathValue = Me.objAppVar.ImagePath
			Return ImagePathValue
		End Get
		Set(ByVal value As String)
			ImagePathValue = value
		End Set
	End Property
	Private TypeValue As String
	Public Property Type() As String
		Get
			Return TypeValue
		End Get
		Set(ByVal value As String)
			TypeValue = value
		End Set
	End Property


	Protected Overrides Sub OnInit(ByVal e As EventArgs)
		MyBase.OnInit(e)
	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If (Not IsPostBack) Then
			If Request.QueryString("Order") = "" Then
				Me.OrderID = ""
			Else
				Me.OrderID = Request.QueryString("Order")
			End If

			If Request.QueryString("User") = "" Then
				Me.UserID = "printing@officemate.co.th"
			Else
				Me.UserID = Request.QueryString("User")
			End If

			Dim clsOrder As New clsOrderEngine
			Dim listOrder As New List(Of clsOrder)
			listOrder = clsOrder.GetInfoOrders(Me.OrderID)

			Dim Engine As New UserEngine
			Dim UserInfo As New ModelUser
			UserInfo = Engine.GetUserPreview(Me.UserID)

			Dim clsCardEn As New clsCardEngine
			Dim clsCards As New clsCard
			Dim listclsCardImg As New List(Of clsCardImageField)
			clsCards = clsCardEn.GetCardByID(listOrder.Item(0).CardID)
			listclsCardImg = clsCards.ImageFields

			If clsCards Is Nothing Then
				Exit Sub
			End If

			Me.lbCustID.Text = UserInfo.CustID
			Me.lbCustName.Text = UserInfo.UserName
			Me.lbMobile.Text = UserInfo.Mobile
			Me.lbContactName.Text = UserInfo.Contact.ContactName
			Me.lbContactEmail.Text = UserInfo.Contact.ContactEmail.ContactEmail
			Me.lbContactMobileNo.Text = UserInfo.Contact.ContactMobileNo
			Me.lbContactPhoneNo.Text = UserInfo.Contact.ContactPhoneNo
			Me.lbContactFaxNo.Text = UserInfo.Contact.ContactFaxNo

			Me.lbOrderID.Text = listOrder.Item(0).OrderID
			Me.lbDateOrder.Text = listOrder.Item(0).OrderDate.ToString("dd/MM/yyyy HH:mm:ss")
			Me.lbAddress1.Text = listOrder.Item(0).ShippingAddr1
			Me.lbAddress2.Text = listOrder.Item(0).ShippingAddr2
			Me.lbAddress3.Text = listOrder.Item(0).ShippingAddr3
			Me.lbAddress4.Text = listOrder.Item(0).ShippingAddr4
			Me.Type = listOrder.Item(0).Type

			''GIFT Add 26/5/2016

			Me.OptionalDesc1.Text = listOrder.Item(0).OptionalDesc1
			Me.OptionalPrice1.Text = listOrder.Item(0).OptionalPrice1
			Me.OptionalDesc2.Text = listOrder.Item(0).OptionalDesc2
			Me.OptionalPrice2.Text = listOrder.Item(0).OptionalPrice2
			Me.lbPaperID.Text = listOrder.Item(0).PaperID
			If listOrder.Item(0).Promotion <> "0.00" Then
				Me.lbPrice.Text = listOrder.Item(0).Promotion
			Else
				Me.lbPrice.Text = listOrder.Item(0).PaperPrice
			End If
			Me.lbNumOrder.Text = listOrder.Item(0).Qty
			Me.lbTotal.Text = listOrder.Item(0).TotalAmt
			Me.lbPaperName.Text = listOrder.Item(0).PaperName

			''End Add

			Me.dlUploadFile.DataSource = listclsCardImg
			Me.dlUploadFile.DataBind()

			Me.dlAttachFile.DataSource = listclsCardImg
			Me.dlAttachFile.DataBind()

			Dim strPaymentDesc As String = ""
			Select Case listOrder.Item(0).PaymentType
				Case "Transfer"
					strPaymentDesc = "�����¡���͹�Թ ��Һѭ�ո�Ҥ��"
				Case "Cash on Delivery"
					strPaymentDesc = "���Թ���·ҧ�¾�ѡ�ҹ�Ϳ������"
				Case "Credit Card EDC"
					strPaymentDesc = "�����Թ���ºѵ��ôԵ�Ѻ��ѡ�ҹ�Ϳ������"
			End Select

			Me.lbPayMentCode.Text = strPaymentDesc

			'If listOrder.Item(0).TotalAmt < 500 Then
			'	lbDelivery.Text = "50.00"
			'	Me.lbSum.Text = listOrder.Item(0).TotAmt
			'Else
			'	lbDelivery.Text = "0.00"
			'	Me.lbSum.Text = listOrder.Item(0).TotAmt
			'End If

			lbDelivery.Text = listOrder.Item(0).DeliveryAmt
			Me.lbSum.Text = listOrder.Item(0).TotAmt
			Me.lbPriceExcVat.Text = listOrder.Item(0).NetAmt
			Me.lbVat.Text = listOrder.Item(0).VatAmt
			Me.lbPaperID.Text = listOrder.Item(0).PaperID
			Me.lbDescription.Text = listOrder.Item(0).Description

			Dim DetailFTP As DataTable
			DetailFTP = clsOrder.SelectFTPfile(listOrder.Item(0).OrderID)
			If DetailFTP.Rows.Count > 0 Then
				Dim User As String = DetailFTP.Rows(0).Item("UserAccount")
				Dim Pass As String = DetailFTP.Rows(0).Item("Password")
				Dim sqlStr As String
				sqlStr = "<table width='700' border='0' cellspacing='1' cellpadding='5'frame='border' >" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td colspan='2' bgcolor='#C9E4C9' style='width: 700px'><p style='font-family:Tahoma; color:#666666; font-size:11px'><b>������������㹡�� FTP Upload File</b></p></td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td width='320' bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>��ʵ� :</td>" & vbNewLine
				sqlStr += "<td width='380' bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'><a href='ftp://upload.officemate.co.th' target='_blank'>ftp://upload.officemate.co.th</a></td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>�����ҹ :</td>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>" & User & "</td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>���ʼ�ҹ :</td>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>" & Pass & "</td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>�����˵� :</td>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>���ʼ����ҹ�������ö�������� 1 �ҷԵ����ҹ��<br/><a href=""" & ImagePath & "/HowtoOrder.aspx#FTP"" target='_blank'>��ҹ�Ըա�� FTP</a></td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "</table>" & vbNewLine
				Me.lbDetailFTP.Text = sqlStr.ToString()
			End If

			Me.lbLinkRO.Text = "<a href=""" & ImagePath & "/ConfirmOrderUploadPreview.aspx?Order=" & lbOrderID.Text & "&User=" & Me.UserID & """>"
			Me.lbLinkRO.Text += ImagePath & "/ConfirmOrderUploadPreview.aspx?Order=" & lbOrderID.Text & "&User=" & Me.UserID & "</a>"

		End If

	End Sub

	Protected Sub dlUploadFile_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlUploadFile.ItemDataBound
		If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
			If CType(e.Item.FindControl("hidReadyToPrint"), HiddenField).Value = "Y" Then
				CType(e.Item.FindControl("Label5"), Label).Text = "�������觾����"
			Else
				CType(e.Item.FindControl("Label5"), Label).Text = "�Ϳ�Ե��� Design"
			End If
		End If

	End Sub

	Protected Sub dlAttachFile_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlAttachFile.ItemDataBound
		If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
			Dim imageUrl As String = CType(e.Item.FindControl("hidimgurl"), HiddenField).Value
			Dim contentType As String = CType(e.Item.FindControl("hidContentType"), HiddenField).Value
			If contentType.ToLower = "image/jpeg" Or contentType.ToLower = "image/jpg" Or contentType.ToLower = "image/bmp" Or contentType.ToLower = "image/gif" Or contentType.ToLower = "image/giff" Or contentType.ToLower = "image/tif" Or contentType.ToLower = "image/tiff" Then
				CType(e.Item.FindControl("Label6"), Label).Text = "<a href=""" & ImagePath & "/CardImages/UploadImage/Currents/" & imageUrl & """>"
				CType(e.Item.FindControl("Label6"), Label).Text += ImagePath & "/CardImages/UploadImage/Currents/" & imageUrl & "</a>"
			Else
				CType(e.Item.FindControl("Label6"), Label).Text = "<a href=""" & ImagePath & "/CardImages/UploadImage/UploadFiles/" & imageUrl & """>"
				CType(e.Item.FindControl("Label6"), Label).Text += ImagePath & "/CardImages/UploadImage/UploadFiles/" & imageUrl & "</a>"
			End If
		End If
	End Sub

End Class
