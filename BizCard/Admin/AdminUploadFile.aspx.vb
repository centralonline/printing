﻿Imports CoreBaseII
Imports System.Collections.Generic
Imports System.IO

Partial Class Admin_AdminUploadFile
	Inherits clsHTTPBaseAdmin

	Dim strPath As String = Server.MapPath("..\")
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		If Not IsPostBack Then
			IsShowImage()
		End If

	End Sub

	'' เช็ค Type File
	Private Sub IsShowImage()
		Dim TempUploadFile As New clsPaperUpload
		Dim CardID As String = Request.QueryString("CardID")
		Dim BGImage As String = TempUploadFile.GetShowImage(CardID).BGImage
		Dim templateImage As String = TempUploadFile.GetShowImage(CardID).templateImage


		If Not String.IsNullOrEmpty(BGImage) Or BGImage <> String.Empty Then
			Background.ImageUrl = "../CardImages/BGCard/" & BGImage
			txtBGImage.Text = BGImage

		Else
			Background.Visible = False
			txtBGImage.Visible = False
			txtNameBG.Visible = False
		End If
		If Not String.IsNullOrEmpty(templateImage) Or templateImage <> String.Empty Then
			TempImage.ImageUrl = "../CardImages/FullCard/" & templateImage
			txtTempImage.Text = templateImage
		Else
			TempImage.Visible = False
			txtTempImage.Visible = False
			txtNameTemp.Visible = False
		End If
	End Sub

	Private Function CheckUploadFile(ByVal File As HttpPostedFile) As clsResultII
		Dim TempResult As New clsResultII
		Dim ext As String = File.FileName.Substring(File.FileName.LastIndexOf(".") + 1).ToLower()
		Dim FileType As New List(Of String)

		With FileType
			.Add("jpg")
		End With

		For Each TypeFile As String In FileType
			If TypeFile = ext Then
				TempResult.Flag = True
				Exit For
			Else
				TempResult.Flag = False
				TempResult.Message = ext
			End If
		Next
		Return TempResult
	End Function

	Private Function CheckFileUploadEmptry(ByVal FileUpload As FileUpload) As Boolean
		Dim isCheckValid As Boolean = True
		If FileUpload.FileName = "" Then
			MessageBox.Show("กรุณาระบุ File ที่ต้องการ Upload ด้วยค่ะ")
			isCheckValid = False
		End If
		Return isCheckValid
	End Function

	Private Function CopyFiles(ByVal File As HttpPostedFile, ByVal strPath As String) As clsResultII
		Dim TempResult As New clsResultII
		Dim TempFileProvider As New clsFileManagerProvider

		TempFileProvider.CopyFile(File, strPath & "\CardImages\FullCard\" & File.FileName)

		TempResult.Flag = True
		TempResult.Message = File.FileName
		Return TempResult
	End Function


	Private Function CopyFilesBGCard(ByVal File As HttpPostedFile, ByVal strPath As String) As clsResultII
		Dim TempResult As New clsResultII
		Dim TempFileProvider As New clsFileManagerProvider

		TempFileProvider.CopyFile(File, strPath & "\CardImages\BGCard\" & File.FileName)

		TempResult.Flag = True
		TempResult.Message = File.FileName
		Return TempResult
	End Function


	Private Function CheckFont(ByVal txtname As String) As Boolean

		Dim expresion As String = String.Empty
		expresion = "^[a-zA-Z0-9]+-\d{4}-\d{2}_[a-zA-Z0-9].[a-zA-Z0-9]{3}$"

		If (Regex.IsMatch(txtname, expresion)) Then
			Return True
		Else
			MessageBox.Show("กรุณากำหนดชื่อไฟล์ให้ถูกต้องด้วยค่ะ")
			Return False
		End If

		Return True

	End Function


	'' Click UploadBackground
	Protected Sub btnBGImage_Click(sender As Object, e As System.EventArgs) Handles btnBGImage.Click
		Dim File As HttpPostedFile = Me.BGImage.PostedFile

		If CheckBGImage(File) Then
			If CheckFont(File.FileName) Then
				Dim CardID As String = Request.QueryString("CardID")
				Dim FileEngine As New clsFileManagerEngine
				Dim TempUploadFile As New clsPaperUpload

				TempUploadFile.SaveBackground(CardID, BGImage.FileName)

				Background.ImageUrl = "../CardImages/BGCard/" & File.FileName
				txtBGImage.Text = File.FileName

				Background.Visible = True
				txtBGImage.Visible = True
				txtNameBG.Visible = True
			End If
		End If

	End Sub

	Private Function CheckBGImage(File As HttpPostedFile) As Boolean
		Dim isUploadDocument As Boolean = False
		Dim TempResult As New clsResultII

		If CheckFileUploadEmptry(BGImage) = False Then
			Return False
		End If

		TempResult = CheckUploadFile(File)

		If TempResult.Flag Then
			isUploadDocument = True
		Else
			MessageBox.Show("ไม่รองรับไฟล์ประเภท: " + TempResult.Message)
			Return False
		End If

		If isUploadDocument = True Then
			TempResult = CopyFilesBGCard(File, strPath)

		End If
		Return True

	End Function

	Private Function CheckTemplateImage(File As HttpPostedFile) As Boolean
		Dim isUploadDocument As Boolean = False
		Dim TempResult As New clsResultII

		If CheckFileUploadEmptry(TemplateImage) = False Then
			Return False
		End If

		TempResult = CheckUploadFile(File)

		If TempResult.Flag Then
			isUploadDocument = True
		Else
			MessageBox.Show("ไม่รองรับไฟล์ประเภท: " + TempResult.Message)
			Return False
		End If

		If isUploadDocument = True Then
			TempResult = CopyFiles(File, strPath)
		End If

		Return True
	End Function

	''Click UoloadTemplate
	Protected Sub btnTemplate_Click(sender As Object, e As System.EventArgs) Handles btnTemplate.Click

		Dim File As HttpPostedFile = Me.TemplateImage.PostedFile

		If CheckTemplateImage(File) Then
			If CheckFont(File.FileName) Then
				Dim CardID As String = Request.QueryString("CardID")
				Dim FileEngine As New clsFileManagerEngine
				Dim TempUploadFile As New clsPaperUpload

				TempUploadFile.SaveTemplate(CardID, TemplateImage.FileName)

				TempImage.ImageUrl = "../CardImages/FullCard/" & File.FileName
				txtTempImage.Text = File.FileName

				TempImage.Visible = True
				txtNameTemp.Visible = True
				txtTempImage.Visible = True

			End If
		End If
	End Sub

	Protected Sub LinkBack_Click(sender As Object, e As System.EventArgs) Handles LinkBack.Click

		If Session("PageNo") = Nothing And Session("PageNo") = Nothing Then
			Response.Redirect("EditActiveTemplate.aspx")
		Else
			If Session("Cat") = Nothing Then
				Response.Redirect("EditActiveTemplate.aspx?PageNo=" & Session("PageNo").ToString)
			Else
				Response.Redirect("EditActiveTemplate.aspx?PageNo=" & Session("PageNo").ToString & "&Cat=" & Session("Cat").ToString)
			End If


		End If
	End Sub


	Protected Sub btnDelBG_Click(sender As Object, e As System.EventArgs) Handles btnDelBG.Click

		Dim CardID As String = Request.QueryString("CardID")
		Dim FileEngine As New clsPaperUpload
		Dim BGImage As String = FileEngine.GetShowImage(CardID).BGImage
		If BGImage <> String.Empty Then
			If FileEngine.DeleteFileBG(CardID) Then
				Background.Visible = False
				txtBGImage.Visible = False
				txtNameBG.Visible = False
				MessageBox.Show("ดำเนินการลบ BackgroudImage เรียบร้อยแล้วค่ะ")
			End If
		Else
			MessageBox.Show("ไม่พบข้อมูลที่จะลบค่ะ")
		End If

	End Sub

	Protected Sub btnDelTemp_Click(sender As Object, e As System.EventArgs) Handles btnDelTemp.Click
		Dim CardID As String = Request.QueryString("CardID")
		Dim FileEngine As New clsPaperUpload
		Dim templateImage As String = FileEngine.GetShowImage(CardID).templateImage
		If templateImage <> String.Empty Then
			If FileEngine.DeleteFileTemp(CardID) Then
				TempImage.Visible = False
				txtTempImage.Visible = False
				txtNameTemp.Visible = False
				MessageBox.Show("ดำเนินการลบ TemplateImage เรียบร้อยแล้วค่ะ")
			End If
		Else
			MessageBox.Show("ไม่พบข้อมูลที่จะลบค่ะ")
		End If


	End Sub
End Class
