﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GallerysUpload.aspx.vb" Inherits="GallerysUpload" title="My Gallery" %>

<%@ Register Src="uc/ucBar.ascx" TagName="ucBar" TagPrefix="uc3" %>

<%@ Register Src="uc/ucCard.ascx" TagName="ucCard" TagPrefix="uc2" %>
<%@ Register Src="uc/ucPaging.ascx" TagName="ucPaging" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" Runat="Server">
	<div style="margin:auto;">
    <img src="images/h-make-card01.png" class="png" alt="นามบัตร" title="นามบัตร" /><br />
    </div>
    
    <div style="padding: 30px; background-color:#fff; margin:auto; width: 920px;">
    <uc3:ucBar ID="UcBar1" runat="server" />
    <uc1:ucPaging ID="UcPaging1" runat="server" />
    <uc2:ucCard ID="UcCard1" runat="server" />
    <uc1:ucPaging ID="UcPaging2" runat="server" />
    	<div style="background-color:#fff; margin:auto; clear:both; padding-top:20px;">
            <div id="btn-card" align="left">
    		<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/UploadCard.aspx" class="btn-face2"><< กลับไปหน้า Upload Card</asp:HyperLink>
            </div>
        </div>
    </div>
</asp:Content>

