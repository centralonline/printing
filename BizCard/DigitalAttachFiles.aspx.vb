Imports System.Collections.Generic
Imports CoreBaseII
Imports System.Data

Partial Class AttachFiles
    Inherits clsHTTPBase
    Dim oCard As clsCard 'oUCard
    Dim oCardOld As clsCard 'oUCard
	Dim strPath As String = Server.MapPath(".")
    Dim TotalFileSize As Double = 0.0
	Dim FileType() As String = HttpContext.Current.Application("objFileType")
	Dim ModeState As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.objSessionVar.ModelUser Is Nothing Then
            Me.Response.Redirect("Main.aspx?id=1")
            Exit Sub
        End If

        SetFromMethod()
        SetInitFromLoad()
        SetObjectCard()
        BindImageUploaded()

        If Not Page.IsPostBack Then
            If Request.QueryString("Mode") IsNot Nothing Then
                ModeState = Request.QueryString("Mode")
            End If
			SetPanelAndBindImage(ModeState)
		End If
	End Sub

    Protected Sub btnAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click

        Dim FileEngine As New clsFileManagerEngine
        Dim CardEngine As New clsCardEngine
        Dim isUploadDocument As Boolean = False
        Dim isUploadImage As Boolean = False

		If CheckFileUploadEmptry(FileUpload1) = False Then
			Exit Sub
		End If

        If Me.FileUpload1.PostedFile IsNot Nothing Then
            Dim File As HttpPostedFile = Me.FileUpload1.PostedFile
			Dim TempResult As New clsResultII

			TempResult = CheckSizeFileUpload(FileUpload1)

            If TempResult.Flag = False Then
                MessageBox.Show(TempResult.Message)
                Exit Sub
            End If

            TempResult = FileEngine.CheckUploadFile(File)

			If TempResult.Flag = True Then
				isUploadDocument = True
			Else
				MessageBox.Show("����ͧ�Ѻ��������: " + TempResult.Message)
				Exit Sub
			End If


            If isUploadDocument = True Then
                TempResult = FileEngine.CopyFile(File, strPath)
            End If

            If TempResult.Flag = False Then
                Exit Sub
            End If

            Dim TempImage As New clsCardImageField
            '���� Image
            Dim readyToPrint As String = ""
            With TempImage
                .imageUrl = TempResult.Message

                ''---------------------Before ---------------------
                If Me.Request.QueryString("Mode") = "Y" Then
                    readyToPrint = "Y"
                Else
                    readyToPrint = "N"
                End If

                .Ready2Print = readyToPrint
                .ImageSize = File.ContentLength / 1024 'KB
                .ContentType = File.ContentType
                .PreviousName = File.FileName.Substring(File.FileName.LastIndexOf("\") + 1)
            End With

            If oCard.ImageFields Is Nothing Then 'oUCard
                Dim NewImgFields As New List(Of clsCardImageField)
                NewImgFields.Add(TempImage)
                oCard.ImageFields = NewImgFields 'oUCard
                oCard.UploadFlag = "Y"
            Else
                oCard.ImageFields.Add(TempImage) 'oUCard
                oCard.UploadFlag = "Y"
            End If

            Session("oCard") = oCard 'oUCard
            Me.BindImage()

        End If
    End Sub

    Protected Sub dlImage_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlImage.ItemCommand
        If e.CommandName = "DeleteImage" Then
            oCard.ImageFields.RemoveAt(e.CommandArgument) 'oUCard

            Dim strImgDel As String = CType(dlImage.Items(e.CommandArgument).FindControl("hidImageUrl"), HiddenField).Value
            Dim FileEngine As New clsFileManagerEngine
            FileEngine.DeleteFile(strPath & "\CardImages\UploadImage\Currents\" & strImgDel)
            FileEngine.DeleteFile(strPath & "\CardImages\UploadImage\Thumbnails\" & strImgDel)
			FileEngine.DeleteFile(strPath & "\CardImages\UploadImage\UploadFiles\" & strImgDel)
            Session("oCard") = oCard 'oUCard
			Me.BindImage()

        End If
    End Sub

    Protected Sub dlImage_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlImage.ItemDataBound
		Dim lbImageSize As Label = CType(e.Item.FindControl("lbImageSize"), Label)
		If lbImageSize IsNot Nothing Then
			Dim imgSize As Integer = CType(lbImageSize.Text, Integer)
			TotalFileSize += imgSize
		End If
        'hidReadyToPrint()
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If CType(e.Item.FindControl("hidReadyToPrint"), HiddenField).Value = "Y" Then
                CType(e.Item.FindControl("LabelType"), Label).Text = "�������觾����"
            Else
                CType(e.Item.FindControl("LabelType"), Label).Text = "�Ϳ�Ե��� Design"
            End If
        End If
        
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Button1.Click
        Dim dlList As DataList = Me.UcFileUploaded1.FindControl("dlImgFields")
        'dlList = Session("datalist")
        Dim str As String = ""
        Dim imageURL As String
        Dim PreviousName As String
        Dim ReadyToPrint As String
        Dim ContentType As String
        Dim NewImgFields As New List(Of clsCardImageField)

        For i As Integer = 0 To dlList.Items.Count - 1
            If CType(dlList.Items(i).FindControl("chkSelect"), CheckBox).Checked = True Then

                imageURL = CType(dlList.Items(i).FindControl("hidImgUrl"), HiddenField).Value
                PreviousName = CType(dlList.Items(i).FindControl("hidPreviousName"), HiddenField).Value
                ReadyToPrint = CType(dlList.Items(i).FindControl("hidReadyToPrint"), HiddenField).Value
                ContentType = CType(dlList.Items(i).FindControl("hidContentType"), HiddenField).Value

                Dim TempImage As New clsCardImageField
                '���� Image
                With TempImage
                    .Ready2Print = ReadyToPrint
                    .imageUrl = imageURL
                    .ContentType = ContentType
                    .PreviousName = PreviousName
                End With
                NewImgFields.Add(TempImage)

                Session("oCardOld") = NewImgFields 'oUCard
            End If
        Next

        Session("Detail") = GetDetail()

        'Upload FTP ������礡�� Upload
		'If cbUploadFTP.Checked Then
		'    objSessionVar.ModelUser.ActionUpload = True
		'    Session("oCard") = oCard
		'Else
		'    objSessionVar.ModelUser.ActionUpload = False
		'End If
        Session("oCard") = oCard
        Response.Redirect("DigitalDetailPaperUpload.aspx")
    End Sub

    Private Function GetDetail() As String
        Dim str As String = ""

        str = ("" + "," + "" + "," + "")

        Return str
    End Function


    Private Sub SetFromMethod()
        Me.Form.Method = "post"
        Me.Form.Enctype = "multipart/form-data"
    End Sub
    Private Sub SetInitFromLoad()
        Me.UcBar1.BarStep = 1
        TotalFileSize = 0.0 '��˹���Ҵ����������������� 0
    End Sub
    Private Sub SetObjectCard()
        oCard = New clsCard 'oUCard
        If Session("oCard") IsNot Nothing Then
            oCard = Session("oCard") 'oUCard
        End If
    End Sub
    Private Sub BindImage()
        If oCard.ImageFields IsNot Nothing Then 'oUCard
            dlImage.DataSource = oCard.ImageFields 'oUCard
            dlImage.DataBind()
        End If
        Me.txtTotalSize.Text = " Total " & String.Format("{0:0.00}", Me.TotalFileSize / 1024) & " MB"
    End Sub
    Private Sub BindImageUploaded()
        If objSessionVar.ModelUser Is Nothing Then
            Exit Sub
        End If

        Dim CardEngine As New clsCardEngine
        Dim TempImageFields As DataTable = CardEngine.GetListImageByUser(Me.objSessionVar.ModelUser.UserID, 10)

        If TempImageFields Is Nothing Then
            Exit Sub
        Else
            Me.UcFileUploaded1.ThisImageFields = TempImageFields
            Me.UcFileUploaded1.ThisRepeat = 2
			'Me.txtFileCount.Text = "�ӹǹ�������� " & TempImageFields.Rows.Count & "���"
        End If
    End Sub
    Private Sub SetPanelAndBindImage(ByVal Mode As String)
        'If Mode = "N" Then  '����Ϳ������ Design
        '    Me.BindImage()
        '    Me.dvOfmDesign.Visible = True
        '    Me.dvReadyPrint.Visible = False
        '    Me.dvDetailDesign.Visible = False
        '    Me.dvDetailPrint.Visible = True
        '    'Me.dvShowDetail.Visible = True
        'Else
        Me.dvOfmDesign.Visible = False
        Me.dvReadyPrint.Visible = True
        Me.dvDetailDesign.Visible = True
        Me.dvDetailPrint.Visible = False
        'Me.dvShowDetail.Visible = False
        'End If
    End Sub
    Private Function CheckFileUploadEmptry(ByVal FileUpload As FileUpload) As Boolean
        Dim isCheckValid As Boolean = True
        If FileUpload1.FileName = "" Then
            MessageBox.Show("��س��к� File ����ͧ��� Upload ���¤��")
            isCheckValid = False
        End If
        Return isCheckValid
    End Function
    Private Function CheckSizeFileUpload(ByVal FileUpload As FileUpload) As clsResultII
        Dim TempResult As New clsResultII
        Dim FileEngine As New clsFileManagerEngine
        TempResult = FileEngine.CheckFileSize(FileUpload.PostedFile, 1024)
        If TempResult.Flag = False Then
            MessageBox.Show(TempResult.Message)
            TempResult.Flag = False
        End If
        Return TempResult
    End Function
End Class
