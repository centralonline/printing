Imports System.Collections.Generic
Partial Class uc_ucSelectPaper
    Inherits System.Web.UI.UserControl

    Private PaperIDValue As String
    Private _test As String

    Public Property PaperID() As String
        Get
            Return PaperIDValue
        End Get
        Set(ByVal value As String)
            PaperIDValue = value
        End Set
    End Property

    Private PaperNameValue As String
    Public Property PaperName() As String
        Get
            Return PaperNameValue
        End Get
        Set(ByVal value As String)
            PaperNameValue = value
        End Set
    End Property

    Private PaperPriceValue As Double
    Public Property PaperPrice() As Double
        Get
            Return PaperPriceValue
        End Get
        Set(ByVal value As Double)
            PaperPriceValue = value
        End Set
    End Property

    Private PaperPromotionValue As Double
    Public Property PaperPromotion() As Double
        Get
            Return PaperPromotionValue
        End Get
        Set(ByVal value As Double)
            PaperPromotionValue = value
        End Set
    End Property

    Private TypeSystemValue As String
    Public Property TypeSystem() As String
        Get
            Return TypeSystemValue
        End Get
        Set(ByVal value As String)
            TypeSystemValue = value
        End Set
    End Property

    Private OrderValue As Integer
    Public Property Order() As Integer
        Get
            Return OrderValue
        End Get
        Set(ByVal value As Integer)
            OrderValue = value
        End Set
    End Property

    Private NetAmtValue As Decimal
    Public Property NetAmt() As Decimal
        Get
            Return NetAmtValue
        End Get
        Set(ByVal value As Decimal)
            NetAmtValue = value
        End Set
    End Property

    Private VatValue As Decimal
    Public Property Vat() As Decimal
        Get
            Return VatValue
        End Get
        Set(ByVal value As Decimal)
            VatValue = value
        End Set
    End Property


    Private SumaryValue As Decimal
    Public Property Summary() As Decimal
        Get
            Return SumaryValue
        End Get
        Set(ByVal value As Decimal)
            SumaryValue = value
        End Set
    End Property

    Private IsUploadValue As Boolean
    Public Property IsUpload() As Boolean
        Get
            Return IsUploadValue
        End Get
        Set(ByVal value As Boolean)
            IsUploadValue = value
        End Set
    End Property

    Private Property test As String
        Get
            Return _test
        End Get
        Set(value As String)
            _test = value
        End Set
    End Property

    'GIFT
    Private ViewValue As String
    Public Property ViewAdd() As String
        Get
            Return ViewValue
        End Get
        Set(ByVal value As String)
            ViewValue = value
        End Set
    End Property
    Private PriceValue As String
    Public Property Price() As String
        Get
            Return PriceValue
        End Get
        Set(ByVal value As String)
            PriceValue = value
        End Set
    End Property
    Private PriceDecriptionValue As String
    Public Property PriceDecription() As String
        Get
            Return PriceDecriptionValue
        End Get
        Set(ByVal value As String)
            PriceDecriptionValue = value
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Dim clsGetListPaper As New clsPaperEngine
            Dim listPaper As New List(Of clsPaper)
			listPaper = clsGetListPaper.GetListPapersize("Digital-Namecard", "card")

            Me.GridView1.DataSource = listPaper
            Me.GridView1.DataBind()
            RadioPrice()
            RadioAngle()
            myInit()
			SetDefualtGetCost()
           
        Else
            RadioPrice()
            RadioAngle()
            GetCost()
            check()
			Session("radioview") = radioview.Text '��������´���ͺ

        End If

    End Sub
    Protected Sub radPaper_CheckedChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim oldrow As GridViewRow
        For Each oldrow In GridView1.Rows
            CType(oldrow.FindControl("radPaper"), RadioButton).Checked = False
            oldrow.BackColor = Drawing.Color.White
        Next
        'Set the new selected row
        Dim rb As RadioButton = CType(sender, RadioButton)
        Dim row As GridViewRow = CType(rb.NamingContainer, GridViewRow)
        CType(row.FindControl("radPaper"), RadioButton).Checked = True
        row.BackColor = Drawing.Color.DarkSeaGreen
        Me.lbPaperID.Text = CType(row.FindControl("lbPaperID"), Label).Text
        Me.lbPaperName.Text = CType(row.FindControl("lbPaperName"), Label).Text
        Me.lbPrice.Text = CType(row.FindControl("lbCost"), Label).Text
        Me.lbPromotion.Text = CType(row.FindControl("lbPromotion"), Label).Text

        radDescription.ClearSelection()
        radDescription.Items(0).Selected = True
        RadioPrice()
        radAngle.ClearSelection()
        radAngle.Items(0).Selected = True
        RadioAngle()
        check()
        GetCost()
    End Sub

    Private Sub myInit()

        Me.lbview1.Text = Me.ViewAdd
        Me.lbPaperID.Text = Me.PaperID
        Me.lbPaperName.Text = Me.PaperName
        Me.lbPromotion.Text = Me.PaperPromotion
        Me.lbPrice.Text = Me.PaperPrice
        Me.lbDelivery.Text = ""

        If lbPaperID.Text = "" Then
            lbPaperID.Text = "--�ѧ��������͡--"
        End If
        If lbPaperName.Text = "" Then
            lbPaperName.Text = "--�ѧ��������͡--"
        End If
        If lbPrice.Text = "" Then
            lbPrice.Text = "0"
        End If
        If lbDelivery.Text = "" Then
			lbDelivery.Text = "0.00"
        End If
        If lbPromotion.Text = "0" Then
            lbPromotion.Text = "-"
        End If

        lbSum.Text = "0"
        lbVat.Text = "0"
        lbVat_card.Text = "0"
        lbSummary.Text = "0"

    End Sub

    Private Sub SetDefualtGetCost()

        Dim Rowindex As Integer = 0
        GridView1.Rows(Rowindex).BackColor = Drawing.Color.DarkSeaGreen
        Me.lbPaperID.Text = CType(GridView1.Rows(Rowindex).FindControl("lbPaperID"), Label).Text
        Me.lbPaperName.Text = CType(GridView1.Rows(Rowindex).FindControl("lbPaperName"), Label).Text
        Me.lbPrice.Text = CType(GridView1.Rows(Rowindex).FindControl("lbCost"), Label).Text
        Me.lbPromotion.Text = CType(GridView1.Rows(Rowindex).FindControl("lbPromotion"), Label).Text
        GetCost()
    End Sub

    Private Sub GetCost() '�ӹǹ�Ҥ�

        'Me.PaperPrice = CDec(lbPrice.Text)
        Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())

        If (Me.TypeSystem <> "Upload") Then

            If lbPromotion.Text = "-" Then
                lbPromotion.Text = "-"
                Me.Label1.Visible = False
            End If

            'If Me.IsUploadValue = True Then
            '    lbPromotion.Text = "-"
            '    Me.Label1.Visible = False
            'End If


            If lbPromotion.Text <> "-" Then
                'Me.Label1.Visible = True
                'Dim Promotion As Double = lbPromotion.Text
                'Me.hidSum.Value = Promotion * Order
                'Me.lbSum.Text = Format(Promotion * Order, "#,##0.00")
                'Me.hidVat.Value = (lbSum.Text * 7) / 100
                'Me.lbVat.Text = Format((lbSum.Text) - ((lbSum.Text) * 7) / 100, "#,##0.00")
                'Me.lbVat_card.Text = Format((lbSum.Text * 7) / 100, "#,##0.00")

                Me.Label1.Visible = True
                Dim Promotion As Double = lbPromotion.Text
                Dim angle, view As Double '�Ҥ����ͺ���˹��� , �ҤҵѴ���
                Me.hidSum.Value = PaperPrice * Order
                Me.lbSum.Text = Format((Promotion * Order), "#,##0.00")
                angle = Format((Order * lbangle.Text), "#,##0.00")
                view = Format((Order * lbview.Text), "#,##0.00")
                Me.lbSum.Text = Format((lbSum.Text + angle + view), "#,##0.00")
                Me.hidVat.Value = ((lbSum.Text * 7) / 107)
                Me.lbVat.Text = Format((lbSum.Text) - (hidVat.Value), "#,##0.00")
                Me.lbVat_card.Text = Format((lbSum.Text * 7) / 107, "#,##0.00")


                Session("sumangle") = Format((angle), "#,##0.00") '�Դ�Թ��ԡ������������ ���ͺ
                Session("sumview") = Format((view), "#,##0.00") '�Դ�Թ��ԡ������������ �Ѵ���
                Session("sumtotal") = Format((Promotion * Order), "#,##0.00")
            Else
                'Me.PaperPrice = CDec(lbPrice.Text)
                'Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())
                'Me.hidSum.Value = (Me.PaperPrice * Order)
                'Me.lbSum.Text = Format((Me.PaperPrice * Order), "#,##0.00")
                'Me.hidVat.Value = ((lbSum.Text * 7) / 100)
                'Me.lbVat.Text = Format((lbSum.Text) - ((lbSum.Text) * 7) / 100, "#,##0.00")
                'Me.lbVat_card.Text = Format((lbSum.Text * 7) / 100, "#,##0.00")
                ''Me.lbVat.Text = Format(((lbSum.Text * 7) / 100), "0.00")

                Me.PaperPrice = CDec(lbPrice.Text)
                Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())
                Dim angle, view As Double
                Me.hidSum.Value = PaperPrice * Order
                Me.lbSum.Text = Format((PaperPrice * Order), "#,##0.00")
                angle = Format((Order * lbangle.Text), "#,##0.00")
                view = Format((Order * lbview.Text), "#,##0.00")
                Me.lbSum.Text = Format((lbSum.Text + angle + view), "#,##0.00")
                Me.hidVat.Value = ((lbSum.Text * 7) / 107)
                Me.lbVat.Text = Format((lbSum.Text) - (hidVat.Value), "#,##0.00")
                Me.lbVat_card.Text = Format((lbSum.Text * 7) / 107, "#,##0.00")


                Session("sumangle") = Format((angle), "#,##0.00") '�Դ�Թ��ԡ������������ ���ͺ
                Session("sumview") = Format((view), "#,##0.00") '�Դ�Թ��ԡ������������ �Ѵ���
                Session("sumtotal") = Format((PaperPrice * Order), "#,##0.00")

            End If

            ' Me.hidSummary.Value = ((Me.lbVat.Text * 1 + Me.lbSum.Text * 1))
            Me.hidSummary.Value = (Me.lbSum.Text * 1)
            If lbSum.Text < 500 Then
                lbDelivery.Text = "50.00"
                'Me.lbSummary.Text = Format(((Me.lbVat.Text * 1 + Me.lbSum.Text * 1) + 50), "0.00")
                Me.lbSummary.Text = Format(((Me.lbSum.Text * 1) + 50), "#,##0.00")
            Else
                lbDelivery.Text = "0.00"
                'Me.lbSummary.Text = Format(((Me.lbVat.Text * 1 + Me.lbSum.Text * 1)), "0.00")
                Me.lbSummary.Text = Format(((Me.lbSum.Text * 1)), "#,##0.00")
            End If

            'Me.Vat = lbVat.Text
            Me.Vat = 0
            Me.NetAmt = lbSum.Text
            Me.Summary = Me.lbSummary.Text
            Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())
            Me.PaperID = Me.lbPaperID.Text

        Else
            Me.lbSum.Text = "-"
            Me.lbVat.Text = "-"
            Me.lbVat_card.Text = "-"
            Me.lbSummary.Text = "-"
            Me.lbVat_card.Text = "-"
            Me.Vat = 0
            Me.NetAmt = 0
            Me.Summary = 0
        End If
    End Sub

    Protected Sub ddlNumber_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNumber.SelectedIndexChanged
        SetCalculatePrircePaper()
    End Sub

    Private Sub SetCalculatePrircePaper()

        If (Me.TypeSystem <> "Upload") Then
            If lbPromotion.Text = "-" Then
                lbPromotion.Text = "-"
                Me.Label1.Visible = False
            End If
            'If Me.IsUploadValue = True Then
            '    lbPromotion.Text = "-"
            '    Me.Label1.Visible = False
            'End If
            If lbPromotion.Text <> "-" Then
                'Me.Label1.Visible = True
                'Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())
                'Dim Promotion As Double = lbPromotion.Text
                'Me.hidSum.Value = Me.PaperPrice * Order
                'Me.lbSum.Text = Format((Promotion * Order), "#,##0.00")
                'Me.hidVat.Value = ((lbSum.Text * 7) / 100)
                'Me.lbVat.Text = Format((lbSum.Text) - ((lbSum.Text) * 7) / 100, "#,##0.00")
                'Me.lbVat_card.Text = Format((lbSum.Text * 7) / 100, "#,##0.00")


                Me.Label1.Visible = True
                Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())
                Dim angle, view As Double
                Dim Promotion As Double = lbPromotion.Text
                Me.hidSum.Value = Me.PaperPrice * Order
                Me.lbSum.Text = Format((Promotion * Order), "#,##0.00")
                angle = Format((Order * lbangle.Text), "#,##0.00")
                view = Format((Order * lbview.Text), "#,##0.00")
                Me.lbSum.Text = Format((lbSum.Text + angle + view), "#,##0.00")
                Me.hidVat.Value = ((lbSum.Text * 7) / 107)
                Me.lbVat.Text = Format((lbSum.Text) - (hidVat.Value), "#,##0.00")
                Me.lbVat_card.Text = Format((lbSum.Text * 7) / 107, "#,##0.00")


                Session("sumangle") = Format((angle), "#,##0.00") '�Դ�Թ��ԡ������������ ���ͺ
                Session("sumview") = Format((view), "#,##0.00") '�Դ�Թ��ԡ������������ �Ѵ���
                Session("sumtotal") = Format((Promotion * Order), "#,##0.00")

            Else
                'Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())
                'Me.PaperPrice = CDec(lbPrice.Text)
                'Me.hidSum.Value = Me.PaperPrice * Order
                'Me.lbSum.Text = Format((Me.PaperPrice * Order), "#,##0.00")
                'Me.hidVat.Value = ((lbSum.Text * 7) / 100)
                'Me.lbVat.Text = Format((lbSum.Text) - ((lbSum.Text) * 7) / 100, "#,##0.00")
                'Me.lbVat_card.Text = Format((lbSum.Text * 7) / 100, "#,##0.00")
                '' Me.lbVat.Text = ((lbSum.Text * 7) / 100)
                Dim angle, view As Double
                Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())
                Me.PaperPrice = CDec(lbPrice.Text)
                Me.hidSum.Value = Me.PaperPrice * Order
                Me.lbSum.Text = Format((PaperPrice * Order), "#,##0.00")
                angle = Format((Order * lbangle.Text), "#,##0.00")
                view = Format((Order * lbview.Text), "#,##0.00")
                Me.lbSum.Text = Format((lbSum.Text + angle + view), "#,##0.00")
                Me.hidVat.Value = ((lbSum.Text * 7) / 107)
                Me.lbVat.Text = Format((lbSum.Text) - (hidVat.Value), "#,##0.00")
                Me.lbVat_card.Text = Format((lbSum.Text * 7) / 107, "#,##0.00")


                Session("sumangle") = Format((angle), "#,##0.00") '�Դ�Թ��ԡ������������ ���ͺ
                Session("sumview") = Format((view), "#,##0.00") '�Դ�Թ��ԡ������������ �Ѵ���
                Session("sumtotal") = Format((PaperPrice * Order), "#,##0.00")

            End If

            ' Me.hidSummary.Value = ((Me.lbVat.Text * 1 + Me.lbSum.Text * 1))
            Me.hidSummary.Value = ((Me.lbSum.Text * 1))
            If lbSum.Text < 500 Then
                lbDelivery.Text = "50.00"
                'Me.lbSummary.Text = Format(((Me.lbVat.Text * 1 + Me.lbSum.Text * 1) + 50), "0.00")
                Me.lbSummary.Text = Format(((Me.lbSum.Text * 1) + 50), "#,##0.00")
            Else
				lbDelivery.Text = "0.00"
                ' Me.lbSummary.Text = Format((Me.lbVat.Text * 1 + Me.lbSum.Text * 1), "0.00")
                Me.lbSummary.Text = Format((Me.lbSum.Text * 1), "#,##0.00")
            End If

            'Me.Vat = lbVat.Text
            Me.Vat = 0
            Me.NetAmt = lbSum.Text
            Me.Summary = Me.lbSummary.Text
            Me.Order = CInt(Me.ddlNumber.SelectedItem.ToString())
            Me.PaperID = Me.lbPaperID.Text

        Else
            Me.lbSum.Text = "-"
            Me.lbVat.Text = "-"

            Me.lbVat_card.Text = "-"
            Me.lbSummary.Text = "-"

            Me.Vat = 0
            Me.NetAmt = 0
            Me.Summary = 0
        End If

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If CType(e.Row.FindControl("lbPromotion"), Label).Text = "0.00" Then
                CType(e.Row.FindControl("lbPromotion"), Label).Text = "-"
                If CType(e.Row.FindControl("lbPromotion"), Label).Text = "-" Then
                    Me.Label1.Visible = False
                End If
            End If
            If e.Row.RowIndex = 0 Then
                CType(e.Row.FindControl("radPaper"), RadioButton).Checked = True
            Else
                CType(e.Row.FindControl("radPaper"), RadioButton).Checked = False
            End If

        End If
    End Sub


    'GIFT CreateBy 09/7/2014
	Private Sub RadioPrice()
		If Not IsPostBack Then
			radioservice(radDescription.SelectedValue, "Cover")
			radDescription.Items(0).Selected = True
		End If

		Dim pricepaper = Split(radDescription.SelectedValue.ToString, "_")
		lbangle.Text = pricepaper(0).Trim()

		If (lbangle.Text <> "0.00") Then
			radioview.Text = pricepaper(1).Trim()
		Else
			radioview.Text = Nothing
		End If

	End Sub

    Private Function radioservice(ByVal Card As String, ByVal Cover As String) As List(Of clsPaper)
        Dim Clsoptionservice As New clsPaperEngine
        Dim radservice As New List(Of clsPaper)
        radioservice = Clsoptionservice.GetService("Card", Cover)

        radDescription.DataSource = radioservice
        radDescription.DataValueField = "PriceForSystem"
        radDescription.DataTextField = "PriceDescription"
        radDescription.DataBind()
        radDescription.Items.Insert(0, (New ListItem("������͡��ԡ�����ͺ", "0.00")))
        Return radservice
    End Function

    Private Sub RadioAngle()
        If Not IsPostBack Then
            RadioAngle(radAngle.SelectedValue, "Angle")
            radAngle.Items(0).Selected = True
        End If

        Dim price = Split(radAngle.SelectedValue.ToString, "_")
        lbview.Text = price(0).Trim()

        If (lbview.Text <> "0.00") Then
            check()
        Else
            ClearCheckbok()
        End If

    End Sub

    Private Function radioAngle(ByVal Card As String, ByVal Angle As String) As List(Of clsPaper)
        Dim Clsoptionservice As New clsPaperEngine
        Dim radservice As New List(Of clsPaper)
        radioAngle = Clsoptionservice.GetService("Card", Angle)

        radAngle.DataSource = radioAngle
        radAngle.DataValueField = "PriceForSystem"
        radAngle.DataTextField = "PriceDescription"
        radAngle.DataBind()
        radAngle.Items.Insert(0, (New ListItem("������͡��ԡ�õѴ���", "0.00")))
        Return radservice
    End Function

    Private Sub check() '��������´��ԡ�õѴ���.
        If CheckBox1.Checked And DropDownList1.SelectedIndex.ToString Then
            lbview1.Text = "�����Һ�"
            lbright1.Text = DropDownList1.SelectedItem.ToString.Trim("1", "2", "3", "4").Trim(".").Substring("4")
        ElseIf CheckBox1.Checked = False Then
            lbview1.Text = ""
            lbright1.Text = ""
            DropDownList1.SelectedIndex = -1
        End If

        If CheckBox2.Checked And DropDownList2.SelectedIndex.ToString Then
            lbview2.Text = "��������ҧ"
            lbright2.Text = DropDownList2.SelectedItem.ToString.Trim("1", "2", "3", "4").Trim(".").Substring("4")
        ElseIf CheckBox2.Checked = False Then
            lbview2.Text = ""
            lbright2.Text = ""
            DropDownList2.SelectedIndex = -1
        End If

        If CheckBox3.Checked And DropDownList3.SelectedIndex.ToString Then
            lbview3.Text = "������º�"
            lbleft1.Text = DropDownList3.SelectedItem.ToString.Trim("1", "2", "3", "4").Trim(".").Substring("4")
        ElseIf CheckBox3.Checked = False Then
            lbview3.Text = ""
            lbleft1.Text = ""
            DropDownList3.SelectedIndex = -1
        End If

        If CheckBox4.Checked And DropDownList4.SelectedIndex.ToString Then
            lbview4.Text = "���������ҧ"
            lbleft2.Text = DropDownList4.SelectedItem.ToString.Trim("1", "2", "3", "4").Trim(".").Substring("4")
        ElseIf CheckBox4.Checked = False Then
            lbview4.Text = ""
            lbleft2.Text = ""
            DropDownList4.SelectedIndex = -1
        End If
        Session("Detailpromotion") = lbview1.Text + lbright1.Text + " " + lbview2.Text + lbright2.Text + " " + lbview3.Text + lbleft1.Text + " " + lbview4.Text + lbleft2.Text
    End Sub

    Private Sub ClearCheckbok()
        DropDownList1.SelectedIndex = -1
        DropDownList2.SelectedIndex = -1
        DropDownList3.SelectedIndex = -1
        DropDownList4.SelectedIndex = -1
        CheckBox1.Checked = False
        CheckBox2.Checked = False
        CheckBox3.Checked = False
        CheckBox4.Checked = False
        lbview1.Text = ""
        lbview2.Text = ""
        lbview3.Text = ""
        lbview4.Text = ""
        lbright1.Text = ""
        lbright2.Text = ""
        lbleft1.Text = ""
        lbleft2.Text = ""
    End Sub

End Class




