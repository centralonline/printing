﻿Imports CoreBaseII
Imports UserEngineTrendyPrint

Partial Class CheckeMail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Check As String
        Dim MailAdd As String = Request("MailAdd")
        Dim IsGetMail As Boolean = Request("IsGetMail")

        Dim Engine As New UserEngine
        Check = Engine.SetNewsletter(MailAdd, IsGetMail)

        If Check = "True" Then
            Response.Write("บันทึกข้อมูลเรียบร้อยแล้วค่ะ")
        ElseIf Check = "False" Then
            Response.Write("Error!!! กรุณาลองใหม่อีกครั้งค่ะ")
        Else
            Response.Write(Check)
        End If

    End Sub
End Class
