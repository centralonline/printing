﻿Imports System.Collections.Generic

Partial Class Admin_EditActiveTemplate
    Inherits clsHTTPBaseAdmin
	Public listCard As New List(Of clsCard)
	Public listCategories As New List(Of clsCategories)
	Public PageNo As String
	Public SearchKey As String
	Public Cat As String




	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim CardProvider As New clsCardProvider
		Dim CardEngine As New clsCardEngine


		PageNo = Request.QueryString("PageNo")
		SearchKey = Request.QueryString("SearchKey")
		Cat = Request.QueryString("Cat")

		Session("PageNo") = PageNo
		Session("Cat") = Cat


		If String.IsNullOrEmpty(PageNo) Then
			PageNo = "1"
		End If
		listCategories = CardEngine.GetListCategories()
		listCard = CardProvider.GetActiveCard(PageNo, SearchKey, Cat)

	End Sub

	Protected Sub IsUpload_Click(sender As Object, e As System.EventArgs) Handles IsUpload.Click

		Dim TempUploadFile As New clsPaperUpload
		Dim PageNo As String = "1"
		listCard = TempUploadFile.GetUploadFileImage(PageNo)

	End Sub
End Class
