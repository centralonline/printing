﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Main.aspx.vb" Inherits="Main" %>

<%@ Register Src="uc/ucFooter.ascx" TagName="ucFooter" TagPrefix="uc2" %>
<%@ Register Src="uc/ucLiveChat.ascx" TagName="ucLiveChat" TagPrefix="uc3" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์ - Officemate Printing Solution
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="สั่งทำนามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์ - Officemate Printing Solution" />
	<meta name="keywords" content="นามบัตร, หัวจดหมาย, การ์ด, โปสเตอร์, ใบปลิว, ราคางานพิมพ์, Officemate Printing Solutiont" />
	<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/trendyprint.css" />
	<link rel="stylesheet" type="text/css" href="css/print.css" />
	<script src="js/jquery-1.3.2.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jsProductSearch.js"></script>
	<script type="text/javascript" src="js/jsUtil.js"></script>
	<script type="text/javascript" src="js/NoIEActivate.js"></script>
	<script type="text/javascript" src="js/AC_RunActiveContent.js"></script>
	<script type="text/javascript" src="js/easySlider1.7.js"></script>
	<script type="text/javascript">
		function newLocation() {
			window.location.href = "DesignCard.aspx";
		}

		$(document).ready(function () {
			$("#slider").easySlider({
				auto: true,
				continuous: true
			});
		});	
	</script>
	<%--<style type="text/css">
		body
		{
			-webkit-filter: grayscale(1);
			filter: grayscale(1);
		}
	</style>--%>
</head>
<body>
	<form id="form1" runat="server">
	<div id="container" align="center">
		<%--div style="background-color: #cacaca; overflow: hidden" align="center"><iframe src="http://magazine.trendyday.com/wp-content/slide-trendyprint.html" width="930" height="30" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>--%>
		<%--<div style="background-color:#161616; height:77px; text-align: center;"><img src="images/header.jpg" style="border: none;" /></div>--%>
		<div><marquee direction="left" scrollamount="5"><font color="black">เรียนผู้ใช้บริการเว็บไซต์ OfficeMate Printing Solution เพื่อพัฒนาระบบและการบริการที่ดีขึ้น ทางบริษัทขอแจ้งปิดปรับปรุงเว็บไซต์ชั่วคราว ตั้งแต่วันที่ <font color="red">8 พ.ค. 62 เวลา 21.00 น.</font> เป็นต้นไปและจะกลับมาใช้งานได้ตามปกติอีกครั้งในวันที่ 9 พ.ค. 62 จึงเรียนมาเพื่อทราบและขออภัยในความไม่สะดวกมา ณ ที่นี้</font></marquee></div>
        <div id="logo">
			<a href="Main.aspx">
				<img src="images/logo-trendyprint.png" class="png" style="width: 250px; height: 51px;" /></a></div>
		<div id="login">
			<asp:Panel ID="Panel1" runat="server" Height="24px" Visible="False" Width="100%"
				CssClass="Tlogin">
				<span class="Tlogin">user:</span>
				<asp:Label ID="lbUserName" runat="server" Text="Label" CssClass="Tlogin"></asp:Label>,
				<a href='<%=ResolveUrl("~/MyAccount.aspx")%>'><span style="font-size: 11px; color: #888;">
					My account</span></a>
				<asp:ImageButton ID="btnLogout" ImageUrl="~/images/btn/logout.gif" runat="server"
					ImageAlign="AbsMiddle" /></asp:Panel>
			<asp:Panel ID="Panel2" runat="server" Height="24px" Visible="False" Width="100%"
				CssClass="Tlogin">
				<p style="padding-right: 20px; vertical-align: middle;">
					<img src="images/t-login.png" style="padding-right: 10px; vertical-align: middle;"
						class="png" width="43" height="17">
					<asp:TextBox ID="txtUserName" runat="server" CssClass="Tloginborder" value="enter your email"
						onclick="if (this.value == 'enter your email') { this.value=''; } " onBlur="if (this.value == '') { this.value='enter your email'; } "></asp:TextBox>
					password
					<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="Tloginborder2"></asp:TextBox>
					<asp:ImageButton ID="btnLogin" ImageUrl="~/images/btn/login.gif" runat="server" ImageAlign="AbsMiddle" />
					<a href="<%=ResolveUrl("~/Forgotpassword.aspx")%>"><span style="font-size: 11px;
						color: #888;">forgot?</span></a> <span style="padding: 0px 10px;">or</span>
					<a href="<%=ResolveUrl("~/Register.aspx")%>">
						<img src="images/t-register.png" style="padding-right: 10px; vertical-align: middle;"
							width="62" height="17" class="png"></a>
				</p>
			</asp:Panel>
		</div>
		<div id="menu-top">
			<img src="images/top-menu.png" style="width: 980px; height: 40px;" border="0" usemap="#topmenu"
				class="png" />
			<map name="topmenu">
				<area shape="rect" coords="3, 1, 113, 40" href="Main.aspx" target="_self" alt="หน้าแรก">
				<area shape="rect" coords="112, 3, 254, 36" href="DesignCard.aspx" target="_self"
					alt="นามบัตร">
				<area shape="rect" coords="501, 2, 614, 36" href="Portfolio.aspx" target="_self"
					alt="ตัวอย่างงานพิมพ์">
				<%--				<area shape="rect" coords="614, 2, 701, 36" href="PriceList.aspx" 
                    target="_self" alt="ราคางานพิมพ์">--%>
				<area shape="rect" coords="614, 2, 701, 36" href="PriceList.aspx" target="_self"
					alt="ราคางานพิมพ์">
				<area shape="rect" coords="700, 2, 830, 36" href="Promotion.aspx" target="_self"
					alt="โปรโมชันงานพิมพ์">
				<area shape="rect" coords="830, 2, 979, 36" href="ContactUs.aspx" target="_self"
					alt="ติดต่อ Officemate Printing Solution">
				<area shape="rect" coords="254, 4, 409, 35" href="DigitalAttachFiles.aspx" target="_self"
					alt="ดิจิตอลโปรการ์ด">
				<%--<area shape="rect" coords="408, 2, 500, 35" href="InkjetAttachFiles.aspx" target="_self" alt="Inkjet">--%>
				<area shape="rect" coords="408, 2, 500, 35" href="InkjetAttachFiles.aspx" target="_self"
					alt="Inkjet">
			</map>
		</div>
		<div id="slider">
			<ul>
				<li><a href="DesignCard.aspx">
					<img src="images/ads/ads-01.png" style="width: 980px; height: 300px;" alt="นามบัตร"
						class="png" /></a></li>
				<li><a href="Promotion.aspx">
					<img src="images/ads/ads-02.png" style="width: 980px; height: 300px;" alt="ข้อดีของการสั่งพิมพ์ที่ Officemate Printing Solution"
						class="png" /></a></li>
				<li><a href="ContactUs.aspx">
					<img src="images/ads/ads-03.png" style="width: 980px; height: 300px;" alt="พิมพ์การ์ด"
						class="png" /></a></li>
			</ul>
		</div>
		<div class="col-md-12">
			<!-- <a href="http://www.officemate.co.th/Activity/news/Mar2015-New-N-Company.html?"
				target="_blank">
				<img src="http://www.officemate.co.th/images/banner/Tab-Banner-02.jpg" class="img-responsive padding"
					style="margin-bottom: 15px; text-align:center; width:100%;" /></a> -->
		</div>
		<div id="guide">
			<img src="images/select-guide.png" style="width: 980px; height: 210px;" usemap="#Guide"
				class="png" />
			<map name="Guide">
				<area shape="rect" coords="14,13,235,202" href="Promotion.aspx" target="_self" alt="โปรโมชันงานพิมพ์">
				<area shape="rect" coords="245,13,477,202" href="Portfolio.aspx" target="_self" alt="ตัวอย่างผลงาน">
				<area shape="rect" coords="484,13,727,202" href="PriceList.aspx" target="_self" alt="ราคางานพิมพ์">
				<area shape="rect" coords="734,13,958,203" href="HowtoOrder.aspx" target="_self"
					alt="วิธีการสั่งพิมพ์">
			</map>
		</div>
		<div id="news" style="padding: 0px 30px;">
			<img src="images/h-news.png" class="png" style="width: 980px; height: 64px;"><br />
			<div style="float: left; padding-right: 10px; width: 700px; vertical-align: top;">
				<a href="http://www.officemate.co.th/Activity/news/LD-Jun17-PrintingSolution.html"
					target="_blank">
					<img src="https://printing.officemate.co.th/images/banner/Printing_200.jpg"
						style="border: 0px; float: left; padding-right: 20px;" alt="Special Price for Special Set  "
						title="Special Price for Special Set "></a>
				<p class="news-head">
					OfficeMate Printing Solution
				</p>
				<p class="news-detail">
					โปรโมชั่นสุดคุ้ม!! ตอนรับเถ้าแก่มือใหม่
				</p>
				<p>
					<a href="/promotion.html" target="_blank"></a>
				</p>
				<p class="clear" />
				&nbsp;</p> <a href="<%=ResolveUrl("~/Templates.aspx")%>" target="_blank">
					<img src="http://www.officemate.co.th/images/banner/banner20.jpg" style="border: 0px;
						float: left; padding-right: 20px;" alt="หน้าฝนนี้จำทำให้คุณมีแต่รอยยิ้ม" title="หน้าฝนนี้จำทำให้คุณมีแต่รอยยิ้ม"></a>
				<p class="news-head">
					&quot;ออฟฟิศเมท&quot;ชวนคุณสั่งพิมพ์นามบัตร
				</p>
				<p class="news-detail">
					ขั้นตอนง่ายๆที่ช่วยให้คุณได้รับนามบัตรที่ตรงกับความต้องการ และพร้อมบริการจัดส่งภายในวันทำการถัดไป
				</p>
				<p>
					<a href="<%=ResolveUrl("~/Templates.aspx")%>" target="_blank"></a>
				</p>
				<p class="clear" />
				&nbsp;</p>
				<p class="clear" />
				&nbsp;</p>
				<img src="images/news/sep-2010-02.jpg" style="border: 0px; float: left; padding-right: 20px;"
					alt="พิมพ์นามบัตรด่วน  4 สี ราคาเพียง 1 บาท" title="พิมพ์นามบัตรด่วน  4 สี ราคาเพียง 1 บาท"></a>
				<p class="news-head">
					พิมพ์นามบัตรด่วน 4 สี ราคาเพียง 1 บาท<br />
					ส่งฟรีให้คุณถึงที่!</a></p>
				<p class="news-detail">
					PrintingSolution เปิดบริการพิมพ์นามบัตรด่วน 4 สี ราคาเพียง 1 บาท แถมส่งฟรีถึงที่บ้าน
					หรือ ที่ทำงานได้อีกด้วย บริการดีๆ แบบนี้ต้องรีบสั่งพิมพ์กันแล้วล่ะค่ะ</p>
				<p class="clear" />
				&nbsp;</p>
				<img src="images/news/sep-2010-01.jpg" style="border: 0px; float: left; padding-right: 20px;"
					alt="ข้อดีของงานพิมพ์ Print on demand จาก Officemate Printing Solution" title="ข้อดีของงานพิมพ์ Print on demand จาก Officemate Printing Solution"></a>
				<p class="news-head">
					ข้อดีของงานพิมพ์ Print on demand จาก Officemate Printing Solution</p>
				<p class="news-detail">
					หลายๆ คนคงอยากรู้ว่า ข้อดีของการสั่งงานพิมพ์ Print on demand กับ Officemate Printing
					Solution มีอะไรบ้าง ทำไมถึงแตกต่างจากการสั่งงานพิมพ์กับที่อื่นๆ <strong>บริการส่งฟรีถึงที่
						ในวันทำการถัดไป</strong> เป็น 1 ในข้อดีที่ใครๆ ก็ชื่นชอบ
				</p>
			</div>
			<div style="float: right; width: 200px; vertical-align: top;">
				<%--<script type="text/javascript">
                        AC_FL_RunContent('codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0', 'width', '200', 'height', '170', 'src', '/images/ktc', 'quality', 'high', 'wmode', 'transparent', 'pluginspage', 'http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash', 'movie', '/images/ktc'); //end AC code
                    </script>--%>
				<noscript>
					<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0"
						width="930" height="100">
						<param name="movie" value="/images/ktc.swf" />
						<param name="quality" value="high" />
						<param name="wmode" value="transparent" />
						<embed src="/images/ktc.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash"
							type="application/x-shockwave-flash" width="200" height="170"></embed>
					</object>
				</noscript>
				<p style="padding-top: 10px;">
				</p>
				<%--<a href="https://printing.officemate.co.th/HowtoOrder.aspx#FTP" target="_blank">
					<img src="images/yousendit.jpg" alt="https://www.yousendit.com/" title="วิธีส่งไฟล์งานพิมพ์ผ่าน FTP"
						border="0"></a>--%>
			</div>
			<p class="clear" />
		</div>
		<p class="clear" />
		&nbsp;<uc2:ucFooter ID="UcFooter1" runat="server" />
	</div>
	</form>
	<div style="text-align: right;">
		<uc3:ucLiveChat ID="ucLiveChat1" runat="server" />
	</div>
</body>
</html>
