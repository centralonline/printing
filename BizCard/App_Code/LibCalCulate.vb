﻿Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.Collections.Generic

Public Class clsCalCulate
	Inherits clsBLBase
#Region "CalCulateCard"
	Private PaperIDValue As String
	Public Property PaperID() As String
		Get
			Return PaperIDValue
		End Get
		Set(ByVal value As String)
			PaperIDValue = value
		End Set
	End Property

	Private DescriptionValue As String
	Public Property Description() As String
		Get
			Return DescriptionValue
		End Get
		Set(ByVal value As String)
			DescriptionValue = value
		End Set
	End Property

	Private PriceValue As String
	Public Property Price() As String
		Get
			Return PriceValue
		End Get
		Set(ByVal value As String)
			PriceValue = value
		End Set
	End Property

	Private StatusValue As String
	Public Property Status() As String
		Get
			Return StatusValue
		End Get
		Set(ByVal value As String)
			StatusValue = value
		End Set
	End Property

	Private PromotionValue As String
	Public Property Promotion() As String
		Get
			Return PromotionValue
		End Get
		Set(ByVal value As String)
			PromotionValue = value
		End Set
	End Property

	Private SizeValue As String
	Public Property Size() As String
		Get
			Return SizeValue
		End Get
		Set(ByVal value As String)
			SizeValue = value
		End Set
	End Property

	Private TypeValue As String
	Public Property Type() As String
		Get
			Return TypeValue
		End Get
		Set(ByVal value As String)
			TypeValue = value
		End Set
	End Property

#End Region

	Public Function GetListDetailCard(ByVal Type As String) As List(Of clsCalCulate)
		Dim ListCard As New List(Of clsCalCulate)
		Dim SqlStr As New StringBuilder
		SqlStr.AppendLine("SELECT * from TBBCPapers where type=@Type")

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			objDB.PatametersAdd("@Type", Type)
			objDB.SelectReQuery(SqlStr.ToString())
			While (objDB.Go)
				Dim Card As New clsCalCulate
				Card.PaperID = objDB.ReadField("PaperID")
				Card.Description = objDB.ReadField("Description")
				Card.Price = objDB.ReadField("Price")
				Card.Status = objDB.ReadField("Status")
				Card.Promotion = objDB.ReadField("PricePromotion")
				Card.Size = objDB.ReadField("Size")
				Card.Type = objDB.ReadField("Type")
				ListCard.Add(Card)
			End While
		End Using

		Return ListCard
	End Function

	Public Function GetSizePoster() As List(Of clsCalCulate)
		Dim ItemSizePoster As New List(Of clsCalCulate)
		Dim SqlStr As New StringBuilder

		SqlStr.AppendLine("SELECT DISTINCT(Size) FROM TBBCPapers where Type='Poster'")

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			objDB.SelectReQuery(SqlStr.ToString())
			While (objDB.Go)
				Dim sizePoster As New clsCalCulate
				sizePoster.Size = objDB.ReadField("Size")
				ItemSizePoster.Add(sizePoster)
			End While
		End Using
		Return ItemSizePoster

	End Function





End Class

