Imports CoreBaseII
Imports System.Data
Imports System.Collections.Generic

Partial Class AddInformation
	Inherits clsHTTPBase

	Dim oCard As clsCard = Nothing
	Dim oTextField As List(Of clsCardTextField)
	Dim oImageField As List(Of clsCardImageField)
	Dim Path As String = Server.MapPath(".")
	Dim CardEngine As New clsCardEngine
	Dim TempCardID As String = ""
	Dim OldCardID As String = ""
	Dim OldCardName As String = ""

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Form.Method = "post"
		Me.Form.Enctype = "multipart/form-data"
		Me.UcBar1.BarStep = 2

		If Me.CheckRequest.Flag = False Then Exit Sub

        If Not Page.IsPostBack Then
            Me.add2.Visible = False

            If Request("CardID") = "" Or Request("CardID").Length <> 7 Then
                Exit Sub
            End If

            Dim regex As Regex = New Regex("((BC)+\d{5})|((CARD)+\d{3})")

            If Request("CardID") <> "" And Request("CardID").Length = 7 Then

                Dim match As Match = regex.Match(Request("CardID"))
                If Not match.Success Then
                    Exit Sub
                End If

                TempCardID = Request("CardID")  ' check ��� �繡�����͡ template ���� ������� ������͡���ŷ�������֧�����Ũҡ Obj Card �������ŷ����
                oCard = CardEngine.GetCardByID(TempCardID) ' �֧������ ���촵�Ẻ ���繢����� Default ���

                If oCard Is Nothing Then
                    Exit Sub
                End If

                If (oCard.ImageFields.Count > 0) Then
                    oCard.ImageFields.Item(0).imageUrl = ""
                End If

                Dim TempSessionCard As clsCard = Session("oCard")
                If TempSessionCard IsNot Nothing Then
                    If TempSessionCard.CardID <> TempCardID Then
                        Me.ChangeTemplateSpec(TempCardID)

                        If oCard.ImageFields.Count > 0 Then
                            oCard.ImageFields.Item(0).imageUrl = ""
                        End If
                    Else
                        ' oCard.ImageFields.Item(0).imageUrl = ""
                    End If
                End If
            Else
                If oCard Is Nothing AndAlso Session("oCard") IsNot Nothing Then
                    oCard = Session("oCard")
                End If
            End If

            'Me.BindCard()
            Me.txtCardName.Text = oCard.CardName  'oCard.CardName
            If oCard.ImageFields.Count > 0 Then
                Me.lbSizeLogo.Text = oCard.ImageFields.Item(0).width & "X" & oCard.ImageFields.Item(0).Height & " " & "mm."
            End If
            Me.BindTextField()
            ''Me.BindImage()
            Me.BindLogo()

            Me.imgPreview1.ImageUrl = "CardImages/FullCard/" & oCard.templateImage

            Session("oCard") = oCard
            Session("OldImageFields") = oCard.ImageFields ' �纤���Ҿ�ҡ ���촵�Ẻ��� ����ź�Ҿ�й������º��º���������Ҿ�ҡ ���촵�Ẻ�֧��ź��

        Else
            oCard = Session("oCard")
        End If
    End Sub

	Private Sub BindCard()
		If oCard.CardName Is Nothing Then
			Exit Sub
		End If
		Me.txtCardName.Text = oCard.CardName  'oCard.CardName
		Me.BindTextField()
		Me.BindImage()
		Me.BindLogo()
	End Sub

	Private Sub BindLogo()
		If oCard Is Nothing Then
			Me.imgLogo.ImageUrl = ""
			Me.imgLogo.Visible = False
			Me.imgLogo.DataBind()
			Exit Sub
		End If

		If oCard.ImageFields IsNot Nothing AndAlso oCard.ImageFields.Count > 0 Then
			If oCard.ImageFields.Item(0).imageUrl <> "" Then
				Me.imgLogo.ImageUrl = "CardImages/UploadImage/Thumbnails/" & oCard.ImageFields.Item(0).imageUrl
				Me.imgLogo.DataBind()
				Me.imgLogo.Visible = True
			End If
		Else
			Me.imgLogo.Visible = False
			Me.imgLogo.DataBind()
		End If
	End Sub

	Private Sub BindTextField()
		If oCard.TextFields Is Nothing Then
			Exit Sub
		End If
		Me.dlText.DataSource = oCard.TextFields
		Me.dlText.DataBind()
	End Sub

	Private Sub BindImage()
		If oCard Is Nothing Then
			Me.dlImage.DataSource = Nothing
			Me.dlImage.DataBind()
			Exit Sub
		End If
		If oCard.ImageFields Is Nothing Then
			Exit Sub
		End If
		Me.dlImage.DataSource = oCard.ImageFields
		Me.dlImage.DataBind()
	End Sub

	Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPreview.Click
		Me.GetTextField()
		Me.GetImageField()
		Me.oCard.CardName = Me.txtCardName.Text
		Me.BindCard()
		' ---------------------------
		Me.add2.Visible = True
		Dim CardEngine As New clsCardEngine
		Session("oBitMap") = CardEngine.GeneratePreview(oCard, Path, 1)
		Session("oBitmapZ") = CardEngine.GeneratePreview(oCard, Path, 1.5)

		Session("oCard") = oCard
		Me.imgPreview.ImageUrl = "~/FileSenderZ.aspx"
		Me.imgPreview.Visible = True
		Me.dtTextFields.DataSource = oCard.TextFields
		Me.dtTextFields.DataBind()

	End Sub

	Private Sub ClearObject()
		Me.imgPreview.ImageUrl = ""
		Me.imgPreview.Visible = False
		Me.imgLogo.ImageUrl = ""
		Me.imgLogo.Visible = False
		Me.imgLogo.DataBind()
		Me.dtTextFields.DataSource = Nothing
		Me.dtTextFields.DataBind()
	End Sub

	Private Sub GetTextField()
		Try
			Dim newListTexts As New Generic.List(Of clsCardTextField)
			If dlText Is Nothing AndAlso dlText.Items.Count = 0 Then
				Exit Sub
			End If
			For Each aItem As DataListItem In dlText.Items
				Dim newText As New clsCardTextField
				With newText
					If oCard.CardID Is Nothing Then
						Exit Sub
					End If
					.CardID = oCard.CardID
					.Seq = CType(aItem.FindControl("hidSeq"), HiddenField).Value
					.FieldName = CType(aItem.FindControl("lbFieldName"), Label).Text
					.TextValue = CType(aItem.FindControl("txtFieldValue"), TextBox).Text
					.PositionTop = CType(aItem.FindControl("hidPositionTop"), HiddenField).Value
					.PositionLeft = CType(aItem.FindControl("hidPositionLeft"), HiddenField).Value
					.zIndex = CType(aItem.FindControl("hidzIndex"), HiddenField).Value
					.BackColor = CType(aItem.FindControl("hidBackColor"), HiddenField).Value
					.ForeColor = CType(aItem.FindControl("hidForeColor"), HiddenField).Value
					.FontFamily = CType(aItem.FindControl("hidFontFamily"), HiddenField).Value
					.FontSize = CType(aItem.FindControl("hidFontSize"), HiddenField).Value
					.CreateBy = CType(aItem.FindControl("hidCreateBy"), HiddenField).Value
					.CreateOn = CType(aItem.FindControl("hidCreateOn"), HiddenField).Value
					.UpdateBy = CType(aItem.FindControl("hidUpdateBy"), HiddenField).Value
					.UpdateOn = CType(aItem.FindControl("hidUpdateOn"), HiddenField).Value
					.FontBold = CType(aItem.FindControl("hidFontBold"), HiddenField).Value
					.FontItalic = CType(aItem.FindControl("hidFontItalic"), HiddenField).Value
					.FontUnderline = CType(aItem.FindControl("hidFontUnderline"), HiddenField).Value
					.Right2Left = CType(aItem.FindControl("hidRight2Left"), HiddenField).Value
					.AlignCenter = CType(aItem.FindControl("hidAlignCenter"), HiddenField).Value

					newListTexts.Add(newText)
				End With
			Next
			oCard.TextFields = newListTexts
		Catch ex As Exception
		End Try
	End Sub

	Private Sub GetImageField()
		Try
			Dim newImages As New List(Of clsCardImageField)
			If dlImage Is Nothing AndAlso dlImage.Items.Count = 0 Then
				Exit Sub
			End If
			For Each aItem As DataListItem In dlImage.Items
				Dim newImage As New clsCardImageField
				With newImage
					If oCard.CardID Is Nothing Then
						Exit Sub
					End If
					.CardID = oCard.CardID
					.imageUrl = CType(aItem.FindControl("hidImageUrl"), HiddenField).Value
					.ImageSize = CType(aItem.FindControl("lbImageSize"), Label).Text
					.Seq = CType(aItem.FindControl("hidSeq"), HiddenField).Value
					.PositionTop = CType(aItem.FindControl("hidPositionTop"), HiddenField).Value
					.PositionLeft = CType(aItem.FindControl("hidPositionLeft"), HiddenField).Value
					.zIndex = CType(aItem.FindControl("hidzIndex"), HiddenField).Value
					.width = CType(aItem.FindControl("hidwidth"), HiddenField).Value
					.Height = CType(aItem.FindControl("hidHeight"), HiddenField).Value
					.CreateBy = CType(aItem.FindControl("hidCreateBy"), HiddenField).Value
					.CreateOn = CType(aItem.FindControl("hidCreateOn"), HiddenField).Value
					.UpdateBy = CType(aItem.FindControl("hidUpdateBy"), HiddenField).Value
					.UpdateOn = CType(aItem.FindControl("hidUpdateOn"), HiddenField).Value
					.PreviousName = CType(aItem.FindControl("lbPreviousName"), Label).Text

					newImages.Add(newImage)
				End With
			Next
			oCard.ImageFields = newImages
		Catch ex As Exception
		End Try

	End Sub

	Protected Sub dlImage_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlImage.ItemCommand
		If e.CommandName = "DeleteImage" Then
			Dim newImages As New List(Of clsCardImageField)
			newImages = oCard.ImageFields
			ClearObject()
			Dim iImageDelete As String = CType(dlImage.Items(e.CommandArgument).FindControl("hidImageUrl"), HiddenField).Value
			Me.DeleteImageFromDLImage(iImageDelete)
			newImages.RemoveAt(e.CommandArgument)


			Me.BindImage()
			Me.BindLogo()

			If newImages.Count = 0 Then
				dlImage.DataSource = Nothing
				dlImage.DataBind()
			Else
				oCard.ImageFields = newImages
			End If
		End If
	End Sub

	Protected Sub btnAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttach.Click

		If dlImage.Items.Count > 0 Then
			MessageBox.Show("��س�ź File Upload ��͹˹�ҹ����¤��")
			Exit Sub
		End If

		If Me.FileUpload1.PostedFile IsNot Nothing Then
			Dim File As HttpPostedFile = Me.FileUpload1.PostedFile
			Dim FileEngine As New clsFileManagerEngine
			Dim TempResult As New clsResultII

			If FileUpload1.FileName = "" Then
				MessageBox.Show("��س��к� File ����ͧ��� Upload ���¤��")
				Exit Sub
			End If

			'  oCard = Nothing

			TempResult = FileEngine.CheckFileSize(File, 200)
			If TempResult.Flag = False Then
				MessageBox.Show(TempResult.Message)
				Exit Sub
			End If

			TempResult = FileEngine.CheckImageFile(File)
			If TempResult.Flag = False Then
				MessageBox.Show("����ͧ�Ѻ��������: " + TempResult.Message)
				Exit Sub
			End If

			TempResult = FileEngine.UploadImage(File, Path)

			If TempResult.Flag = False Then
				Exit Sub
			End If

			Dim newImages As List(Of clsCardImageField) = Nothing
			newImages = CardEngine.GetCardByID(oCard.CardID).ImageFields

			With newImages.Item(0)
				.imageUrl = TempResult.Message
				.ImageSize = File.ContentLength / 1024 '˹����� KB
				.PreviousName = File.FileName.Substring(File.FileName.LastIndexOf("\") + 1)
				.ContentType = File.ContentType
			End With

			If dlImage.Items.Count > 0 Then
				Me.DeleteImageFromDLImage(CType(dlImage.Items(0).FindControl("hidImageUrl"), HiddenField).Value)
			End If

			oCard.ImageFields = newImages
			oCard.CardName = Me.txtCardName.Text '����
			Session("ImageFields") = newImages
			Me.BindImage()
			Me.BindLogo()
			'Me.BindCard()
		End If
	End Sub

	Private Sub DeleteImageFromDLImage(ByVal strImg As String)
		Dim FlagDelete As Boolean = True
		Dim OldTemplateImageField As List(Of clsCardImageField) = Session("OldImageFields")
		For Each TempImage As clsCardImageField In OldTemplateImageField
			If (TempImage.imageUrl = strImg) Then
				FlagDelete = False
			End If
		Next
		If (FlagDelete = True) Then
			Dim FileEngine As New clsFileManagerEngine
			oCard = Nothing
			' FileEngine.DeleteFile(Path & "\CardImages\UploadImage\Currents\" & strImg)
			'FileEngine.DeleteFile(Path & "\CardImages\UploadImage\Thumbnails\" & strImg)
		End If
	End Sub

	Private Function CheckRequest() As clsResultII
		Dim TempResult As New clsResultII
		If Request("CardID") Is Nothing AndAlso Session("oCard") Is Nothing Then
			Response.Write("<script>alert('��س����͡ Template ��͹');</script>")
			Response.Write("<script>history.go(-1)</script>")
			TempResult.Flag = False
			Return TempResult
		End If
		TempResult.Flag = True
		Return TempResult
	End Function

	Private Sub ChangeTemplateSpec(ByVal NewID As String)
		'�Ӣ����Ũҡ Obj Card ������ա������¹�ŧ���������� ����� template �ٻẺ���� 
		Dim CardEngine As New clsCardEngine
		Dim TempNewCard As clsCard = CardEngine.GetCardByID(NewID)

		'������ text
		Dim TempTextFields As New List(Of clsCardTextField)
		Dim TempOldCard As clsCard = Session("oCard")
		For Each anewText As clsCardTextField In TempNewCard.TextFields	' ǹ Loop ��� session �������� 
			If TempOldCard.TextFields IsNot Nothing Then
				For Each aText As clsCardTextField In TempOldCard.TextFields ' ǹ Loop � template ��Ƿ����������
					' ����繢��������ǡѹ (FieldName) ����纤��ŧ�������
					If anewText.FieldName = aText.FieldName Then
						anewText.TextValue = aText.TextValue
					End If
				Next
			End If
			TempTextFields.Add(anewText)
		Next

			TempNewCard.TextFields = TempTextFields

			'������ Image
			Dim TempImageFields As List(Of clsCardImageField) = Nothing
			TempImageFields = Session("ImageFields")
			If TempImageFields IsNot Nothing AndAlso TempImageFields.Count > 0 Then
				TempImageFields.Item(0).PositionLeft = TempNewCard.ImageFields.Item(0).PositionLeft
				TempImageFields.Item(0).PositionTop = TempNewCard.ImageFields.Item(0).PositionTop
				TempNewCard.ImageFields = TempImageFields
			End If

			oCard = TempNewCard
			Session("oCard") = TempNewCard
	End Sub

	Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
		Me.GetTextField()
		Me.GetImageField()
		oCard.CardName = Me.txtCardName.Text '����
		Me.BindCard()
		' ---------------------------
		Dim CardEngine As New clsCardEngine
		Session("oBitMap") = CardEngine.GeneratePreview(oCard, Path)
		Session("oBitmapZ") = CardEngine.GeneratePreview(oCard, Path, 1.5)
		Session("oCard") = oCard

		Response.Redirect("DetailPaper.aspx")
		'-----------------------
	End Sub

End Class
