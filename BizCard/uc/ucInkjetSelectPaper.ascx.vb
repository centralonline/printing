Imports System.Collections.Generic
Partial Class uc_ucInkjetSelectPaper
    Inherits System.Web.UI.UserControl
    Private PaperIDValue As String
    Private _test As String

    Private IsUploadValue As Boolean
    Public Property IsUpload() As String
        Get
            Return IsUploadValue
        End Get
        Set(ByVal value As String)
            IsUploadValue = value
        End Set
    End Property

    Private listPaperValue As String
    Public Property listPaper() As String
        Get
            Return listPaperValue
        End Get
        Set(ByVal value As String)
            listPaperValue = value
        End Set
    End Property
    Private ddlsizetextValue As String
    Public Property ddlsize() As String
        Get
            Return ddlsizetextValue
        End Get
        Set(ByVal value As String)
            ddlsizetextValue = value
        End Set
    End Property
    Private PaperValue As String
    Public Property PapersizeID() As String
        Get
            Return PaperValue
        End Get
        Set(ByVal value As String)
            PaperValue = value
        End Set
    End Property

    Private PaperSizeValue As String
    Public Property PaperSize() As String
        Get
            Return PaperSizeValue
        End Get
        Set(ByVal value As String)
            PaperSizeValue = value
        End Set
    End Property

    Private TypeSystemValue As String
    Public Property TypeSystem() As String
        Get
            Return TypeSystemValue
        End Get
        Set(ByVal value As String)
            TypeSystemValue = value
        End Set
    End Property

    'gift ����������㹡�äԴ��Һ�ԡ���Թ���
    'Createby 26/6/2014
    Private orderValue As String
    Public Property order() As String
        Get
            Return orderValue
        End Get
        Set(ByVal value As String)
            orderValue = value
        End Set
    End Property

    Private sumserviceValue As Double
    Public Property sumservice() As Double
        Get
            Return sumserviceValue
        End Get
        Set(ByVal value As Double)
            sumserviceValue = value
        End Set
    End Property

    Private sumValue As Double
    Public Property sum() As Double
        Get
            Return sumValue
        End Get
        Set(ByVal value As Double)
            sumValue = value
        End Set
    End Property
    Private SummaryValue As Double
    Public Property Summary() As Double
        Get
            Return SummaryValue
        End Get
        Set(ByVal value As Double)
            SummaryValue = value
        End Set
    End Property
    Private PPaperIDValue As String
    Public Property PaperID() As String
        Get
            Return PPaperIDValue
        End Get
        Set(ByVal value As String)
            PPaperIDValue = value
        End Set
    End Property
    Private NetAmtValue As String
    Public Property NetAmt() As String
        Get
            Return NetAmtValue
        End Get
        Set(ByVal value As String)
            NetAmtValue = value
        End Set
    End Property
    Private sumviewValue As String
    Public Property sumview() As String
        Get
            Return sumviewValue
        End Get
        Set(ByVal value As String)
            sumviewValue = value
        End Set
    End Property
    Private papernameValue As String
    Public Property papername() As String
        Get
            Return papernameValue
        End Get
        Set(ByVal value As String)
            papernameValue = value
        End Set
    End Property
    Private serviceValue As String
    Public Property service() As String
        Get
            Return serviceValue
        End Get
        Set(ByVal value As String)
            serviceValue = value
        End Set
    End Property
    Private PriceValue As String
    Public Property Price() As String
        Get
            Return PriceValue
        End Get
        Set(ByVal value As String)
            PriceValue = value
        End Set
    End Property

    'Gift ���������觫����к� Inkjet 
    'Createby 26/6/2014

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not IsPostBack) Then
            Dim clsGetListPaper As New clsPaperEngine
            Dim listPaper As New List(Of clsPaper)
            listPaper = clsGetListPaper.GetListPaperdigital("Inkjet")
            ddlType.Items.Clear()

            Me.ddlType.DataSource = listPaper
            Me.ddlType.DataTextField = "size"
            Me.ddlType.DataValueField = "size"
            Me.ddlType.DataBind()
            txtQuantity.Text = ""
            clear()
        Else
            PapersizeID = ddlType.SelectedValue
            papername = ddlTypename.SelectedValue

            End If
    End Sub

    Protected Sub ddlType_Load(ByVal sender As Object, e As System.EventArgs) Handles ddlType.Load '�������ҹ

        If ddlType.Text = "Display" Then
            BindDataPaperBySize(GetDataPaperBySize(ddlType.Text))
            txtheight.Enabled = False
            txtwidth.Enabled = False
            lbheight.Visible = False
            lbwidth.Visible = False
            txtwidth.BorderStyle = BorderStyle.None
            txtheight.BorderStyle = BorderStyle.None
            txtwidth.BackColor = Drawing.Color.DarkGray
            txtheight.BackColor = Drawing.Color.DarkGray
            lbQtyPrice.Visible = False
            lbQtyDisplay.Visible = True
        Else
            BindDataPaperBySize(GetDataPaperBySize(ddlType.Text))
            txtheight.Enabled = True
            txtwidth.Enabled = True
            lbheight.Visible = True
            lbwidth.Visible = True
            txtwidth.BorderStyle = Nothing
            txtheight.BorderStyle = Nothing
            txtwidth.BackColor = Drawing.Color.Empty
            txtheight.BackColor = Drawing.Color.Empty
            lbQtyDisplay.Visible = False
            lbQtyPrice.Visible = True
        End If

        Try
            If PapersizeID = ddlType.SelectedValue Then
                Me.ddlTypename.SelectedValue = papername
            Else
                ddlTypename.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ddlTypename_Load(sender As Object, e As System.EventArgs) Handles ddlTypename.Load '��������ʴ�

        GetDatapaperByDecription(ddlTypename.Text)
        If txtQuantity.Text = "" Then
            clear()
        End If

    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Me.ddlTypename.Items.Clear()
        BindDataPaperBySize(GetDataPaperBySize(ddlType.Text))
            txtQuantity.Text = ""
            txtheight.Text = ""
            txtwidth.Text = ""
            clear()
    End Sub

    Protected Sub ddlTypename_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlTypename.SelectedIndexChanged
        GetDatapaperByDecription(ddlTypename.Text)
        txtQuantity.Text = ""
        txtheight.Text = ""
        txtwidth.Text = ""
        clear()
    End Sub

    Protected Sub txtQuantity_TextChanged(sender As Object, e As System.EventArgs) Handles txtQuantity.TextChanged
        If ddlType.Text = "Display" Or txtheight.Text <> "" And txtwidth.Text <> "" And txtQuantity.Text <> "" Then
            If txtQuantity.Text = "" Then
                clear()
            Else
                Getcost() '�ӹǹ�Թ Display
            End If
        End If

    End Sub

    Private Sub BindDataPaperBySize(ByVal papername As List(Of clsPaper))
        Me.ddlTypename.Items.Clear()
        Me.ddlTypename.DataSource = papername
        Me.ddlTypename.DataValueField = "PaperID"
        Me.ddlTypename.DataTextField = "Description"
        Me.ddlTypename.DataBind()
        Me.ddlTypename.SelectedIndex = -1
    End Sub

    Private Function GetDataPaperBySize(ByVal paperSize As String) As List(Of clsPaper)
        Dim clsGetListPaper1 As New clsPaperEngine
        Dim namepaper As New List(Of clsPaper)
        ddlTypename.Items.Clear()
        namepaper = clsGetListPaper1.GetListPapersize(ddlType.Text, "Inkjet")
        Return namepaper
    End Function

    Private Function GetDatapaperByDecription(ByVal Description As String) As List(Of clsPaper)
        Dim clsGetddlnameDecription As New clsPaperEngine
        Dim namepaperDecription As New List(Of clsPaper)
        namepaperDecription = clsGetddlnameDecription.GetListPaperDecription(ddlTypename.Text, ddlType.Text, "Inkjet")
        Me.lbPaperID.Text = namepaperDecription.Item(0).PaperID
        Me.lbPrice.Text = Format((namepaperDecription.Item(0).Price), "#,##0")
        Me.lbPromotion.Text = namepaperDecription.Item(0).Promotion
        Session("Typename") = namepaperDecription.Item(0).Typename
        Return namepaperDecription

    End Function

    Private Sub clear() 'ź���� Textbox
        lbVat.Text = "0.00"
        lbVat_card.Text = "0.00"
        lbDelivery.Text = "0.00"
        lbSummary.Text = "0.00"
        lbQty.Text = "0.00"
    End Sub

    Private Sub Getcost() '�ӹǹ�ʹ�Թ Inkjet  Indoor and Outdoor
        '�ٵá�äӹǹ Inkjet
        '��Ҵ(cm2.) = ���ҧ * ���
        '��Ҵ(cm2.) = ��Ҵ(cm2.) * 0.0001
        '�Ҥҵ��˹��� = ��Ҵ(cm2.) * �Ҥҵ��˹��� m2.
        '�Ҥ��ط�� = �Ҥҵ��˹��� * �ӹǹ

        Me.order = txtQuantity.Text
        If ddlType.Text <> "Display" Then
            If lbPromotion.Text <> "0" Then

                Dim Price As Double = lbPrice.Text
                Me.service = (txtheight.Text * txtwidth.Text) '��Ҵ (cm2.)
                Me.sumservice = (service * 0.0001) '��Ҵ (cm2.)
                Me.lbQty.Text = Format((sumservice * Price), "#,##0.00") '�Ҥҵ��˹���
                Me.sum = Format((lbQty.Text * order), "#,##0.00")
                Me.lbVat_card.Text = Format((Me.sum * 7) / 107, "#,##0.00")
                Me.lbVat.Text = Format((Me.sum - lbVat_card.Text), "#,##0.00")
                Dim Vat As Double = lbVat.Text
                Dim Vat_card As Double = lbVat_card.Text
                Me.Summary = Format((Vat + Vat_card), "#,##0.00")

            Else

                Dim Promotion As Double = lbPromotion.Text
                Dim Price As Double = lbPrice.Text
                Me.service = (txtheight.Text * txtwidth.Text)
                Me.sumservice = (service * 0.0001)
                Me.lbQty.Text = Format((sumservice * Promotion), "#,##0.00")
                Me.sum = Format((lbQty.Text * order), "#,##0.00")
                Me.lbVat_card.Text = Format((Me.sum * 7) / 107, "#,##0.00")
                Me.lbVat.Text = Format((Me.sum - lbVat_card.Text), "#,##0.00")
                Dim Vat As Double = lbVat.Text
                Dim Vat_card As Double = lbVat_card.Text
                Me.Summary = Format((Vat + Vat_card), "#,##0.00")

            End If

        Else '�ӹǹ�ʹ�Թ Display
            Dim Price As Double = lbPrice.Text
            lbQty.Text = Format((Price), "#,##0.00") '�Ҥҵ��˹���
            Me.sum = Format((Price * order), "#,##0.00")
            Me.lbVat_card.Text = Format((Me.sum * 7) / 107, "#,##0.00")
            Me.lbVat.Text = Format((Me.sum - lbVat_card.Text), "#,##0.00")
            Dim Vat As Double = lbVat.Text
            Dim Vat_card As Double = lbVat_card.Text
            Me.Summary = Format((Vat + Vat_card), "#,##0.00")

        End If

        If lbVat.Text < 500 Then
            lbDelivery.Text = "50.00"

            Me.lbSummary.Text = Format(((Me.Summary * 1) + 50), "#,##0.00")
        Else
            lbDelivery.Text = "0.00"

            Me.lbSummary.Text = Format(((Me.Summary * 1)), "#,##0.00")
        End If

        Me.Session("Typewidth") = "���ҧ" + "" + txtwidth.Text + "" + "cm" + "  " + "���" + "" + txtheight.Text + "" + "cm" '�觤���ʴ�������� (Invoice)
        Me.Session("NetAmt") = Format((Me.sum), "#,##0.00")
        Me.Session("TotAmt") = Me.lbSummary.Text
        Me.Session("lbVat_Card") = Me.lbVat_card.Text
        Me.Session("lbDelivery") = Me.lbDelivery.Text
        Me.Session("lbVat") = Me.lbVat.Text
        Me.Session("sumtotal") = Format((Me.sum), "#,##0.00")
        Me.Session("sumangle") = "0.00"
        Me.Session("sumview") = Me.sumview

        Me.Session("Type") = ddlType.Text
        Me.Session("Price") = lbQty.Text
        Me.sumview = 0
        Me.NetAmt = 0

    End Sub

    Protected Sub txtwidth_TextChanged(sender As Object, e As System.EventArgs) Handles txtwidth.TextChanged '���ҧ
        If txtheight.Text <> "" And txtwidth.Text <> "" And txtQuantity.Text <> "" Then
            Getcost()
        End If
        txtheight.Focus()
    End Sub

    Protected Sub txtheight_TextChanged(sender As Object, e As System.EventArgs) Handles txtheight.TextChanged '���
        If txtheight.Text <> "" And txtwidth.Text <> "" And txtQuantity.Text <> "" Then
            Getcost()
        End If
        txtQuantity.Focus()
    End Sub

End Class

