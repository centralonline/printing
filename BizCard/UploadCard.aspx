﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UploadCard.aspx.vb" Inherits="UploadCard" title="อัพโหลดไฟล์นามบัตร เพื่อสั่งพิมพ์นามบัตร trendyprint.net" %>

<%@ Register Src="uc/ucCard.ascx" TagName="ucCard" TagPrefix="uc2" %>
<%@ Register Src="uc/ucHeadPanel1.ascx" TagName="ucHeadPanel1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" Runat="Server">
<asp:Panel ID="Panel4" runat="server" Width="100%">
	<div style="margin:auto;">
    <img src="images/h-make-card01.png" class="png" alt="ทำนามบัตร" title="ทำนามบัตร" /><br />
    </div>
    
    <div style="padding: 30px; background-color:#fff; margin:auto; width: 920px;">	
	
    	<span style="font-size: 80%; padding:20px 0px 20px 0px;">< ต้องการกลับไปเลือก <a href="Templates.aspx">ทำนามบัตรจาก Template</a></span><br /><br />
        <h1>บริการสั่งพิมพ์นามบัตรออนไลน์ | ประเภท Upload ไฟล์</h1>
    	<div style="background-color:#fff; width:600px; align:center; border: 1px solid #ddd; padding:10px;">
    		<p class="head-column">กรุณาเลือกรูปแบบการ UpLoad File ที่คุณต้องการ</p>
    		<p style="font-size: 90%;">
        	<asp:RadioButton ID="radReadyPrint" runat="server" Text="รูปแบบนามบัตรที่รับ File ที่พร้อมจัดพิมพ์" GroupName="xx" /><br/>
        	<asp:RadioButton ID="radOfmDesign" runat="server" Text="รูปแบบนามบัตรที่รับ File เพื่อให้ Printing Solution ดีไซน์ก่อนจัดพิมพ์" GroupName="xx"  />
        	</p> 
    
    		<div id="btn-card">
    		<asp:ImageButton ID="btnSelectTemplate" runat="server" ImageUrl="images/btnupload_03.jpg" />
        	</div>
            <div>
            <div style="background-color:#fff; float:left; font-weight:bold;"><b>My Gallery</b></div><br /><br />
        	<uc2:ucCard ID="UcCard1" runat="server" />
        	<p style="font-size: 80%; padding-top:20px;"><img src="images/icon/arrow-left.gif" style="vertical-align:middle;"> <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/GallerysUpload.aspx">My Upload Gallery </asp:HyperLink></p>
            </div>
    	</div>    		
    </div>                                       
</asp:Panel>                      
</asp:Content>

