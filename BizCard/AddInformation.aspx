<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	CodeFile="AddInformation.aspx.vb" Inherits="AddInformation" Title="��������ŧ�Ẻ��������ú��ǹ Officemate Printing Solution"
	EnableEventValidation="false" %>

<%@ Register Src="uc/ucBar.ascx" TagName="ucBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<div style="margin: auto;">
		<img src="images/h-make-card01.png" class="png" alt="�ӹ���ѵ�" title="�ӹ���ѵ�" /><br />
	</div>
	<div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<uc1:ucBar ID="UcBar1" runat="server" />
		<h1>
			��ԡ����觾�������ѵ��͹�Ź� | ������ Template Card</h1>
		<asp:Panel ID="pnCard" runat="server">
			<div style="width: 400px; float: left;">
				<p class="head-column">
					2.1 ��������ŧ�Ẻ��������ú��ǹ</p>
				<asp:DataList ID="dlText" runat="server" Width="300">
					<HeaderTemplate>
						<table border="0" width="100%">
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td align="right" style="white-space: nowrap;">
								<asp:Label ID="lbFieldName" runat="server" Text='<%# Eval("FieldName") %>'></asp:Label>
								&nbsp;:
							</td>
							<td align="left">
								<asp:TextBox ID="txtFieldValue" runat="server" Text='<%#Eval("TextValue") %>' Width="200"></asp:TextBox>
								<asp:HiddenField ID="hidSeq" runat="server" Value='<%#Eval("Seq") %>' />
								<asp:HiddenField ID="hidPositionTop" runat="server" Value='<%#Eval("PositionTop") %>' />
								<asp:HiddenField ID="hidPositionLeft" runat="server" Value='<%#Eval("PositionLeft") %>' />
								<asp:HiddenField ID="hidzIndex" runat="server" Value='<%#Eval("zIndex") %>' />
								<asp:HiddenField ID="hidBackColor" runat="server" Value='<%#Eval("BackColor") %>' />
								<asp:HiddenField ID="hidForeColor" runat="server" Value='<%#Eval("ForeColor") %>' />
								<asp:HiddenField ID="hidFontFamily" runat="server" Value='<%#Eval("FontFamily") %>' />
								<asp:HiddenField ID="hidFontSize" runat="server" Value='<%#Eval("FontSize") %>' />
								<asp:HiddenField ID="hidCreateBy" runat="server" Value='<%#Eval("CreateBy") %>' />
								<asp:HiddenField ID="hidCreateOn" runat="server" Value='<%#Eval("CreateOn") %>' />
								<asp:HiddenField ID="hidUpdateBy" runat="server" Value='<%#Eval("UpdateBy") %>' />
								<asp:HiddenField ID="hidUpdateOn" runat="server" Value='<%#Eval("UpdateOn") %>' />
								<asp:HiddenField ID="hidFontBold" runat="server" Value='<%#Eval("FontBold") %>' />
								<asp:HiddenField ID="hidFontItalic" runat="server" Value='<%#Eval("FontItalic") %>' />
								<asp:HiddenField ID="hidFontUnderline" runat="server" Value='<%#Eval("FontUnderline") %>' />
								<asp:HiddenField ID="hidRight2Left" runat="server" Value='<%#Eval("Right2Left")%>' />
								<asp:HiddenField ID="hidAlignCenter" runat="server" Value='<%#Eval("AlignCenter")%>' />
							</td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:DataList>
				<asp:Panel ID="pnUpload" runat="server">
					<p class="head-column">
						2.2 Upload logo</p>
					File :
					<asp:FileUpload ID="FileUpload1" runat="server" class="btn-face2" />
					<asp:Button ID="btnAttach" runat="server" Text="Attach File" class="btn-face" />
					<asp:Image ID="imgLogo" runat="server" ImageUrl="~/CardImages/UploadImage/Thumbnails/Default.jpg"
						Visible="False" />
					<asp:Panel ID="Panel1" runat="server" Height="100px">
						<asp:DataList ID="dlImage" runat="server" Width="100%" Height="112px">
							<HeaderTemplate>
								<table border="0" width="100%">
									<tr style="background: #ccc; padding: 5px;">
										<td>
											<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/attach.gif" />
										</td>
										<td>
											Size(KB)
										</td>
										<td>
											Remove
										</td>
									</tr>
							</HeaderTemplate>
							<ItemTemplate>
								<tr style="background: #eee; padding: 5px;">
									<td>
										<asp:Label ID="lbPreviousName" runat="server" Text='<%#Eval("PreviousName") %>'></asp:Label>
									</td>
									<td>
										<asp:Label ID="lbImageSize" runat="server" Text='<%#Eval("ImageSize") %>'></asp:Label>
										<asp:HiddenField ID="hidSeq" runat="server" Value='<%#Eval("Seq")  %>' />
										<asp:HiddenField ID="hidPositionTop" runat="server" Value='<%#Eval("PositionTop") %>' />
										<asp:HiddenField ID="hidPositionLeft" runat="server" Value='<%#Eval("PositionLeft") %>' />
										<asp:HiddenField ID="hidzIndex" runat="server" Value='<%#Eval("zIndex") %>' />
										<asp:HiddenField ID="hidwidth" runat="server" Value='<%#Eval("width") %>' />
										<asp:HiddenField ID="hidHeight" runat="server" Value='<%#Eval("Height") %>' />
										<asp:HiddenField ID="hidCreateBy" runat="server" Value='<%#Eval("CreateBy") %>' />
										<asp:HiddenField ID="hidCreateOn" runat="server" Value='<%#Eval("CreateOn") %>' />
										<asp:HiddenField ID="hidUpdateBy" runat="server" Value='<%#Eval("UpdateBy") %>' />
										<asp:HiddenField ID="hidUpdateOn" runat="server" Value='<%#Eval("UpdateOn") %>' />
										<asp:HiddenField ID="hidImageUrl" runat="server" Value='<%#Eval("imageUrl") %>' />
									</td>
									<td>
										<asp:ImageButton ID="btnDelete" ImageUrl="~/images/delete.gif" runat="server" CommandArgument='<%# Container.ItemIndex %>'
											CommandName="DeleteImage" />
									</td>
								</tr>
							</ItemTemplate>
							<SeparatorTemplate>
							</SeparatorTemplate>
							<FooterTemplate>
								</table>
							</FooterTemplate>
						</asp:DataList>
					</asp:Panel>
				</asp:Panel>
			</div>
			<div style="width: 400px; float: right; padding: 20px; border: 1px solid #ddd;">
				<asp:Image ID="Image1" runat="server" ImageUrl="~/images/addtopic.gif" /><br />
				<br />
				<asp:Image ID="imgPreview1" runat="server" ImageUrl="~/CardImages/FullCard/default.jpg"
					CssClass="BorderCard" /><br />
				��Ҵ�ͧ Logo :
				<asp:Label ID="lbSizeLogo" runat="server" Text="Label"></asp:Label><br />
				<br />
				<asp:ImageButton ID="btnPreview" runat="server" ImageUrl="images/btn2_03.jpg" /><br />
				<br />
				<asp:Image ID="imgPreview" runat="server" Visible="false" CssClass="BorderCard" BorderColor="Silver"
					BorderStyle="Solid" BorderWidth="1px" /><br />
				<asp:Label ID="Label1" runat="server" Text="��駪��͹���ѵ� : "></asp:Label><asp:TextBox
					ID="txtCardName" runat="server"></asp:TextBox>
				<br />
				*��õ�駪��͹������������ My Gallery&nbsp; �ͧ�س
				<br />
				<br />
			</div>
			<br class="clear" />
		</asp:Panel>
		<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
			<asp:Image ID="add2" runat="server" ImageUrl="images/addtopic2.gif" /><br />
			<br />
			<asp:DataList ID="dtTextFields" runat="server" BackColor="White" BorderColor="#336666"
				BorderStyle="solid" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Both"
				Width="900px">
				<FooterStyle BackColor="White" ForeColor="#333333" />
				<SelectedItemStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
				<ItemTemplate>
					<table width="100%">
						<tr>
							<td style="width: 80px; height: 15px; text-align: center;">
								<asp:Label ID="Label1" runat="server" Text='<%# eval("Seq") %>'></asp:Label>.
							</td>
							<td colspan="1" style="height: 15px; width: 200px; text-align: left;">
								<asp:Label ID="Label3" runat="server" Text='<%# eval("FieldName") %>'></asp:Label>
							</td>
							<td colspan="2" style="height: 15px; text-align: left;">
								<asp:Label ID="Label2" runat="server" Text='<%# eval("TextValue") %>'></asp:Label>
							</td>
						</tr>
					</table>
				</ItemTemplate>
				<ItemStyle BackColor="White" ForeColor="#333333" />
				<HeaderTemplate>
					<table width="100%">
						<tr>
							<td style="width: 80px; height: 15px; text-align: center;">
								<asp:Label ID="Label1" runat="server" Text="�ӴѺ���"></asp:Label>
							</td>
							<td colspan="1" style="height: 15px; text-align: left; width: 200px;">
								<asp:Label ID="Label4" runat="server" Text="��Ǣ��"></asp:Label>
							</td>
							<td colspan="2" style="height: 15px; text-align: left">
								<asp:Label ID="Label2" runat="server" Text="�����ŷ���͡"></asp:Label>
							</td>
						</tr>
					</table>
				</HeaderTemplate>
				<HeaderStyle BackColor="#C9E4C9" Font-Bold="True" ForeColor="DimGray" />
			</asp:DataList>
		</div>
		<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
			<p>
				<b>��� Upload File ���� ���͹�����㹡�èѴ�����</b></p>
			- �س����ö Upload File ��������§ 1 �����ҹ��<br />
			- ����������ͧ�Ѻ�� ��� .jpg , .Bmp, .gif, .tif, .tiff<br />
			- ���ŵ�ͧ����ѵ�����Ẻ�������������������<br />
			- ��Ҵ�ͧ�������������ö Upload ������Թ 2 MB<br />
			<br />
			<p>
				<b>�����˵�</b></p>
			- �������Ѵ�ͧ���� �������Ѻ��Ҵ��Ф��������´�ͧ��������� Upload ��<br />
			- �������ö������վ������ �� ���з�͹�ʧ ���Թ �շͧ �� Spot �շ���ա�á�˹������
			�����駧ҹ���ͺ��<br />
		</div>
		<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
			<div id="btn-card" align="left">
				<asp:HyperLink ID="HyperLink2" NavigateUrl="~/Templates.aspx" runat="server" class="btn-face2"><< ���͡ Template ����</asp:HyperLink>
				<asp:HyperLink ID="HyperLink1" NavigateUrl="~/DesignCard.aspx" runat="server" class="btn-face2"><< ��Ѻ�˹�� Gallery</asp:HyperLink>
				<asp:ImageButton ID="btnNext" runat="server" ImageUrl="~/images/next_03.gif" Style="float: right;" />
			</div>
		</div>
</asp:Content>
