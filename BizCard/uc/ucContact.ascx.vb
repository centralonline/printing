Imports System.Collections.Generic
Partial Class uc_ucContact
	Inherits clsUCBase

	
    Public ReadOnly Property ContactName() As String
        Get
            Return lbContactName.Text
        End Get
    End Property

    Public ReadOnly Property ContactMobileNo() As String
        Get
            Return lbContactMobileNo.Text
        End Get
    End Property

    Public ReadOnly Property ContactPhoneNo() As String
        Get
            Return lbContactPhoneNo.Text
        End Get
    End Property

    Public ReadOnly Property ContactFaxNo() As String
        Get
            Return lbContactFaxNo.Text
        End Get
    End Property

    Public ReadOnly Property ContactEmail() As String
        Get
            Return lbContactEmail.Text
        End Get
    End Property


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If objSessionVar.ModelUser IsNot Nothing Then
			If objSessionVar.ModelUser.Contact IsNot Nothing Then
                lbContactName.Text = objSessionVar.ModelUser.Contact.ContactName
                lbContactMobileNo.Text = objSessionVar.ModelUser.Contact.ContactMobileNo
                lbContactPhoneNo.Text = objSessionVar.ModelUser.Contact.ContactPhoneNo
                lbContactFaxNo.Text = objSessionVar.ModelUser.Contact.ContactFaxNo
                lbContactEmail.Text = objSessionVar.ModelUser.Contact.ContactEmail.ContactEmail
			End If
		End If
	End Sub
End Class
