﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucConfirmOrderUploadNew.ascx.vb"
	Inherits="uc_ucConfirmOrderUploadNew" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Untitled Document</title>
	<style type="text/css">
		.style1
		{
			height: 21px;
		}
		.style2
		{
			width: 100px;
			height: 21px;
		}
	</style>
</head>
<body>
	<div>
		<asp:Image ID="Image1" runat="server" ImageUrl="../images/logo-trendyprint.png" /></div>
	<div style="vertical-align: top;">
		<p style="font-family: Tahoma; color: #666666; font-size: 11px">
			<b>บริษัท ซีโอแอล จำกัด (มหาชน)</b><br />
			24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ 10250
			<br />
			โทร : 02-739-5555(120 สาย) แฟ็กซ์ : 02-763-5555(30 สาย)<br />
			Website : www.officemate.co.th, e-Mail : <a href="mailto:printing@officemate.co.th">
				printing@officemate.co.th</a>
		</p>
	</div>
	<div class="box-general">
		<div class="box-300">
			<b>เลขที่ใบสั่งทำของท่านคือ</b>
			<asp:Label ID="lbOrderID" runat="server" Text="lbOrderID" Font-Bold="True"></asp:Label></div>
		<br class="clear" />
		<div class="box-300">
			<b>วันที่ทำการสั่ง: </b>
			<asp:Label ID="lbDateOrder" runat="server" Text="lbDateOrder"></asp:Label></div>
		<br class="clear" />
		<div class="box-300">
			<b>รหัสลูกค้า:</b> <span class="hilight">
				<asp:Label ID="lbCustID" runat="server" Text="lbCustID"></asp:Label></span></div>
		<div class="box-300">
			<b>ชื่อลูกค้า:</b>
			<asp:Label ID="lbCustName" runat="server" Text="lbCustName"></asp:Label></div>
		<div class="box-300">
			<b>เบอร์มือถือ:</b>
			<asp:Label ID="lbMobile" runat="server" Text="lbMobile"></asp:Label></div>
		<br class="clear" />
	</div>
	<div class="box-general">
		<b>สถานที่จัดส่ง:</b>
		<asp:Label ID="lbAddress1" runat="server" Text="lbAddress1"></asp:Label><br class="clear" />
		<asp:Label ID="lbAddress2" runat="server" Text="lbAddress2"></asp:Label>&nbsp;
		<asp:Label ID="lbAddress3" runat="server" Text="lbAddress3"></asp:Label>&nbsp;
		<asp:Label ID="lbAddress4" runat="server" Text="lbAddress4"></asp:Label>
		<br class="clear" />
		<br class="clear" />
		<div class="box-300">
			<b>ผู้รับสินค้า:</b>
			<asp:Label ID="lbContactName" runat="server" Text="lbContactName"></asp:Label></div>
		<div class="box-300">
			<b>อีเมล์:</b>
			<asp:Label ID="lbContactEmail" runat="server" Text="lbContactEmail"></asp:Label></div>
		<div class="box-300">
			<b>เบอร์มือถือ:</b>
			<asp:Label ID="lbContactMobileNo" runat="server" Text="lbContactMobileNo"></asp:Label></div>
		<div class="box-300">
			<b>เบอร์โทร:</b>
			<asp:Label ID="lbContactPhoneNo" runat="server" Text="lbContactPhoneNo"></asp:Label></div>
		<div class="box-300">
			<b>เบอร์แฟกซ์:</b>
			<asp:Label ID="lbContactFaxNo" runat="server" Text="lbContactFaxNo"></asp:Label></div>
		<br class="clear" />
	</div>
	<div class="box-general">
		<b>สรุปรายการที่สั่งทำ:</b>
		<br />
		<asp:Panel ID="Panel1" runat="server">
			<table width="100%" border="0" cellspacing="1" cellpadding="5">
				<tr>
					<td bgcolor="#C9E4C9">
						<div align="left">
							<strong>รายละเอียด</strong></div>
					</td>
					<td bgcolor="#C9E4C9">
						<div align="right">
							ราคาต่อหน่วย<br />
							(บาท)</div>
					</td>
					<td bgcolor="#C9E4C9">
						<div align="right">
							จำนวนที่สั่งทำ (ใบ)</div>
					</td>
					<td bgcolor="#C9E4C9">
						<div align="right">
							จำนวนเงิน (บาท)</div>
					</td>
				</tr>
				<tr>
					<td align="left" bgcolor="#FFFFFF">
						รหัสกระดาษ :
						<asp:Label ID="lbPaperID" runat="server" Text="lbPaperID"></asp:Label>
					</td>
					<td align="right" bgcolor="#FFFFFF" width="100px">
						<asp:Label ID="lbPrice" runat="server" Text="lbPrice"></asp:Label>
					</td>
					<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
						<asp:Label ID="lbNumOrder" runat="server" Text="lbNumOrder"></asp:Label>
					</td>
					<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
						<asp:Label ID="lbTotal" runat="server" Text="lbTotal"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="left" bgcolor="#FFFFFF">
						<%-- รหัสกระดาษ :
                    <asp:Label ID="lbPaperID" runat="server" Text="lbPaperID"></asp:Label>--%>
					</td>
					<td bgcolor="#ffffff" colspan="2" style="text-align: right">
					</td>
					<%--<td align="right" bgcolor="#ffffff" style="width: 100px">
                    &nbsp;
                </td>--%>
				</tr>
				<tr>
					<td align="left" bgcolor="#ffffff">
						ชนิดกระดาษ :
						<asp:Label ID="lbPaperName" runat="server" Text="lbPaperName"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="left" bgcolor="#FFFFFF">
						บริการตัดมุม :
						<asp:Label ID="lbOptionalDesc2" runat="server" Text="lbOptionalDesc2"></asp:Label>
					</td>
					<td colspan="2" align="right" bgcolor="#FFFFFF">
						ค่าบริการ :
					</td>
					<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
						<asp:Label ID="lbOptionalPrice2" runat="server" Text="lbOptionalPrice2"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="left" bgcolor="#FFFFFF">
						บริการเคลือบ :
						<asp:Label ID="lbOptionalDesc1" runat="server" Text="lbOptionalDesc1"></asp:Label>
					</td>
					<td colspan="2" align="right" bgcolor="#FFFFFF">
						ค่าบริการ :
					</td>
					<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
						<asp:Label ID="lbOptionalPrice1" runat="server" Text="lbOptionalPrice1"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="left" bgcolor="#FFFFFF">
						การชำระเงิน :
						<asp:Label ID="lbPayMentCode" runat="server" Text="lbPayMentCode"></asp:Label>
					</td>
					<td bgcolor="#ffffff" colspan="2" style="text-align: right; height: 25px;">
						ราคาสุทธิ (ก่อน vat) :
					</td>
					<td align="right" bgcolor="#ffffff" style="width: 100px; height: 25px;">
						<asp:Label ID="lbPriceExcVat" runat="server" Text="lbPriceExcVat"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td colspan="2" align="right" bgcolor="#FFFFFF">
						ภาษีมูลค่าเพิ่ม 7% :
					</td>
					<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
						<asp:Label ID="lbVat" runat="server" Text="lbVat"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td colspan="2" bgcolor="#FFFFFF" align="right">
						ค่าจัดส่ง :
					</td>
					<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
						<asp:Label ID="lbDelivery" runat="server" Text="lbDelivery"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" bgcolor="#ededed" class="style1" colspan="3">
						<b>จำนวนเงินรวมทั้งสิ้น:</b>
					</td>
					<td align="right" bgcolor="#ededed" class="style2">
						<asp:Label ID="lbSum" runat="server" Text="lbSum"></asp:Label>
					</td>
				</tr>
			</table>
		</asp:Panel>
		<div align="right">
			***ราคานี้ยังไม่รวมค่าออกแบบ หรือค่าบริการอื่นๆหากมีจะแจ้งให้ทราบภายหลังในทันที</div>
			<asp:Label ID="lbDetailFTP" runat="server" Text=""></asp:Label>
	</div>
	<div class="box-general">
		<strong>ไฟล์ที่แนบ:</strong>
		<asp:DataList ID="dlUploadFile" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
			GridLines="Both" Width="100%" BackColor="White" BorderColor="#336666" BorderStyle="Double"
			BorderWidth="3px" CellPadding="3">
			<ItemTemplate>
				<table width="100%">
					<tr>
						<td style="width: 110px">
							<asp:Label ID="Label3" runat="server" Text='<%# eval("PreviousName") %>'></asp:Label>
						</td>
						<td colspan="2">
							<asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
						</td>
					</tr>
				</table>
				<asp:HiddenField ID="hidReadyToPrint" runat="server" Value='<%# eval("Ready2Print") %>' />
			</ItemTemplate>
			<HeaderTemplate>
				<table width="100%">
					<tr>
						<td>
							ชื่อไฟล์
						</td>
						<td style="width: 3px">
						</td>
						<td>
							ประเภทไฟล์
						</td>
					</tr>
				</table>
			</HeaderTemplate>
			<FooterStyle BackColor="White" ForeColor="#000066" />
			<SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
			<ItemStyle ForeColor="#666666" />
			<HeaderStyle BackColor="#C9E4C9" Font-Bold="True" ForeColor="DimGray" />
		</asp:DataList>
		<br br class="clear" />
		<%--<strong>บริการเสริมพิเศษ :</strong><asp:Label ID="lbview1" runat="server" Text=""></asp:Label>--%>
		<br br class="clear" />
		<asp:TextBox ID="lbDescription" runat="server" ReadOnly="True" TextMode="MultiLine"
			Height="80px" Width="504px"></asp:TextBox><br />
		***เมื่อจัดพิมพ์สีของงานพิมพ์อาจจะแตกต่างจากนี้
	</div>
	<table width="680" style="font-size: 12px;" font-weight="font-weight:bold;">
		<tr>
			<td valign="top" style="height: 29px; width: 710px;">
				<div align="right" id="divNoprint" class="noprint">
					<asp:HyperLink ID="btnPrint" runat="server" ImageUrl="../images/icon_pri.gif" Style="float: left;"></asp:HyperLink>
					&nbsp;
					<asp:HyperLink ID="btnExit" runat="server" ImageUrl="../images/print_ex.gif" Style="float: right;"
						NavigateUrl="~/Main.aspx"></asp:HyperLink>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>
