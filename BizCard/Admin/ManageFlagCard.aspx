<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPageAdmin.master" AutoEventWireup="false"
	CodeFile="ManageFlagCard.aspx.vb" Inherits="Admin_ManageFlagCard" Title="Admin ManageFlagCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<link href="Css/AdminTool.css" rel="stylesheet" type="text/css" />
	<div id="dvShowCard" style="display: none;">
	</div>
	<%=""%>
	<%If listCard IsNot Nothing AndAlso listCard.Count > 0 Then%>
	<div>
		<table style="width: 70%">
			<tr>
				<%If listCard.Item(0).ToTalPage > 0 Then%>
				<td style="text-align: left">
					˹�ҷ��:<%=PageNo%>/<%=listCard.Item(0).ToTalPage%>
				</td>
				<td style="text-align: Right">
					goto:
					<%	For i As Integer = 1 To listCard.Item(0).ToTalPage%>
					<span><a href=" <%=ResolveUrl("~/Admin/ManageFlagCard.aspx?PageNo=" & i)%>">
						<%=i%></a></span>
					<%	 Next%>
				</td>
				<%Else%>
				<td style="text-align: left">
					˹�ҷ��:1/1
				</td>
				<td style="text-align: Right">
					goto: <span><a href=" <%=ResolveUrl("~/Admin/ManageFlagCard.aspx?PageNo=1")%>">1</a></span>
				</td>
				<%End If%>
			</tr>
		</table>
	</div>
	<div class="datagrid">
		<table id="tbCard">
			<thead>
				<tr style="background-color: #cfe; height: 40px">
					<th align="center" style="width: 100px">
						���ʹ���ѵ�
					</th>
					<th align="center" style="width: 100px">
						���͹���ѵ�
					</th>
					<th align="center" style="width: 100px">
						���� Background
					</th>
					<th align="center" style="width: 100px">
						CardActive
					</th>
					<th align="center" style="width: 100px">
						���ҧ��
					</th>
					<th align="center" style="width: 300px;">
						�ٵ�����ҧ
					</th>
					<th align="center" style="width: 100px">
						��䢢�����
					</th>
					<th align="center" style="width: 100px">
						��駤����� Active
					</th>
					<th>
						Upload File
					</th>
				</tr>
			</thead>
			<tbody>
				<%		Dim Count As Integer = 0
					For Each ObjCard As clsCard In listCard
				%>
				<tr style="background-color: #fff">
					<td align="center">
						<%=ObjCard.CardID%>
					</td>
					<td align="center">
						<%=ObjCard.CardName%>
					</td>
					<td align="center">
						<%=ObjCard.BGImage%>
					</td>
					<td align="center">
						<%=ObjCard.CardActive%>
					</td>
					<td align="center">
						<%=ObjCard.CreateBy%>
					</td>
					<td align="center" height="200px">
						<img alt="Card" style="cursor: pointer;" src="../CardImages/FullCard/<%=ObjCard.templateImage%>"
							id="<%=ObjCard.CardID%>" class="clsCardID" />
					</td>
					<td align="center">
						<input id="btnEdit_<%=ObjCard.CardID%>" type="button" value="Edit" class="clsEdit"
							style="width: 80px" />
					</td>
					<td align="center">
						<input id="btnActive_<%=ObjCard.CardID%>" type="button" value="Active" class="clsActive"
							style="width: 80px" />
					</td>
					<td align="center">
						<input id="btnUploads_<%=ObjCard.CardID%>" type="button" value="Uploads File" class="clsUpload"
							style="width: 100px" />
					</td>
				</tr>
				<%
					Count += 1
				Next%>
			</tbody>
		</table>
	</div>
	<%Else%>
	[No Data]
	<%End If%>
	<script language="javascript" type="text/javascript">

		$("#dvShowCard").dialog({
			title: "This Card",
			bgiframe: true,
			autoOpen: false,
			height: 500,
			width: 500,
			resizable: false,
			modal: true
		});

		$(function () {
			$("#dvShowCard").bind("dialogclose", function () {
				window.location.reload();
			});

			$(".clsCardID").click(function () {
				var CardID = this.id;
				$("#dvShowCard").dialog("open");
				$("#dvShowCard").load('ShowCard.aspx?CardID=' + CardID);
			});

			$(".clsEdit").click(function () {
				var ID = this.id;
				var CardID = "";
				var CardID = ID.split("_");
				window.location = ('EditTemplate.aspx?Type=Manage&CardID=' + CardID[1]);
			});

			$(".clsActive").click(function () {
				var ID = this.id;
				var CardID = "";
				var CardID = ID.split("_");
				var Active = "Y"
				$.ajax({
					type: "POST",
					url: "ajManageFlagCard.aspx",
					data: { "CardID": CardID[1], "Active": Active },
					success: function (msg) {
						alert(msg);
						window.location = "ManageFlagCard.aspx";
					}
				});
			});
			$(".clsUpload").click(function () {
				var ID = this.id;
				var CardID = "";
				var CardID = ID.split("_");
				window.location = ('AdminUploadFile.aspx?Type=Upload&CardID=' + CardID[1]);
			});
		});
        
	</script>
</asp:Content>
