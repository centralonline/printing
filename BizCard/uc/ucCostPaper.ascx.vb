Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.Data
Imports System.Collections.Generic
Partial Class uc_ucCostPaper
    Inherits clsUCBase

    Dim ListPaper As List(Of clsPaper)
    Dim clsListPaper As New clsPaperEngine
#Region "Create Property"
    Private PaperIDValue As String
    Public Property PaperID() As String
        Get
            Return PaperIDValue
        End Get
        Set(ByVal value As String)
            PaperIDValue = value
        End Set
    End Property

    Private TypePaperValue As String
    Public Property TypePaper() As String
        Get
            Return TypePaperValue
        End Get
        Set(ByVal value As String)
            TypePaperValue = value
        End Set
    End Property
    Private PriceValue As Decimal
    Public Property Price() As Decimal
        Get
            Return PriceValue
        End Get
        Set(ByVal value As Decimal)
            PriceValue = value
        End Set
    End Property
    Private OrderValue As Decimal
    Public Property Order() As Decimal
        Get
            Return OrderValue
        End Get
        Set(ByVal value As Decimal)
            OrderValue = value
        End Set
    End Property
    Private DeliveryCostValue As Decimal
    Public Property DeliveryCost() As Decimal
        Get
            Return DeliveryCostValue
        End Get
        Set(ByVal value As Decimal)
            DeliveryCostValue = value
        End Set
    End Property
    Private SumaryValue As Decimal
    Public Property Summary() As Decimal
        Get
            Return SumaryValue
        End Get
        Set(ByVal value As Decimal)
            SumaryValue = value
        End Set
    End Property
    Private NetAmtValue As Decimal
    Public Property NetAmt() As Decimal
        Get
            Return NetAmtValue
        End Get
        Set(ByVal value As Decimal)
            NetAmtValue = value
        End Set
    End Property
    Private VatValue As Decimal
    Public Property Vat() As Decimal
        Get
            Return VatValue
        End Get
        Set(ByVal value As Decimal)
            VatValue = value
        End Set
    End Property
    Private TypeSystemValue As String
    Public Property TypeSystem() As String
        Get
            Return TypeSystemValue
        End Get
        Set(ByVal value As String)
            TypeSystemValue = value
        End Set
    End Property
#End Region
    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        MyBase.OnInit(e)
        If (Page.IsPostBack = False) Then
            ListPaper = clsListPaper.GetListPapersize("Card", True)
            ddlPaper.DataSource = ListPaper
            ddlPaper.DataTextField = "Description"
            ddlPaper.DataValueField = "PaperID"
            ddlPaper.DataBind()
            Me.Session("Price") = ListPaper
            Me.lbPrice.Text = ListPaper.Item(Me.ddlPaper.SelectedIndex).Price.ToString()

            Me.lbDelivery.Text = Me.DeliveryCost
            Dim a As Double = lbPrice.Text
            Dim b As Double = Me.ddlNumber.SelectedItem.ToString()

            If (Me.TypeSystem <> "Upload") Then
                Me.lbSum.Text = a * b
                Me.lbVat.Text = (lbSum.Text * 7) / 100

                Dim c As Double = lbSum.Text
                Dim d As Double = lbVat.Text
                Me.lbSummary.Text = c + d
            Else
                Me.lbSum.Text = "-"
                Me.lbVat.Text = "-"
                Me.lbSummary.Text = "-"
            End If
          
            Me.TypePaper = Me.ddlPaper.SelectedItem.ToString()
            Me.PaperID = Me.ddlPaper.SelectedValue.ToString()
            Me.Price = lbPrice.Text
            Me.DeliveryCost = 0
            Me.Order = Me.ddlNumber.SelectedItem.ToString()

            If (Me.TypeSystem <> "Upload") Then
                Me.Vat = lbVat.Text
                Me.NetAmt = lbSum.Text
                Me.Summary = Me.lbSummary.Text
            Else
                Me.Vat = 0
                Me.NetAmt = 0
                Me.Summary = 0
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If (Not IsPostBack) Then
        Me.TypePaper = Me.ddlPaper.SelectedItem.ToString()
        Me.PaperID = Me.ddlPaper.SelectedValue.ToString()
        Me.Price = lbPrice.Text
        Me.DeliveryCost = 0
        Me.Order = Me.ddlNumber.SelectedItem.ToString()
        If (Me.TypeSystem <> "Upload") Then
            Me.Vat = lbVat.Text
            Me.NetAmt = lbSum.Text
            Me.Summary = Me.lbSummary.Text
        Else
            Me.lbSum.Text = "-"
            Me.lbVat.Text = "-"
            Me.lbSummary.Text = "-"

            Me.Vat = 0
            Me.NetAmt = 0
            Me.Summary = 0
        End If

        ' End If
    End Sub

    Protected Sub ddlPaper_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPaper.SelectedIndexChanged
        Dim index As Integer = Me.ddlPaper.SelectedIndex

        Dim ListPaper1 As New List(Of clsPaper)
        ListPaper1 = Me.objSessionVar.Paper
        lbPrice.Text = ListPaper1.Item(index).Price.ToString()

        Me.lbDelivery.Text = Me.DeliveryCost
        Dim a As Double = lbPrice.Text
        Dim b As Double = Me.ddlNumber.SelectedItem.ToString()

        If (Me.TypeSystem <> "Upload") Then
            Me.lbSum.Text = a * b
            Me.lbVat.Text = (lbSum.Text * 7) / 100

            Dim c As Double = lbSum.Text
            Dim d As Double = lbVat.Text
            Me.lbSummary.Text = c + d
        Else
            Me.lbSum.Text = "-"
            Me.lbVat.Text = "-"
            Me.lbSummary.Text = "-"

        End If
       

        Me.TypePaper = Me.ddlPaper.SelectedItem.ToString()
        Me.PaperID = Me.ddlPaper.SelectedValue.ToString()
        Me.Price = lbPrice.Text
        Me.DeliveryCost = 0
        Me.Order = Me.ddlNumber.SelectedItem.ToString()
        If (Me.TypeSystem <> "Upload") Then
            Me.Vat = lbVat.Text
            Me.NetAmt = lbSum.Text
            Me.Summary = Me.lbSummary.Text
        Else
            Me.Vat = 0
            Me.NetAmt = 0
            Me.Summary = 0
        End If
       


    End Sub

    Protected Sub ddlNumber_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim a As Double = lbPrice.Text
        Dim b As Double = Me.ddlNumber.SelectedItem.ToString()

        If (Me.TypeSystem <> "Upload") Then
            Me.lbSum.Text = a * b
            Me.lbVat.Text = (lbSum.Text * 7) / 100

            Dim c As Double = lbSum.Text
            Dim d As Double = lbVat.Text
            Me.lbSummary.Text = c + d
        Else
            Me.lbSum.Text = "-"
            Me.lbVat.Text = "-"
            Me.lbSummary.Text = "-"
        End If
       


        Me.TypePaper = Me.ddlPaper.SelectedItem.ToString()
        Me.PaperID = Me.ddlPaper.SelectedValue.ToString()
        Me.Price = lbPrice.Text
        Me.DeliveryCost = 0
        Me.Order = Me.ddlNumber.SelectedItem.ToString()
        If (Me.TypeSystem <> "Upload") Then
            Me.Vat = lbVat.Text
            Me.NetAmt = lbSum.Text
            Me.Summary = Me.lbSummary.Text
        Else
            Me.Vat = 0
            Me.NetAmt = 0
            Me.Summary = 0
        End If
        


    End Sub
End Class
