﻿Imports Microsoft.VisualBasic

Public Class ModelInvoice
	'แพร์เพิ่ม NewUser
	Private InvAddr1Value As String
	Public Property InvAddr1() As String
		Get
			Return InvAddr1Value
		End Get
		Set(ByVal value As String)
			InvAddr1Value = value
		End Set
	End Property

	'แพร์เพิ่ม เก็บที่อยู่สำหรับออกใบกำกับภาษี
	Private InvAddr2Value As String
	Public Property InvAddr2() As String
		Get
			Return InvAddr2Value
		End Get
		Set(ByVal value As String)
			InvAddr2Value = value
		End Set
	End Property

	'แพร์เพิ่ม เก็บที่อยู่สำหรับออกใบกำกับภาษี
	Private InvAddr3Value As String
	Public Property InvAddr3() As String
		Get
			Return InvAddr3Value
		End Get
		Set(ByVal value As String)
			InvAddr3Value = value
		End Set
	End Property

	'แพร์เพิ่ม เก็บที่อยู่สำหรับออกใบกำกับภาษี
	Private InvAddr4Value As String
	Public Property InvAddr4() As String
		Get
			Return InvAddr4Value
		End Get
		Set(ByVal value As String)
			InvAddr4Value = value
		End Set
	End Property

End Class

