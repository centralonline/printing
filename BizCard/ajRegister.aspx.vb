﻿Imports CoreBaseII
Imports UserEngineTrendyPrint

Partial Class ajRegister
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim UserID As String = Request("UserID")
        Dim Password As String = Request("Password")
        '------ ทำ Object เพื่อใช้ในการ Insert
        Dim Name As String = Request("Name")
        Dim NickName As String = Request("NickName")
        Dim Mobile As String = Request("Mobile")
        Dim CheckNews As String = Request("CheckNews")

        ''------Insert
        Dim ObjectUser As New ModelRegisterData

        With ObjectUser
            .FullName = Name
            .Email = UserID
            .DisplayName = NickName
            .Password = Password
            .MobilePhone = Mobile
            .IsSubscribeNewsletter = CheckNews
        End With

        Dim Engine As New UserEngine
        Dim Message As String = ""
        Message = Engine.RegisterUser(ObjectUser)
        If Message = "True" Then
            Engine.Login(UserID, Password)
        End If

        Response.Write(Message)

    End Sub
End Class
