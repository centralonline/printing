﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucDigitalSelectPaper.ascx.vb"
    Inherits="uc_ucDigitalSelectPaper" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="ucHeadPanel1.ascx" TagName="ucHeadPanel1" TagPrefix="uc1" %>
<style type="text/css">
    .style12
    {
        width: 225px;
        height: 23px;
    }
    .style14
    {
        width: 225px;
        height: 22px;
    }
    .style15
    {
        height: 22px;
    }
    </style>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
    <contenttemplate>
	<table width="100%">
	<tr>
		<td style="height: 21px; text-align: left" valign="top">
			<p class="head-column">
				รายละเอียด</p>
			<table width="100%">
            <tr>
            <td style="width: 225px">
            ประเภทงาน :
            </td>
            <td>
            
                <asp:DropDownList ID="ddlsizetext" runat="server"  AutoPostBack="true" 
                    AppendDataBoundItems="True" Height="19px" Width="180px" />
            </td>
            </tr>
				<tr>
					<td style="width: 225px; height: 22px;">
						ชื่อกระดาษ :
					</td>
					<td style="height: 22px">
						<asp:DropDownList ID="ddlnametext" runat="server" 
                    AppendDataBoundItems="True" Height="23px" Width="250px" AutoPostBack="True" >
					        <asp:ListItem>--SELECT--</asp:ListItem>
                        </asp:DropDownList>
					</td>
				</tr>
                <tr>
					<td style="width: 225px">
						รหัสกระดาษ :
					</td>
					<td>
						<asp:Label ID="lbPaperID" runat="server" Text="Label"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 22px;">
						ราคาต่อหน่วย :
					</td>
					<td style="height: 22px">
						<asp:Label ID="lbPrice" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px;">
						ราคาโปรโมชั่น :
					</td>
					<td>
						<asp:Label ID="lbPromotion" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						<asp:Label ID="Label1" runat="server" Text="บาท" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td class="style12">
						เลือกบริการเคลือบ :</td>
					<td>
						<asp:DropDownList ID="ddloption" runat="server" Width="155px" Height="27px" 
                            AutoPostBack="True">
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td style="width: 225px;">
						ราคาเคลือบต่อหน่วย :
					</td>
					<td>
						<asp:Label ID="lbQty" runat="server" CssClass="hilight" Text="Label"></asp:Label>
                        บาท</td>
				</tr>
				<tr>
					
                    <td style="height: 24px">
                        จำนวนที่สั่งทำ :</td>
                        <td>
                            <asp:TextBox ID="txtQuantity" runat="server" Width="51px" AutoPostBack="True"></asp:TextBox>
                            ใบ<span style="color: #ff0033">*</span></td>
					<%--<td style="height: 24px">
						<asp:DropDownList ID="ddlNumber" runat="server" AutoPostBack="True" CssClass="textdd"
							OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged">
							<asp:ListItem>100</asp:ListItem>
							<asp:ListItem>200</asp:ListItem>
							<asp:ListItem>300</asp:ListItem>
							<asp:ListItem>400</asp:ListItem>
							<asp:ListItem>500</asp:ListItem>
							<asp:ListItem>600</asp:ListItem>
							<asp:ListItem>700</asp:ListItem>
							<asp:ListItem>800</asp:ListItem>
							<asp:ListItem>900</asp:ListItem>
							<asp:ListItem>1000</asp:ListItem>
						</asp:DropDownList>
						ใบ <span style="color: #ff0033">** สั่งทำขึ้นตํ่า 100 ใบ</span>
					</td>--%>
				</tr>
				<tr>
					<td style="width: 225px; height: 22px">
						รวมมูลค่าสินค้า(ก่อน vat) :
					</td>
					<td style="height: 22px">
						<asp:Label ID="lbVat" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td class="style14">
						ภาษีมูลค่าเพิ่ม 7% :
					</td>
					<td class="style15">
						<asp:Label ID="lbVat_card" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px">
						ค่าจัดส่ง :</td>
					<td>
						<asp:Label ID="lbDelivery" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<%--<tr id="tr_sum" runat="server" visible="false">
					<td style="width: 225px">
						รวมมูลค่าสินค้า
					</td>
					<td>
						<asp:Label ID="lbSum" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>--%>
				<tr>
					<td style="width: 225px">
						<strong>จำนวนเงินรวมทั้งสิ้น </strong>
					</td>
					<td>
						<asp:Label ID="lbSummary" runat="server" CssClass="hilight"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 21px;">
					</td>
					<td style="height: 21px">
					</td>
				</tr>
				<tr>
					<td colspan="2" style="height: 21px;">
						<b>*หมายเหตุ:</b><br />
						ยอดการสั่งซื้อ 499 บาทขึ้นไปบริการส่งฟรี<br />
						กรณียอดสั่งซื้อต่ำกว่า 499 บาท คิดค่าบริการจัดส่ง 50 บาทต่อใบสั่งซื้อ
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
   </contenttemplate>
</asp:UpdatePanel>
<asp:HiddenField ID="hidSum" runat="server" />
<asp:HiddenField ID="hidVat" runat="server" />
<asp:HiddenField ID="hidSummary" runat="server" />
