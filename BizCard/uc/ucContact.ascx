﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucContact.ascx.vb" Inherits="uc_ucContact" %>
<p class="head-column">
    ผู้ติดต่อสั่งสินค้า</p>
<table>
    <tr>
        <td style="width:150px;">
            ชื่อผู้ติดต่อ:
        </td>
        <td>
            <asp:Label ID="lbContactName" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            เบอร์มือถือ:
        </td>
        <td>
            <asp:Label ID="lbContactMobileNo" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            เบอร์โทรศัพท์:
        </td>
        <td>
            <asp:Label ID="lbContactPhoneNo" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            เบอร์แฟ็กซ์:
        </td>
        <td>
            <asp:Label ID="lbContactFaxNo" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            อีเมล์:
        </td>
        <td>
            <asp:Label ID="lbContactEmail" runat="server"></asp:Label>
        </td>
    </tr>
</table>
