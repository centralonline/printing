﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucTop.ascx.vb" Inherits="uc_ucTop" %>



<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
	Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Panel ID="Panel3" runat="server" Visible="False" Width="100%">
	<div class="alert">
		คุณยังไม่ได้ทำการ Login ค่ะ
	</div>
</asp:Panel>

<!--Banner Top Header-->
<%--<div style="background-color:#581451; height:100px; text-align: center;">
<img src="/images/banner/2APR2014-H.jpg" alt="ขอพระองค์ทรงพระเจริญ" width="1000"height="100" border="0" /></div>--%>

<%--<div style="" align="center">
		<iframe src="http://www.officemate.co.th/Activity/slide-ofm1.html" width="930"
			height="30" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
		</iframe>
	</div>
--%>

<%--<div style="background-color:#cd9100; height:100px; text-align: center;">
		<img src="/images/banner/28APR2014-head.jpg" alt="ถวายพระพรวันราชาภิเษก" width="1000"
			height="100" border="0" /></div>
--%>
<div id="logo">
	<a href='<%=ResolveUrl("~/main.aspx")%>'>
		<img src="images/logo-trendyprint.png" class="png" style="width: 250px; height: 51px;" /></a></div>
<div id="login">
	<asp:Panel ID="Panel1" runat="server" Height="24px" Visible="False" Width="100%"
		CssClass="Tlogin">
		<span class="Tlogin">user:</span>
		<asp:Label ID="lbUserName" runat="server" Text="Label" CssClass="Tlogin"></asp:Label>
		, <a href='<%=ResolveUrl("~/MyAccount.aspx")%>'><span style="font-size: 11px; color: #888;">
			My Account</span></a>
		<asp:ImageButton ID="btnLogout" ImageUrl="~/images/btn/logout.gif" runat="server"
			ImageAlign="AbsMiddle" />
	</asp:Panel>
	<asp:Panel ID="Panel2" runat="server" Height="24px" Visible="False" Width="100%"
		CssClass="Tlogin">
		<p style="padding-right: 20px; vertical-align: middle;">
			<img src="images/t-login.png" style="padding-right: 10px; vertical-align: middle;"
				class="png" width="43" height="17">
			<asp:TextBox ID="txtUserName" runat="server" CssClass="Tloginborder" value="enter your email"
				onclick="if (this.value == 'enter your email') { this.value=''; } " onBlur="if (this.value == '') { this.value='enter your email'; } "></asp:TextBox>
			password
			<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="Tloginborder2"></asp:TextBox>
			<asp:ImageButton ID="btnLogin" ImageUrl="~/images/btn/login.gif" runat="server" ImageAlign="AbsMiddle" />
			&nbsp;<a href='<%=ResolveUrl("~/Forgotpassword.aspx")%>'><span style="font-size: 11px;
				color: #888;">forgot?</span></a> <span style="padding: 0px 10px;">or</span>
			<a href="<%=ResolveUrl("~/Register.aspx")%>">
				<img src="images/t-register.png" style="padding-right: 10px; vertical-align: middle;"
					width="62" height="17" class="png"></a>
		</p>
	</asp:Panel>
</div>
<div id="menu-top">
	<img src="images/top-menu.png" style="width: 980px; height: 40px;" border="0" usemap="#topmenu"
		class="png">
	<map name="topmenu">
    <area shape="rect" coords="3, 1, 113, 40" href="Main.aspx" target="_self" alt="หน้าแรก">
		<area shape="rect" coords="112, 3, 254, 36" href="DesignCard.aspx" target="_self" alt="นามบัตร">
		<area shape="rect" coords="254, 4, 409, 35" href="DigitalAttachFiles.aspx" target="_self" alt="ดิจิตอลโปรการ์ด">
		<area shape="rect" coords="408, 2, 500, 35" href="InkjetAttachFiles.aspx" target="_self" alt="Inkjet">
        <%--<area shape="rect" coords="408, 2, 500, 35" href="ContactUs.aspx" target="_self" alt="Inkjet">--%>
		<area shape="rect" coords="501, 2, 614, 36" href="Portfolio.aspx" target="_self" alt="ตัวอย่างงานพิมพ์">
		<%--<area shape="rect" coords="614, 2, 701, 36" href="PriceList.aspx" target="_self" alt="ราคางานพิมพ์">--%>
        <area shape="rect" coords="614, 2, 701, 36" href="PriceList.aspx" target="_self" alt="ราคางานพิมพ์">
		<area shape="rect" coords="700, 2, 830, 36" href="Promotion.aspx" target="_self" alt="โปรโมชันงานพิมพ์">
		<area shape="rect" coords="830, 2, 979, 36" href="ContactUs.aspx" target="_self" alt="ติดต่อ trendyprint.net">

		<%--<area shape="rect" coords="3,1,95,40" href="Main.aspx" target="_self" alt="หน้าแรก">
		<area shape="rect" coords="96,3,210,36" href="DesignCard.aspx" target="_self" alt="นามบัตร">
		<area shape="rect" coords="311,4,425,35" href="DigitalAttachFiles.aspx" target="_self" alt="ดิจิตอลโปรการ์ด">
		<area shape="rect" coords="426,2,515,35" href="InkjetAttachFiles.aspx" target="_self" alt="Inkjet">
		<area shape="rect" coords="516,2,655,36" href="Portfolio.aspx" target="_self" alt="ตัวอย่างงานพิมพ์">
		<area shape="rect" coords="656,2,735,36" href="PriceList.aspx" target="_self" alt="ราคางานพิมพ์">
		<area shape="rect" coords="736,2,855,36" href="Promotion.aspx" target="_self" alt="โปรโมชันงานพิมพ์">
		<area shape="rect" coords="856,2,965,36" href="ContactUs.aspx" target="_self" alt="ติดต่อ trendyprint.net">--%>

	</map>
</div>
