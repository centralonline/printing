Imports System.Collections.Generic
Imports System.Data
Imports UserEngineTrendyPrint

Partial Class uc_ucConfirmOrderPreview
	Inherits clsUCBase

	Private OrderIDValue As String
	Public Property OrderID() As String
		Get
			Return OrderIDValue
		End Get
		Set(ByVal value As String)
			OrderIDValue = value
		End Set
	End Property
	Private CardIDValue As String
	Public Property CardID() As String
		Get
			Return CardIDValue
		End Get
		Set(ByVal value As String)
			CardIDValue = value
		End Set
	End Property
	Private CardSizeValue As String
	Public Property CardSize() As String
		Get
			Return CardSizeValue
		End Get
		Set(ByVal value As String)
			CardSizeValue = value
		End Set
	End Property
	Private LogoUrlValue As String
	Public Property LogoUrl() As String
		Get
			Return LogoUrlValue
		End Get
		Set(ByVal value As String)
			LogoUrlValue = value
		End Set
	End Property
	Private UserIDValue As String
	Public Property UserID() As String
		Get
			Return UserIDValue
		End Get
		Set(ByVal value As String)
			UserIDValue = value
		End Set
	End Property

	Private ImagePathValue As String
	Public Property ImagePath() As String
		Get
			ImagePathValue = Me.objAppVar.ImagePath
			Return ImagePathValue
		End Get
		Set(ByVal value As String)
			ImagePathValue = value
		End Set
	End Property

	Private serviceValuse As String
	Public Property serviceAdd() As String
		Get
			Return serviceValuse
		End Get
		Set(ByVal value As String)
			serviceValuse = value
		End Set
	End Property


	Protected Overrides Sub OnInit(ByVal e As EventArgs)
		MyBase.OnInit(e)
	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If (Not IsPostBack) Then

			If Request.QueryString("Order") = "" Then
				Me.OrderID = ""
			Else
				Me.OrderID = Request.QueryString("Order")
			End If

			If Request.QueryString("User") = "" Then
				Me.UserID = "printing@officemate.co.th"
			Else
				Me.UserID = Request.QueryString("User")
			End If

			Dim clsOrder As New clsOrderEngine
			Dim listOrder As New List(Of clsOrder)
			listOrder = clsOrder.GetInfoOrders(Me.OrderID)

			Dim Engine As New UserEngine
			Dim UserInfo As New ModelUser
			UserInfo = Engine.GetUserPreview(Me.UserID)

			Dim clsCardEn As New clsCardEngine
			Dim clsCards As New clsCard
			Dim listclsCard As New List(Of clsCardTextField)
			Dim listclsCardImg As New List(Of clsCardImageField)
			clsCards = clsCardEn.GetCardByID(listOrder.Item(0).CardID)
			listclsCard = clsCardEn.GetListCardTextFields(listOrder.Item(0).CardID)
			listclsCardImg = clsCards.ImageFields

			If clsCards Is Nothing Then
				Exit Sub
			End If

			Me.lbCustID.Text = UserInfo.CustID
			Me.lbCustName.Text = UserInfo.UserName
			Me.lbMobile.Text = UserInfo.Mobile
			Me.lbContactName.Text = UserInfo.Contact.ContactName
			Me.lbContactEmail.Text = UserInfo.Contact.ContactEmail.ContactEmail
			Me.lbContactMobileNo.Text = UserInfo.Contact.ContactMobileNo
			Me.lbContactPhoneNo.Text = UserInfo.Contact.ContactPhoneNo
			Me.lbContactFaxNo.Text = UserInfo.Contact.ContactFaxNo

			Me.lbOrderID.Text = listOrder.Item(0).OrderID
			Me.lbDateOrder.Text = listOrder.Item(0).OrderDate.ToString("dd/MM/yyyy HH:mm:ss")
			Me.lbAddress1.Text = listOrder.Item(0).ShippingAddr1
			Me.lbAddress2.Text = listOrder.Item(0).ShippingAddr2
			Me.lbAddress3.Text = listOrder.Item(0).ShippingAddr3
			Me.lbAddress4.Text = listOrder.Item(0).ShippingAddr4
			Me.lbPaperID.Text = listOrder.Item(0).PaperID
			''GIFT Add 26/5/2016
			'GIFT ������¡���ʴ���������´
			'��ԡ�õѴ���
			lbOptionalPrice2.Text = listOrder.Item(0).OptionalPrice2
			lbOptionalDesc2.Text = listOrder.Item(0).OptionalDesc2
			'��ԡ�����ͺ
			lbOptionalPrice1.Text = listOrder.Item(0).OptionalPrice1
			lbOptionalDesc1.Text = listOrder.Item(0).OptionalDesc1
			Me.lbTotal.Text = Format(Convert.ToDecimal(listOrder.Item(0).TotalAmt), "#,##0.00")
			''End Add

			Me.dtTextFields.DataSource = listclsCard
			Me.dtTextFields.DataBind()

			Dim strPaymentDesc As String = ""
			Select Case listOrder.Item(0).PaymentType
				Case "Transfer"
					strPaymentDesc = "�����¡���͹�Թ ��Һѭ�ո�Ҥ��"
				Case "Cash on Delivery"
					strPaymentDesc = "���Թ���·ҧ�¾�ѡ�ҹ�Ϳ������"
				Case "Credit Card EDC"
					strPaymentDesc = "�����Թ���ºѵ��ôԵ�Ѻ��ѡ�ҹ�Ϳ������"
			End Select

			Me.lbCardID.Text = listOrder.Item(0).CardID
			Me.lbPaperName.Text = listOrder.Item(0).PaperName
			Me.lbSize.Text = clsCards.Height & "X" & clsCards.Width
			Me.lbPayMentCode.Text = strPaymentDesc

			If listOrder.Item(0).Promotion <> "0.00" Then
				Me.lbPrice.Text = listOrder.Item(0).Promotion
			Else
				Me.lbPrice.Text = listOrder.Item(0).PaperPrice
			End If

			'If Total < 500 Then
			'	lbDelivery.Text = "50.00"
			'	Me.lbSum.Text = listOrder.Item(0).TotAmt
			'Else
			'	lbDelivery.Text = "0.00"
			'	Me.lbSum.Text = listOrder.Item(0).TotAmt
			'End If

			lbDelivery.Text = listOrder.Item(0).DeliveryAmt
			Me.lbSum.Text = Format(Convert.ToDecimal(listOrder.Item(0).TotAmt), "#,##0.00")
			Me.lbNumOrder.Text = listOrder.Item(0).Qty
			Me.lbPriceExcVat.Text = Format(Convert.ToDecimal(listOrder.Item(0).NetAmt), "#,##0.00")
			Me.lbVat.Text = listOrder.Item(0).VatAmt
			Me.lbDescription.Text = listOrder.Item(0).Description
			Me.ImageShow.ImageUrl = "../CardImages/FullSizes/" & listOrder.Item(0).CardID & ".jpg"

			lbLinkRO.Text = "<a href=""" & ImagePath & "/ConfirmOrderPreview.aspx?Order=" & lbOrderID.Text & "&User=" & Me.UserID & """>"
			lbLinkRO.Text += ImagePath & "/ConfirmOrderPreview.aspx?Order=" & lbOrderID.Text & "&User=" & Me.UserID & " </a>"
			If listclsCardImg.Count > 0 Then
				lbLinkLogo.Text = "<a href=""" & ImagePath & "/CardImages/UploadImage/Currents/" & listclsCardImg.Item(0).imageUrl & """>"
				lbLinkLogo.Text += ImagePath & "/CardImages/UploadImage/Currents/" & listclsCardImg.Item(0).imageUrl & "</a>"
			End If
		End If
		Dim sqlStr As String = ""
		SqlStr += "<strong>��ԡ������������: </strong>" & Me.lbview1.Text & "</td></tr></table>" & vbNewLine
	End Sub

End Class
