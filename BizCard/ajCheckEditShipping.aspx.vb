﻿Imports CoreBaseII
Imports UserEngineTrendyPrint
Imports OfficeMate.Framework
Imports OfficeMate.Framework.Data.Repositories
Imports System.Data
Imports OfficeMate.Framework.Data

Partial Class ajCheckEditShipping
	Inherits clsHTTPBase

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		'ข้อมูลContact
		Dim ContactName As String = Request("ContactName")
		Dim ContactMobileNo As String = Request("ContactMobileNo")
		Dim ContactPhoneNo As String = Request("ContactPhoneNo")
		Dim ContactFaxNo As String = Request("ContactFaxNo")
		'ข้อมูลInvoice
		Dim InvAddr1 As String = Request("InvAddr1")

		Dim InvAddr2 As String = Request("InvAddr2")
		Dim InvAddr3 As String = Request("InvAddr3")
		Dim InvAddr4 As String = Request("InvAddr4")
		'ข้อมูลShipping
		Dim ShipPhoneNo As String = Request("ShipPhoneNo")
		Dim ShipAddr1 As String = Request("ShipAddr1")
		Dim ShipAddr2 As String = Request("ShipAddr2")
		Dim ShipAddr3 As String = Request("ShipAddr3")
		Dim ShipProvince As String = Request("ShipProvince")
		Dim ShipZipCode As String = Request("ShipZipCode")
		Dim ShipRemark As String = Request("ShipRemark")

		Dim ObjectContect As New ModelContact
		Dim ObjectInvoice As New ModelInvoice
		Dim ObjectShipping As New ModelShipping

		With ObjectContect
			.ContactID = objSessionVar.ModelUser.Contact.ContactID
			.ContactName = ContactName
			.ContactMobileNo = ContactMobileNo
			.ContactPhoneNo = ContactPhoneNo
			.ContactFaxNo = ContactFaxNo
			.ContactEmail = New ModelContactEmail
			.ContactEmail.ContactEmail = objSessionVar.ModelUser.Contact.ContactEmail.ContactEmail
		End With

		With ObjectInvoice
			.InvAddr1 = InvAddr1
			.InvAddr2 = InvAddr2
			.InvAddr3 = InvAddr3
			.InvAddr4 = InvAddr4
		End With

		With ObjectShipping
			.ShipID = objSessionVar.ModelUser.Shipping.Item(0).ShipID
			.ShipContactor = objSessionVar.ModelUser.Shipping.Item(0).ShipContactor
			.ShipAddr1 = ShipAddr1
			.ShipAddr2 = ShipAddr2
			.ShipAddr3 = ShipAddr3
			.ShipAddr4 = ShipProvince + " " + ShipZipCode
			.ShipPhoneNo = ShipPhoneNo
			.ShipProvince = ShipProvince
			.ShipZipCode = ShipZipCode
			.ShipRemark = ShipRemark
		End With

		Dim Engine As New UserEngine
		Dim Check As Boolean




		Using connection As New SqlClient.SqlConnection(Me.objAppVar.DBConfigMas.ConnectionString)
			Dim selectContact As Contact
			Dim selectShipping As CustomerShipping
			Dim repoShipping As New ShippingRepository(connection)
			Dim repoContact As New ContactRepository(connection)

			If String.IsNullOrEmpty(objSessionVar.ModelUser.Contact.ContactID) Then
				selectContact = repoContact.SelectContact(objSessionVar.ModelUser.Contact.ContactID)
			Else
				selectContact = repoContact.SelectContactIsDefault(objSessionVar.ModelUser.CustID)
			End If

			If String.IsNullOrEmpty(objSessionVar.ModelUser.Shipping.Item(0).ShipID) Then
				selectShipping = repoShipping.SelectShipping(objSessionVar.ModelUser.Shipping.Item(0).ShipID)
			Else
				selectShipping = repoShipping.SelectShippingIsDefault(objSessionVar.ModelUser.CustID)
			End If



			Dim repoPhone As New PhoneNumberRepository(connection)
			Dim phone As New OfficeMate.Framework.Data.PhoneNumber
			Dim phoneContact As New PhoneNumber
			Dim mobileContact As New PhoneNumber
			Dim faxContact As New PhoneNumber

			Dim mobileShipping As New PhoneNumber
			Dim phoneShipping As New PhoneNumber
			Dim faxShipping As New PhoneNumber

			PhoneNumberHelper.SelectDefaultOrFirstPhoneNumber(selectContact.Phones, phoneContact, mobileContact, faxContact)
			PhoneNumberHelper.SelectDefaultOrFirstPhoneNumber(selectShipping.Phones, phoneShipping, mobileShipping, faxShipping)


			If Not String.IsNullOrEmpty(ContactPhoneNo) Then
				'ยังไม่มี ลบเบอร์เดิม insert + set default
				If phoneContact IsNot Nothing Then
					phoneContact.PhoneNo = ContactPhoneNo
					repoPhone.UpdatePhoneNumber(phoneContact, "biz")
				Else
					phone.PhoneNumberType = PhoneNumber.PhoneType.PhoneOut
					phone.PhoneNo = ContactPhoneNo
					phone.Extension = String.Empty
					phone.CustomerId = objSessionVar.ModelUser.CustID
					phone.SequenceId = objSessionVar.ModelUser.Contact.ContactID
					phone.TypeDataSource = PhoneNumber.PhoneDataSourceType.Contact
					repoPhone.InsertPhoneNumber(phone, "biz")
				End If
			End If

			If Not String.IsNullOrEmpty(ContactFaxNo) Then
				If faxContact IsNot Nothing Then
					faxContact.PhoneNo = ContactFaxNo
					repoPhone.UpdatePhoneNumber(faxContact, "biz")
				Else
					phone.PhoneNumberType = PhoneNumber.PhoneType.FaxOut
					phone.PhoneNo = ContactFaxNo
					phone.Extension = String.Empty
					phone.CustomerId = objSessionVar.ModelUser.CustID
					phone.SequenceId = objSessionVar.ModelUser.Contact.ContactID
					phone.TypeDataSource = PhoneNumber.PhoneDataSourceType.Contact
					repoPhone.InsertPhoneNumber(phone, "biz")
				End If
			End If

			If Not String.IsNullOrEmpty(ContactMobileNo) Then
				If mobileContact IsNot Nothing Then
					mobileContact.PhoneNo = ContactMobileNo
					repoPhone.UpdatePhoneNumber(mobileContact, "biz")
				Else
					phone.PhoneNumberType = PhoneNumber.PhoneType.Mobile
					phone.PhoneNo = ContactMobileNo
					phone.Extension = String.Empty
					phone.CustomerId = objSessionVar.ModelUser.CustID
					phone.SequenceId = objSessionVar.ModelUser.Contact.ContactID
					phone.TypeDataSource = PhoneNumber.PhoneDataSourceType.Contact
					repoPhone.InsertPhoneNumber(phone, "biz")
				End If
			End If


			If Not String.IsNullOrEmpty(ShipPhoneNo) Then
				If phoneShipping IsNot Nothing Then
					phoneShipping.PhoneNo = ShipPhoneNo
					repoPhone.UpdatePhoneNumber(phoneShipping, "biz")
				Else
					phone.PhoneNumberType = PhoneNumber.PhoneType.PhoneOut
					phone.PhoneNo = ShipPhoneNo
					phone.Extension = String.Empty
					phone.CustomerId = objSessionVar.ModelUser.CustID
					phone.SequenceId = objSessionVar.ModelUser.Contact.ContactID
					phone.TypeDataSource = PhoneNumber.PhoneDataSourceType.Shipping
					repoPhone.InsertPhoneNumber(phone, "biz")
				End If
			End If


		End Using


		Check = Engine.EditShipping(ObjectContect, ObjectInvoice, ObjectShipping)

		If Check Then
			Response.Write("บันทึกข้อมูลเรียบร้อยแล้วค่ะ")
		Else
			Response.Write("Error!!! กรุณาลองใหม่อีกครั้งค่ะ")
		End If
	End Sub

End Class
