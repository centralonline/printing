﻿
Partial Class Admin_CreateCardTemplate
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Dim CardID As String = Request("CardID")
		Dim Mode As String = Request("Mode")
		Dim Seq As String = Request("Seq")

		Dim Result As Boolean
		Dim Engine As New clsCardEngine

		If (Mode = "del") Then
			Result = Engine.DeleteRowTextFileds(CardID, Seq)
		Else
			Result = Engine.AddNewRowTextFileds(CardID)
		End If


		Response.Write(Result.ToString)
	End Sub
End Class
