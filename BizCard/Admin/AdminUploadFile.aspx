﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPageAdmin.master" AutoEventWireup="false"
	CodeFile="AdminUploadFile.aspx.vb" Inherits="Admin_AdminUploadFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<link href="Css/AdminTool.css" rel="stylesheet" type="text/css" />
	<style>
		.btn-face2
		{
			background-color: #FFF;
			padding: 4px 10px;
			font-size: 13px;
			color: #333;
			font-weight: bold;
			cursor: pointer;
			vertical-align: middle;
			text-decoration: none;
			border: 1px solid #999;
		}
		.btn-face
		{
			background-color: #0054A6;
			padding: 4px 10px;
			font-size: 13px;
			color: #FFF;
			cursor: pointer;
			vertical-align: middle;
			text-decoration: none;
			border: 1px solid #003286;
			font-weight: bold;
		}
		.Img
		{
			border-color: Silver;
			border-width: 1px;
			border-style: Solid;
			padding-left: 40px;
		}
		.des
		{
			font-size: 12px;
			color: #333;
			font-weight: bold;
		}
		#btn-card
		{
			padding: 20px 0px;
			border-top: 1px solid #DDD;
			border-bottom: 1px solid #DDD;
			text-decoration: none;
			margin: 10px 0px;
		}
	</style>
	<div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<h2 class="head-column">
			Uploads TemplateImage</h2>
		<div>
			<div>
				<div class="btn-face2" style="padding-top: 15px;">
					BackgroudImage :
					<asp:FileUpload ID="BGImage" runat="server" Width="320px" CssClass="Upload" BackColor="silver" />
					<asp:Button ID="btnBGImage" runat="server" Text="Attach File" class="btn-face" />
					<asp:Button ID="btnDelBG" runat="server" Text="Delete File" class="btn-face" />
					<h5 style="color: Red; margin: auto; font-weight: initial;">
						***การตั้งชื่อ BackgroudImage : Category-Year-Day_B</h5>
				</div>
				<br />
				<div>
					<asp:Image ID="Background" runat="server" CssClass="Img"></asp:Image><br />
				</div>
				<div style="padding-left: 70px">
					<asp:Label ID="txtNameBG" CssClass="des" runat="server" Text="Name :"></asp:Label>
					<asp:Label ID="txtBGImage" CssClass="des" runat="server" Text=""></asp:Label>
				</div>
				<br />
				<div class="btn-face2" style="padding-top: 15px;">
					FullCardImage :
					<asp:FileUpload ID="TemplateImage" runat="server" Width="320px" CssClass="Upload"
						BackColor="silver" />
					<asp:Button ID="btnTemplate" runat="server" Text="Attach File" class="btn-face" />
					<asp:Button ID="btnDelTemp" runat="server" Text="Delete File" class="btn-face" />
					<h5 style="color: Red; margin: auto; font-weight: initial;">
						***การตั้งชื่อ FullCardImage : Category-Year-Day_P</h5>
				</div>
				<br />
				<div>
					<asp:Image ID="TempImage" runat="server" CssClass="Img"></asp:Image><br />
				</div>
				<div style="padding-left: 70px">
					<asp:Label ID="txtNameTemp" CssClass="des" runat="server" Text="Name:"></asp:Label>
					<asp:Label ID="txtTempImage" CssClass="des" runat="server" Text=""></asp:Label>
				</div>
			</div>
		</div>
		<br />
		<div id="btn-card" align="left">
			<asp:LinkButton ID="LinkBack" runat="server" class="btn-face2" BackColor="#E0DDDD"><< Back To Edit Active Template"</asp:LinkButton></div>
	</div>
</asp:Content>
