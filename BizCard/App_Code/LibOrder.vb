Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports CoreBaseII
Imports System.Data

Public Class clsOrder
	Inherits clsUCBase
#Region "Create Property Fields"

	Private OrderIDValue As String
	Public Property OrderID() As String
		Get
			Return OrderIDValue
		End Get
		Set(ByVal value As String)
			OrderIDValue = value
		End Set
	End Property

	Private CustIDValue As String
	Public Property CustID() As String
		Get
			Return CustIDValue
		End Get
		Set(ByVal value As String)
			CustIDValue = value
		End Set
	End Property

	Private OrderDateValue As DateTime
	Public Property OrderDate() As DateTime
		Get
			Return OrderDateValue
		End Get
		Set(ByVal value As DateTime)
			OrderDateValue = value
		End Set
	End Property

	Private TypeValue As String
	Public Property Type() As String
		Get
			Return TypeValue
		End Get
		Set(ByVal value As String)
			TypeValue = value
		End Set
	End Property

	Private QtyValue As String
	Public Property Qty() As String
		Get
			Return QtyValue
		End Get
		Set(ByVal value As String)
			QtyValue = value
		End Set
	End Property

	Private PaperIDValue As String
	Public Property PaperID() As String
		Get
			Return PaperIDValue
		End Get
		Set(ByVal value As String)
			PaperIDValue = value
		End Set
	End Property

	Private NetAmtValue As String
	Public Property NetAmt() As String
		Get
			Return NetAmtValue
		End Get
		Set(ByVal value As String)
			NetAmtValue = value
		End Set
	End Property

	Private VatAmtValue As String
	Public Property VatAmt() As String
		Get
			Return VatAmtValue
		End Get
		Set(ByVal value As String)
			VatAmtValue = value
		End Set
	End Property

	Private DesiignAmtValue As String
	Public Property DesiignAmt() As String
		Get
			Return DesiignAmtValue
		End Get
		Set(ByVal value As String)
			DesiignAmtValue = value
		End Set
	End Property

	Private TotAmtValue As String
	Public Property TotAmt() As String
		Get
			Return TotAmtValue
		End Get
		Set(ByVal value As String)
			TotAmtValue = value
		End Set
	End Property


	Public ReadOnly Property TotalAmtToDB() As Decimal
		Get
			Return Convert.ToDecimal(TotAmt)
		End Get

	End Property


	Private DeliveryAmtValue As String
	Public Property DeliveryAmt() As String
		Get
			Return DeliveryAmtValue
		End Get
		Set(ByVal value As String)
			DeliveryAmtValue = value
		End Set
	End Property

	Private PaymentCodeValue As String
	Public Property PaymentCode() As String
		Get
			Return PaymentCodeValue
		End Get
		Set(ByVal value As String)
			PaymentCodeValue = value
		End Set
	End Property

	Private PaymentTypeValue As String
	Public Property PaymentType() As String
		Get
			Return PaymentTypeValue
		End Get
		Set(ByVal value As String)
			PaymentTypeValue = value
		End Set
	End Property

	Private ShippingSeqNoValue As Integer
	Public Property ShippingSeqNo() As Integer
		Get
			Return ShippingSeqNoValue
		End Get
		Set(ByVal value As Integer)
			ShippingSeqNoValue = value
		End Set
	End Property

	Private ContactorSeqNoValue As Integer
	Public Property ContactorSeqNo() As Integer
		Get
			Return ContactorSeqNoValue
		End Get
		Set(ByVal value As Integer)
			ContactorSeqNoValue = value
		End Set
	End Property

	Private CardIDValue As String
	Public Property CardID() As String
		Get
			Return CardIDValue
		End Get
		Set(ByVal value As String)
			CardIDValue = value
		End Set
	End Property

	Private Image1Value As Integer
	Public Property Image1() As Integer
		Get
			Return Image1Value
		End Get
		Set(ByVal value As Integer)
			Image1Value = value
		End Set
	End Property

	Private Image2Value As Integer
	Public Property Image2() As Integer
		Get
			Return Image2Value
		End Get
		Set(ByVal value As Integer)
			Image2Value = value
		End Set
	End Property

	Private CreateByValue As String
	Public Property CreateBy() As String
		Get
			Return CreateByValue
		End Get
		Set(ByVal value As String)
			CreateByValue = value
		End Set
	End Property

	Private CreateOnValue As DateTime
	Public Property CreateOn() As DateTime
		Get
			Return CreateOnValue
		End Get
		Set(ByVal value As DateTime)
			CreateOnValue = value
		End Set
	End Property

	Private UpdateByValue As String
	Public Property UpdateBy() As String
		Get
			Return UpdateByValue
		End Get
		Set(ByVal value As String)
			UpdateByValue = value
		End Set
	End Property

	Private UpdateOnValue As DateTime
	Public Property UpdateOn() As DateTime
		Get
			Return UpdateOnValue
		End Get
		Set(ByVal value As DateTime)
			UpdateOnValue = value
		End Set
	End Property

	Private DescriptionValue As String
	Public Property Description() As String
		Get
			Return DescriptionValue
		End Get
		Set(ByVal value As String)
			DescriptionValue = value
		End Set
	End Property

	'GIFT �ʴ���¡���Թ���� Invoice
	Private serviceValue As String
	Public Property serviceAdd() As String
		Get
			Return serviceValue
		End Get
		Set(ByVal value As String)
			serviceValue = value
		End Set
	End Property


	Private PaperNameValue As String
	Public Property PaperName() As String
		Get
			Return PaperNameValue
		End Get
		Set(ByVal value As String)
			PaperNameValue = value
		End Set
	End Property

	Private PaperPriceValue As String
	Public Property PaperPrice() As String
		Get
			Return PaperPriceValue
		End Get
		Set(ByVal value As String)
			PaperPriceValue = value
		End Set
	End Property

	Private ShippingAddr1Value As String
	Public Property ShippingAddr1() As String
		Get
			Return ShippingAddr1Value
		End Get
		Set(ByVal value As String)
			ShippingAddr1Value = value
		End Set
	End Property

	Private ShippingAddr2Value As String
	Public Property ShippingAddr2() As String
		Get
			Return ShippingAddr2Value
		End Get
		Set(ByVal value As String)
			ShippingAddr2Value = value
		End Set
	End Property

	Private ShippingAddr3Value As String
	Public Property ShippingAddr3() As String
		Get
			Return ShippingAddr3Value
		End Get
		Set(ByVal value As String)
			ShippingAddr3Value = value
		End Set
	End Property
	Private ShippingAddr4Value As String
	Public Property ShippingAddr4() As String
		Get
			Return ShippingAddr4Value
		End Get
		Set(ByVal value As String)
			ShippingAddr4Value = value
		End Set
	End Property
	Private ShippingAddr5Value As String
	Public Property ShippingAddr5() As String
		Get
			Return ShippingAddr5Value
		End Get
		Set(ByVal value As String)
			ShippingAddr5Value = value
		End Set
	End Property
	Private RemarkValue As String
	Public Property Remark() As String
		Get
			Return RemarkValue
		End Get
		Set(ByVal value As String)
			RemarkValue = value
		End Set
	End Property
	Private PromotionValue As String
	Public Property Promotion() As String
		Get
			Return PromotionValue
		End Get
		Set(ByVal value As String)
			PromotionValue = value
		End Set
	End Property


	Private BusinessTypeValue As String
	Public Property BusinessType() As String
		Get
			Return BusinessTypeValue
		End Get
		Set(ByVal value As String)
			BusinessTypeValue = value
		End Set
	End Property


	Private CardStyleValue As String
	Public Property CardStyle() As String
		Get
			Return CardStyleValue
		End Get
		Set(ByVal value As String)
			CardStyleValue = value
		End Set
	End Property

	Private ColorShadeValue As String
	Public Property ColorShade() As String
		Get
			Return ColorShadeValue
		End Get
		Set(ByVal value As String)
			ColorShadeValue = value
		End Set
	End Property

	'������� FTP 
	Private FolderNameValue As String
	Public Property FolderName() As String
		Get
			Return FolderNameValue
		End Get
		Set(ByVal value As String)
			FolderNameValue = value
		End Set
	End Property
	'������� FTP
	Private UserAccountValue As String
	Public Property UserAccount() As String
		Get
			Return UserAccountValue
		End Get
		Set(ByVal value As String)
			UserAccountValue = value
		End Set
	End Property
	'������� FTP
	Private PasswordValue As String
	Public Property Password() As String
		Get
			Return PasswordValue
		End Get
		Set(ByVal value As String)
			PasswordValue = value
		End Set
	End Property

	Private StoreIdValue As String
	Public Property StoreId() As String
		Get
			If StoreIdValue Is Nothing Then
				Return ""
			End If
			Return StoreIdValue
		End Get
		Set(ByVal value As String)
			StoreIdValue = value
		End Set
	End Property

	'GIFT 26/5/2016
	Private OptionalDesc1Value As String
	Public Property OptionalDesc1() As String
		Get
			Return OptionalDesc1Value
		End Get
		Set(ByVal value As String)
			OptionalDesc1Value = value
		End Set
	End Property

	Private OptionalPrice1Value As String
	Public Property OptionalPrice1() As String
		Get
			Return OptionalPrice1Value
		End Get
		Set(ByVal value As String)
			OptionalPrice1Value = value
		End Set
	End Property

	Private OptionalDesc2Value As String
	Public Property OptionalDesc2() As String
		Get
			Return OptionalDesc2Value
		End Get
		Set(ByVal value As String)
			OptionalDesc2Value = value
		End Set
	End Property

	Private OptionalPrice2Value As String
	Public Property OptionalPrice2() As String
		Get
			Return OptionalPrice2Value
		End Get
		Set(ByVal value As String)
			OptionalPrice2Value = value
		End Set
	End Property

	Private TotalAmtValue As String
	Public Property TotalAmt() As String
		Get
			Return TotalAmtValue
		End Get
		Set(ByVal value As String)
			TotalAmtValue = value
		End Set
	End Property


#End Region

End Class

Public Class clsOrderEngine
	Inherits clsOrderProvider
	Public Function InsertOrder(ByVal Parameter As String) As String
		Dim retVal As String
		retVal = Me.InsertOrdersProvider(Parameter)
		Return retVal
	End Function
	Public Function InsertOrder(ByVal Order As clsOrder) As String
		Dim retVal As String
		retVal = Me.InsertOrderNewProvider(Order)
		Return retVal
	End Function
	Public Function GetInfoOrders(ByVal OrderId As String) As List(Of clsOrder)
		Dim listOrder As New List(Of clsOrder)
		Dim clsOrderProvider As New clsOrderProvider
		listOrder = clsOrderProvider.GetInfoOrder(OrderId)
		Return listOrder
	End Function

	'������� Upload file FTP
	Public Function UpdateFTPfile(ByVal OrderID As String, ByVal UserID As String) As clsResultII
		Dim Result As New clsResultII
		Result = Me.UpdateFTPfileProvider(OrderID, UserID)
		Return Result
	End Function

	'������� Select file FTP
	Public Function SelectFTPfile(ByVal OrderID As String) As DataTable
		Dim DetailFTP As DataTable
		DetailFTP = Me.SelectFTPfileProvider(OrderID)
		Return DetailFTP
	End Function

	'������� SelectFTPuploadfile
	Public Function SelectFTPuploadfile() As List(Of clsOrder)
		Dim listFTP As New List(Of clsOrder)
		listFTP = Me.SelectFTPuploadfileProvider()
		Return listFTP
	End Function

	'������� DeleteFTPuploadfile
	Public Function DeleteFTPuploadfile(ByVal FolderName As String) As clsResultII
		Dim Result As New clsResultII
		Result = Me.DeleteFTPuploadfileProvider(FolderName)
		Return Result
	End Function

	'������� 17/01/2011 
	'View Order for Admin
	Public Function ViewOrderForAdmin(ByVal OrderID As String, ByVal FromDate As String, ByVal ToDate As String) As List(Of clsOrder)
		Dim listOrder As New List(Of clsOrder)
		listOrder = Me.ViewOrderForAdminProvider(OrderID, FromDate, ToDate)
		Return listOrder
	End Function
End Class

Public Class clsOrderProvider
	Inherits clsBLBase
	Friend Function InsertOrdersProvider(ByVal Parameter As String) As String
		Dim SqlStr As New StringBuilder
		Dim OrderID As String = Nothing

		SqlStr.AppendLine("declare @OrderID varchar (12)")
		SqlStr.AppendLine("Exec spc_GetMaxIDInc 'TBBCOrders','OrderID','SI','RO','','', @OrderID output ")
		SqlStr.AppendLine("select @OrderID as OrderID")
		SqlStr.AppendLine("Insert Into TBBCOrders Values(")
		SqlStr.AppendLine("@OrderID, @Parameter")
		SqlStr.AppendLine(")")

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			objDB.ParametersClear()
			SqlStr = SqlStr.Replace("@Parameter", Parameter)
			objDB.SelectReQuery(SqlStr.ToString())
			If (objDB.Go()) Then
				OrderID = objDB.ReadField("OrderID")
			End If
		End Using

		Return OrderID
	End Function
	Public Function InsertOrderNewProvider(ByVal Order As clsOrder) As String
		Dim OrderID As String = ""
		Dim SqlStr As New StringBuilder
		With SqlStr
			.AppendLine("declare @OrderID varchar (12)")
			.AppendLine("Exec spc_GetMaxIDInc 'TBBCOrders','OrderID','SI','RO','','', @OrderID output ")
			.AppendLine("select @OrderID as OrderID")
			.AppendLine("Insert Into TBBCOrders(")
			.AppendLine("OrderID,CustID,OrderDate,CardID,Type,Qty,")
			.AppendLine("PaperID,NetAmt,VatAmt,DesignAmt,TotAmt,DeliveryAmt,")
			.AppendLine("PaymentCode,PaymentType,ShippingSeqNo,ContactorSeqNo,")
			.AppendLine("Image1,Image2,Description,BusinessType,CardStyle,ColorShade,")
			.AppendLine("ShipAddr1,ShipAddr2,ShipAddr3,ShipAddr4,ShipAddr5,")
			.AppendLine("CreateBy,CreateOn,UpdateBy,UpdateOn,storeID,OptionalDesc1,OptionalPrice1,OptionalDesc2,OptionalPrice2,TotalAmt")
			.AppendLine(")")
			.AppendLine("Values(")
			.AppendLine("@OrderID,@CustID,getdate(),@CardID,@Type,@Qty,")
			.AppendLine("@PaperID,@NetAmt,@VatAmt,@DesignAmt,@TotAmt,@DeliveryAmt,")
			.AppendLine("@PaymentCode,@PaymentType,@ShippingSeqNo,@ContactorSeqNo,")
			.AppendLine("@Image1,@Image2,@Description,@BusinessType,@CardStyle,@ColorShade,")
			.AppendLine("@ShipAddr1,@ShipAddr2,@ShipAddr3,@ShipAddr4,@ShipAddr5,")
			.AppendLine("@CreateBy,getdate(),@UpdateBy,getdate(),@storeID,@OptionalDesc1,")
			.AppendLine("@OptionalPrice1,@OptionalDesc2,@OptionalPrice2,@TotalAmt")
			.AppendLine(")")
		End With
		'-----[ Set reference clsDBIII in this project ]----- 
		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@CustID", Order.CustID)
				.PatametersAdd("@Type", Order.Type)
				.PatametersAdd("@Qty", Convert.ToInt32(Order.Qty))
				.PatametersAdd("@PaperID", Order.PaperID)
                .PatametersAdd("@NetAmt", Convert.ToDecimal(Order.NetAmt))
				.PatametersAdd("@VatAmt", Convert.ToDecimal(Order.VatAmt))
				.PatametersAdd("@DesignAmt", Order.DesiignAmt)
				.PatametersAdd("@TotAmt", Convert.ToDecimal(Order.TotAmt))
                .PatametersAdd("@DeliveryAmt", Convert.ToDecimal(Order.DeliveryAmt))
				.PatametersAdd("@PaymentCode", Order.PaymentCode)
				.PatametersAdd("@PaymentType", Order.PaymentType)
				.PatametersAdd("@ShippingSeqNo", Order.ShippingSeqNo)
				.PatametersAdd("@ContactorSeqNo", Order.ContactorSeqNo)
				.PatametersAdd("@CardID", Order.CardID)
				.PatametersAdd("@Image1", Order.Image1)
				.PatametersAdd("@Image2", Order.Image2)
				.PatametersAdd("@CreateBy", Order.CreateBy)
				.PatametersAdd("@UpdateBy", Order.UpdateBy)
				.PatametersAdd("@Description", Order.Description)
				.PatametersAdd("@ShipAddr1", Order.ShippingAddr1)
				.PatametersAdd("@ShipAddr2", Order.ShippingAddr2)
				.PatametersAdd("@ShipAddr3", Order.ShippingAddr3)
				.PatametersAdd("@ShipAddr4", Order.ShippingAddr4)
				.PatametersAdd("@ShipAddr5", "")
				.PatametersAdd("@BusinessType", Order.BusinessType)
				.PatametersAdd("@CardStyle", Order.CardStyle)
				.PatametersAdd("@ColorShade", Order.ColorShade)
				.PatametersAdd("@storeID", Order.StoreId)
				''gift 26/5/2016
                .PatametersAdd("@OptionalDesc1", Order.OptionalDesc1)
				.PatametersAdd("@OptionalPrice1", Convert.ToDecimal(Order.OptionalPrice1))
                .PatametersAdd("@OptionalDesc2", Order.OptionalDesc2)
                .PatametersAdd("@OptionalPrice2", Convert.ToDecimal(Order.OptionalPrice2))
                .PatametersAdd("@TotalAmt", Convert.ToDecimal(Order.TotalAmt))
			End With
			Try
				ObjDB.OpenConnWithBeginTran()
				ObjDB.SelectReQuery(SqlStr.ToString)
				If (ObjDB.Go()) Then
					OrderID = ObjDB.ReadField("OrderID")
				End If
				ObjDB.CloseConnWithCommitTran()
			Catch ex As Exception
				If ObjDB.OnTransaction Then
					ObjDB.CloseConWithRollbackTran()
				End If
			Finally
				ObjDB.SelectClose()
			End Try
		End Using

		Return OrderID
	End Function

	Friend Function GetInfoOrder(ByVal OrderId As String) As List(Of clsOrder)
		Dim listOrder As New List(Of clsOrder)
		Dim SqlStr As New StringBuilder

		With SqlStr
			.AppendLine("SELECT	TBBCOrders.OrderID, TBBCOrders.CustID, TBBCOrders.OrderDate, ")
			.AppendLine("		TBBCOrders.Qty, TBBCOrders.PaperID, TBBCOrders.NetAmt, ")
			.AppendLine("		TBBCOrders.VatAmt,TBBCOrders.DesignAmt, TBBCOrders.TotAmt, ")
			.AppendLine("		TBBCOrders.PaymentCode, TBBCOrders.PaymentType, TBBCOrders.CardID, TBBCOrders.Image1, ")
			.AppendLine("		TBBCOrders.Image2,TBBCOrders.Description, TBBCPapers.Price,")
			.AppendLine("		TBBCPapers.Description AS PaperName,TBBCOrders.ShipAddr1,")
			.AppendLine("		TBBCOrders.ShipAddr2, TBBCOrders.ShipAddr3,TBBCOrders.ShipAddr4,")
			.AppendLine("		TBBCOrders.Description,TBBCPapers.PricePromotion AS PricePromotion,")
			.AppendLine("		TBBCOrders.BusinessType,TBBCOrders.CardStyle,TBBCOrders.ColorShade,")

			''GIFT Add 26/5/2016
			.AppendLine("		TBBCOrders.OptionalDesc1,TBBCOrders.OptionalPrice1,TBBCOrders.OptionalDesc2,TBBCOrders.OptionalPrice2,TBBCOrders.TotalAmt,TBBCOrders.Type,TBBCOrders.DeliveryAmt")
			''End Add

			.AppendLine("FROM	TBBCOrders INNER JOIN TBBCPapers ")
			.AppendLine("ON     TBBCOrders.PaperID = TBBCPapers.PaperID")
			.AppendLine("WHERE  TBBCOrders.OrderID = @OrderID ")
		End With

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			objDB.ParametersClear()
			objDB.PatametersAdd("@OrderID", OrderId)
			objDB.SelectReQuery(SqlStr.ToString())

			While (objDB.Go)
				Dim Order As New clsOrder
				Order.OrderID = objDB.ReadField("OrderID")
				Order.CustID = objDB.ReadField("CustID")
				Order.OrderDate = objDB.ReadField("OrderDate")
				Order.Qty = objDB.ReadField("Qty")
				Order.PaperID = objDB.ReadField("PaperID")
				Order.NetAmt = objDB.ReadField("NetAmt")
				Order.VatAmt = objDB.ReadField("VatAmt")
				Order.DesiignAmt = objDB.ReadField("DesignAmt")
				Order.TotAmt = objDB.ReadField("TotAmt")
				Order.PaymentCode = objDB.ReadField("PaymentCode")
				Order.PaymentType = objDB.ReadField("PaymentType")
				Order.CardID = objDB.ReadField("CardID")
				Order.Image1 = objDB.ReadField("Image1")
				Order.Image2 = objDB.ReadField("Image2")
				Order.Description = objDB.ReadField("Description")
				Order.PaperName = objDB.ReadField("PaperName")
				Order.PaperPrice = objDB.ReadField("Price")
				Order.ShippingAddr1 = objDB.ReadField("ShipAddr1")
				Order.ShippingAddr2 = objDB.ReadField("ShipAddr2")
				Order.ShippingAddr3 = objDB.ReadField("ShipAddr3")
				Order.ShippingAddr4 = objDB.ReadField("ShipAddr4")
				Order.Description = objDB.ReadField("Description")
				Order.Promotion = objDB.ReadField("PricePromotion")
				Order.BusinessType = objDB.ReadField("BusinessType")
				Order.CardStyle = objDB.ReadField("CardStyle")
				Order.ColorShade = objDB.ReadField("ColorShade")

				''GIFT Add 26/5/2016
				Order.OptionalDesc1 = objDB.ReadField("OptionalDesc1")
				Order.OptionalPrice1 = objDB.ReadField("OptionalPrice1")
				Order.OptionalDesc2 = objDB.ReadField("OptionalDesc2")
				Order.OptionalPrice2 = objDB.ReadField("OptionalPrice2")
				Order.DeliveryAmt = objDB.ReadField("DeliveryAmt")
				Order.TotalAmt = objDB.ReadField("TotalAmt")
				Order.Type = objDB.ReadField("Type")

				listOrder.Add(Order)
			End While
		End Using
		Return listOrder
	End Function

	'������� UpdateFTPfile
	Public Function UpdateFTPfileProvider(ByVal OrderID As String, ByVal UserID As String) As clsResultII

		Dim Result As New clsResultII
		Dim SqlStr As New StringBuilder

		With SqlStr
			.AppendLine("Update TBBCuploadfile SET OrderID = '" & OrderID & "',CreateOn = getdate(),CreateBy = '" & UserID & "',")
			.AppendLine("UpdateOn = getdate(),UpdateBy = '" & UserID & "' ")
			.AppendLine("Where FolderName = (Select top 1 FolderName from TBBCuploadfile where OrderID = '')")
		End With

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With objDB
				Try
					Result.Flag = objDB.ExecuteCommand(SqlStr.ToString, clsDBIII.eExecute.UpdateType)
				Catch ex As Exception
					Throw ex
				End Try
			End With
		End Using

		Return Result
	End Function

	'������� SelectFTPfile
	Public Function SelectFTPfileProvider(ByVal OrderID As String) As DataTable
		Dim SqlStr As New StringBuilder
		Dim TempDS As New DataSet

		SqlStr.AppendLine("Select FolderName,UserAccount,Password from TBBCuploadfile where OrderID = '" & OrderID & "'")

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			TempDS = objDB.getDataSet(SqlStr.ToString(), "TBBCuploadfile")
		End Using

		Return TempDS.Tables("TBBCuploadfile")
	End Function

	'������� SelectFTPuploadfile()
	Public Function SelectFTPuploadfileProvider() As List(Of clsOrder)
		Dim listOrder As New List(Of clsOrder)
		Dim Order As New clsOrder
		Dim SqlStr As New StringBuilder
		Dim ds As New DataSet

		SqlStr.AppendLine("Select * from TBBCuploadfile")

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ds = objDB.getDataSet(SqlStr.ToString(), "TBBCuploadfile")
		End Using

		If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			For Each aRow As DataRow In ds.Tables(0).Rows
				Order = New clsOrder
				With Order
					.FolderName = aRow("FolderName")
					.UserAccount = aRow("UserAccount")
					.Password = aRow("Password")
					.OrderID = aRow("OrderID")
					.CreateOn = aRow("CreateOn")
					.CreateBy = aRow("CreateBy")
					.UpdateOn = aRow("UpdateOn")
					.UpdateBy = aRow("UpdateBy")
					listOrder.Add(Order)
				End With
			Next
		End If

		Return listOrder
	End Function

	'������� DeleteFTPuploadfile
	Public Function DeleteFTPuploadfileProvider(ByVal FolderName As String) As clsResultII
		Dim Result As New clsResultII
		Dim SqlStr As New StringBuilder


		With SqlStr
			.AppendLine("Update TBBCuploadfile SET OrderID = '',")
			.AppendLine("UpdateOn = getdate(),UpdateBy = '" & objSessionVar.ModelUser.UserName & "' ")
			.AppendLine("Where FolderName in (" & FolderName & ")")
		End With

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With objDB
				Try
					Result.Flag = objDB.ExecuteCommand(SqlStr.ToString, clsDBIII.eExecute.UpdateType)
				Catch ex As Exception
					Throw ex
				End Try
			End With
		End Using

		Return Result
	End Function

	'������� 17/01/2011 
	'View Order for Admin
	Public Function ViewOrderForAdminProvider(ByVal OrderID As String, ByVal FromDate As String, ByVal ToDate As String) As List(Of clsOrder)
		Dim listOrder As New List(Of clsOrder)
		Dim SqlStr As New StringBuilder

		With SqlStr
			.AppendLine("Select	OrderID,OrderDate,T1.CreateBy,ShipAddr1,UploadFlag")
			.AppendLine("From	TBBCOrders AS T1 inner join TBBCCards AS T2")
			.AppendLine("On		T1.CardID = T2.CardID")
			If OrderID <> "" Then
				.AppendLine("Where	OrderID = @OrderID")
			Else
				.AppendLine("Where	OrderDate Between @FromDate and @ToDate")
				.AppendLine("Order  by OrderID Desc")
			End If
		End With

		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			objDB.ParametersClear()
			objDB.PatametersAdd("@OrderID", OrderID)
			objDB.PatametersAdd("@FromDate", FromDate)
			objDB.PatametersAdd("@ToDate", ToDate)
			objDB.SelectReQuery(SqlStr.ToString())
			While (objDB.Go)
				Dim Order As New clsOrder
				Order.OrderID = objDB.ReadField("OrderID")
				Order.CreateBy = objDB.ReadField("CreateBy")
				Order.OrderDate = objDB.ReadField("OrderDate")
				Order.ShippingAddr1 = objDB.ReadField("ShipAddr1")
				Order.Type = objDB.ReadField("UploadFlag")
				listOrder.Add(Order)
			End While
		End Using
		Return listOrder
	End Function
End Class