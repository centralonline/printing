Imports System.Collections.Generic
Imports UserEngineTrendyPrint
Partial Class uc_ucTop
    Inherits clsUCBase

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogin.Click
        Dim UserID As String = Me.txtUserName.Text
        Dim Password As String = Me.txtPassword.Text
        IsLogin(UserID, Password)

        If Not IsLogin(UserID, Password) Then
            ShowMessage("��سҵ�Ǩ�ͺ Username ����  Password ���١��ͧ���¤��")
        Else
            SwitchLoginPanel(True)
            'Response.Redirect(Request.ServerVariables("URL"))
        End If
    End Sub
    Protected Sub btnLogout_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogout.Click
		SwitchLoginPanel(False)
        Me.objSessionVar.ModelUser = Nothing
        Session("oCard") = Nothing
		Response.Redirect("~/Main.aspx", True)
    End Sub
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		SwitchLoginPanel(False)
		'If Page.IsPostBack = False Then
		If (Me.objSessionVar.ModelUser IsNot Nothing) Then
			SwitchLoginPanel(True)
		End If

		'	End If
	End Sub
	Private Sub SwitchLoginPanel(ByVal Open As Boolean)
		If Open = True Then	'Login ����
			Me.lbUserName.Text = Me.objSessionVar.ModelUser.UserName
            Me.Panel1.Visible = True
            Me.Panel2.Visible = False
			Me.Panel3.Visible = False
		Else '�ѧ����� Login
			Me.Panel1.Visible = False
            Me.Panel2.Visible = True
			Me.Panel3.Visible = True
		End If
	End Sub
	Private Function IsLogin(ByVal userid As String, ByVal password As String) As Boolean
		Dim Engine As New UserEngine
		Dim resultIsLogin As Boolean
		resultIsLogin = Engine.Login(userid, password)
		Return resultIsLogin
	End Function
End Class
