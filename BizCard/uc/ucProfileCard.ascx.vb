Imports System.Collections.Generic
Partial Class uc_ucProfileCard
    'Inherits System.Web.UI.UserControl
    Inherits clsUCBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            Dim clsCard As clsCard = Me.Session("oCard")
            Me.dlCardProfile.DataSource = clsCard.TextFields
            Me.dlCardProfile.DataBind()
        End If
    End Sub
End Class
