Imports System.Collections.Generic

Partial Class uc_ucCategories
    Inherits clsUCBase
    Public listCategory As New List(Of clsCategories)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim TempCardEngine As New clsCardEngine
            listCategory = TempCardEngine.GetListCategories()
        End If
    End Sub

End Class
