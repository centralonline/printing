<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Preview.aspx.vb" Inherits="Preview" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        &nbsp;
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:TextBox Text="Pakawan Narmtorng" runat="server" id="Text" /><br><br>

<asp:dropdownlist runat="server" id="BackgroundColor">
	<asp:ListItem Value="red">Red</asp:ListItem>
	<asp:ListItem Value="green">Green</asp:ListItem>
	<asp:ListItem Value="navy">Navy</asp:ListItem>
	<asp:ListItem Value="orange" Selected="true" >Orange</asp:ListItem>
</asp:dropdownlist>

<asp:dropdownlist runat="server" id="Font">
	<asp:ListItem Value="Arial">Arial</asp:ListItem>
	<asp:ListItem Value="Angsana New">Angsana New</asp:ListItem>
	<asp:ListItem Value="Courier">Courier</asp:ListItem>
	<asp:ListItem Value="Microsoft San Serif">Microsoft San Serif</asp:ListItem>
	<asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
	<asp:ListItem Value="2548_D6"  Selected="true" >2548_D6</asp:ListItem>
	<asp:ListItem Value="Trenz">Trenz</asp:ListItem>
	<asp:ListItem Value="PSL Kanda ModernAD">PSL Kanda ModernAD</asp:ListItem>
</asp:dropdownlist>
        <asp:DropDownList ID="Size" runat="server">
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem  Selected="true">30</asp:ListItem>
        </asp:DropDownList><br><br>

<asp:Button runat="Server" id="SubmitButton" Text="Generate Image" />&nbsp;<br />
    <br />
        &nbsp;
    <asp:image id="Image1" ImageUrl="~/FileSender.aspx" runat="server" ></asp:image>
    
       <br />
        &nbsp;
   <script>
function saveImageAs (imgOrURL) {
if (typeof imgOrURL == 'object')
imgOrURL = imgOrURL.src;
window.win = open (imgOrURL);
setTimeout('win.document.execCommand("SaveAs")', 500);
}
</script>

<a href="#" onclick="saveImageAs(document.getElementById('Image1')); return false;" >save image</a>

    </div>
    </form>
</body>
</html>
