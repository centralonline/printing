Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Collections.Generic
Imports CoreBaseII

Public Class clsAppVarWC

#Region "Properties"

	Private RefreshDateValue As DateTime
	Public ReadOnly Property RefreshDate() As DateTime
		Get
			Return Me.RefreshDateValue
		End Get
	End Property

	Private SiteNameValue As String
	Public ReadOnly Property SiteName() As String
		Get
			Return SiteNameValue
		End Get
	End Property

	Public ReadOnly Property SiteNameWithLang() As String
		Get
			Return SiteNameValue
		End Get
	End Property

	Private NumOfVisitorValue As Integer
	Public Property NumOfVisitor() As Integer
		Get
			Return NumOfVisitorValue
		End Get
		Set(ByVal value As Integer)
			NumOfVisitorValue = value
		End Set
	End Property

	Private NumOfUserOnlineValue As Integer
	Public Property NumOfUserOnline() As Integer
		Get
			Return NumOfUserOnlineValue
		End Get
		Set(ByVal value As Integer)
			NumOfUserOnlineValue = value
		End Set
	End Property

	Private ThisDBConfig As CoreBaseII.clsDBConfig
	Public Property DBConfig() As CoreBaseII.clsDBConfig
		Get
			Return Me.ThisDBConfig
		End Get
		Set(ByVal value As CoreBaseII.clsDBConfig)
			Me.ThisDBConfig = value
		End Set
	End Property

	Private ThisDBConfigMas As CoreBaseII.clsDBConfig
	Public Property DBConfigMas() As CoreBaseII.clsDBConfig
		Get
			Return Me.ThisDBConfigMas
		End Get
		Set(ByVal value As CoreBaseII.clsDBConfig)
			Me.ThisDBConfigMas = value
		End Set
	End Property

	Private ThisControl As clsControl
	Public Property Control() As clsControl
		Get
			Return Me.ThisControl
		End Get
		Set(ByVal value As clsControl)
			Me.ThisControl = value
		End Set
	End Property

	Private ThisProvinces As DataTable
	Public Property Provinces() As DataTable
		Get
			Return Me.ThisProvinces
		End Get
		Set(ByVal value As DataTable)
			Me.ThisProvinces = value
		End Set
	End Property

	Private ImagePathValue As String
	Public Property ImagePath() As String
		Get
			Return Me.ImagePathValue
		End Get
		Set(ByVal value As String)
			Me.ImagePathValue = value
		End Set
	End Property

	Private MailNameValue As String
	Public Property MailName() As String
		Get
			Return Me.MailNameValue
		End Get
		Set(ByVal value As String)
			Me.MailNameValue = value
		End Set
	End Property

	'Gift SEND MAIL 03/06/2016
	Private MailAdminValue As String
	Public Property MailAdmin() As String
		Get
			If SiteName = "Y" Then
                MailAdminValue = "surapon@officemate.co.th surachart@officemate.co.th waraporn_k@officemate.co.th panupan.ja@officemate.co.th kannapit@officemate.co.th"
			Else
                MailAdminValue = "kannapit@officemate.co.th panupan.ja@officemate.co.th"
			End If
			Return MailAdminValue
		End Get
		Set(ByVal value As String)
			Me.MailNameValue = value
		End Set
	End Property


#End Region

#Region "Method"

	Private objDB As clsDBIII
	Private SqlStr As String

	Public Sub onSessionStart()
		'Try
		'    '--> get old num of visitor value and insert new num of visitor value to db
		'    SqlStr = "Select NumOfVisitor From TBWControls "
		'    Dim TempDS As DataSet = Me.objDB.getDataSet(SqlStr, "tb1")
		'    Dim TempOldNumOfVisitor As Integer = TempDS.Tables(0).Rows(0)(0)
		'    Me.NumOfVisitorValue = TempOldNumOfVisitor + 1
		'    SqlStr = "Update TBWControls Set NumOfVisitor = " & Me.NumOfVisitorValue
		'    Me.objDB.ExecuteCommand(SqlStr, clsDBII.eExecute.UpdateType)

		'    '--> caculate number of online user
		'    Me.NumOfUserOnline = Me.NumOfUserOnline + 1
		'    If (Me.NumOfUserOnline Mod Me.ThisControl.CountUserStep) = 0 Then
		'        SqlStr = "Insert Into TBWUserOnlineLogs  values (" & Me.NumOfUserOnline & ",'" & MyDateTime.ToEnUsYMDHM(Now) & "') "
		'        Me.objDB.ExecuteCommand(SqlStr, clsDBII.eExecute.UpdateType)
		'    End If

		'Catch ex As Exception
		'    Throw ex
		'Finally
		'    Me.objDB.SelectClose()
		'End Try

	End Sub

	Public Sub onSessionStop()
		'Try
		'    '--> caculate number of online user
		'    Me.NumOfUserOnline = Me.NumOfUserOnline - 1
		'    If (Me.NumOfUserOnline Mod Me.Control.CountUserStep) = 0 Then
		'        SqlStr = "Insert Into TBWUserOnlines  values (" & Me.NumOfUserOnline & ",'" & MyDateTime.ToEnUsYMDHM(Now) & "') "
		'        Me.objDB.ExecuteCommand(SqlStr, clsDBII.eExecute.UpdateType)
		'    End If
		'Catch ex As Exception
		'    Throw ex
		'Finally
		'    Me.objDB.SelectClose()
		'End Try

	End Sub

	Public Sub onPageRequest()
		Try
			Dim Request As HttpRequest = HttpContext.Current.Request
			Dim Session As HttpSessionState = HttpContext.Current.Session
			If (Not Request.FilePath.ToLower().Contains(".aspx") OrElse Request.FilePath.ToLower().Contains("/b2cv4/aj")) Then
				Return
			End If
			Dim SessionID As String = ""
			If (Session IsNot Nothing) Then
				SessionID = Session.SessionID
			End If
			SqlStr = "insert into TBWPageRequests (URL,Params,SessionID,IPAddress,CreateOn) Values('" & Request.FilePath & "','" & Request.Url.Query & "','" & SessionID & "','" & Request.UserHostAddress & "',GetDate())"
			Me.objDB.ExecuteCommand(SqlStr, clsDBII.eExecute.UpdateType)
		Catch ex As Exception
			Throw ex
		Finally
			Me.objDB.SelectClose()
		End Try
	End Sub

#End Region

	Public Sub New()
		Me.refreshAppVar()
	End Sub

	Public Shared Function GetInstance() As clsAppVarWC
		Dim TempAppVar As clsAppVarWC = CType(System.Web.HttpContext.Current.Application("objAppVar"), clsAppVarWC)
		Return TempAppVar
	End Function

	Public Sub refreshAppVar()
		'--> Set Refresh Date
		Me.RefreshDateValue = DateTime.Now

		'--> Set Site Name
		Me.SiteNameValue = ConfigurationManager.AppSettings("SiteName")

		'--> Set DBConfig
		If Me.SiteNameValue = "Y" Then
			Dim dbServer As String = ConfigurationManager.AppSettings("ProdDBServerName")
			Dim dbName As String = ConfigurationManager.AppSettings("ProdDBName")
			Dim dbUser As String = clsCrypto.Decrypt(ConfigurationManager.AppSettings("ProdDBUser"))
			Dim dbPass As String = clsCrypto.Decrypt(ConfigurationManager.AppSettings("ProdDBPassword"))
			Me.ThisDBConfig = New clsDBConfig(dbServer, dbName, dbUser, dbPass, clsDBConfig.eAppType.WebApp)

			Dim dbServerMas As String = ConfigurationManager.AppSettings("ProdDBServerNameMas")
			Dim dbNameMas As String = ConfigurationManager.AppSettings("ProdDBNameMas")
			Dim dbUserMas As String = clsCrypto.Decrypt(ConfigurationManager.AppSettings("ProdDBUserMas"))
			Dim dbPassMas As String = clsCrypto.Decrypt(ConfigurationManager.AppSettings("ProdDBPasswordMas"))
			Me.ThisDBConfigMas = New clsDBConfig(dbServerMas, dbNameMas, dbUserMas, dbPassMas, clsDBConfig.eAppType.WebApp)
            Me.MailNameValue = "printing2@officemate.co.th"
            Me.ImagePathValue = "https://printing.officemate.co.th"

		Else
			Dim dbServer As String = ConfigurationManager.AppSettings("DevDBServerName")
			Dim dbName As String = ConfigurationManager.AppSettings("DevDBName")
			Dim dbUser As String = clsCrypto.Decrypt(ConfigurationManager.AppSettings("DevDBUser"))
			Dim dbPass As String = clsCrypto.Decrypt(ConfigurationManager.AppSettings("DevDBPassword"))
			Me.ThisDBConfig = New clsDBConfig(dbServer, dbName, dbUser, dbPass, clsDBConfig.eAppType.WebApp)

			Dim dbServerMas As String = ConfigurationManager.AppSettings("DevDBServerNameMas")
			Dim dbNameMas As String = ConfigurationManager.AppSettings("DevDBNameMas")
			Dim dbUserMas As String = clsCrypto.Decrypt(ConfigurationManager.AppSettings("DevDBUserMas"))
			Dim dbPassMas As String = clsCrypto.Decrypt(ConfigurationManager.AppSettings("DevDBPasswordMas"))
			Me.ThisDBConfigMas = New clsDBConfig(dbServerMas, dbNameMas, dbUserMas, dbPassMas, clsDBConfig.eAppType.WebApp)
			Me.MailNameValue = "order@trendyprint.int"

			Dim stringUrl As String = HttpContext.Current.Request.Url.Authority
			If stringUrl = "http://www.officemate.dev/" Then
				Me.ImagePathValue = "http://" & stringUrl & "/printingsolution"
				'Me.ImagePathValue = stringUrl
			Else
				Me.ImagePathValue = "http://" & stringUrl & "/BizCard"
			End If
		End If

		'--> Create objDB
		objDB = clsDBIII.GetInstance(Me.DBConfig)

	End Sub

End Class
