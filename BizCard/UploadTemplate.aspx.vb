Imports System.Data.OleDb
Imports System.Data
Imports System.IO
Partial Class UploadTemplateCard
    Inherits System.Web.UI.Page

    Dim paths As String
    Dim CurrentFileName As String = ""
    Dim CurrentPath As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("Path") IsNot Nothing) Then
            paths = Session("Path")
        End If
        If (Page.IsPostBack = False) Then
            CurrentPath = Request.PhysicalApplicationPath & "UploadFile\"
            Dim Filename() As String
            Filename = Me.GetFileInfo(CurrentPath)
            If (Filename IsNot Nothing) Then
                For i As Integer = 0 To Filename.Length - 1
                    Me.ddlFileInfo.Items.Add(Filename(i))
                Next
            End If
            CurrentPath = CurrentPath & ddlFileInfo.SelectedItem.Text
            ImportAttendence(CurrentPath)
            Session("Path") = CurrentPath

        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim result As Integer = 0
        CurrentFileName = Me.FileUpload1.FileName
        If (Path.GetExtension(CurrentFileName).ToLower <> ".xls") Then
            MsgBox("The file upload is not type excel")
            'ElseIf FileUpload1.PostedFile.ContentLength > 131072 Then
        Else
            CurrentPath = Request.PhysicalApplicationPath & "UploadFile\"
            result = Me.ValidateFileInfo(CurrentPath, CurrentFileName)
            If result = 0 Then
                If FileUpload1.HasFile Then
                    CurrentPath += CurrentFileName
                    FileUpload1.SaveAs(CurrentPath)
                    'lbFilename.Text = CurrentFileName

                    Session("Path") = CurrentPath
                    ImportAttendence(CurrentPath)

                    Dim Filename() As String
                    Filename = Me.GetFileInfo(CurrentPath)
                    If (Filename IsNot Nothing) Then
                        For i As Integer = 0 To Filename.Length - 1
                            Me.ddlFileInfo.Items.Add(Filename(i))
                        Next
                    End If

                End If
            Else
                MsgBox("The file upload is duplicate")
            End If
        End If
    End Sub

    Public Sub ImportAttendence(ByVal PrmPathExcelFile As String)
        'Fetch Data from Excel
        Dim MyConnection As System.Data.OleDb.OleDbConnection = Nothing

        Dim dt As New DataTable
        Try
            MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; " & _
            "data source='" & PrmPathExcelFile & " '; " & "Extended Properties=Excel 8.0;")

            Dim objConn As New OleDbConnection(MyConnection.ConnectionString)
            objConn.Open()
            dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)


            Dim excelSheets(dt.Rows.Count) As String
            Dim i As Integer = 0
            Dim row As DataRow
            For Each row In dt.Rows
                excelSheets(i) = row("Table_Name").ToString()
                i = i + 1
            Next

            DropDownList1.DataSource = dt
            DropDownList1.DataTextField = "Table_Name"
            DropDownList1.DataBind()

        Catch ex As Exception
            MyConnection.Close()
            Throw ex
        End Try
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        If (Me.DropDownList1.Items.Count > 0) Then
            Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
            Dim MyConnection As System.Data.OleDb.OleDbConnection = Nothing
            Dim DtSet As System.Data.DataSet


            MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0; " & _
            "data source='" & paths & " '; " & "Extended Properties=Excel 8.0;")
            MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [" & Me.DropDownList1.SelectedItem.Text & "]", MyConnection)
            MyCommand.TableMappings.Add("Table", "Attendence")
            DtSet = New System.Data.DataSet
            MyCommand.Fill(DtSet)

            Me.Panel1.Visible = True

            Me.GridView1.DataSource = DtSet.Tables(0)
            Me.GridView1.DataBind()
        End If

    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        If e.Row.RowType = DataControlRowType.Header Then
            'e.Row.Cells(0).Text = "��Ǣ��"
            ' Me.hdfName.Value = e.Row.Cells(1).Text
            'e.Row.Cells(1).Text = "��ͤ���"
            'e.Row.Cells(2).Text = "(���ҧ x �٧): mm."
            'e.Row.Cells(3).Text = "��Ҵ�ͧ��ͤ���"
            'e.Row.Cells(4).Text = "���˹�(�ش X,Y)"
            'e.Row.Cells(5).Text = "���Ϳ͹��"
            'e.Row.Cells(6).Text = "��Ҵ�͹��"
            'e.Row.Cells(7).Text = "�տ͹��"
            'e.Row.Cells(8).Text = "�ٻẺ�͹��"

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            If (e.Row.DataItemIndex <= 2) Then
                e.Row.Visible = False
            End If

        End If

    End Sub

    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click

        Dim i As Integer = 0
        Dim Seq As Integer = 1
        Dim splitPositionValue() As Double
        Dim CardName As String = Me.hdfName.Value
        Dim Description As String = ""
        Dim BGColor As String = "white"
        Dim BGImage As String = ""
        Dim TemplateImage As String = ""
        Dim Width As Double
        Dim Height As Double
        Dim TmpFlag As String = "Y"
        Dim CountOrder As Integer = 0
        Dim UploadFlag As String = "N"
        Dim Owner As String = "ofm"
        Dim CreateBy As String = "ofm"
        Dim UpdateBy As String = "ofm"
        Dim UserID As String = "ofm"
        Dim Category As String = Me.GridView1.Rows(1).Cells(1).Text
        Dim CardID As String = ""
        Dim ClsUpload As New clsUploadTemplateEngine

        CardID = ClsUpload.GetMaxID()

        If Me.txtDescription.Text = "" Then
            Description = "Officemate"
        Else
            Description = Me.txtDescription.Text
        End If
        splitPositionValue = SplitPosition(Me.GridView1.Rows(0).Cells(1).Text.ToString())
        Width = splitPositionValue(0)
        Height = splitPositionValue(1)
        BGImage = GetFilenameBG()
        TemplateImage = GetFilenameTemplate()


        Dim strQuery As String = ""

        strQuery += "'" & CardID & "','" & CardName & "','" & Description & "','" & BGColor & "'," & vbNewLine
        strQuery += "'" & BGImage & "','" & TemplateImage & "','" & TmpFlag & "'," & Width & "," & Height & "," & vbNewLine
        strQuery += "" & CountOrder & ",'" & UploadFlag & "','" & Owner & "','" & CreateBy & "',GetDate()," & vbNewLine
        strQuery += "'" & UpdateBy & "',GetDate(),'" & UserID & "','" & Category & "'"

        'Me.txtQuery.Text = strQuery
        ClsUpload.InsertUploadTemplateEngine(strQuery)


        For i = 0 To Me.GridView1.Rows.Count - 1
            Dim strQuery2 As String = ""
            Dim FieldName As String = ""
            Dim TextValue As String = ""
            Dim PositionTop As Double
            Dim PositionLeft As Double
            Dim zIndex As Double
            Dim BackColor As String
            Dim ForeColor As String
            Dim FontFamily As String
            Dim FontSize As String
            Dim FontBold As String
            Dim Fontltalic As String = "N"
            Dim FontUnderline As String = "N"
            Dim Right2Left As String = "N"
            Dim AlignCenter As String = "N"

            If (i > 2) Then

                If (Me.GridView1.Rows(i).Cells(0).Text.ToString().ToLower() <> "logo") Then

                    If (Me.GridView1.Rows(i).Cells(0).Text <> "&nbsp;") Then
                        strQuery2 = ""
                        FieldName = Me.GridView1.Rows(i).Cells(0).Text
                        TextValue = Me.GridView1.Rows(i).Cells(1).Text

                        splitPositionValue = SplitPosition(Me.GridView1.Rows(i).Cells(4).Text.ToString())
                        PositionLeft = splitPositionValue(0)
                        PositionTop = splitPositionValue(1)
                        zIndex = Seq

                        BackColor = "#FFFFFF"
                        ForeColor = Me.GridView1.Rows(i).Cells(7).Text.ToUpper
                        FontFamily = Me.GridView1.Rows(i).Cells(5).Text
                        FontSize = SplitFontsize(Me.GridView1.Rows(i).Cells(6).Text)
                        FontBold = Me.GridView1.Rows(i).Cells(8).Text
                        Right2Left = GetFlagRight2Left(Me.GridView1.Rows(i).Cells(3).Text)


                        strQuery2 = "'" & CardID & "'," & Seq & " ,'" & FieldName & "','" & TextValue & "'," & PositionTop & "," & PositionLeft & ","
                        strQuery2 += "" & Seq & ",'" & BackColor & "','" & ForeColor & "','" & FontFamily & "','" & FontSize & "',"
                        strQuery2 += "'" & CreateBy & "',GetDate(),'" & UpdateBy & "',GetDate(),'" & FontBold & "','" & Fontltalic & "',"
                        strQuery2 += "'" & FontUnderline & "','" & Right2Left & "','" & AlignCenter & "'"
                        'Me.txtQuery2.Text += strQuery2 & vbNewLine
                        ClsUpload.InsertUploadTBBCTextFieldsEngine(strQuery2)

                        Seq += 1
                    End If

                Else
                    Dim strQuery3 As String = ""
                    Dim splitSize(2) As Double
                    Seq = 1
                    splitPositionValue = SplitPosition(Me.GridView1.Rows(i).Cells(2).Text.ToString())
                    PositionLeft = splitPositionValue(1)
                    PositionTop = splitPositionValue(0)

                    splitSize = SplitPosition(Me.GridView1.Rows(0).Cells(1).Text.ToString())
                    Width = splitSize(0)
                    Height = splitSize(1)


                    strQuery3 = "'" & CardID & "'," & Seq & ",'Logo.png'," & PositionLeft & "," & PositionTop & ","
                    strQuery3 += "" & Seq & "," & Width & "," & Height & ",'" & CreateBy & "',GetDate(),"
                    strQuery3 += "'" & UpdateBy & "',GetDate(),1,'Logo.png','image/pjpeg'"
                    ' Me.txtQuery3.Text += strQuery3 & vbNewLine
                    ClsUpload.InsertUploadTBBCImageFieldsEngine(strQuery3)
                End If

            End If

        Next
    End Sub

    Private Function SplitPosition(ByVal getPosition As String) As Double()
        Dim splitPositions(2) As Double
        Dim str() As String
        Dim strCheck() As String
        strCheck = getPosition.Split("mm.")
        If strCheck.Length > 0 Then

            str = strCheck(0).Split("x")

            If (str.Length = 0) Then
                str = strCheck(0).Split(",")
            Else
                For i As Integer = 0 To str.Length - 1
                    splitPositions(i) = CType(str(i).ToString(), Double)
                Next
            End If
        End If
        Return splitPositions
    End Function
    Private Function SplitFontsize(ByVal getFontsize As String) As Double
        Dim splitFontsizes(2) As Double
        Dim strCheck() As String
        strCheck = getFontsize.Split("pt")
        If strCheck.Length > 0 Then
            splitFontsizes(0) = CType(strCheck(0).ToString(), Double)
        End If

        Return splitFontsizes(0)
    End Function
    Private Function GetFilenameBG() As String
        Dim strName As String = Me.BGUpload.FileName
        If (strName = "") Then
            strName = ""
        End If
        Return strName
    End Function
    Private Function GetFlagRight2Left(ByVal getRight2Left As String) As String
        Dim Right2Left As String = ""
        If (getRight2Left.ToLower = "r") Then
            Right2Left = "Y"
        Else
            Right2Left = "N"
        End If
        Return Right2Left

    End Function
    Private Function GetFilenameTemplate() As String
        Dim strName As String = Me.FullUpload.FileName
        If (strName = "") Then
            strName = ""
        End If
        Return strName
    End Function
    Private Function GetFileInfo(ByVal path As String) As String()
        Dim theFolder As New DirectoryInfo(path)
        Dim Filenames() As String = Nothing
        If (theFolder.GetFiles().Length > 0) Then
            Dim Filename(theFolder.GetFiles().Length) As String
            Dim nextFile As FileInfo
            Dim i As Integer = 0
            For Each nextFile In theFolder.GetFiles()
                Filename(i) = nextFile.Name.ToString
                i = i + 1
            Next nextFile
            Filenames = Filename
        End If
        Return Filenames
    End Function
    Private Function ValidateFileInfo(ByVal path As String, ByVal GetFilename As String) As Integer
        Dim theFolder As New DirectoryInfo(path)
        Dim Result As Integer = 0
        Dim Filenames() As String = Nothing
        If (theFolder.GetFiles().Length > 0) Then
            Dim Filename(theFolder.GetFiles().Length) As String
            Dim nextFile As FileInfo
            Dim i As Integer = 0
            For Each nextFile In theFolder.GetFiles()
                If (nextFile.Name.ToString() = GetFilename) Then
                    Result = 1
                End If
            Next nextFile
        End If
        Return Result
    End Function
    Private Function GetFolderInfo(ByVal path As String) As String()
        Dim theFolder As New DirectoryInfo(path)
        Dim Foldernames() As String = Nothing
        If (theFolder.GetDirectories.Length > 0) Then
            Dim Foldername(theFolder.GetDirectories.Length) As String
            Dim nextFolder As DirectoryInfo
            Dim i As Integer = 0
            For Each nextFolder In theFolder.GetDirectories()
                Foldername(i) = nextFolder.Name.ToString
                i = i + 1
            Next nextFolder
            Foldernames = Foldername
        End If
        Return Foldernames
    End Function

    'Protected Sub Button1_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    Dim CurrentFileName As String = ""
    '    Dim CurrentPath As String = ""
    '    Dim result As Integer = 0

    '    CurrentFileName = Me.FileUpload1.FileName


    '    CurrentPath = Request.PhysicalApplicationPath
    '    CurrentPath += "UploadFile\"
    '    result = Me.ValidateFileInfo(CurrentPath, CurrentFileName)

    '    Dim theFile As New FileInfo(CurrentPath)
    '    'Dim theFolder As New DirectoryInfo("D:\Projects\BizCard\BizCard\Uploads")

    '    Dim FileName() As String = Me.GetFileInfo(CurrentPath)
    '    Dim FolderName() As String = Me.GetFolderInfo("D:\Projects\BizCard\BizCard\")

    '    'For i As Integer = 0 To FileName.Length - 1
    '    '    Me.listBoxFile.Items.Add(FileName(i))
    '    'Next

    '    'For i As Integer = 0 To FolderName.Length - 1
    '    '    listBoxFolders.Items.Add(FolderName(i))
    '    'Next



    '    'For Each nextFolder In theFolder.GetDirectories()
    '    '    listBoxFolders.Items.Add(nextFolder.Name)
    '    'Next nextFolder

    '    'For Each nextFile In theFolder.GetFiles()
    '    '    Me.listBoxFile.Items.Add(nextFile.Name)
    '    'Next nextFile



    'End Sub

    Protected Sub btnUploadImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadImage.Click
        Dim result As Integer = 0
        CurrentFileName = Me.FileUpload1.FileName
        If (Path.GetExtension(CurrentFileName).ToLower <> ".jpg") Then
            MsgBox("The file upload is not type jpg")
            'ElseIf FileUpload1.PostedFile.ContentLength > 131072 Then
        Else
            CurrentPath = Request.PhysicalApplicationPath & "FullCard\" '-----------
            result = Me.ValidateFileInfo(CurrentPath, CurrentFileName)
            If result = 0 Then
                If FileUpload1.HasFile Then
                    CurrentPath += CurrentFileName
                    FileUpload1.SaveAs(CurrentPath)
                    'lbFilename.Text = CurrentFileName

                    Session("Path") = CurrentPath
                    ImportAttendence(CurrentPath)

                    Dim Filename() As String
                    Filename = Me.GetFileInfo(CurrentPath)
                    If (Filename IsNot Nothing) Then
                        For i As Integer = 0 To Filename.Length - 1
                            Me.ddlFileInfo.Items.Add(Filename(i))
                        Next
                    End If

                End If
            Else
                MsgBox("The file upload is duplicate")
            End If
        End If
    End Sub
End Class
