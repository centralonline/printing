﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports CoreBaseII
Imports System.Collections.Specialized
Imports System.Net
Imports System.IO
Imports System.Web
Imports System.Text
Imports System.Configuration

Public Class clsMailService

#Region "Send Mail Mothod"

	Public Shared Function toOFMFormat(ByVal SiteNameWithLang As String, ByVal MailBody As String) As String
		Try
			Dim TempFormat As String
			TempFormat = GetMailContentByAspx(SiteNameWithLang & "MailFormat.aspx", "Lang=" & HttpContext.Current.Session("lang"))
			TempFormat = TempFormat.Replace("[MailBody]", MailBody)
			Return TempFormat
		Catch ex As Exception
			writeLog(ex)
		End Try
		Return ""
	End Function

	Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
		Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}"
		strRegex += "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\"
		strRegex += ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
		Dim re As New Regex(strRegex)
		If (re.IsMatch(inputEmail)) Then
			Return True
		Else
			Return False
		End If
	End Function

	Public Shared Function SendMail(ByVal EmailFrom As String, ByVal EMailFromName As String, ByVal EmailTo As String, ByVal EmailSubject As String, ByVal EmailMessage As String, Optional ByVal IsHTML As Boolean = True, Optional ByVal BCC As String = Nothing) As Boolean
		Dim Result As Boolean = True
		' System.Web.Mail.SmtpMail.SmtpServer is obsolete in 2.0
		' System.Net.Mail.SmtpClient is the alternate class for this in 2.0
		Dim TempSmtpClient As New SmtpClient()
		Dim TempMessage As New MailMessage()

		Try
			Dim TempMailAddress As New MailAddress(EmailFrom, EMailFromName)

			' You can specify the host name or ipaddress of your server   
			' Default in IIS will be localhost
			TempSmtpClient.Host = ConfigurationManager.AppSettings("SmtpClientHost")
			If (ConfigurationManager.AppSettings("SmtpClientUserName") = "") Then
				TempSmtpClient.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("SmtpClientUserName"), ConfigurationManager.AppSettings("SmtpClientPassword"))
			End If

			'TempSmtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis

			'Default port will be 25
			TempSmtpClient.Port = 25

			'From address will be given as a MailAddress Object
			TempMessage.From = TempMailAddress

			' To address collection of MailAddress
			TempMessage.To.Add(EmailTo)

			TempMessage.Subject = EmailSubject
			TempMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8")

			' CC and BCC optional
			' MailAddressCollection class is used to send the email to various users
			'You can specify Address as new MailAddress("admin1@yoursite.com")
			'TempMessage.CC.Add("admin1@yoursite.com")
            'TempMessage.CC.Add("panupan.ja@officemate.co.th")

			' You can specify Address directly as string
			If (BCC IsNot Nothing) Then
				TempMessage.Bcc.Add(New MailAddress(BCC))
			End If
			
			'Body can be Html or text format
			'Specify true if it  is html TempMessage
			TempMessage.IsBodyHtml = IsHTML

			' Message body content
			TempMessage.Body = EmailMessage

			' Send SMTP mail
			TempSmtpClient.Send(TempMessage)

		Catch ex As Exception
			writeLog(ex)
			'Throw ex
		End Try
		Return Result
	End Function

#End Region

#Region "GetMailContentByAspx"
	Public Shared Function GetMailContentByAspx(ByVal FullURLStr As String, ByVal Params As String) As String

		'Try
		'	Dim TempDataSet As New DataSet
		'	'Dim TempBytesRespData As Byte()
		'	Dim encoding As Encoding = System.Text.Encoding.GetEncoding("utf-8")
		'	Dim bytesParam As Byte() = encoding.GetBytes("Params")
		'	Dim myHttpWebRequest As HttpWebRequest = WebRequest.Create(FullURLStr)	'Create WebRequest Instant and Convert to HttpWebRequest
		'	myHttpWebRequest.Method = "POST"
		'	myHttpWebRequest.ContentType = "application/x-www-form-urlencoded"
		'	myHttpWebRequest.ContentLength = bytesParam.Length

		'	Dim strmParam As Stream = myHttpWebRequest.GetRequestStream	 'get a stream to use to write request data.
		'	strmParam.Write(bytesParam, 0, bytesParam.Length) 'write param to it
		'	strmParam.Close()

		Return FunctionMail()

		'Catch ex As WebException
		'	writeLog(ex)
		'End Try

	End Function

	Private Shared Function StreamToByteArray(ByVal stm As Stream) As Byte()
		Dim buffer As Byte()
		ReDim buffer(32768)
		Dim ms As New MemoryStream()
		Dim read As Integer
		read = stm.Read(buffer, 0, buffer.Length)
		While read > 0
			ms.Write(buffer, 0, read)
			read = stm.Read(buffer, 0, buffer.Length)
		End While
		Return ms.ToArray()
	End Function

	Private Shared Function FunctionMail() As String

		Dim SqlStr As String = ""
		SqlStr += "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"" > " & vbNewLine
		SqlStr += "<html xmlns=""http://www.w3.org/1999/xhtml"">" & vbNewLine
		SqlStr += "<head><title>" & vbNewLine
		SqlStr += "Mail Contact" & vbNewLine
		SqlStr += "</title>" & vbNewLine
		SqlStr += "<style type=""text/css"">" & vbNewLine
		SqlStr += "body" & vbNewLine
		SqlStr += "{" & vbNewLine
		SqlStr += "margin-left: 0px;" & vbNewLine
		SqlStr += "margin-top: 0px;" & vbNewLine
		SqlStr += "margin-right: 0px;" & vbNewLine
		SqlStr += "margin-bottom: 0px;" & vbNewLine
		SqlStr += "color: #4d4d46;" & vbNewLine
		SqlStr += "font-size: 13px;" & vbNewLine
		SqlStr += "}" & vbNewLine
		SqlStr += ".red()" & vbNewLine
		SqlStr += "{" & vbNewLine
		SqlStr += "color: #FF0000;" & vbNewLine
		SqlStr += "font-weight: bold;" & vbNewLine
		SqlStr += "}" & vbNewLine
		SqlStr += "a()" & vbNewLine
		SqlStr += "{" & vbNewLine
		SqlStr += "color: #0277c4;" & vbNewLine
		SqlStr += "text-decoration: none;" & vbNewLine
		SqlStr += "}" & vbNewLine
		SqlStr += "a:hover()" & vbNewLine
		SqlStr += "{" & vbNewLine
		SqlStr += "color: #0099FF;" & vbNewLine
		SqlStr += "text-decoration: underline;" & vbNewLine
		SqlStr += "}" & vbNewLine
		SqlStr += "</style>" & vbNewLine
		SqlStr += "</head>" & vbNewLine
		SqlStr += "<body style=""color: black; font-family: 'Microsoft Sans Serif'; background-color: white;" & vbNewLine
		SqlStr += "font-size: 11pt;"">" & vbNewLine
		SqlStr += "<div style=""padding: 10px; background-color: #f8f8f8; width: 780px"">" & vbNewLine
		SqlStr += "<div>" & vbNewLine
		SqlStr += "<img src=""/images/logo.gif""/></div> " & vbNewLine
		SqlStr += "<div style=""background-color: #000080; height: 5px"">" & vbNewLine
		SqlStr += "</div>" & vbNewLine
		SqlStr += "<div style=""background-color: #445C8C; padding: 5px; height: 13px"">" & vbNewLine
		SqlStr += "</div>" & vbNewLine
		SqlStr += "<div style=""background-color: #efefef;"">" & vbNewLine
		SqlStr += "<div>" & vbNewLine
		SqlStr += "<div style=""display: inline; background-color: #4d4d46; color: #dadada; padding: 3px 60px 3px 20px; font-size: 14px;"">" & vbNewLine
		SqlStr += "Contact Us</div>" & vbNewLine
		SqlStr += "</div>" & vbNewLine
		SqlStr += "<div style=""padding: 30px"">" & vbNewLine
		SqlStr += "[MailBody]" & vbNewLine
		SqlStr += "</div>" & vbNewLine
		SqlStr += "</div>" & vbNewLine
		SqlStr += "<div style=""background-color: #445C8C; padding: 5px; padding: 5px; height: 13px""> " & vbNewLine
		SqlStr += "</div>" & vbNewLine
		SqlStr += "<div style=""background-color: #000080; height: 5px""> " & vbNewLine
		SqlStr += "</div>" & vbNewLine
		SqlStr += "<div style=""background-color: #f8f8f8; font-size: 13px; color: #666666; padding: 10px 0 10px 30px"">" & vbNewLine
		SqlStr += "บริษัท ซีโอแอล จำกัด ( มหาชน )<br />" & vbNewLine
		SqlStr += "24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ 10250<br />" & vbNewLine
		SqlStr += "OfficeMate Contact Center: Tel: 02-739-5555, Fax: 02-763-5555<br />" & vbNewLine
		SqlStr += "เวลาทำการ จันทร์ - ศุกร์ 8:30 - 18:00 น.<br />" & vbNewLine
		SqlStr += "© สงวนลิขสิทธิ์ เว็บไซต์ <a href=""http://www.Officemate.co.th" & vbNewLine
		SqlStr += "target=""_blank"">officemate.co.th</a>" & vbNewLine
		SqlStr += "โดย <a href=""http://www.officemate.co.th""target=""_blank"">OfficeMate</a></div> " & vbNewLine
		SqlStr += "</div>" & vbNewLine
		SqlStr += "</body>" & vbNewLine
		SqlStr += "</html>" & vbNewLine

		Return SqlStr
	End Function

	Private Shared Sub writeLog(ByVal ex As Exception)
		Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
		message += Environment.NewLine
		message += "-----------------------------------------------------------"
		message += Environment.NewLine
		message += String.Format("Message: {0}", ex.Message)
		message += Environment.NewLine
		message += String.Format("StackTrace: {0}", ex.StackTrace)
		message += Environment.NewLine
		message += String.Format("Source: {0}", ex.Source)
		message += Environment.NewLine
		message += String.Format("TargetSite: {0}", ex.TargetSite.ToString())
		message += Environment.NewLine
		message += "-----------------------------------------------------------"
		message += Environment.NewLine
		'Dim path As String = HttpContext.Current.Server.MapPath("~") & "\CardImages\Thumbnails\yourfile.txt"
		'Using writer As New StreamWriter(path, True)
		'writer.WriteLine(message)
		'writer.Close()
		'End Using
		SendMail(message)

	End Sub

	Private Shared Sub SendMail(ByVal mesError As String)

		Dim Message As New Net.Mail.MailMessage()
		Message.From = New Net.Mail.MailAddress("printing@officemate.co.th")
        Message.To.Add("panupan.ja@officemate.co.th")
		Message.Bcc.Add(New Net.Mail.MailAddress("surapon@officemate.co.th"))
		Message.Subject = "เกิดการทำงานผิดพลาดบางอย่างของระบบค่ะ"

		Dim MailClient As New Net.Mail.SmtpClient()
		MailClient.Host = ConfigurationManager.AppSettings("SmtpClientHost")
		If (ConfigurationManager.AppSettings("SmtpClientUserName") = "") Then
			MailClient.Credentials = New Net.NetworkCredential(ConfigurationManager.AppSettings("SmtpClientUserName"), ConfigurationManager.AppSettings("SmtpClientPassword"))
		End If
		Dim HtmlBody As Net.Mail.AlternateView
		HtmlBody = Net.Mail.AlternateView.CreateAlternateViewFromString(mesError)
		Message.AlternateViews.Add(HtmlBody)
		MailClient.Send(Message)

	End Sub

#End Region

End Class
