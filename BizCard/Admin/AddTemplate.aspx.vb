﻿Imports System.Collections.Generic

Partial Class Admin_AddTemplate
    Inherits clsHTTPBaseAdmin
    Public listCategory As New List(Of clsCategories)
    Public listFontFamily As New List(Of clsFontFamily)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim CardEngine As New clsCardEngine
        listCategory = CardEngine.GetListCategories()
        listFontFamily = CardEngine.GetListFontFamily()

    End Sub
End Class
