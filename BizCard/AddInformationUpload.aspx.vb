Imports System.Collections.Generic

Partial Class AddInformationUpload
	Inherits clsHTTPBase

	Dim oCard As clsCard 'oUCard

	Dim FileType() As String = HttpContext.Current.Application("objFileType")
	Dim FileTypeImage() As String = HttpContext.Current.Application("objFileTypeImage")

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Me.UcBar1.BarStep = 2
		oCard = New clsCard	'oUCard

		If Session("oCard") IsNot Nothing Then 'oUCard
			oCard = Session("oCard") 'oUCard
		End If

		If (Not Page.IsPostBack) Then
			Me.BindImage()
			Me.BindText()
		End If
	End Sub

	Private Sub BindImage()
		If oCard.ImageFields IsNot Nothing Then	'oUCard
			dlImage.DataSource = oCard.ImageFields 'oUCard
			dlImage.DataBind()
		End If
	End Sub

	Private Sub BindText()
		If oCard.TextFields IsNot Nothing Then 'oUCard
			dlText.DataSource = oCard.TextFields 'oUCard
			dlText.DataBind()
		Else
			Dim ListText As New List(Of clsCardTextField)

			Dim fText() As String = {"����-ʡ��[��]", "����-ʡ��[�ѧ���]", "���˹�[��]", "���˹�[�ѧ���]", "˹��§ҹ[��]", "˹��§ҹ[�ѧ���]", "��������÷Ѵ1", "��������÷Ѵ2", "������ӧҹ", "������Ͷ��", "����ῡ��", "������", "�Ǻ䫴����ѷ", "��ͤ�������1", "��ͤ�������2", "��ͤ�������3"}
			Dim fValue() As String = {"���� ���ʡ��", "FirstName LastName", "���˹�", "Position", "����ѷ �Ϳ������ �ӡѴ(��Ҫ�)", "Officemate.co.,Ltd.", "�Ҥ������ �Ţ��� 24 ����آ���Է 77 �.��͹�ت 66/1", "�ǧ�ǹ��ǧ ࢵ�ǹ��ǧ ��ا෾� 10250", "02-739-5555 (120 ������)", "0-8000-00000", "02-763-5555 (30 ������)", "support@officemate.co.th", "www.officemate.co.th", "", "", ""}

			For i As Integer = 0 To fText.Length - 1
				Dim TextField As New clsCardTextField

				With TextField
					.FieldName = fText(i)
					.TextValue = fValue(i)

					' ======== ����Ӥѭ
					.PositionTop = 0.0
					.PositionLeft = 0.0
					.zIndex = i
					.BackColor = ""
					.ForeColor = ""
					.FontFamily = ""
					.FontSize = ""
					.CreateBy = Me.objSessionVar.ModelUser.UserID
					.CreateOn = Now
					.UpdateBy = Me.objSessionVar.ModelUser.UserID
					.UpdateOn = Now
					.FontBold = "N"
					.FontItalic = "N"
					.FontUnderline = "N"
					.Right2Left = "N"
					.AlignCenter = "N"
				End With

				ListText.Add(TextField)
			Next
			oCard.TextFields = ListText	'oUCard
			dlText.DataSource = oCard.TextFields 'oUCard
			dlText.DataBind()
			Session("oCard") = oCard 'oUCard
		End If
	End Sub

	Protected Sub dlImage_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlImage.ItemDataBound
		Dim hidContentType As HiddenField = CType(e.Item.FindControl("hidContentType"), HiddenField)
		Dim imgUpload As Image = CType(e.Item.FindControl("imgUpload"), Image)

		If hidContentType IsNot Nothing Then
			For ft As Int16 = 0 To FileType.Length - 1
				If hidContentType.Value.Equals(FileType(ft)) = True Then
					If imgUpload IsNot Nothing Then imgUpload.ImageUrl = "images/" & FileTypeImage(ft)
				End If
			Next
		End If

	End Sub

	Private Sub GetTextField()
		Dim newListTexts As New Generic.List(Of clsCardTextField)
		Dim i As Int16 = 0
		For Each aItem As DataListItem In dlText.Items
			i += 1
			Dim newText As New clsCardTextField
			With newText
				.FieldName = CType(aItem.FindControl("Label1"), Label).Text
				.TextValue = CType(aItem.FindControl("TextBox1"), TextBox).Text

				' ======== ����Ӥѭ
				.PositionTop = 0.0
				.PositionLeft = 0.0
				.zIndex = i
				.BackColor = ""
				.ForeColor = ""
				.FontFamily = ""
				.FontSize = ""
				.CreateBy = Me.objSessionVar.ModelUser.UserID
				.CreateOn = Now
				.UpdateBy = Me.objSessionVar.ModelUser.UserID
				.UpdateOn = Now
				.FontBold = "N"
				.FontItalic = "N"
				.FontUnderline = "N"
				.Right2Left = "N"
				.AlignCenter = "N"

				newListTexts.Add(newText)
			End With
		Next

		oCard.TextFields = newListTexts
	End Sub

	Private Sub GetImageField()
		Dim newImages As New List(Of clsCardImageField)
		Dim i As Int16 = 0
		For Each aItem As DataListItem In dlImage.Items
			i += 1
			Dim newImage As New clsCardImageField
			With newImage
				.imageUrl = CType(aItem.FindControl("hidUrl"), HiddenField).Value
				.ContentType = CType(aItem.FindControl("hidContentType"), HiddenField).Value
				.PreviousName = CType(aItem.FindControl("txtImgName"), Label).Text
				.ImageSize = CType(aItem.FindControl("hidImageSize"), HiddenField).Value
				.Ready2Print = CType(aItem.FindControl("hidReadyToPrint"), HiddenField).Value


				' ======== ����Ӥѭ               
				.PositionTop = 0.0
				.PositionLeft = 0.0
				.zIndex = i
				.width = 0
				.Height = 0
				.CreateBy = Me.objSessionVar.ModelUser.UserID
				.CreateOn = Now
				.UpdateBy = Me.objSessionVar.ModelUser.UserID
				.UpdateOn = Now

				newImages.Add(newImage)
			End With
		Next

		oCard.ImageFields = newImages
	End Sub

	Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
		Me.GetImageField()
		Me.GetTextField()
		oCard.UploadFlag = "Y"
		Session("oCard") = oCard 'oUCard
		Page.Response.Redirect("DetailPaperUpload.aspx")
	End Sub

End Class
