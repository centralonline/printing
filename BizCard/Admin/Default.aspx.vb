﻿Imports CoreBaseII

Partial Class Admin_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim FolderName As String = Request("FolderName")
        Dim objEngine As New clsOrderEngine
        Dim Result As New clsResultII

        If FolderName = "" Then
            Response.Write("กรุณาเลือก Folder ที่ต้องการด้วยค่ะ")
            Exit Sub
        End If

        FolderName = Left(FolderName, FolderName.Length - 1)
        Result = objEngine.DeleteFTPuploadfile(FolderName)

        If Result.Flag = True Then
            Response.Write("เปิดสิทธิการใช้งานเรียบร้อยแล้วค่ะ")
        Else
            Response.Write("Error!!! กรุณาลองใหม่อีกครั้ง")
        End If

    End Sub

End Class
