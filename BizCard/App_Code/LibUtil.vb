Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.Object
Imports System.IO
Imports System.Diagnostics
Imports System.Text
Imports System.Collections.Queue
Imports System
Imports System.Web
Imports System.Collections
Imports System.Collections.Generic


#Region "Add By Draft.."

Public Class MyWindow
    Private Shared m_executingPages As Hashtable = New Hashtable

    Private Sub New()
    End Sub

    Public Shared Sub ShowModalDialog(ByVal pURL As String, ByVal width As Integer, ByVal height As Integer)
        If Not m_executingPages.Contains(HttpContext.Current.Handler) Then
            Dim executingPage As Page = CType(HttpContext.Current.Handler, Page)
            If Not (executingPage Is Nothing) Then
                Dim messageQueue As Queue = New Queue
                messageQueue.Enqueue(pURL & "#" & width & "#" & height)
                m_executingPages.Add(HttpContext.Current.Handler, messageQueue)
                AddHandler executingPage.Unload, AddressOf ExecutingPage_Unload
            End If
        Else
            Dim queue As Queue = CType(m_executingPages(HttpContext.Current.Handler), Queue)
            queue.Enqueue(pURL)
        End If
    End Sub

    Private Shared Sub ExecutingPage_Unload(ByVal sender As Object, ByVal e As EventArgs)
        Dim queue As Queue = CType(m_executingPages(HttpContext.Current.Handler), Queue)
        If Not (queue Is Nothing) Then
            Dim sb As StringBuilder = New StringBuilder
            Dim iMsgCount As Integer = queue.Count
            sb.Append("<script language='javascript'>" & vbCrLf)
            sb.Append(" //<xbegin>" & vbCrLf)
            Dim sMsg As String
            While System.Math.Max(System.Threading.Interlocked.Decrement(iMsgCount), iMsgCount + 1) > 0
                sMsg = CType(queue.Dequeue, String)
                sMsg = sMsg.Replace("" & Microsoft.VisualBasic.Chr(10) & "", "\n")
                sMsg = sMsg.Replace("""", "'")
                sb.Append("var pop = window.showModalDialog( '" + sMsg.Split("#")(0) + "','','dialogHeight:" & sMsg.Split("#")(2) & "px,dialogWidth:" & sMsg.Split("#")(1) & "');")
                sb.Append(" pop ")
            End While
            sb.Append(" //<xend>" & vbCrLf)
            sb.Append("</script>" & vbCrLf)
            m_executingPages.Remove(HttpContext.Current.Handler)
            HttpContext.Current.Response.Write(sb.ToString)
        End If
    End Sub
End Class

Public Class MessageBox
    Private Shared m_executingPages As Hashtable = New Hashtable

    Private Sub New()
    End Sub

    Public Shared Sub Show(ByVal sMessage As String)
        'sMessage = sMessage.Replace("\", "\\")
        'sMessage = sMessage.Replace(vbCrLf, "\n")
        'sMessage = sMessage.Replace("^n", "\n")
        If Not m_executingPages.Contains(HttpContext.Current.Handler) Then
            Dim executingPage As Page = CType(HttpContext.Current.Handler, Page)
            If Not (executingPage Is Nothing) Then
                Dim messageQueue As Queue = New Queue
                messageQueue.Enqueue(sMessage)
                m_executingPages.Add(HttpContext.Current.Handler, messageQueue)
                AddHandler executingPage.Unload, AddressOf ExecutingPage_Unload
            End If
        Else
            Dim queue As Queue = CType(m_executingPages(HttpContext.Current.Handler), Queue)
            queue.Enqueue(sMessage)
        End If
    End Sub

    Private Shared Sub ExecutingPage_Unload(ByVal sender As Object, ByVal e As EventArgs)
        Dim queue As Queue = CType(m_executingPages(HttpContext.Current.Handler), Queue)
        If Not (queue Is Nothing) Then
            Dim sb As StringBuilder = New StringBuilder
            Dim iMsgCount As Integer = queue.Count
            sb.Append("<script language='javascript'>" & vbCrLf)
            sb.Append(" //<xbegin>" & vbCrLf)
            Dim sMsg As String
            While System.Math.Max(System.Threading.Interlocked.Decrement(iMsgCount), iMsgCount + 1) > 0
                sMsg = CType(queue.Dequeue, String)
                sMsg = sMsg.Replace("" & Microsoft.VisualBasic.Chr(10) & "", "\n")
                sMsg = sMsg.Replace("""", "'")
                sb.Append("alert( """ + sMsg + """ );")
            End While
            sb.Append(" //<xend>" & vbCrLf)
            sb.Append("</script>" & vbCrLf)
            m_executingPages.Remove(HttpContext.Current.Handler)
            HttpContext.Current.Response.Write(sb.ToString)
        End If
    End Sub
End Class

'Public Class MyURLChars
'    Public Const ChrQuestionMark As String = "^1"
'    Public Const ChrEqual As String = "^2"
'    Public Const ChrAnd As String = "^3"

'    Public Shared Function ReplaceMyOperator(ByVal str As String)
'        str = str.Replace("?", MyURLChars.ChrQuestionMark)
'        str = str.Replace("=", MyURLChars.ChrEqual)
'        str = str.Replace("&", MyURLChars.ChrAnd)
'        Return str
'    End Function

'    Public Shared Function UnReplaceMyOperator(ByVal str As String)
'        str = str.Replace(MyURLChars.ChrQuestionMark, "?")
'        str = str.Replace(MyURLChars.ChrEqual, "=")
'        str = str.Replace(MyURLChars.ChrAnd, "&")
'        Return str
'    End Function

'End Class

Public Class MyDateTime

    Public Shared Function ToEnUsYMD(ByVal InputDate As DateTime) As String
        Return InputDate.ToString("yyyy-MM-dd", New System.Globalization.CultureInfo("en-US"))
    End Function

    Public Shared Function ToEnUsYMDHM(ByVal InputDate As DateTime) As String
        Return InputDate.ToString("yyyy-MM-dd hh:mm", New System.Globalization.CultureInfo("en-US"))
    End Function

    Public Shared Function ToEnUsDMY(ByVal InputDate As DateTime) As String
        Return InputDate.ToString("dd-MM-yyyy", New System.Globalization.CultureInfo("en-US"))
    End Function

    Public Shared Function ToEnUsDMYHM(ByVal InputDate As DateTime) As String
        Return InputDate.ToString("dd-MM-yyyy hh:mm", New System.Globalization.CultureInfo("en-US"))
    End Function

    Public Shared Function ToDate(ByVal ddMMyyyy As String) As DateTime
        Dim TempStr() As String = ddMMyyyy.Split(New Char() {"-", "/", ","})
        Return New Date(TempStr(2), TempStr(1), TempStr(0))
    End Function

End Class

Public Class MyLog
    Public Shared Sub Write(ByVal detail As String)
        Dim str As String = ""
        str += vbCrLf & vbCrLf & DateTime.Now & "  :  " & vbCrLf & detail
        Dim TempFilePath As String = AppDomain.CurrentDomain.BaseDirectory 'ConfigurationManager.AppSettings("MainFolder")
        My.Computer.FileSystem.WriteAllText(TempFilePath & "log\Log.txt", str, True)
    End Sub

    Public Shared Sub WriteErrorLog(ByVal detail As String)
        Dim str As String = ""
        str += vbCrLf & vbCrLf & DateTime.Now & "  :  " & vbCrLf & detail
        Dim TempFilePath As String = AppDomain.CurrentDomain.BaseDirectory 'ConfigurationManager.AppSettings("MainFolder")
        My.Computer.FileSystem.WriteAllText(TempFilePath & "log\ErrorLog.txt", str, True)
    End Sub

    Public Shared Sub WriteProcessDurationTime(ByVal StartTime As Date, ByVal ClassMethodName As String)
        'Dim str As String = ""
        'str += vbCrLf & vbCrLf & DateTime.Now & " : ProcessDurationTime of " & ClassMethodName & " = " & vbCrLf & String.Format("{0}", Now.Subtract(StartTime).Duration)
        'Dim TempFilePath As String = ConfigurationManager.AppSettings("MainFolder")
        'My.Computer.FileSystem.WriteAllText(TempFilePath & "\ProcessDurationTime.txt", str, True)
    End Sub
End Class

Public Class MyMail
    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If (inputEmail Is Nothing) Then
            Return False
        End If
        Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}"
        strRegex += "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\"
        strRegex += ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
        Dim re As Regex = New Regex(strRegex)
        If (re.IsMatch(inputEmail)) Then
            Return True
        Else
            Return False
        End If
    End Function
End Class

Public Class MyParams
    Private dict As New Dictionary(Of String, String)

    Public Sub New(ByVal strParams As String)
        Dim strs() As String = strParams.Split("&")
        For Each s As String In strs
            Dim keyvalue() As String = s.Split("=")
            If (keyvalue.Length = 2) Then
                dict.Add(keyvalue(0), keyvalue(1))
            End If
        Next
    End Sub

    Public Sub New(ByVal strParams As String, ByVal QueChar As String, ByVal EquChar As String, ByVal AndChar As String)
        strParams = strParams.Replace(QueChar, "?")
        strParams = strParams.Replace(EquChar, "=")
        strParams = strParams.Replace(AndChar, "&")

        Dim strs() As String = strParams.Split("&")
        For Each s As String In strs
            Dim keyvalue() As String = s.Split("=")
            If (keyvalue.Length = 2) Then
                dict.Add(keyvalue(0), keyvalue(1))
            End If
        Next
    End Sub

    Public Function SetValue(ByVal key As String, ByVal value As String) As String
        If (dict.ContainsKey(key)) Then
            dict.Remove(key)
        End If
        dict.Add(key, value)
        Return Me.GetFullHiddenStr()
    End Function

    Public Function GetValue(ByVal key As String) As String
        If (dict.ContainsKey(key)) Then
            Return dict.Item(key)
        Else
            Return Nothing
        End If
    End Function

    Public Function Remove(ByVal key As String) As String
        If (dict.ContainsKey(key)) Then
            Me.dict.Remove(key)
        End If
        Return Me.GetFullHiddenStr()
    End Function

    Public Function ContainsKey(ByVal key As String) As Boolean
        Return dict.ContainsKey(key)
    End Function

    Public ReadOnly Property FullHiddenString() As String
        Get
            Return Me.GetFullHiddenStr()
        End Get
    End Property

    Private Function GetFullHiddenStr() As String
        Dim result As String = ""
        Dim en As IDictionaryEnumerator = dict.GetEnumerator()
        en.Reset()
        While en.MoveNext()
            result += en.Key & "=" & en.Value & "&"
        End While
        Return result
    End Function

End Class

Public Class MyURLChars
    Public Const ChrQuestionMark As String = "^1"
    Public Const ChrEqual As String = "^2"
    Public Const ChrAnd As String = "^3"
    Public Const ChrSingleQuote As String = "^7"

    Public Shared Function ReplaceMyOperator(ByVal str As String) As String
        str = str.Replace("?", MyURLChars.ChrQuestionMark)
        str = str.Replace("=", MyURLChars.ChrEqual)
        str = str.Replace("&", MyURLChars.ChrAnd)
        Return str
    End Function

    Public Shared Function UnReplaceMyOperator(ByVal str As String) As String
        str = str.Replace(MyURLChars.ChrQuestionMark, "?")
        str = str.Replace(MyURLChars.ChrEqual, "=")
        str = str.Replace(MyURLChars.ChrAnd, "&")
        Return str
    End Function

End Class

Public Class MyDefault

    Public Shared Function IsNumeric(ByVal obj As Object, ByVal DefaultValue As Object) As Object
        If (obj Is Nothing OrElse Information.IsDBNull(obj) OrElse Not Information.IsNumeric(obj)) Then
            Return DefaultValue
        End If
        Return obj
    End Function

    Public Shared Function IsNullToDefault(ByVal obj As Object, ByVal DefaultValue As Object) As Object
        If (obj Is Nothing OrElse Information.IsDBNull(obj)) Then
            Return DefaultValue
        End If
        Return obj
    End Function

End Class

Public Class MyLang
    Public Shared Function Show(ByVal Thai As String, ByVal Eng As String) As String
        Dim Session As HttpSessionState = HttpContext.Current.Session
        If (Session("lang") IsNot Nothing AndAlso Session("lang") = "th") Then
            Return Thai
        End If
        If (Session("lang") IsNot Nothing AndAlso Session("lang") = "en") Then
            Return Eng
        End If
        Return Thai
    End Function
End Class


#End Region

#Region "Add By K@i"
Public Class clsMyUtil
    Public Shared Function ReplaceSingleCode(ByVal TempString As String, ByVal StrToHTML As Boolean) As String
        If TempString Is Nothing Then
            Return ""
        End If
        If StrToHTML = True Then
            TempString = TempString.Replace("'", "&rsquo;")
        Else
            TempString = TempString.Replace("&rsquo;", "'")
        End If
        Return TempString
    End Function

    Public Shared Function RandomPassword(ByVal Length As Integer) As String
        Dim stringPassword As String = Nothing
        Dim arrPossibleChars As Char() = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray()
        Dim rand As System.Random = New Random
        Dim i As Integer = 0
        For i = 0 To Length
            Dim intRandom As Integer = rand.Next(arrPossibleChars.Length)
            stringPassword = stringPassword & arrPossibleChars(intRandom).ToString()
        Next
        Return Date.Now.Year & Date.Now.Month & Date.Now.Day & stringPassword
        'Return Date.Now.ToString.Substring(0, 9).Replace("/", "") & Date.Now.TimeOfDay.ToString.Substring(0, 8).Replace(":", "") & stringPassword
    End Function

    Public Shared Function ChangeFormatDate(ByVal strDate As String) As String
        Dim sDate() As String = strDate.Split(" ")
        sDate = sDate(0).Split("/")
        Return sDate(1) & "/" & sDate(0) & "/" & sDate(2)
    End Function

    ''' <summary>
    ''' Convert mm to Pixel
    ''' </summary>
    ''' <param name="mm"></param>
    ''' <param name="dpi">dot / Inch</param>
    ''' <returns>Integer</returns>
    ''' <remarks>Return round </remarks>
    Public Shared Function mm2Pxs(ByVal mm As Double, ByVal dpi As Integer) As Single
        Return CInt(mm * (dpi / 25.4))
    End Function
    Public Shared Function mm2Pxs(ByVal mm As Double, ByVal Zoom As Double, ByVal dpi As Integer) As Single
        'If (Zoom = 1.5) Then
        Return CInt(Format((mm * Zoom), "0.00") * Format((dpi / 25.4), "0.00")) - 8
        ' Else
        ' Return CInt(mm * (dpi / 25.4))
        ' End If

    End Function
    Public Shared Function mm2Px(ByVal mm As Double, ByVal dpi As Integer) As Integer
        Return Math.Round(mm * (dpi / 25.4))
    End Function

    Public Shared Function GetImageSize(ByVal OldSize As clsImageSize, ByVal FixSize As clsImageSize) As clsImageSize
        Dim TempSize As clsImageSize = OldSize
        If OldSize.Height > FixSize.Height OrElse OldSize.Width > FixSize.Width Then
            If OldSize.Width > FixSize.Width Then
                TempSize.Height = Math.Ceiling((FixSize.Width * OldSize.Height) / OldSize.Width)
                TempSize.Width = FixSize.Width
            End If
            If TempSize.Height > FixSize.Height Then
                TempSize.Width = Math.Ceiling((FixSize.Height * TempSize.Width) / TempSize.Height)
                TempSize.Height = FixSize.Height
            End If
        End If
        Return TempSize
    End Function

    Public Shared Function GetHtmlFromColor(ByVal oColor As System.Drawing.Color) As String
        Return System.Drawing.ColorTranslator.ToHtml(oColor)
    End Function

    Public Shared Function GetColorFromHtml(ByVal HtmlCode As String) As System.Drawing.Color
        Return System.Drawing.ColorTranslator.FromHtml(HtmlCode)
    End Function
End Class
#End Region
