Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Imports System.IO

Public Class clsCrypto
    '8 bytes randomly selected for both the Key and the Initialization Vector
    'the IV is used to encrypt the first block of text so that any repetitive 
    'patterns are not apparent 
    Private Shared KEY_64() As Byte = {42, 54, 93, 156, 78, 4, 218, 15}
    Private Shared IV_64() As Byte = {55, 103, 126, 79, 36, 84, 167, 3}


    '24 byte or 192 bit key and IV for TripleDES
    Private Shared KEY_192() As Byte = {42, 45, 93, 156, 78, 4, 218, 15, _
    15, 167, 44, 80, 26, 250, 155, 112, _
    2, 94, 11, 204, 119, 35, 184, 197}
    Private Shared IV_192() As Byte = {55, 103, 126, 79, 36, 84, 167, 3, _
    42, 5, 62, 83, 184, 7, 209, 13, _
    145, 23, 200, 58, 173, 10, 121, 222}

    'Standard DES encryption
    Public Shared Function Encrypt(ByVal value As String) As String

        If value <> "" Then
            Dim cryptoProvider As DESCryptoServiceProvider = _
            New DESCryptoServiceProvider()
            Dim ms As MemoryStream = New MemoryStream()
            Dim cs As CryptoStream = _
            New CryptoStream(ms, cryptoProvider.CreateEncryptor(KEY_64, IV_64), _
            CryptoStreamMode.Write)
            Dim sw As StreamWriter = New StreamWriter(cs)

            sw.Write(value)
            sw.Flush()
            cs.FlushFinalBlock()
            ms.Flush()

            'convert back to a string
            Return Convert.ToBase64String(ms.GetBuffer(), 0, CInt(ms.Length))
        End If
        Return Nothing
    End Function


    'Standard DES decryption
    Public Shared Function Decrypt(ByVal value As String) As String
        If value <> "" Then
            Try
                Dim cryptoProvider As DESCryptoServiceProvider = _
                New DESCryptoServiceProvider()

                'convert from string to byte array
                Dim buffer() As Byte = Convert.FromBase64String(value)
                Dim ms As MemoryStream = New MemoryStream(buffer)
                Dim cs As CryptoStream = _
                New CryptoStream(ms, cryptoProvider.CreateDecryptor(KEY_64, IV_64), _
                CryptoStreamMode.Read)
                Dim sr As StreamReader = New StreamReader(cs)
                Return sr.ReadToEnd()
            Catch ex As Exception
                Throw New Exception("My Security Exception")
            End Try
        End If
        Return Nothing
    End Function


    'Add By Surapon
    Public Shared Function Data_Hex_Asc(ByRef Data As String) As String
        '(This Function for Converting data into Hex )
        'Dim Data1 As String = ""
        Dim sData As String = ""
        Dim Data1 As String = ""
        While (Data.Length > 0)
            '    //first take two hex value using substring.
            '    //then convert Hex value into ascii.
            '    //then convert ascii value into character.
            Data1 = System.Convert.ToChar(System.Convert.ToUInt32(Data.Substring(0, 2), 16)).ToString()
            sData = sData + Data1
            Data = Data.Substring(2, Data.Length - 2)
        End While

        Return sData

    End Function
    Public Shared Function Hex_Data_Asc(ByRef Data As String) As String
        '    //first take each charcter using substring.
        '    //then convert character into ascii.
        '    //then convert ascii value into Hex Format

        Dim sValue As String = ""
        Dim sHex As String = ""

        While (Data.Length > 0)
            sValue = Conversion.Hex(Strings.Asc(Data.Substring(0, 1).ToString()))
            Data = Data.Substring(1, Data.Length - 1)
            sHex = sHex + sValue
        End While

        Return sHex
    End Function








    'public string Data_Asc_Hex(ref string Data)
    '{
  
    '}


End Class
