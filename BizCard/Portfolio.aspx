﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Portfolio.aspx.vb" Inherits="Portfolio" %>
<%@ Register Src="uc/ucFooter.ascx" TagName="ucFooter" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ตัวอย่างผลงานการพิมพ์ trendyprint.net</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Portfolio ตัวอย่างผลงานการพิมพ์ นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว" />
<meta name="keywords" content="Portfolio, ตัวอย่างผลงานการพิมพ์, นามบัตร, หัวจดหมาย, การ์ด, โปสเตอร์, ใบปลิว, ราคางานพิมพ์, trendyprint.net" />
<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/trendyprint.css" />
<link rel="stylesheet" type="text/css" href="css/print.css" />
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />


<script type="text/javascript" src="js/jsProductSearch.js"></script>
<script type="text/javascript" src="js/jsUtil.js"></script>
<script type="text/javascript" src="js/NoIEActivate.js"></script>
<script type="text/javascript" src="js/AC_RunActiveContent.js"></script>    
<script src="js/jquery-1.3.2.js" type="text/javascript"></script>

<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="js/lightbox.js"></script>


<style type="text/css">
        .style1 { color: #FF0000;}
		.column-register {text-align:left;}
		.clear { clear:both;}
        </style>
    
</head>
<body>

<div id="container" align="center">
<form id="form1" runat="server">
	<div style="padding: 30px; margin:auto; width: 920px; border-bottom: 1px solid #999;">    
    	<a href="<%=ResolveUrl("~/main.aspx")%>" target="_self"><img src="images/logo-trendyprint.png" border="0" alt="Officemate Printing Solution" title="นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์" /></a>
    		<div style="float:right; width:100px; vertical-align:bottom; text-align:right;">< <a href="main.aspx">กลับหน้าแรก</a></div>
    	</div>
    	<div align="left" style="padding: 30px; margin:auto; width: 920px;"> 
   	<h1>ตัวอย่างผลงานการพิมพ์</h1>
	<div class="port-photo">
	<a href="images/port/L/039.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/039.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/038.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/038.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>

	<div class="port-photo">
	<a href="images/port/L/037.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/037.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/036.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/036.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/035.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/035.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/034.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/034.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/033.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/033.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>

	<div class="port-photo">
	<a href="images/port/L/032.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/032.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/031.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/031.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/030.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/030.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/029.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/029.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>

	<div class="port-photo">
	<a href="images/port/L/028.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/028.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/027.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/027.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/026.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/026.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/025.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/025.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>

	<div class="port-photo">
	<a href="images/port/L/024.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/024.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/023.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/023.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/022.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/022.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/021.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/021.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/020.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/020.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>

	<div class="port-photo">
	<a href="images/port/L/019.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/019.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/018.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/018.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/017.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/017.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/016.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/016.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/015.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/015.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>

	<div class="port-photo">
	<a href="images/port/L/014.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/014.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/013.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/013.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/012.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/012.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/011.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/011.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>

	<div class="port-photo">
	<a href="images/port/L/010.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/010.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/009.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/009.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/008.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/008.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/007.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/007.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/006.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/005.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>

	<div class="port-photo">
	<a href="images/port/L/005.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/005.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/004.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/004.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/003.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/003.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/002.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/002.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<div class="port-photo">
	<a href="images/port/L/001.jpg" target="_blank" rel="lightbox[showreel]"><img src="images/port/S/001.jpg" alt="ดูภาพขยาย" border="0"></a>	
	</div>
	<p class="clear">
    	
    	</div>
	<br class="clear" />
	
	
</form>

<uc2:ucFooter ID="UcFooter1" runat="server" />  
</div>

</body>
</html>
