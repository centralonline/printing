﻿Imports CoreBaseII
Imports UserEngineTrendyPrint
Partial Class ajCheckEditProfile
    Inherits clsHTTPBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'ข้อมูลทั่วไป
        Dim UserName As String = Request("Username")
        Dim NickName As String = Request("NickName")
        Dim Mobile As String = Request("Mobile")

        Dim ObjectUser As New ModelUser

        With ObjectUser
            .UserID = objSessionVar.ModelUser.UserID
            .Password = objSessionVar.ModelUser.Password
            .UserName = UserName
            .NickName = NickName
            .Mobile = Mobile
        End With

        Dim Engine As New UserEngine
        Dim Check As New Boolean
        Check = Engine.EditUserInfo(ObjectUser)

        If Check Then
            Response.Write("บันทึกข้อมูลเรียบร้อยแล้วค่ะ")
        Else
            Response.Write("Error!!! กรุณาลองใหม่อีกครั้งค่ะ")
        End If
    End Sub

End Class
