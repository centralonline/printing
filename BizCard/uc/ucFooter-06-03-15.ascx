﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucFooter.ascx.vb" Inherits="uc_ucFooter" %>
<div id="menu-foot" align="center">
	<div class="foot-column">
		<p>
			<strong>e-Mail Exclusives</strong></p>
		<p>
			กรุณากรอก e-Mail Address ของท่าน</p>
		<p>
			<input id="radOK" type="radio" name="SubscribeGroup" checked="checked" value="1" onclick="return radOK_onclick()" />ต้องการรับข่าวสาร
			Promotion จาก Printing Solution<br />
			<input id="radNO" type="radio" name="SubscribeGroup" value="0" />ยกเลิกรับข่าวสารจาก
			Printing Solution</p>
		<input id="txtMailAdd" type="text" class="Tloginborder" style="width: 150px; vertical-align: middle;"
			value="enter your email" onclick="if (this.value == 'enter your email') { this.value=''; } " />
		<input id="btnMailAdd" type="button" name="btnMailAdd" value="ok" class="btn-face"
			onclick="return btnMailAdd_onclick()" />
		<br />
	</div>
	<div class="foot-column">
		<p>
			<strong>Print & Delivery Service</strong></p>
		<p>
			<a href="DesignCard.aspx">สั่งทำนามบัตร</a><br>
			<a href="ContactUs.aspx">พิมพ์หัวจดหมาย</a><br>
			<a href="ContactUs.aspx">พิมพ์การ์ด</a><br>
			<a href="ContactUs.aspx">โปสเตอร์ และ ใบปลิว</a><br>
			<a href="Portfolio.aspx">ตัวอย่างผลงาน</a>
		</p>
	</div>
	<div class="foot-column">
		<p>
			<strong>Help</strong></p>
		<p>
			<a href="PriceList.aspx">ราคางานพิมพ์</a><br />
			<a href="Promotion.aspx">โปรโมชั่นงานพิมพ์</a><br />
			<a href="ContactUs.aspx">ติดต่อเรา</a><br />
			<a href="FAQ.aspx">คำถามพบบ่อย</a><br />
			<a href="HowtoOrder.aspx">วิธีการสั่งพิมพ์</a>
		</p>
	</div>
	<div class="foot-column">
		<p>
			<strong>OfficeMate Service</strong></p>
		<p>
			<a href="http://www.OfficeMate.co.th/" target="_blank">OfficeMate.co.th</a><br />
			<a href="http://eprocurement.officemate.co.th/" target="_blank">OfficeMate e-Procurement</a><br />
		</p>
	</div>
</div>
<div id="foot">
	<p>
		&copy; สงวนลิขสิทธิ์ Printing Solution | เว็บไซต์สั่งทำ นามบัตร หัวจดหมาย การ์ด
		โปสเตอร์ ใบปลิว ตรวจสอบราคางานพิมพ์
		<!--BEGIN WEB STAT CODE-->
		<script language="javascript1.1"> __th_page="<%=Request.ServerVariables("SCRIPT_NAME")%>";</script>
		<script language="javascript1.1" src="http://hits.truehits.in.th/data/t0030033.js"></script>
		<noscript>
			<a target="_blank" href="http://truehits.net/stat.php?id=t0030033">
				<img src="http://hits.truehits.in.th/noscript.php?id=t0030033" alt="Thailand Web
Stat" border="0" width="14" height="17"></a> <a target="_blank" href="http://truehits.net/">
	Truehits.net</a>
		</noscript>
		<!-- END WEBSTAT CODE -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-11520015-3']);
			_gaq.push(['_trackPageview']);

			(function () {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

			})();
			$(function () {
				$("#btnMailAdd").click(function () {
					var MailAdd = $("#txtMailAdd").val();
					if (MailAdd == "enter your email") {
						alert('กรุณาระบุ e-Mail ของท่านด้วยค่ะ');
					}
					else {
						var IsGetMail = false;
						var the_value = jQuery('input:radio:checked').val();
						if (the_value == '1') {
							var IsGetMail = true;
						}
						$.ajax({
							type: 'POST',
							url: "CheckeMail.aspx",
							data: { "MailAdd": MailAdd, "IsGetMail": IsGetMail },
							success: function (msg) {
								if (msg != 'ok') {
									alert(msg);
									$("#txtMailAdd").val('');
								}
							}
						});
					}
				});
			});

			function btnMailAdd_onclick() {

			}

			function radOK_onclick() {

			}

		</script>
		<br />
		<strong>บริษัท ออฟฟิศเมท จำกัด ( มหาชน)</strong> เลขที่ 24 ซ. อ่อนนุช 66/1 สวนหลวง
		กรุงเทพ 10250 Printing Solution Contact Center: 02-730-7777 , Fax: 02-763-5555, E-Mail:printing@officemate.co.th
	</p>
</div>
