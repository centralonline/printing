﻿Imports System.Collections.Generic

Partial Class Admin_ManageFlagCard
    Inherits clsHTTPBaseAdmin
	Public listCard As New List(Of clsCard)
	Public PageNo As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CardProvider As New clsCardProvider

		PageNo = Request.QueryString("PageNo")
		Dim Cat As String = Request.Form("Cat")

		If String.IsNullOrEmpty(PageNo) Then
			PageNo = "1"
		End If

		listCard = CardProvider.GetUnActiveCard(PageNo)

    End Sub

End Class
