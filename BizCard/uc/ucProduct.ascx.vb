
Partial Class uc_ucProduct
    'Inherits System.Web.UI.UserControl
    Inherits clsUCBase

    Private CardIdValue As String
    Public Property CardID() As String
        Get
            Return CardIdValue
        End Get
        Set(ByVal value As String)
            CardIdValue = value
        End Set
    End Property
    Private CardNameValue As String
    Public Property CardName() As String
        Get
            Return CardNameValue
        End Get
        Set(ByVal value As String)
            CardNameValue = value
        End Set
    End Property
    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        Try
            MyBase.OnInit(e)
            If Me.Session("oCard") IsNot Nothing Then
                Dim oCard As clsCard = Me.Session("oCard")
                Me.CardID = oCard.CardID
                Me.CardName = oCard.CardName
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lbCardID.Text = "Auto Generate" 'Me.CardID 
        Me.lbCardName.Text = Me.CardName
    End Sub
End Class
