﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucPayMents.ascx.vb" Inherits="uc_ucPayMents" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:UpdatePanel ID="UpdatePanel3" runat="server">
    <ContentTemplate>
        <p class="head-column">
            เลือกประเภทการชำระเงิน</p>
        <img src="<%=ResolveUrl("~/images/icon_atm.jpg")%>" alt="" /><asp:RadioButton ID="rdBank"
            runat="server" Text="ชำระโดยการโอนเงิน เข้าบัญชีธนาคาร <BR> ชื่อบัญชี <b>บริษัท ซีโอแอล จำกัด (มหาชน)</b> (ธนาคาร+เลขที่บัญชี)<BR>เมื่อโอนเงินสำเร็จแล้ว ส่งหลักฐานยืนยันการชำระเงินผ่านช่องทางใดช่องทางหนึ่ง ดังนี้<BR>• สแกนใบ payin หรือ print screen หน้าจอการโอนเงิน(กรณี Online) มาที่ <b>contact@Officemate.co.th</b><BR>• ส่ง Fax สำเนาใบ payin เข้ามาที่ <b>02-763-5555</b>"
            OnCheckedChanged="rdBank_CheckedChanged" AutoPostBack="True" GroupName="xx" Checked="true" />
        <div align="center">
            <table id="bank" width="100%" border="0" cellpadding="2" cellspacing="0" align="center">
                <tr style="background-color: #d9ebba;">
                    <td>
                        <strong>ธนาคาร</strong>
                    </td>
                    <td>
                        <strong>สาขา</strong>
                    </td>
                    <td>
                        <strong>บัญชี</strong>
                    </td>
                    <td>
                        <strong>เลขที่บัญชี</strong>
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <img src="<%=ResolveUrl("~/images/logobank_ktc.gif")%>" alt="" />กสิกรไทย
                    </td>
                    <td>
                        เอกมัย
                    </td>
                    <td>
                        ออมทรัพย์
                    </td>
                    <td>
                        059-2-75825-3
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <img src="<%=ResolveUrl("~/images/logobank_bb.gif")%>" alt="" />กรุงเทพ
                    </td>
                    <td>
                        ซีคอนสแควร์
                    </td>
                    <td>
                        ออมทรัพย์
                    </td>
                    <td>
                        232-0-22516-8
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <img src="<%=ResolveUrl("~/images/logobank_scb.gif")%>" alt="" />ไทยพาณิชย์
                    </td>
                    <td>
                        ถนนศรีนครินทร์-อ่อนนุช
                    </td>
                    <td>
                        ออมทรัพย์
                    </td>
                    <td>
                        104-2-20869-4
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <img src="<%=ResolveUrl("~/images/logobank_kt.gif")%>" alt="" />กรุงไทย
                    </td>
                    <td>
                        พัฒนาการ 65
                    </td>
                    <td>
                        ออมทรัพย์
                    </td>
                    <td>
                        188-1-01683-8
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <img src="<%=ResolveUrl("~/images/logobank_ks.gif")%>" alt="" />กรุงศรีอยุธยา
                    </td>
                    <td>
                        สี่แยกศรีนครินทร์-อ่อนนุช
                    </td>
                    <td>
                        ออมทรัพย์
                    </td>
                    <td>
                        188-1-37829-9
                    </td>
                </tr>
            </table>
        </div>
        <%If ProvinceType = 1 Then%>
        <br />
        <br />
        <img src="<%=ResolveUrl("~/images/icon_busoffice_03.jpg")%>" alt="" /><asp:RadioButton ID="rdDestination"
            runat="server" Text="ชำระเงินด้วยเงินสดกับพนักงานจัดส่งของ Officemate (cash-on-delivery)<BR>หมายเหตุ: เฉพาะลูกค้าที่อยู่ในพื้นที่การจัดส่งเท่านั้น(กรุงเทพฯ ฉะเชิงเทรา ชลบุรี นนทบุรี นครปฐม ระยอง ปทุมธานี อยุธยา สมุทรปราการ สมุทรสาคร)"
            OnCheckedChanged="rdDestination_CheckedChanged" AutoPostBack="True" GroupName="xx" />
        <br />
        <br />
        <img src="<%=ResolveUrl("~/images/logobank_medc.gif")%>" alt="" /><asp:RadioButton ID="rdCredit" runat="server"
            Text="ชำระเงินด้วยบัตรเครดิตกับพนักงานจัดส่งของ Officemate<BR>ชำระเงินด้วยบัตรเครดิตกับพนักงานจัดส่งของ Officemate เมื่อได้รับสินค้า (Mobile EDC)<BR>หมายเหตุ: เฉพาะลูกค้าที่อยู่ในพื้นที่การจัดส่งเท่านั้น(กรุงเทพฯ ฉะเชิงเทรา ชลบุรี นนทบุรี นครปฐม ระยอง ปทุมธานี อยุธยา สมุทรปราการ สมุทรสาคร)"
            OnCheckedChanged="rdCredit_CheckedChanged" AutoPostBack="True" GroupName="xx" />
        <%End If%>
    </ContentTemplate>
</asp:UpdatePanel>