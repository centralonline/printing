﻿Imports Microsoft.VisualBasic

Public Class ModelShipping
#Region "Create Property Fields"
	Private ShipIDValue As String
	Public Property ShipID() As String
		Get
			Return ShipIDValue
		End Get
		Set(ByVal value As String)
			ShipIDValue = value
		End Set
	End Property
	Private ShipContactorValue As String
	Public Property ShipContactor() As String
		Get
			Return ShipContactorValue
		End Get
		Set(ByVal value As String)
			ShipContactorValue = value
		End Set
	End Property

    Private ShipAddr1Value As String
    Public Property ShipAddr1() As String
        Get
            Return ShipAddr1Value
        End Get
        Set(ByVal value As String)
            ShipAddr1Value = value
        End Set
    End Property

    Private ShipAddr2Value As String
    Public Property ShipAddr2() As String
        Get
            Return ShipAddr2Value
        End Get
        Set(ByVal value As String)
            ShipAddr2Value = value
        End Set
    End Property

    Private ShipAddr3Value As String
    Public Property ShipAddr3() As String
        Get
            Return ShipAddr3Value
        End Get
        Set(ByVal value As String)
            ShipAddr3Value = value
        End Set
    End Property

    Private ShipAddr4Value As String
    Public Property ShipAddr4() As String
        Get
            Return ShipAddr4Value
        End Get
        Set(ByVal value As String)
            ShipAddr4Value = value
        End Set
    End Property

    Private ShipPhoneNoValue As String
    Public Property ShipPhoneNo() As String
        Get
            Return ShipPhoneNoValue
        End Get
        Set(ByVal value As String)
            ShipPhoneNoValue = value
        End Set
    End Property

	Private ShipProvinceValue As String
	Public Property ShipProvince() As String
		Get
			Return ShipProvinceValue
		End Get
		Set(ByVal value As String)
			ShipProvinceValue = value
		End Set
	End Property

	Private ShipZipCodeValue As String
	Public Property ShipZipCode() As String
		Get
			Return ShipZipCodeValue
		End Get
		Set(ByVal value As String)
			ShipZipCodeValue = value
		End Set
	End Property

	Private ShipRemarkValue As String
	Public Property ShipRemark() As String
		Get
			Return ShipRemarkValue
		End Get
		Set(ByVal value As String)
			ShipRemarkValue = value
		End Set
	End Property

#End Region
End Class


