﻿
Partial Class Admin_ajEditImageField
    Inherits clsHTTPBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'TBBCImageFields
        Dim CardID As String = Request("CardID")
        Dim ImageUrl2 As String = Request("ImageUrl2")
        Dim PositionTop2 As String = Request("PositionTop2")
        Dim PositionLeft2 As String = Request("PositionLeft2")
        Dim zIndex2 As String = Request("zIndex2")
        Dim Width2 As String = Request("Width2")
        Dim Height2 As String = Request("Height2")
        Dim ImageSize2 As String = Request("ImageSize2")
        Dim PreviousName2 As String = Request("PreviousName2")

        Dim ObjImageFields As New clsCardImageField

        With ObjImageFields
            .CardID = CardID
            .imageUrl = ImageUrl2
            .PositionTop = PositionTop2
            .PositionLeft = PositionLeft2
            .zIndex = zIndex2
            .width = Width2
            .Height = Height2
            .ImageSize = ImageSize2
            .PreviousName = PreviousName2
        End With

        Dim LibCard As New clsCardEngine
        Dim Complete As Boolean = LibCard.EditImageFields(ObjImageFields)

        If Complete = False Then
            Response.Write("Error!!! กรุณาลองใหม่อีกครั้งค่ะ")
        Else
            Response.Write("แก้ไขข้อมูลเรียบร้อยแล้วค่ะ")
        End If

    End Sub
End Class
