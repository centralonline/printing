﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucCustomer.ascx.vb" Inherits="uc_ucCustomer" %>
<p class="head-column">
    ข้อมูลลูกค้าและใบกำกับภาษี</p>
<table>
    <tr>
        <td style="width:150px;">
            รหัสลูกค้า:
        </td>
        <td>
            <asp:Label ID="lbCustID" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            ที่อยู่ใบกำกับภาษี:
        </td>
        <td>
            <asp:Label ID="lbInvAddr1" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:Label ID="lbInvAddr2" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:Label ID="lbInvAddr3" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:Label ID="lbInvAddr4" runat="server"></asp:Label>
        </td>
    </tr>
</table>
