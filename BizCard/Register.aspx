﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Register.aspx.vb" Inherits="Register" %>

<%@ Register Src="uc/ucFooter.ascx" TagName="ucFooter" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>REGISTER - นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/trendyprint.css" />
    <link rel="stylesheet" type="text/css" href="css/print.css" />

    <script src="js/jquery-1.3.2.js" type="text/javascript"></script>

    <script src="js/jquery.metadata.js" type="text/javascript"></script>

    <script src="js/jquery.validate.min.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function() {

        $("#RegisterForm").validate({
            rules: {
            txtrepassword: {
				required: true,
				maxlength: 50,
				equalTo: "#txtpassword"
			},
			txtremail: {
				required: true,
				maxlength: 50,
				equalTo: "#txtemail"
			}
			},
            messages: {
                txtfullname: {
                    required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                    maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 100 ตัว ค่ะ!</span>'
                },
                txtnickname: {
                    required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                    maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 100 ตัว ค่ะ!</span>'
                },
                txtmobile: {
                    required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                    minlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลให้ครบ 10 ตัว ค่ะ!</span>'
                },
                txtemail: {
                    required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                    maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 100 ตัว ค่ะ!</span>',
                    email: '<span style="color:red;" >&nbsp;รูปแบบอีเมล์ไม่ถูกต้อง!</span>'
                },
                txtremail: {
                    required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                    maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 100 ตัว ค่ะ!</span>',
                    email: '<span style="color:red;" >&nbsp;รูปแบบอีเมล์ไม่ถูกต้อง!</span>',
                    equalTo: '<span style="color:red;" >&nbsp;กรุณาใส่ e-mail ให้ตรงกันด้วย ค่ะ!</span>'
                },
                txtpassword:{
                         required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                         maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 50 ตัว ค่ะ!</span>'
                },
                txtrepassword:{
                         required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                         maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 50 ตัว ค่ะ!</span>',
                         equalTo: '<span style="color:red;" >&nbsp;กรุณาใส่ password ให้ตรงกันด้วย ค่ะ!</span>'

               }
            }
        });
        });
        
        function SubmitForm() {

            $("#RegisterForm").validate({
                rules: {
                txtrepassword: {
                    required: true,
                    maxlength: 50,
                    equalTo: "#txtpassword"
                },
                txtremail: {
				    required: true,
				    maxlength: 50,
				    equalTo: "#txtemail"
			    }
                },
                messages: {
                    txtfullname: {
                        required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                        maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 100 ตัว ค่ะ!</span>'
                    },
                    txtnickname: {
                        required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                        maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 100 ตัว ค่ะ!</span>'
                    },
                    txtmobile: {
                        required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                        minlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลให้ครบ 10 ตัว ค่ะ!</span>',
                        maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 10 ตัว ค่ะ!</span>'
                    },
                    txtemail: {
                        required: '<span style="color:red;" >&nbspกรุณาใส่ข้อมูลค่ะ!</span>',
                        maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 100 ตัว ค่ะ!</span>',
                        email: '<span style="color:red;" >&nbsp;รูปแบบอีเมล์ไม่ถูกต้อง!</span>'
                    },
                    txtremail: {
                        required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                        maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 100 ตัว ค่ะ!</span>',
                        email: '<span style="color:red;" >&nbsp;รูปแบบอีเมล์ไม่ถูกต้อง!</span>',
                        equalTo: '<span style="color:red;" >&nbsp;กรุณาใส่ e-mail ให้ตรงกันด้วย ค่ะ!</span>'
                    },
                    txtpassword: {
                        required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                        maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 50 ตัว ค่ะ!</span>'
                    },
                    txtrepassword: {
                        required: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลค่ะ!</span>',
                        maxlength: '<span style="color:red;" >&nbsp;กรุณาใส่ข้อมูลไม่เกิน 50 ตัว ค่ะ!</span>',
                        equalTo: '<span style="color:red;" >&nbsp;กรุณาใส่ password ให้ตรงกันด้วย ค่ะ!</span>'
                    }
                }
            });

            if ($("#RegisterForm").valid() == true) {
                var url = '<%=ResolveUrl("~/ajRegister.aspx")%>';
                var UserID = $("#txtemail").attr('value');
                var Password = $("#txtpassword").attr('value');
                var Name = $("#txtfullname").attr('value');
                var Mobile = $("#txtmobile").attr('value');
                var NickName = $("#txtnickname").attr('value');
                var CheckNews = $("#chkNews").attr("checked");
                $.post(url, { UserID: UserID, Password: Password, Name: Name, Mobile: Mobile, NickName: NickName, CheckNews: CheckNews },
       		            function (data) {
       		            	if (data == "True") {
       		            		alert("ลงทะเบียนเรียบร้อยแล้วค่ะ ท่านสามารถเพิ่มที่อยู่จัดส่งได้ที่  My Account ในส่วนข้อมูลจัดส่งสินค้าค่ะ");
       		            		window.location = '<%=ResolveUrl("~/MyAccount.aspx")%>';
       		            	}
       		            	else if (data == "False") {
       		            		alert("เกิดความผิดพลาด กรุณาลองใหม่อีกครั้งค่ะ");
       		            	}
       		            	else {
       		            		alert(data);
       		            	}
       		            }
                 )
            }
        }
    </script>

    <script type="text/javascript">
       var _gaq = _gaq || [];
           _gaq.push(['_setAccount', 'UA-11520015-3']);
           _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();
    </script>

    <script language="javascript" type="text/javascript">
        function newLocation(){
            window.location.href = "DesignCard.aspx";
        }
    
    </script>

    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .column-register
        {
            text-align: left;
        }
        .clear
        {
            clear: both;
        }
    </style>
</head>
<body>
    <div id="container" align="center">
        <form id="RegisterForm" name="RegisterForm" action="Register.aspx" method="post">
        <input type="hidden" name="HdAction" id="HdAction" value="Accept" />
        <div style="padding: 30px; margin: auto; width: 920px; border-bottom: 1px solid #999;">
            <a href="main.aspx" target="_self">
                <img src="images/logo-trendyprint.png" border="0" alt="trendyprint.net"
                    title="นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์" /></a>
            <div style="float: right; width: 100px; vertical-align: bottom; text-align: right;">
                < <a href="main.aspx">กลับหน้าแรก</a></div>
        </div>
        <div align="left" style="padding: 30px; margin: auto; width: 920px;">
            <h1>
                Member Registration</h1>
            <div id="ctl00_BodyCenter_panAccount" style="">
                <div class="column-register">
                </div>
                <div class="column-register">
                    <b><font color="#ff6600">ข้อมูลการสมัครสมาชิก</font></b><br />
                </div>
                <br class="clear" />
                <div class="column-register">
                    <b>e-Mail Address</b><br />
                </div>
                <div class="column-register">
                    <input name="txtemail" id="txtemail" style="width: 200px;" type="text" class="required email"
                        maxlength="100" />&nbsp; <span class="style1">*</span>&nbsp;<span class="cute">eg.user@company.co.th</span>
                </div>
                <br class="clear" />
                <div class="column-register">
                    <b>Re-enter e-Mail Address</b><br />
                </div>
                <div class="column-register">
                    <input name="txtremail" id="txtremail" style="width: 200px;" type="text" class="required email equalTo"
                        maxlength="100" />&nbsp; <span class="style1">*</span>
                </div>
                <br class="clear" />
                <div class="column-register">
                    <b>สร้างรหัสผ่าน</b>
                </div>
                <div class="column-register">
                    <input name="txtpassword" id="txtpassword" style="width: 200px;" type="password"
                        class="required maxlength" maxlength="50" />&nbsp; <span class="style1">*</span>
                </div>
                <br class="clear" />
                <div class="column-register">
                    <b>ยืนยันรหัสผ่าน</b>
                </div>
                <div class="column-register">
                    <input name="txtrepassword" id="txtrepassword" style="width: 200px;" type="password"
                        class="required maxlength equalTo" maxlength="50" />&nbsp; <span class="style1">*</span>
                </div>
                <br class="clear" />
            </div>
            <hr />
            <div id="ctl00_BodyCenter_panBilling">
                <div class="column-register">
                </div>
                <div class="column-register">
                    <b><font color="#ff6600">ข้อมูลส่วนตัว</font></b><br />
                </div>
                <br class="clear" />
                <div class="column-register">
                    <b>ชื่อเล่น-นามแฝง</b>
                </div>
                <div class="column-register">
                    <input name="txtnickname" id="txtnickname" style="width: 200px;" type="text" maxlength="100" 
                        class="required maxlength"/>&nbsp; <span class="style1">*</span>
                </div>
                <br class="clear" />
                <div class="column-register">
                    <b>ชื่อ-นามสกุล</b>
                </div>
                <div class="column-register">
                    <input name="txtfullname" id="txtfullname" style="width: 200px;" type="text" maxlength="100"
                        class="required maxlength" />&nbsp; <span class="style1">*</span>
                </div>
                <br class="clear" />
                <div class="column-register">
                    <b>โทรศัพท์มือถือ</b>
                </div>
                <div class="column-register">
                    <input name="txtmobile" id="txtmobile" style="width: 200px;" type="text" minlength="10" maxlength="10"
                        class="required maxlength minlength" />&nbsp; <span class="style1">*</span>
                </div>
                <br class="clear" />
                <div class="column-register">
                <br/>
                    <input id="chkNews" type="checkbox" name="chkNews" />&nbsp; <b>ต้องการรับข่าวสารและโปรโมชั่นต่างๆจาก
                        </b>Officemate Printing Solution
                </div>
                <br />
                <br class="clear" />
                <div class="column-register">
                </div>
                <div class="column-register" style="border-top: solid 1px #999999; padding-top: 20px;">
                    <input id="btnAdd" type="button" name="btnAdd" value="Register" onclick="SubmitForm(); return false;"
                        class="btn-face" />
                </div>
            </div>
        </div>
        </form>
        <uc2:ucFooter ID="UcFooter1" runat="server" />
    </div>
</body>
</html>
