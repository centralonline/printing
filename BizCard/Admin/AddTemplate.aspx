﻿<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPageAdmin.master" AutoEventWireup="false"
    CodeFile="AddTemplate.aspx.vb" Inherits="Admin_AddTemplate" Title="Admin AddTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">

    <script language="javascript" type="text/javascript">

        $(document).ready(function() {

            if ("<%=objSessionVar.ThisCardID%>" != "") {
                $("#txtCardID").val("<%=objSessionVar.ThisCardID%>")
                var CardID = "<%=objSessionVar.ThisCardID%>";
                $.post("DisplayAllTextField.aspx", { CardID: CardID },
                function(data) {
                    if (data != "") {
                        $('#dvDisplayAllTextField').html(data);
                    }
                });
            }

            $(".checknum").keypress(function(e) {

                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    alert("กรุณาป้อนข้อมูลเฉพาะตัวเลขเท่านั้น");
                    return false;
                }
            });

            $("#ddRight2Left").click(function() {
                if ($("#ddRight2Left").attr("value") == "Y") {
                    $("#ddAlignCenter").attr("disabled", "disabled");
                }
                else {
                    $("#ddAlignCenter").removeAttr("disabled");
                }
            });

            $("#ddAlignCenter").click(function() {
                if ($("#ddAlignCenter").attr("value") == "Y") {
                    $("#ddRight2Left").attr("disabled", "disabled");
                }
                else {
                    $("#ddRight2Left").removeAttr("disabled");
                }
            });

            $("#btnCancel").click(function() {
                window.location.href = "Main.aspx";
            });

            $("#btnAdd").click(function() {
                if ($("#txtFieldName").val() == "" || $("#txtPositionTop").val() == "" || $("#txtPositionLeft").val() == "" || $("#txtForeColor").val() == "" || $("#txtFontFamily").val() == "" || $("#txtFontSize").val() == "") {
                    alert("กรุณากรอก ข้อมูล/ตำแหน่งของข้อมูลบนCard ให้ครบก่อนAddด้วยค่ะ");
                    return false;
                }

                var Seq = $("#txtSeq").val();
                var FieldName = $("#txtFieldName").val();
                var TextValue = $("#txtTextValue").val();
                var PositionTop = $("#txtPositionTop").val();
                var PositionLeft = $("#txtPositionLeft").val();
                var zIndex = $("#txtSeq").val();
                var BackColor = "#FFFFFF";
                var ForeColor = $("#txtForeColor").val();
                var FontFamily = $("#ddFontFamily").attr("value");
                var FontSize = $("#txtFontSize").val();
                var FontBold = $("#ddFontBold").attr("value");
                var FontItalic = $("#ddFontItalic").attr("value");
                var FontUnderline = $("#ddFontUnderline").attr("value");
                var Right2Left = $("#ddRight2Left").attr("value");
                var AlignCenter = $("#ddAlignCenter").attr("value");


                $.ajax({
                    type: 'POST',
                    url: "ajAddTempplate.aspx",
                    data: { "Seq": Seq, "FieldName": FieldName, "TextValue": TextValue, "PositionTop": PositionTop, "PositionLeft": PositionLeft, "zIndex": zIndex, "BackColor": BackColor, "ForeColor": ForeColor, "FontFamily": FontFamily, "FontSize": FontSize, "FontBold": FontBold, "FontItalic": FontItalic, "FontUnderline": FontUnderline, "Right2Left": Right2Left, "AlignCenter": AlignCenter },
                    success: function(msg) {
                        if (msg == 'Error!!! กรุณาลองใหม่อีกครั้งค่ะ') {
                            alert(msg);
                        }
                        else {
                            $("#txtCardID").val(msg)
                            $.post("DisplayAllTextField.aspx", { CardID: msg },
                function(data) {
                    if (data != "") {
                        $('#dvDisplayAllTextField').html(data);
                    }
                });
                        }
                    }
                });

            });

            $("#btnSave").click(function() {
                if ($("#txtCardName1").val() == "" || $("#txtBGImage1").val() == "" || $("#txtBGColor1").val() == "" || $("#txtTemplateImage1").val() == "" || $("#txtWidth1").val() == "" || $("#txtHeight1").val() == "") {
                    alert("กรุณากรอก ข้อมูลCard ให้ครบก่อนSaveด้วยค่ะ");
                    return false;
                }
                if ($("#txtCardID").val() == "") {
                    alert("กรุณาเพิ่ม ข้อมูล/ตำแหน่งของข้อมูลบน Card ด้วยค่ะ");
                    return false;
                }
                var CardName1 = $("#txtCardName1").val();
                var BGImage1 = $("#txtBGImage1").val();
                var BGColor1 = $("#txtBGColor1").val();
                var TemplateImage1 = $("#txtTemplateImage1").val();
                var Width1 = $("#txtWidth1").val();
                var Height1 = $("#txtHeight1").val();
                var Category1 = $("#ddCategory").attr("value");
                //-----------------------------------
                if ($("#chkNoLogo").attr("checked") == true) {
                    var ImageUrl2 = "";
                    var PositionTop2 = "0.0";
                    var PositionLeft2 = "0.0";
                    var Width2 = "0";
                    var Height2 = "0";
                }
                else {
                    if ($("#txtPositionTop2").val() == "" || $("#txtPositionLeft2").val() == "" || $("#txtWidth2").val() == "" || $("#txtHeight2").val() == "") {
                        alert("กรุณากรอก ข้อมูลLogo ให้ครบก่อนSaveด้วยค่ะ");
                        return false;
                    }
                    var ImageUrl2 = "Logo.png";
                    var PositionTop2 = $("#txtPositionTop2").val();
                    var PositionLeft2 = $("#txtPositionLeft2").val();
                    var Width2 = $("#txtWidth2").val();
                    var Height2 = $("#txtHeight2").val();
                }
                var zIndex2 = "1";
                var ImageSize2 = "1";
                var PreviousName2 = "Logo.png";
                var CardID = $("#txtCardID").val();

                $.ajax({
                    type: 'POST',
                    url: "ajCardInformation.aspx",
                    data: { "CardName1": CardName1, "BGImage1": BGImage1, "BGColor1": BGColor1, "TemplateImage1": TemplateImage1, "Width1": Width1, "Height1": Height1, "Category1": Category1, "ImageUrl2": ImageUrl2, "PositionTop2": PositionTop2, "PositionLeft2": PositionLeft2, "zIndex2": zIndex2, "Width2": Width2, "Height2": Height2, "ImageSize2": ImageSize2, "PreviousName2": PreviousName2, "CardID": CardID },
                    success: function(msg) {
                        alert(msg);
                        if (msg == "บันทึกข้อมูลเรียบร้อยแล้วค่ะ") {
                            $("#txtCardID").val("");
                            window.location.href = "AddTemplate.aspx";
                        }
                    }
                });

            });

        });   
    </script>

    <table>
        <tr>
            <td align="center">
                <div align="left">
                    <b>ข้อมูล Card</b>
                    <table id="Table3">
                        <tr style="background-color: #cfe" align="center">
                            <td align="center">
                                ชื่อนามบัตร <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                ภาพ Background<span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                สีพื้นหลัง <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                ชื่อภาพPreview<span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                ขนาด(กว้าง x สูง)<span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                หมวด<span style="color: #CC3300">*</span>
                            </td>
                        </tr>
                        <tr align="center">
                            <td align="center">
                                <input id="txtCardName1" type="text" style="width: 100px;" />
                            </td>
                            <td align="center">
                                <input id="txtBGImage1" type="text" style="width: 120px;" />
                            </td>
                            <td align="center">
                                <input id="txtBGColor1" type="text" value="white" style="width: 80px;" />
                            </td>
                            <td align="center">
                                <input id="txtTemplateImage1" type="text" style="width: 100px;" />
                            </td>
                            <td align="center">
                                <input id="txtWidth1" type="text" value="90" style="width: 45px;" class="checknum" /><span style="color: #000000"> X </span>
                                <input id="txtHeight1" type="text" value="55" style="width: 45px;" class="checknum" />
                            </td>
                            <td align="center">
                                <select id="ddCategory" style="width: 120px;">
                                    <%
                                        For Each item As clsCategories In listCategory%>
                                    <option value="<%=item.CategoryName%>">
                                        <%=item.CategoryName%></option>
                                    <%
                                    Next%>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td align="center" style="width: 236px">
                <div align="left">
                    <b>ข้อมูล Logo</b>&nbsp;&nbsp;<input id="chkNoLogo" type="checkbox" />ไม่มี Logo
                    <table id="Table2">
                        <tr style="background-color: #cfe" align="center">
                            <%--<td align="center">
                                ImageUrl <span style="color: #CC3300">*</span>
                            </td>--%>
                            <td align="center">
                                แกน Y <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                แกน X <span style="color: #CC3300">*</span>
                            </td>
                            <%-- <td align="center">
                                zIndex <span style="color: #CC3300">*</span>
                            </td>--%>
                            <td align="center">
                                กว้าง <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                สูง <span style="color: #CC3300">*</span>
                            </td>
                            <%--<td align="center">
                                Image<br />
                                Size <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                Previous Name <span style="color: #CC3300">*</span>
                            </td>--%>
                        </tr>
                        <tr align="center">
                            <%--<td align="center">
                                <input id="txtImageUrl2" type="text" value="Logo.png" style="width: 100px;" />
                            </td>--%>
                            <td align="center">
                                <input id="txtPositionTop2" type="text" style="width: 60px;" class="checknum" />
                            </td>
                            <td align="center">
                                <input id="txtPositionLeft2" type="text" style="width: 60px;" class="checknum" />
                            </td>
                            <%-- <td align="center">
                                <input id="txtzIndex2" type="text" value="1" style="width: 40px;" class="checknum" />
                            </td>--%>
                            <td align="center">
                                <input id="txtWidth2" type="text" value="30" style="width: 40px;" class="checknum" />
                            </td>
                            <td align="center">
                                <input id="txtHeight2" type="text" value="15" style="width: 40px;" class="checknum" />
                            </td>
                            <%-- <td align="center">
                                <input id="txtImageSize2" type="text" value="1" style="width: 40px;" class="checknum" />
                            </td>
                            <td align="center">
                                <input id="txtPreviousName2" type="text" value="Logo.png" style="width: 100px;" />
                            </td>--%>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <div align="left">
                    <br />
                    <b>ข้อมูล/ตำแหน่งของข้อมูลบน Card</b> <span style="color: #CC3300">(กรุณาเพิ่มข้อมูลในส่วนนี้ให้ครบก่อนนะค่ะ)</span>
                    <table id="TextInfo">
                        <tr style="background-color: #cfe" align="center">
                            <td align="center">
                                ลำดับ <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                ชื่อคอลัมน์ <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                ข้อความ<br />
                                Guide
                            </td>
                            <td align="center">
                                แกน Y <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                แกน X <span style="color: #CC3300">*</span>
                            </td>
                            <%-- <td align="center">
                                 zIndex <span style="color: #CC3300">*</span>
                            </td>--%>
                            <%--<td align="center">
                                BackColor <span style="color: #CC3300">*</span>
                            </td>--%>
                            <td align="center">
                                ForeColor <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                ชื่อ Font <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                Font<br />
                                Size <span style="color: #CC3300">*</span>
                            </td>
                            <td align="center">
                                Font<br />
                                Bold
                            </td>
                            <td align="center">
                                Font<br />
                                Italic
                            </td>
                            <td align="center">
                                Font<br />
                                Underline
                            </td>
                            <td align="center">
                                Right<br />
                                2Left
                            </td>
                            <td align="center">
                                Align<br />
                                Center
                            </td>
                        </tr>
                        <tr align="center">
                            <td align="center">
                                <input id="txtSeq" type="text" style="width: 40px;" class="checknum" />
                            </td>
                            <td align="center">
                                <input id="txtFieldName" type="text" style="width: 100px;" />
                            </td>
                            <td align="center">
                                <input id="txtTextValue" type="text" style="width: 100px;" />
                            </td>
                            <td align="center">
                                <input id="txtPositionTop" type="text" style="width: 60px;" class="checknum" />
                            </td>
                            <td align="center">
                                <input id="txtPositionLeft" type="text" style="width: 60px;" class="checknum" />
                            </td>
                            <%-- <td align="center">
                                <input id="txtzIndex" type="text" style="width: 40px;" class="checknum" />
                            </td>--%>
                            <%--<td align="center">
                                <input id="txtBackColor" type="text" style="width: 80px;" maxlength="7" value="#FFFFFF" />
                            </td>--%>
                            <td align="center">
                                <input id="txtForeColor" type="text" style="width: 80px;" maxlength="7" value="#000000" />
                            </td>
                            <td align="center">
                                <%--<input id="txtFontFamily" type="text" style="width: 100px;" />--%>
                                <select id="ddFontFamily" name="ddFontFamily">
                                    <%
                                        For Each item As clsFontFamily In listFontFamily%>
                                    <option value="<%=item.FontName%>">
                                        <%=item.FontName%></option>
                                    <%
                                    Next%>
                                </select>
                            </td>
                            <td align="center">
                                <input id="txtFontSize" type="text" style="width: 40px;" maxlength="5" class="checknum" />
                            </td>
                            <td align="center">
                                <select id="ddFontBold" name="ddFontBold">
                                    <option value="N" selected="selected">No</option>
                                    <option value="Y">Yes</option>
                                </select>
                            </td>
                            <td align="center">
                                <select id="ddFontItalic" name="ddFontItalic">
                                    <option value="N" selected="selected">No</option>
                                    <option value="Y">Yes</option>
                                </select>
                            </td>
                            <td align="center">
                                <select id="ddFontUnderline" name="ddFontUnderline">
                                    <option value="N" selected="selected">No</option>
                                    <option value="Y">Yes</option>
                                </select>
                            </td>
                            <td align="center">
                                <select id="ddRight2Left" name="ddRight2Left">
                                    <option value="N" selected="selected">No</option>
                                    <option value="Y">Yes</option>
                                </select>
                            </td>
                            <td align="center">
                                <select id="ddAlignCenter" name="ddAlignCenter">
                                    <option value="N" selected="selected">No</option>
                                    <option value="Y">Yes</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div align="right">
                    <input id="btnAdd" type="button" value="เพิ่มข้อมูลบน Card" style="width: 120px;" />
                </div>
            </td>
        </tr>
    </table>
    <br />
    <div id="dvDisplayAllTextField">
    </div>
    <br />
    <div align="right">
        <input id="txtCardID" type="hidden" value="" />
        <input id="btnSave" type="button" value="Save Card" style="width: 120px;" />
        &nbsp;<input id="btnCancel" type="button" value="Cancel Card" style="width: 120px;" />
        <br />
    </div>
</asp:Content>
