﻿Imports System.Collections.Generic
Imports CoreBaseII

Partial Class Admin_ShowOrder
    Inherits clsHTTPBase
    Public ObjOrder As New List(Of clsOrder)
	Public PageNo As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim OrderID As String = Request("OrderID")
        Dim FromDate As String = Request("FromDate")
        Dim ToDate As String = Request("ToDate")
		Dim OrderEngine As New clsOrderEngine
		Dim CardProvider As New clsCardProvider

        ObjOrder = OrderEngine.ViewOrderForAdmin(OrderID, FromDate, ToDate)

    End Sub
End Class
