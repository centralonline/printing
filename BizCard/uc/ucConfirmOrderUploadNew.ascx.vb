Imports System.Collections.Generic
Imports System.Data
Partial Class uc_ucConfirmOrderUploadNew
	Inherits clsUCBase

	Private OrderIDValue As String
	Public Property OrderID() As String
		Get
			Return OrderIDValue
		End Get
		Set(ByVal value As String)
			OrderIDValue = value
		End Set
	End Property
	Private CardIDValue As String
	Public Property CardID() As String
		Get
			Return CardIDValue
		End Get
		Set(ByVal value As String)
			CardIDValue = value
		End Set
	End Property
	Private CardSizeValue As String
	Public Property CardSize() As String
		Get
			Return CardSizeValue
		End Get
		Set(ByVal value As String)
			CardSizeValue = value
		End Set
	End Property
	Private SendMailValue As String
	Public Property SendMail() As String
		Get
			Return SendMailValue
		End Get
		Set(ByVal value As String)
			SendMailValue = value
		End Set
	End Property
	Private UserIDValue As String
	Public Property UserID() As String
		Get
			Return UserIDValue
		End Get
		Set(ByVal value As String)
			UserIDValue = value
		End Set
	End Property
	Private ImagePathValue As String
	Public Property ImagePath() As String
		Get
			ImagePathValue = Me.objAppVar.ImagePath
			Return ImagePathValue
		End Get
		Set(ByVal value As String)
			ImagePathValue = value
		End Set
	End Property

	Private MailNameValue As String
	Public Property MailName() As String
		Get
			MailNameValue = Me.objAppVar.MailName
			Return MailNameValue
		End Get
		Set(ByVal value As String)
			MailNameValue = value
		End Set
	End Property

	Private serviceValue As String
	Public Property serviceAdd() As String
		Get
			Return serviceValue
		End Get
		Set(ByVal value As String)
			serviceValue = value
		End Set
	End Property


	Protected Overrides Sub OnInit(ByVal e As EventArgs)
		MyBase.OnInit(e)
	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If (Not IsPostBack) Then


			Dim clsOrder As New clsOrderEngine
			Dim listOrder As New List(Of clsOrder)

			Me.btnPrint.Attributes.Add("onclick", "window.print();")
			listOrder = clsOrder.GetInfoOrders(Me.OrderID)

			Dim ClsCardEn As New clsCardEngine
			Dim clsCards As New clsCard

			clsCards = ClsCardEn.GetCardByID(Me.CardID)

			Dim listclsCardimg As New List(Of clsCardImageField)
			If Request.QueryString("CardID") <> "" Or Request.QueryString("CardUpload") <> "N" Then
				Dim clsCardEngine As New clsCardEngine
				Dim TcardID As String = Request.QueryString("CardID")
				Dim cardID As String = TcardID.Substring(1, 7)
				Dim clsCard As New clsCard
				clsCard = clsCardEngine.GetCardByID(cardID)
				listclsCardimg = clsCard.ImageFields '''''
				Me.dlUploadFile.DataSource = clsCard.ImageFields
				Me.dlUploadFile.DataBind()
			End If

			'If clsCards Is Nothing Then
			'    Exit Sub
			'End If

			Dim strPaymentDesc As String = ""
			Select Case listOrder.Item(0).PaymentType
				Case "Transfer"
					strPaymentDesc = "�����¡���͹�Թ ��Һѭ�ո�Ҥ��"
				Case "Cash on Delivery"
					strPaymentDesc = "���Թ���·ҧ�¾�ѡ�ҹ�Ϳ������"
				Case "Credit Card EDC"
					strPaymentDesc = "�����Թ���ºѵ��ôԵ�Ѻ��ѡ�ҹ�Ϳ������"
			End Select

			Me.lbPaperName.Text = listOrder.Item(0).PaperName
			Me.lbOrderID.Text = listOrder.Item(0).OrderID
			Me.lbDateOrder.Text = listOrder.Item(0).OrderDate.ToString("dd/MM/yyyy HH:mm:ss")
			Me.lbPayMentCode.Text = strPaymentDesc

			If listOrder.Item(0).Promotion = "0.00" Then
				Me.lbPrice.Text = listOrder.Item(0).PaperPrice
			Else
				Me.lbPrice.Text = listOrder.Item(0).Promotion
			End If

			'GIFT ���� 14/06/2016
			Me.lbDelivery.Text = listOrder.Item(0).DeliveryAmt
			Me.lbSum.Text = listOrder.Item(0).TotAmt
			Me.lbNumOrder.Text = listOrder.Item(0).Qty
			Me.lbPriceExcVat.Text = listOrder.Item(0).NetAmt
			Me.lbVat.Text = listOrder.Item(0).VatAmt
			Me.lbPaperID.Text = listOrder.Item(0).PaperID

			Me.lbAddress1.Text = listOrder.Item(0).ShippingAddr1
			Me.lbAddress2.Text = listOrder.Item(0).ShippingAddr2
			Me.lbAddress3.Text = listOrder.Item(0).ShippingAddr3
			Me.lbAddress4.Text = listOrder.Item(0).ShippingAddr4
			Me.lbDescription.Text = listOrder.Item(0).Description


			''GIFT Add 26/5/2016
			'GIFT ������¡���ʴ���������´
			'��ԡ�õѴ���
			lbOptionalPrice2.Text = listOrder.Item(0).OptionalPrice2
			lbOptionalDesc2.Text = listOrder.Item(0).OptionalDesc2
			'��ԡ�����ͺ
			lbOptionalPrice1.Text = listOrder.Item(0).OptionalPrice1
			lbOptionalDesc1.Text = listOrder.Item(0).OptionalDesc1
			Me.lbTotal.Text = listOrder.Item(0).TotalAmt
			''End Add

			Me.lbCustID.Text = listOrder.Item(0).CustID
			Me.lbCustName.Text = Me.objSessionVar.ModelUser.UserName
			Me.lbMobile.Text = Me.objSessionVar.ModelUser.Mobile

			Me.lbContactName.Text = Me.objSessionVar.ModelUser.Contact.ContactName
			Me.lbContactMobileNo.Text = Me.objSessionVar.ModelUser.Contact.ContactMobileNo
			Me.lbContactPhoneNo.Text = Me.objSessionVar.ModelUser.Contact.ContactPhoneNo
			Me.lbContactFaxNo.Text = Me.objSessionVar.ModelUser.Contact.ContactFaxNo
			Me.lbContactEmail.Text = Me.objSessionVar.ModelUser.Contact.ContactEmail.ContactEmail

			' Fix ��ͤ��� FTP 㹡���ʴ�
			'If Request.QueryString("FTPUpLoad") = "N" Then
			'MessageBox.Show("���ǹ�ͧ��� Upload File ���� FTP �ҧ���˹�ҷ��еԴ��͡�Ѻ令�� �����Ţ Order �ͧ��ҹ��� : " & listOrder.Item(0).OrderID)
			'Me.lbDetailFTP.Text = "���ǹ�ͧ��� Upload File ���� FTP �ҧ���˹�ҷ��еԴ��͡�Ѻ令�� �����Ţ Order �ͧ��ҹ��� : " & listOrder.Item(0).OrderID
			'End If

			' Fix ��ͤ��� UploadFile 㹡���ʴ�
			If Request.QueryString("CardUpload") = "N" Then
				MessageBox.Show("��ҹ�����ӡ�� Upload File �ҧ���˹�ҷ��еԴ��͡�Ѻ令�� �����Ţ Order �ͧ��ҹ��� : " & listOrder.Item(0).OrderID)
				Me.lbDetailFTP.Text = "��ҹ�����ӡ�� Upload File �ҧ���˹�ҷ��еԴ��͡�Ѻ令�� �����Ţ Order �ͧ��ҹ��� : " & listOrder.Item(0).OrderID
			End If

			If objSessionVar.ModelUser.ActionUpload = True Then
				Dim DetailFTP As DataTable
				Dim OrderFTP As New clsOrderEngine
				DetailFTP = OrderFTP.SelectFTPfile(listOrder.Item(0).OrderID)
				Dim User As String = DetailFTP.Rows(0).Item("UserAccount")
				Dim Pass As String = DetailFTP.Rows(0).Item("Password")
				Dim sqlStr As String
				sqlStr = "<table width='700' border='0' cellspacing='1' cellpadding='5'frame='border' >" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td colspan='2' bgcolor='#C9E4C9' style='width: 700px'><p style='font-family:Tahoma; color:#666666; font-size:11px'><b>������������㹡�� FTP Upload File</b></p></td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td width='320' bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>��ʵ� :</td>" & vbNewLine
				sqlStr += "<td width='380' bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'><a href='ftp://upload.officemate.co.th' target='_blank'>ftp://upload.officemate.co.th</a></td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>�����ҹ :</td>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>" & User & "</td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>���ʼ�ҹ :</td>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>" & Pass & "</td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "<tr>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>�����˵� :</td>" & vbNewLine
				sqlStr += "<td bgcolor='#FFFFFF' style='font-family:Tahoma; color:#666666; font-size:12px'>���ʼ����ҹ�������ö�������� 1 �ҷԵ����ҹ��<br/><a href=""" & ImagePath & "/HowtoOrder.aspx#FTP"" target='_blank'>��ҹ�Ըա�� FTP</a></td>" & vbNewLine
				sqlStr += "</tr>" & vbNewLine
				sqlStr += "</table>" & vbNewLine
				Me.lbDetailFTP.Text = sqlStr.ToString()
			End If

			If (Me.SendMail = "") Then
				Mail2User(listOrder, listclsCardimg)
				Mail2Contact(listOrder, listclsCardimg)
			End If

		End If
	End Sub

	Private Sub Mail2User(ByVal listOrder As List(Of clsOrder), ByVal listclsCardimg As List(Of clsCardImageField))
		Dim Message As New Net.Mail.MailMessage()
		
		Message.From = New Net.Mail.MailAddress(MailName)
		Message.To.Add(Trim(Me.objSessionVar.ModelUser.Contact.ContactEmail.ContactEmail))

		Message.Bcc.Add(New Net.Mail.MailAddress("surapon@officemate.co.th"))
		Message.Bcc.Add(New Net.Mail.MailAddress("waraporn_k@officemate.co.th"))
        Message.Bcc.Add(New Net.Mail.MailAddress("panupan.ja@officemate.co.th"))

		Message.Subject = "Officemate Printing Solution: Print & Delivery Service Center [Order: " & listOrder.Item(0).OrderID & "]"
		Message.IsBodyHtml = True

		Dim SqlStr As String = ""
		SqlStr += "<div align='left'><table align='center' border='0'><tr><td>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='0' cellpadding='0' style='width: 700px'><tr>" & vbNewLine
        SqlStr += "<td style='width: 260px; height: 70px;'><img src='https://printing.officemate.co.th/images/logo-trendyprint.png'></td>" & vbNewLine
		SqlStr += "<td style='width: 440px; height: 70px;'>" & vbNewLine
		SqlStr += "<p style='font-family: Tahoma; color: #666666; font-size: 11px;'>" & vbNewLine
		SqlStr += "<b>����ѷ ������� �ӡѴ (��Ҫ�)</b><br />" & vbNewLine
		SqlStr += "24 �.��͹�ت 66/1 �ǧ�ǹ��ǧ ࢵ�ǹ��ǧ ��ا෾� 10250<br />" & vbNewLine
		SqlStr += "�� : 02-739-5555(120 ���) �硫� : 02-763-5555(30 ���)<br />" & vbNewLine
		SqlStr += "Website : www.officemate.co.th, e-Mail : <a href='mailto:printing@officemate.co.th'>printing@officemate.co.th</a>" & vbNewLine
		SqlStr += "</p></td></tr></table>" & vbNewLine
		SqlStr += "<table width='680' border='0' cellspacing='0' cellpadding='2'>" & vbNewLine
		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px'><br />" & vbNewLine
		SqlStr += "<strong>�Ţ������觷Ӣͧ��ҹ��� </strong>" & listOrder.Item(0).OrderID & "</td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px'>�ѹ���ӡ����� :" & listOrder.Item(0).OrderDate & "<br /></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='1' cellpadding='5' style='width: 700px'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>�����١���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'><span class='hilight'>" & listOrder.Item(0).CustID & "</span></td>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>�����١���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbCustName.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������Ͷ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbMobile.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "</tr></table></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' style='font-family: Tahoma; color: #666666; font-size: 11px'>&nbsp;</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='1' cellpadding='5' style='width: 700px'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>ʶҹ���Ѵ��:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbAddress1.Text & "</td>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>����Ѻ�Թ���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbContactName.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbAddress2.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactEmail.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'><asp:Label ID='lbAddress3' runat='server' Text='lbAddress3'></asp:Label></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������Ͷ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactMobileNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbAddress4.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactPhoneNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>����ῡ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactFaxNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "</table></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td style='font-family: Tahoma; color: #666666; font-size: 11px'><br /><strong>��ػ��¡�÷����觷�:</strong></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table width='100%' border='0' cellspacing='1' cellpadding='5'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'><strong>��������´</strong></div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�Ҥ�/˹���<br />�ҷ</div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�ӹǹ�����觷�<br />(�)</div>" & vbNewLine
		SqlStr += "</td><td bgcolor='#C9E4C9'><div align='center'>�ӹǹ�Թ<br />(�ҷ)</div></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>���ʡ�д��:" & Me.lbPaperID.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' width='100px'>" & Me.lbPrice.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbNumOrder.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbTotal.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#ffffff'>��Դ��д��:" & Me.lbPaperName.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' align='right'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' align='right'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' align='right'></td>" & vbNewLine

		'Gift ������ԡ�õѴ���
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ԡ�õѴ���:" & Me.lbOptionalDesc2.Text & "</td>" & vbNewLine
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>��Һ�ԡ��:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbOptionalPrice2.Text & "</td></tr>" & vbNewLine
		'Gift ��ԡ�����ͺ
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ԡ�����ͺ:" & Me.lbOptionalDesc1.Text & "</td>" & vbNewLine
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>��Һ�ԡ��:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbOptionalPrice1.Text & "</td></tr>" & vbNewLine

		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine

		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ê����Թ:" & Me.lbPayMentCode.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right'>�Ҥ��ط�� (��͹ vat):</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px'>" & Me.lbPriceExcVat.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'></td>"
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>������Ť������ 7%:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbVat.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF' style='height: 23px'></td>" & vbNewLine
		SqlStr += "<td align='right' colspan='2' bgcolor='#FFFFFF' style='height: 23px'>��ҨѴ�� :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px; height: 23px;'>" & Me.lbDelivery.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF' style='height: 23px'></td>" & vbNewLine
		SqlStr += "<td align='right' colspan='2' bgcolor='#FFFFFF' style='height: 23px'>�ӹǹ�Թ���������:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px; height: 23px;'>" & Me.lbSum.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr><td bgcolor='#FFFFFF' style='font-size: 11px; color: #666666; font-family: Tahoma' colspan='4'>"
		SqlStr += "<div align='right' >***�Ҥҹ���ѧ����������͡Ẻ ���ͤ�Һ�ԡ�������ҡ�ը�������Һ�����ѧ㹷ѹ��</div></td></tr>" & vbNewLine
		SqlStr += "</table></td></tr>" & vbNewLine

		'�����͹� FTP
		'If objSessionVar.ModelUser.ActionUpload = True Or Request.QueryString("FTPUpLoad") = "N" Then
		'	SqlStr += "<tr><td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		'	SqlStr += "<div class='box-general'>" & Me.lbDetailFTP.Text & "</div></td></tr>" & vbNewLine
		'End If
		'����͹� Upload File
		If Request.QueryString("CardUpload") = "N" Then
			SqlStr += "<tr><td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
			SqlStr += "<div class='box-general'>" & Me.lbDetailFTP.Text & "</div></td></tr>" & vbNewLine
		End If


		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px; height: 90px;' valign='top'><br />" & vbNewLine
		SqlStr += "<strong>�����˵��١���: </strong>" & Me.lbDescription.Text & "</td></tr></table>" & vbNewLine

		If listclsCardimg.Count > 0 Then
			SqlStr += "<strong><br /><span style='font-size: 11 px; color: #666666; font-family:Tahoma;'>�����Ṻ :</span></strong><br />" & vbNewLine
			SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'>"
			Dim file As String = ""
			For Each obj As clsCardImageField In listclsCardimg
				SqlStr += "<tr>" & vbNewLine
				SqlStr += "<td align='left' bgcolor='#ffffff' style='font-size: 11px; width: 150px; color: #666666; font-family: Tahoma'>" & obj.PreviousName.ToString & "</td>"
				SqlStr += "<td align='left' bgcolor='#ffffff' style='font-size: 11px; width: 265px; color: #666666; font-family: Tahoma'>"
				If obj.Ready2Print.ToString = "Y" Then
					file = "�������觾����"
				Else
					file = "�Ϳ�Ե��� Design"
				End If
				SqlStr += "<div align='left'>" & file & "</div></td></tr>"
			Next
			SqlStr += "</table>" & vbNewLine
		End If
		SqlStr += "<br />" & vbNewLine
		SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'><tr>"
		SqlStr += "<td style='font-size: 11px; color: #666666; font-family: Tahoma'><strong>��������´��������ͧ�ҹ����� :</strong><br/>" & vbNewLine
		SqlStr += "��������áԨ : " & listOrder.Item(0).BusinessType & "<br/>"
		SqlStr += "ⷹ�ͧ�ҹ����� : " & listOrder.Item(0).CardStyle & "<br/>"
		SqlStr += "ⷹ�բͧ�ҹ����� : " & listOrder.Item(0).ColorShade & "<br/>"
		SqlStr += "</td></tr></table>" & vbNewLine
		SqlStr += "<br />" & vbNewLine
		SqlStr += "</td></tr></table>" & vbNewLine
		SqlStr += "</td></tr></table></div>" & vbNewLine

		Dim HtmlBody As Net.Mail.AlternateView
		HtmlBody = Net.Mail.AlternateView.CreateAlternateViewFromString(SqlStr, Nothing, "text/html")

		Dim imgTopdesign As String = ImagePath & "/images/logo-trendyprint.png"

		Dim client2 As New System.Net.WebClient()
		'Dim streamImage As IO.Stream
		'streamImage = client2.OpenRead(imgTopdesign)
		'Dim img1 As New Net.Mail.LinkedResource(streamImage, "image/gif")
		'img1.ContentId = "pic1"
		'HtmlBody.LinkedResources.Add(img1)

		Message.AlternateViews.Add(HtmlBody)

		Try
			Dim MailClient As New Net.Mail.SmtpClient()
			MailClient.Host = ConfigurationManager.AppSettings("SmtpClientHost")
			''If (ConfigurationManager.AppSettings("SmtpClientUserName") = "") Then
			MailClient.Credentials = New Net.NetworkCredential(ConfigurationManager.AppSettings("SmtpClientUserName"), ConfigurationManager.AppSettings("SmtpClientPassword"))
			''End If

			MailClient.Send(Message)
		Catch ex As Exception

		End Try

	End Sub
	Private Sub Mail2Contact(ByVal listOrder As List(Of clsOrder), ByVal listclsCardimg As List(Of clsCardImageField))
		Dim Message As New Net.Mail.MailMessage()

		Message.From = New Net.Mail.MailAddress(MailName)
		Message.To.Add(New Net.Mail.MailAddress(MailName))

		Message.Bcc.Add(New Net.Mail.MailAddress("printing@officemate.co.th"))
		Message.Bcc.Add(New Net.Mail.MailAddress("surapon@officemate.co.th"))
		''Message.Bcc.Add(New Net.Mail.MailAddress("surachart@officemate.co.th"))
		Message.Bcc.Add(New Net.Mail.MailAddress("waraporn_k@officemate.co.th"))
        Message.Bcc.Add(New Net.Mail.MailAddress("panupan.ja@officemate.co.th"))

		Message.Subject = "Officemate Printing Solution: Print & Delivery Service Center For Admin [Order: " & listOrder.Item(0).OrderID & "]"
		Message.IsBodyHtml = True

		Dim SqlStr As String = ""
		SqlStr += "<div align='left'><table align='center' border='0'><tr><td>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='0' cellpadding='0' style='width: 700px'><tr>" & vbNewLine
        SqlStr += "<td style='width: 260px; height: 70px;'><img src='https://printing.officemate.co.th/images/logo-trendyprint.png'></td>" & vbNewLine
		SqlStr += "<td style='width: 440px; height: 70px;'>" & vbNewLine
		SqlStr += "<p style='font-family: Tahoma; color: #666666; font-size: 11px;'>" & vbNewLine
		SqlStr += "<b>����ѷ ������� �ӡѴ (��Ҫ�)</b><br />" & vbNewLine
		SqlStr += "24 �.��͹�ت 66/1 �ǧ�ǹ��ǧ ࢵ�ǹ��ǧ ��ا෾� 10250<br />" & vbNewLine
		SqlStr += "�� : 02-739-5555(120 ���) �硫� : 02-763-5555(30 ���)<br />" & vbNewLine
		SqlStr += "Website : www.officemate.co.th, e-Mail : <a href='mailto:printing@officemate.co.th'>printing@officemate.co.th</a>" & vbNewLine
		SqlStr += "</p></td></tr></table>" & vbNewLine
		SqlStr += "<table width='680' border='0' cellspacing='0' cellpadding='2'>" & vbNewLine
		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px'><br />" & vbNewLine
		SqlStr += "<strong>�Ţ������觷Ӣͧ��ҹ��� </strong>" & listOrder.Item(0).OrderID & "</td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px'>�ѹ���ӡ����� :" & listOrder.Item(0).OrderDate & "<br /></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='1' cellpadding='5' style='width: 700px'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>�����١���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'><span class='hilight'>" & listOrder.Item(0).CustID & "</span></td>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>�����١���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbCustName.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������Ͷ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbMobile.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "</tr></table></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' style='font-family: Tahoma; color: #666666; font-size: 11px'>&nbsp;</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='1' cellpadding='5' style='width: 700px'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>ʶҹ���Ѵ��:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbAddress1.Text & "</td>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>����Ѻ�Թ���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbContactName.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbAddress2.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactEmail.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'><asp:Label ID='lbAddress3' runat='server' Text='lbAddress3'></asp:Label></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������Ͷ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactMobileNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbAddress4.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactPhoneNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>����ῡ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactFaxNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "</table></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td style='font-family: Tahoma; color: #666666; font-size: 11px'><br /><strong>��ػ��¡�÷����觷�:</strong></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table width='100%' border='0' cellspacing='1' cellpadding='5'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'><strong>��������´</strong></div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�Ҥ�/˹���<br />�ҷ</div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�ӹǹ�����觷�<br />(�)</div>" & vbNewLine
		SqlStr += "</td><td bgcolor='#C9E4C9'><div align='center'>�ӹǹ�Թ<br />(�ҷ)</div></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>���ʡ�д��:" & Me.lbPaperID.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' width='100px'>" & Me.lbPrice.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbNumOrder.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbTotal.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#ffffff'>��Դ��д��:" & Me.lbPaperName.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' align='right'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' align='right'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' align='right'></td>" & vbNewLine

		'Gift ������ԡ�õѴ���
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ԡ�õѴ���:" & Me.lbOptionalDesc2.Text & "</td>" & vbNewLine
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>��Һ�ԡ��:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbOptionalPrice2.Text & "</td></tr>" & vbNewLine
		'Gift ��ԡ�����ͺ
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ԡ�����ͺ:" & Me.lbOptionalDesc1.Text & "</td>" & vbNewLine
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>��Һ�ԡ��:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbOptionalPrice1.Text & "</td></tr>" & vbNewLine

		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine

		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ê����Թ:" & Me.lbPayMentCode.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right'>�Ҥ��ط�� (��͹ vat):</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px'>" & Me.lbPriceExcVat.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'></td>"
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>������Ť������ 7%:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbVat.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF' style='height: 23px'></td>" & vbNewLine
		SqlStr += "<td align='right' colspan='2' bgcolor='#FFFFFF' style='height: 23px'>��ҨѴ�� :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px; height: 23px;'>" & Me.lbDelivery.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF' style='height: 23px'></td>" & vbNewLine
		SqlStr += "<td align='right' colspan='2' bgcolor='#FFFFFF' style='height: 23px'>�ӹǹ�Թ���������:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px; height: 23px;'>" & Me.lbSum.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr><td bgcolor='#FFFFFF' style='font-size: 11px; color: #666666; font-family: Tahoma' colspan='4'>"
		SqlStr += "<div align='right' >***�Ҥҹ���ѧ����������͡Ẻ ���ͤ�Һ�ԡ�������ҡ�ը�������Һ�����ѧ㹷ѹ��</div></td></tr>" & vbNewLine
		SqlStr += "</table></td></tr>" & vbNewLine

		'�����͹� FTP
		'If objSessionVar.ModelUser.ActionUpload = True Or Request.QueryString("FTPUpLoad") = "N" Then
		'	SqlStr += "<tr><td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		'	SqlStr += "<div class='box-general'>" & Me.lbDetailFTP.Text & "</div></td></tr>" & vbNewLine
		'End If
		'����͹� Upload File
		If Request.QueryString("CardUpload") = "N" Then
			SqlStr += "<tr><td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
			SqlStr += "<div class='box-general'>" & Me.lbDetailFTP.Text & "</div></td></tr>" & vbNewLine
		End If

		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px; height: 90px;' valign='top'><br />" & vbNewLine
		SqlStr += "<strong>�����˵��١���: </strong>" & Me.lbDescription.Text & "</td></tr></table>" & vbNewLine
		If listclsCardimg.Count > 0 Then
			SqlStr += "<strong><br /><span style='font-size: 11 px; color: #666666; font-family:Tahoma;'>�����Ṻ :</span></strong><br />" & vbNewLine
			SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'>"
			Dim file As String = ""
			For Each obj As clsCardImageField In listclsCardimg
				SqlStr += "<tr>" & vbNewLine
				SqlStr += "<td align='left' bgcolor='#ffffff' style='font-size: 11px; width: 150px; color: #666666; font-family: Tahoma'>" & obj.PreviousName.ToString & "</td>"
				SqlStr += "<td align='left' bgcolor='#ffffff' style='font-size: 11px; width: 265px; color: #666666; font-family: Tahoma'>"
				If obj.Ready2Print.ToString = "Y" Then
					file = "�������觾����"
				Else
					file = "�Ϳ�Ե��� Design"
				End If
				SqlStr += "<div align='left'>" & file & "</div></td></tr>"
			Next
			SqlStr += "</table>" & vbNewLine
			SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'>" & vbNewLine
			For Each obj As clsCardImageField In listclsCardimg
				SqlStr += "<tr>" & vbNewLine
				SqlStr += "<td style='font-size: 11px; color: #666666; font-family: Tahoma'>Link File Attach : " & obj.PreviousName.ToString & "<br />" & vbNewLine
				SqlStr += "<a href=""" & ImagePath & "/CardImages/UploadImage/UploadFiles/" & obj.imageUrl.ToString & """>"
				SqlStr += ImagePath & "/CardImages/UploadImage/UploadFiles/" & obj.imageUrl.ToString & "</a>" & vbNewLine
				SqlStr += "</td></tr>" & vbNewLine
			Next
			SqlStr += "</table>" & vbNewLine
		End If
		SqlStr += "<br />" & vbNewLine
		SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'><tr>"
		SqlStr += "<td style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<span style='font-size: 11 px; color: #666666; font-family:Tahoma;'>��������´��������ͧ�ҹ����� :</span></strong><BR/>" & vbNewLine
		SqlStr += "��������áԨ : " & listOrder.Item(0).BusinessType & "<br/>"
		SqlStr += "ⷹ�ͧ�ҹ����� : " & listOrder.Item(0).CardStyle & "<br/>"
		SqlStr += "ⷹ�բͧ�ҹ����� : " & listOrder.Item(0).ColorShade & "<br/>"
		SqlStr += "</td></tr></table>" & vbNewLine
		SqlStr += "<br />" & vbNewLine
		SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'><tr>"
		SqlStr += "<td style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<span style='font-size: 11 px; color: #666666; font-family:Tahoma;'> Link � RO:</span></strong><BR/>" & vbNewLine
		SqlStr += "<a href=""" & ImagePath & "/ConfirmOrderUploadPreview.aspx?Order=" & Me.lbOrderID.Text & "&User=" & Me.UserID & """>"
		SqlStr += ImagePath & "/ConfirmOrderUploadPreview.aspx?Order=" & lbOrderID.Text & " &User=" & Me.UserID & "</a>"
		SqlStr += "</td></tr></table>" & vbNewLine
		SqlStr += "<br />" & vbNewLine
		SqlStr += "</td></tr></table>" & vbNewLine
		SqlStr += "</td></tr></table></div>" & vbNewLine

		Dim HtmlBody As Net.Mail.AlternateView
		HtmlBody = Net.Mail.AlternateView.CreateAlternateViewFromString(SqlStr, Nothing, "text/html")

		'Dim imgTopdesign As String = ImagePath & "/images/logo-trendyprint.png"
		'Dim client2 As New System.Net.WebClient()
		'Dim streamImage As IO.Stream
		'streamImage = client2.OpenRead(imgTopdesign)
		'Dim img1 As New Net.Mail.LinkedResource(streamImage, "image/gif")
		'img1.ContentId = "pic1"
		'HtmlBody.LinkedResources.Add(img1)

		Message.AlternateViews.Add(HtmlBody)

		Try
			Dim MailClient As New Net.Mail.SmtpClient()
			MailClient.Host = ConfigurationManager.AppSettings("SmtpClientHost")
			''If (ConfigurationManager.AppSettings("SmtpClientUserName") = "") Then
			MailClient.Credentials = New Net.NetworkCredential(ConfigurationManager.AppSettings("SmtpClientUserName"), ConfigurationManager.AppSettings("SmtpClientPassword"))
			''End If

			MailClient.Send(Message)
		Catch ex As Exception

		End Try
	End Sub
	Protected Sub dlUploadFile_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlUploadFile.ItemDataBound
		If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
			If CType(e.Item.FindControl("hidReadyToPrint"), HiddenField).Value = "Y" Then
				CType(e.Item.FindControl("Label5"), Label).Text = "���������觾����"
			Else
				CType(e.Item.FindControl("Label5"), Label).Text = "�����觷�����Ϳ�Է��� Design"
			End If
		End If
	End Sub

End Class