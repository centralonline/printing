﻿Imports CoreBaseII
Imports UserEngineTrendyPrint

Partial Class forgotpassword
    Inherits System.Web.UI.Page

    Private objEngine As clsEmail

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim MailFrom As String = "contact@Officemate.co.th"
        Dim MailName As String = "Forgot Password"
        Dim MailTO As String = Me.txtEmail.Text
        Dim MailSubject As String = "TrendyPrint System Forgot Password"
        Dim MailMessage As New StringBuilder

        Try
            If txtEmail.Text = "" Then
                MessageBox.Show("กรุณาระบุ e-mail ของท่านค่ะ")
                Exit Sub
            End If
            Dim Engine As New UserEngine
            Dim Email As String = txtEmail.Text
            Dim Password As String = Engine.ForgotPassword(Email)
            If Password = "" Then
                MessageBox.Show("Account นี้ไม่มีในระบบค่ะ")
                Exit Sub
            End If

            With MailMessage
                .AppendLine("<div align=left style='border: 1px solid #ddd; padding:20px;'>" & vbNewLine)
                .AppendLine("<img src='https://printing.officemate.co.th/images/logo-trendyprint.gif'>" & vbNewLine)
                .AppendLine("<div style='font-family:Tahoma; color:#333; font-size:13px; border-top: 1px solid #ddd; border-bottom: 1px solid #ddd; padding:20px;'>" & vbNewLine)
                .AppendLine("<span style='font-family:Tahoma; color:#333; font-size:16px; font-weight:bold;'>Your Account Information</span>" & vbNewLine)
                .AppendLine("<br /><br /><b>คุณสามารถใช้ข้อมูล Username และ Password ด้านล่างนี้ในการ login เข้าสู่ระบบ เพื่อแก้ไขรหัสผ่านของคุณค่ะ</b>" & vbNewLine)
                .AppendLine("<br /><br /><b>UserName ของคุณคือ</b> : " & Email & "" & vbNewLine)
                .AppendLine("<br /><br /><b>Password ของคุณคือ</b> : " & Password & " </div>" & vbNewLine)
				.AppendLine("<p style='font-family:Tahoma; color:#666; font-size:11px'><b>บริษัท ซีโอแอล จำกัด (มหาชน)</b>" & vbNewLine)
                .AppendLine("<br /> 24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ 10250 <br />โทร : 02-739-5555(120 สาย) แฟ็กซ์ : 02-763-5555(30 สาย)" & vbNewLine)
                .AppendLine("<br /> Website : https://printing.officemate.co.th/, e-Mail : <a href='mailto:contact@Officemate.co.th'>contact@Officemate.co.th</a></p>" & vbNewLine)
                .AppendLine("</div>" & vbNewLine)
            End With

            Dim Result As New clsResultII
            Dim objMail As New clsMailService
            Result.Flag = clsMailService.SendMail(MailFrom, MailName, MailTO, MailSubject, MailMessage.ToString, True)
            If Result.Flag Then
                MessageBox.Show("ระบบทำการส่งรหัสผ่านของท่านให้ทาง e-mail เรียบร้อยแล้วค่ะ")
                txtEmail.Text = ""
            End If
        Catch ex As Exception
            labError.Text = ex.Message
            txtEmail.Text = ""
        End Try
    End Sub
End Class
