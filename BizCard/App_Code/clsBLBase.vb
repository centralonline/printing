Imports Microsoft.VisualBasic

Public Class clsBLBase
    Protected objSessionVar As clsSessionVarWC
    Protected objAppVar As clsAppVarWC

    Public Sub New()
        Me.objAppVar = HttpContext.Current.Application("objAppVar")
        Me.objSessionVar = HttpContext.Current.Session("objSessionVar")
    End Sub

End Class
