﻿// JScript File

var search = new String();
search  = window.location.search;

function DeleteParam(param) {
   search = search.replace("?","");
   var params  = new Array();
   params = search.split("&");
   var newSearch = new String();
   for (i=0;i<params.length;i++) {
       var str = params[i].split("=")[0];
       if (str != param && str != "") {
            newSearch += params[i] + "&";
       }
   }
   search = newSearch;
}
function ChangePage(PageNo) {
   DeleteParam("PageNo");
   DeleteParam("Operation");
   var new_search = "?" + search + "PageNo="+PageNo+"&Operation=ChangePage";
   SendRequest(new_search);
}
function ChangeSortBy(SortBy) {
   DeleteParam("Operation");
   DeleteParam("SortBy");
   var new_search = "?" + search + "SortBy=" + SortBy+"&Operation=ChangeSortBy";
   SendRequest(new_search);
}
function SearchJob(key) {
   var txtSearch = getObj("txtSearch");
   DeleteParam("Operation");
   DeleteParam("Key");
   search = "";
   var new_search = "?" + search + "Key=" + txtSearch.value+"&QueryType=24&Operation=ChangeQueryType";
   SendRequest(new_search);
}
function SendRequest(pSearch)
{
   var new_location  = new String();
   new_location = window.location.href.replace(window.location.search,"");
   new_location += pSearch;
   window.location = new_location;
}

function ChangePageSize(PageSize)
{
   DeleteParam("Operation");
   DeleteParam("PageSize");
   var new_search = "?" + search + "PageSize=" + PageSize+"&Operation=ChangePageSize";
   SendRequest(new_search);
}

function FaqDisplay(faqID, txtID, imgID, Lang)
{
    var faq=document.getElementById(faqID);
    var txt=document.getElementById(txtID);
    var img=document.getElementById(imgID);
       
    var hidLang = getObj("hidLang");
    
    faq.style.display = (faq.style.display == "none")?"":"none";    
    img.src = (faq.style.display == "none")?"images/icon_arrow_down.gif":"images/icon_arrow_up.gif";
    if (hidLang.value == "en")
        txt.innerHTML = (faq.style.display == "none")?"show answer":"hide answer";
    else
        txt.innerHTML = (faq.style.display == "none")?"คลิ๊กเพื่อดูคำตอบ":"คลิ๊กเพื่อซ่อนคำตอบ";  
}




