<%@ Page Title="Admin EditTemplate" Language="VB" MasterPageFile="~/Admin/MasterPageAdmin.master"
	AutoEventWireup="false" CodeFile="EditTemplate.aspx.vb" Inherits="Admin_EditTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="server">
	<script language="javascript" type="text/javascript">

		$(document).ready(function () {

			$("#btnBackManage").click(function () {
				window.location = "ManageFlagCard.aspx";
			});

			$("#CreateNewCard").click(function () {
				var CardID = $("#hidCardID").attr("value");
				$.ajax({
					type: 'POST',
					url: "CreateNewRowTemplate.aspx",
					data: { "CardID": CardID },
					success: function (msg) {
						if (msg === "True") {
							window.location = "EditTemplate.aspx?Type=Manage&CardID=" + CardID;
						}
						else {
							alert("�Դ��ͼԴ��Ҵ�ѹ����ö�Ѵ��â���������")
						}
					}
				});
			});

			$(".clsDeleteText").click(function () {
				var CardID = $("#hidCardID").attr("value");

				$(this).attr("id")
				var seq = new Array();
				seq = $(this).attr("id").split("_");

				$.ajax({
					type: 'POST',
					url: "CreateNewRowTemplate.aspx",
					data: { "CardID": CardID, "Mode": "del", "Seq": seq[1] },
					success: function (msg) {
						if (msg === "True") {
							alert("ź���������º�������Ǥ��");
							window.location = "EditTemplate.aspx?Type=Manage&CardID=" + CardID;
						}
						else {
							alert("�Դ��ͼԴ��Ҵ�ѹ����ö�Ѵ��â���������")
						}
					}
				});
			});


			$("#btnBackEdit").click(function () {
				window.location = "EditActiveTemplate.aspx";
			});

			$(".checknum").keypress(function (e) {

				//if the letter is not digit then display error and don't type anything
				if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
					//display error message
					alert("��سһ�͹������੾�е���Ţ��ҹ��");
					return false;
				}
			});

			$("#btnSaveCard").click(function () {
				if ($("#txtCardName1").val() == "" || $("#txtBGImage1").val() == "" || $("#txtBGColor1").val() == "" || $("#txtTemplateImage1").val() == "" || $("#txtWidth1").val() == "" || $("#txtHeight1").val() == "") {
					alert("��سҡ�͡ ������Card ���ú��͹Save���¤��");
					return false;
				}
				var CardID = $("#hidCardID").attr("value");
				var CardName1 = $("#txtCardName1").val();
				var BGImage1 = $("#txtBGImage1").val();
				var BGColor1 = $("#txtBGColor1").val();
				var TemplateImage1 = $("#txtTemplateImage1").val();
				var Width1 = $("#txtWidth1").val();
				var Height1 = $("#txtHeight1").val();
				var Category1 = $("#ddCategory").attr("value");

				$.ajax({
					type: 'POST',
					url: "ajEditCard.aspx",
					data: { "CardID": CardID, "CardName1": CardName1, "BGImage1": BGImage1, "BGColor1": BGColor1, "TemplateImage1": TemplateImage1, "Width1": Width1, "Height1": Height1, "Category1": Category1 },
					success: function (msg) {
						alert(msg);
					}
				});
			});

			$("#btnSaveImage").click(function () {
				if ($("#chkNoLogo").attr("checked") == true) {
					var ImageUrl2 = "";
					var PositionTop2 = "0.0";
					var PositionLeft2 = "0.0";
					var Width2 = "0";
					var Height2 = "0";
				}
				else {
					if ($("#txtPositionTop2").val() == "" || $("#txtPositionLeft2").val() == "" || $("#txtWidth2").val() == "" || $("#txtHeight2").val() == "") {
						alert("��سҡ�͡ ������Logo ���ú��͹Save���¤��");
						return false;
					}
					var ImageUrl2 = "Logo.png";
					var PositionTop2 = $("#txtPositionTop2").val();
					var PositionLeft2 = $("#txtPositionLeft2").val();
					var Width2 = $("#txtWidth2").val();
					var Height2 = $("#txtHeight2").val();
				}
				var zIndex2 = "1";
				var ImageSize2 = "1";
				var PreviousName2 = "Logo.png";
				var CardID = $("#hidCardID").attr("value");

				$.ajax({
					type: 'POST',
					url: "ajEditImageField.aspx",
					data: { "CardID": CardID, "ImageUrl2": ImageUrl2, "PositionTop2": PositionTop2, "PositionLeft2": PositionLeft2, "zIndex2": zIndex2, "Width2": Width2, "Height2": Height2, "ImageSize2": ImageSize2, "PreviousName2": PreviousName2 },
					success: function (msg) {
						alert(msg);
					}
				});

			});

			$(".clsEditText").click(function () {
				var CardID = $("#hidCardID").attr("value");
				var Seq = $(this).attr("id");
				var FieldName = $("#txtFieldName_" + Seq).attr("value");
				var TextValue = $("#txtTextValue_" + Seq).attr("value");
				var PositionTop = $("#txtPositionTop_" + Seq).attr("value");
				var PositionLeft = $("#txtPositionLeft_" + Seq).attr("value");
				var zIndex = $(this).attr("id");
				var BackColor = "#FFFFFF";
				var ForeColor = $("#txtForeColor_" + Seq).attr("value");
				var FontFamily = $("#ddFontFamily_" + Seq).attr("value");
				var FontSize = $("#txtFontSize_" + Seq).attr("value");
				var FontBold = $("#ddFontBold_" + Seq).attr("value");
				var FontItalic = $("#ddFontItalic_" + Seq).attr("value");
				var FontUnderline = $("#ddFontUnderline_" + Seq).attr("value");
				var Right2Left = $("#ddRight2Left_" + Seq).attr("value");
				var AlignCenter = $("#ddAlignCenter_" + Seq).attr("value");

				if ($("#txtFieldName_" + Seq).val() == "" || $("#txtPositionTop_" + Seq).val() == "" || $("#txtPositionLeft_" + Seq).val() == "" || $("#txtForeColor_" + Seq).val() == "" || $("#txtFontFamily_" + Seq).val() == "" || $("#txtFontSize_" + Seq).val() == "") {
					alert("��سҡ�͡ ������/���˹觢ͧ�����ź�Card ���ú��͹Add���¤��");
					return false;
				}

				$.ajax({
					type: 'POST',
					url: "ajEditTextField.aspx",
					data: { "CardID": CardID, "Seq": Seq, "FieldName": FieldName, "TextValue": TextValue, "PositionTop": PositionTop, "PositionLeft": PositionLeft, "zIndex": zIndex, "BackColor": BackColor, "ForeColor": ForeColor, "FontFamily": FontFamily, "FontSize": FontSize, "FontBold": FontBold, "FontItalic": FontItalic, "FontUnderline": FontUnderline, "Right2Left": Right2Left, "AlignCenter": AlignCenter },
					success: function (msg) {
						alert(msg);
					}
				});
			});
		});

	</script>
	<link href="Css/AdminTool.css" rel="stylesheet" type="text/css" />
	<form method="post" name="myform" id="myform" action="<%=ResolveUrl("~/Admin/CreateCardTemplate.aspx")%>">
	<fieldset style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<h2 class="head-column">
			<b>CardID =
				<%=lisCard.CardID%></b>
			<input id="hidCardID" type="hidden" value="<%=lisCard.CardID%>" />
		</h2>
		<table>
			<tr>
				<td align="center">
					<div align="left" class="datagrid">
						<b>������ Card</b>
						<table id="CardInfo">
							<thead>
								<tr style="background-color: #cfe" align="center">
									<th align="center">
										���͹���ѵ� <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										�Ҿ Background <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										�վ����ѧ <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										�����ҾPreview <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										��Ҵ(���ҧ x �٧)<span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										��Ǵ <span style="color: #CC3300">*</span>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr align="center">
									<td align="center">
										<input id="txtCardName1" type="text" style="width: 100px;" value="<%=lisCard.CardName%>" />
									</td>
									<td align="center">
										<input id="txtBGImage1" type="text" style="width: 150px;" value="<%=lisCard.BGImage%>" />
									</td>
									<td align="center">
										<input id="txtBGColor1" type="text" style="width: 80px;" value="<%=lisCard.BGColor%>" />
									</td>
									<td align="center">
										<input id="txtTemplateImage1" type="text" style="width: 150px;" value="<%=lisCard.TemplateImage%>" />
									</td>
									<td align="center">
										<input id="txtWidth1" type="text" style="width: 45px;" class="checknum" value="<%=lisCard.Width%>" /><span
											style="color: #000000"> X </span>
										<input id="txtHeight1" type="text" style="width: 45px;" class="checknum" value="<%=lisCard.Height%>" />
									</td>
									<td align="center">
										<select id="ddCategory" style="width: 120px;">
											<%
												Dim SetCategories As String
												For Each item As clsCategories In listCategory
													SetCategories = ""
													If item.CategoryName = lisCard.Category Then
														SetCategories = "selected=selected"
																			  End If%>
											<option value="<%=item.CategoryName%>" <%=SetCategories%>>
												<%=item.CategoryName%></option>
											<%
																  Next%>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
						<br />
					</div>
					<br />
					<div align="left">
						<input id="btnSaveCard" type="button" value="Save Card" /></div>
				</td>
				<td align="center">
					<div align="left" class="datagrid">
						<b>������ Logo</b>&nbsp;&nbsp;<input id="chkNoLogo" type="checkbox" />����� Logo
						<table id="ImageInfo">
							<thead>
								<tr style="background-color: #cfe" align="center">
									<th align="center">
										᡹ Y <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										᡹ X <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										���ҧ <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										�٧ <span style="color: #CC3300">*</span>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr align="center">
									<td align="center">
										<input id="txtPositionTop2" type="text" style="width: 60px;" class="checknum" value="<%=lisCard.ImageFields(0).PositionTop%>" />
									</td>
									<td align="center">
										<input id="txtPositionLeft2" type="text" style="width: 60px;" class="checknum" value="<%=lisCard.ImageFields(0).PositionLeft%>" />
									</td>
									<td align="center">
										<input id="txtWidth2" type="text" style="width: 40px;" class="checknum" value="<%=lisCard.ImageFields(0).Width%>" />
									</td>
									<td align="center">
										<input id="txtHeight2" type="text" style="width: 40px;" class="checknum" value="<%=lisCard.ImageFields(0).Height%>" />
									</td>
								</tr>
							</tbody>
						</table>
						<br />
					</div>
					<br />
					<div align="left">
						<input id="btnSaveImage" type="button" value="Save Image" /></div>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<br />
					<br />
					<div align="left" class="datagrid">
						<br />
						<b>������/���˹觢ͧ�����ź� Card</b>
						<table id="TextInfo" style="padding: 10px;">
							<thead>
								<tr style="background-color: #cfe">
									<th align="center">
										�ӴѺ <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										���ͤ������ <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										��ͤ���<br />
										Guide
									</th>
									<th align="center">
										᡹ Y <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										᡹ X <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										ForeColor <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										���� Font <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										Font<br />
										Size <span style="color: #CC3300">*</span>
									</th>
									<th align="center">
										Font<br />
										Bold
									</th>
									<th align="center">
										Font<br />
										Italic
									</th>
									<th align="center">
										Font<br />
										Underline
									</th>
									<th align="center">
										Right<br />
										2Left
									</th>
									<th align="center">
										Align<br />
										Center
									</th>
									<th>
									</th>
									<th>
									</th>
								</tr>
							</thead>
							<tbody>
								<%		Dim Count As Integer = 0
									For Each ObjectTextFields As clsCardTextField In lisCard.TextFields
								%>
								<tr align="center">
									<td align="center">
										<%=lisCard.TextFields(Count).Seq%>
									</td>
									<td align="center">
										<input id="txtFieldName_<%=lisCard.TextFields(Count).Seq%>" name="txtFieldName" type="text"
											style="width: 100px;" value="<%=lisCard.TextFields(Count).FieldName%>" />
									</td>
									<td align="center">
										<input id="txtTextValue_<%=lisCard.TextFields(Count).Seq%>" type="text" style="width: 200px;"
											value="<%=lisCard.TextFields(Count).TextValue%>" />
									</td>
									<td align="center">
										<input id="txtPositionTop_<%=lisCard.TextFields(Count).Seq%>" type="text" style="width: 60px;"
											class="checknum" value="<%=lisCard.TextFields(Count).PositionTop%>" />
									</td>
									<td align="center">
										<input id="txtPositionLeft_<%=lisCard.TextFields(Count).Seq%>" type="text" style="width: 60px;"
											class="checknum" value="<%=lisCard.TextFields(Count).PositionLeft%>" />
									</td>
									<td align="center">
										<input id="txtForeColor_<%=lisCard.TextFields(Count).Seq%>" type="text" style="width: 80px;"
											maxlength="7" value="<%=lisCard.TextFields(Count).ForeColor%>" />
									</td>
									<td align="center">
										<select id="ddFontFamily_<%=lisCard.TextFields(Count).Seq%>" name="ddFontFamily">
											<%
												Dim SetFontFamily As String
												For Each item As clsFontFamily In listFontFamily
													SetFontFamily = ""
													If item.FontName = lisCard.TextFields(Count).FontFamily Then
														SetFontFamily = "selected=selected"
																			   End If%>
											<option id="FontName" value="<%=item.FontName%>" <%=SetFontFamily%>>
												<%=item.FontName%></option>
											<%  Next
											%>
										</select>
									</td>
									<td align="center">
										<input id="txtFontSize_<%=lisCard.TextFields(Count).Seq%>" type="text" style="width: 40px;"
											class="checknum" maxlength="5" value="<%=lisCard.TextFields(Count).FontSize%>" />
									</td>
									<td align="center">
										<select id="ddFontBold_<%=lisCard.TextFields(Count).Seq%>" name="ddFontBold">
											<%If lisCard.TextFields(Count).FontBold = "Y" Then%>
											<option value="Y" selected="selected">Yes</option>
											<option value="N">No</option>
											<%Else%>
											<option value="Y">Yes</option>
											<option value="N" selected="selected">No</option>
											<%End If%>
										</select>
									</td>
									<td align="center">
										<select id="ddFontItalic_<%=lisCard.TextFields(Count).Seq%>" name="ddFontItalic">
											<%If lisCard.TextFields(Count).FontItalic = "Y" Then%>
											<option value="Y" selected="selected">Yes</option>
											<option value="N">No</option>
											<%Else%>
											<option value="Y">Yes</option>
											<option value="N" selected="selected">No</option>
											<%End If%>
										</select>
									</td>
									<td align="center">
										<select id="ddFontUnderline_<%=lisCard.TextFields(Count).Seq%>" name="ddFontUnderline">
											<%If lisCard.TextFields(Count).FontUnderline = "Y" Then%>
											<option value="Y" selected="selected">Yes</option>
											<option value="N">No</option>
											<%Else%>
											<option value="Y">Yes</option>
											<option value="N" selected="selected">No</option>
											<%End If%>
										</select>
									</td>
									<td align="center">
										<select id="ddRight2Left_<%=lisCard.TextFields(Count).Seq%>" name="ddRight2Left">
											<%If lisCard.TextFields(Count).Right2Left = "Y" Then%>
											<option value="Y" selected="selected">Yes</option>
											<option value="N">No</option>
											<%Else%>
											<option value="Y">Yes</option>
											<option value="N" selected="selected">No</option>
											<%End If%>
										</select>
									</td>
									<td align="center">
										<select id="ddAlignCenter_<%=lisCard.TextFields(Count).Seq%>" name="ddAlignCenter">
											<%If lisCard.TextFields(Count).AlignCenter = "Y" Then%>
											<option value="Y" selected="selected">Yes</option>
											<option value="N">No</option>
											<%Else%>
											<option value="Y">Yes</option>
											<option value="N" selected="selected">No</option>
											<%End If%>
										</select>
									</td>
									<td>
										<span id="<%=lisCard.TextFields(Count).Seq%>" class="clsEditText" style="color: Red;
											cursor: pointer; font-weight: bold; text-decoration: underline;">Save</span>
									</td>
									<td>
										<span id="del_<%=lisCard.TextFields(Count).Seq%>" class="clsDeleteText" style="color: Red;
											cursor: pointer; font-weight: bold; text-decoration: underline;">Delete</span>
									</td>
								</tr>
								<%
									Count += 1
											  Next%>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" colspan="2">
					<br />
					<input id="CreateNewCard" type="button" value="CreateNewRow" />
				</td>
			</tr>
		</table>
		<br />
		<div id="btn-card" align="left">
			<%If Request("Type") = "Edit" Then%>
			<input id="btnBackEdit" type="button" class="btn-face2" style="background-color: #E0DDDD"
				value="Back To Edit Active Template" />
			<%Else%>
			<input id="btnBackManage" type="button" class="btn-face2" style="background-color: #E0DDDD"
				value="<< Back To Manage Flag" />
			<%End If%>
		</div>
	</fieldset>
	</form>
</asp:Content>
