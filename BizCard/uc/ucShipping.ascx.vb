Imports System.Collections.Generic

Partial Class uc_ucShipping
    Inherits clsUCBase

    Public ReadOnly Property ShipAddr1() As String
        Get
            Return lbShipAddr1.Text
        End Get
    End Property

    Public ReadOnly Property ShipAddr2() As String
        Get
            Return lbShipAddr2.Text
        End Get
    End Property

    Public ReadOnly Property ShipAddr3() As String
        Get
            Return lbShipAddr3.Text
        End Get
    End Property

    Public ReadOnly Property ShipAddr4() As String
        Get
            Return lbShipAddr4.Text
        End Get

    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If objSessionVar.ModelUser IsNot Nothing Then
                If objSessionVar.ModelUser.Shipping.Count > 0 Then
                    lbCustID.Text = objSessionVar.ModelUser.CustID
                    lbCustName.Text = objSessionVar.ModelUser.Shipping.Item(0).ShipContactor
                    lbShipPhoneNo.Text = objSessionVar.ModelUser.Shipping.Item(0).ShipPhoneNo
                    lbShipAddr1.Text = objSessionVar.ModelUser.Shipping.Item(0).ShipAddr1
                    lbShipAddr2.Text = objSessionVar.ModelUser.Shipping.Item(0).ShipAddr2
                    lbShipAddr3.Text = objSessionVar.ModelUser.Shipping.Item(0).ShipAddr3
                    lbShipAddr4.Text = objSessionVar.ModelUser.Shipping.Item(0).ShipAddr4
                End If
            End If

        End If


    End Sub
End Class
