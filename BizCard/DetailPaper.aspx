﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	CodeFile="DetailPaper.aspx.vb" Inherits="DetailPaper" Title="เลือกกระดาษ ระบุจำนวนสั่งทำ และสถานที่จัดส่ง" %>

<%@ Register Src="uc/ucBar.ascx" TagName="ucBar" TagPrefix="uc1" %>
<%@ Register Src="uc/ucSelectPaper.ascx" TagName="ucSelectPaper" TagPrefix="uc2" %>
<%@ Register Src="uc/ucProduct.ascx" TagName="ucProduct" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<div style="margin: auto;">
		<img src="images/h-make-card01.png" class="png" alt="ทำนามบัตร" title="ทำนามบัตร" /><br />
	</div>
	<div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<uc1:ucBar ID="UcBar1" runat="server" />
		<h1>
			บริการสั่งพิมพ์นามบัตรออนไลน์ | ประเภท Template Card</h1>
		<uc3:ucProduct ID="UcProduct1" runat="server" />
		<br />
		<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
			<uc2:ucSelectPaper ID="UcSelectPaper1" runat="server" />
		</div>
		<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
			<asp:Label ID="lbTalkError" runat="server" Font-Size="Small" ForeColor="Red" Text="กรุณา LogIn ด้วยค่ะ"></asp:Label>
		</div>
		<div style="background-color: #fff; margin: auto; clear: both;">
			<div id="btn-card" align="left">
				<asp:HyperLink ID="HyperLink2" NavigateUrl="~/AddInformation.aspx" runat="server"
					class="btn-face2"><< กลับไปแก้ไขรายละเอียด</asp:HyperLink>
				<asp:ImageButton ID="btnNext" runat="server" ImageUrl="~/images/next_03.gif" Style="float: right;" />
			</div>
		</div>
	</div>
</asp:Content>
