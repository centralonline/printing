Imports System.Drawing
Imports System.Drawing.Text
Imports System.Drawing.Imaging

Partial Class Preview
    Inherits clsHTTPBase

    Protected Sub SubmitButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitButton.Click
        Dim oBitmap As Bitmap = New Bitmap(400, 250)
        Dim oGraphic As Graphics = Graphics.FromImage(oBitmap)
        Dim oColor As System.Drawing.Color

        Dim sColor As String = Me.BackgroundColor.SelectedValue
        Dim sText As String = Me.Text.Text
        Dim sFont As String = Me.Font.SelectedValue

        Select Case sColor
            Case "red"
                oColor = Color.Red
            Case "green"
                oColor = Color.Green
            Case "navy"
                oColor = Color.Navy
            Case "orange"
                oColor = Color.Orange
            Case Else
                oColor = Color.Gray
        End Select

        Dim oBrush As New SolidBrush(oColor)
        oGraphic.FillRectangle(oBrush, 0, 0, 400, 250)
        oGraphic.TextRenderingHint = TextRenderingHint.AntiAliasGridFit

        '-->add bg image (background)
        Dim oImageBG As Image = New Bitmap(Server.MapPath("images/bg.jpg"))
        oGraphic.DrawImage(oImageBG, 0, 0, 400, 250)

        '-->add bg image
        Dim oImage As Image = New Bitmap(Server.MapPath("images/hellbluegirl.jpg"))
        oGraphic.DrawImage(oImage, 250, 50)

        '-->write text
        Dim oFont As New Font(sFont, Me.Size.SelectedValue)
        Dim oPoint As New PointF(105.0F, 55.0F)
        Dim oBrushWrite As New SolidBrush(Color.White)

        Dim sf As New StringFormat
        'sf.FormatFlags = StringFormatFlags.DirectionRightToLeft
        sf.Alignment = StringAlignment.Center

        oGraphic.DrawString("Left2Right", oFont, oBrushWrite, 200, 20, sf)

        '-----+-

        'Dim oFont As New Font(oTextField.FontFamily, oTextField.FontSize * Zoom, FontStyle, GraphicsUnit.Pixel)

        'Dim oPoint As New Point(oTextField.PositionTop, oTextField.PositionLeft)
        'Dim oBrush As New SolidBrush(Me.GetColorFromHtml(oTextField.ForeColor))

        'oGraphic.DrawString("Right2Left", oFont, oBrushWrite, 300, 50, sf)
        'oGraphic.DrawString("3 2 1", oFont, oBrushWrite, 300, 80, sf)
        'oGraphic.DrawString("Damronkgiet Somkom", oFont, oBrushWrite, 300, 110, sf)
        '-------


        Session("oBitMap") = oBitmap

        '-->save file on server disk
        'Dim ici As ImageCodecInfo = GetImageCodec("image/jpeg")
        'Dim eps As New EncoderParameters(1)
        'eps.Param(0) = New EncoderParameter(Encoder.Quality, 100) 'or whatever other quality value you want (Max = 100)
        'oBitmap.Save(Server.MapPath("test2_200.jpg"), ici, eps)

    End Sub

End Class
