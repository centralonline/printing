Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.Data
Imports System.Collections.Generic
Partial Class uc_ucStepNavigator
    Inherits clsUCBase

    Dim ListPaper As List(Of clsPaper)
    Dim clsListPaper As New clsPaperEngine

    Private TypePaperValue As String
    Public Property TypePaper() As String
        Get
            Return TypePaperValue
        End Get
        Set(ByVal value As String)
            TypePaperValue = value
        End Set
    End Property
    Private TypePaperIDValue As String
    Public Property TypePaperID() As String
        Get
            Return TypePaperIDValue
        End Get
        Set(ByVal value As String)
            TypePaperIDValue = value
        End Set
    End Property
    Private PriceValue As String
    Public Property Price() As String
        Get
            Return PriceValue
        End Get
        Set(ByVal value As String)
            PriceValue = value
        End Set
    End Property
    Private NumberOrderValue As String
    Public Property NumberOrder() As String
        Get
            Return NumberOrderValue
        End Get
        Set(ByVal value As String)
            NumberOrderValue = value
        End Set
    End Property
    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        MyBase.OnInit(e)
        ListPaper = clsListPaper.GetListPapersize("Card", True)
        ddlPaper.DataSource = ListPaper
        ddlPaper.DataTextField = "Description"
        ddlPaper.DataValueField = "PaperID"
        ddlPaper.DataBind()
        Me.Session("Price") = ListPaper

        Me.TypePaper = ListPaper.Item(Me.ddlPaper.SelectedIndex).Description.ToString()
        Me.TypePaperID = ListPaper.Item(Me.ddlPaper.SelectedIndex).PaperID.ToString()
        Me.Price = ListPaper.Item(Me.ddlPaper.SelectedIndex).Price.ToString()
        Me.NumberOrder = Me.ddlNumber.SelectedItem.ToString()

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '-------------------Ex. Paper ------------------------------
        If (Me.Session("Price") Is Nothing) Then

        Else
            ListPaper = Me.Session("Price")
        End If
        If (Not IsPostBack) Then
            txtPrice.Text = ListPaper.Item(Me.ddlPaper.SelectedIndex).Price.ToString()
        End If

        Me.TypePaper = ListPaper.Item(Me.ddlPaper.SelectedIndex).Description.ToString()
        Me.TypePaperID = ListPaper.Item(Me.ddlPaper.SelectedIndex).PaperID.ToString()
        Me.Price = ListPaper.Item(Me.ddlPaper.SelectedIndex).Price.ToString()
        Me.NumberOrder = Me.ddlNumber.SelectedItem.ToString()
     
        '-----------------------------------------------------------
    End Sub
    Protected Sub ddlPaper_SelectedIndexChanged2(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim index As Integer = Me.ddlPaper.SelectedIndex
        txtPrice.Text = ListPaper.Item(index).Price.ToString()
        Me.TypePaper = ListPaper.Item(Me.ddlPaper.SelectedIndex).Description.ToString()
        Me.TypePaperID = ListPaper.Item(Me.ddlPaper.SelectedIndex).PaperID.ToString()
        Me.Price = ListPaper.Item(Me.ddlPaper.SelectedIndex).Price.ToString()
    End Sub

    Protected Sub ddlNumber_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.NumberOrder = Me.ddlNumber.SelectedItem.ToString()
    End Sub
End Class
