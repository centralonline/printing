Imports System.Collections.Generic
Imports UserEngineTrendyPrint
Partial Class Main
    Inherits clsHTTPBase

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		SwitchLoginPanel(False)

		If Page.IsPostBack = False Then
			If (Request.QueryString("id") = 1) Then
				ShowMessage("��س���ͤ�Թ���¤��")
			End If

			If Me.objSessionVar.ModelUser IsNot Nothing Then
				SwitchLoginPanel(True)
			Else
				SwitchLoginPanel(False)
			End If

			If (Request.QueryString("UN") <> "") Then
				Dim PD As String = Request.QueryString("PD")
				Dim UN As String = Request.QueryString("UN")

				PD = clsCrypto.Data_Hex_Asc(PD)
				PD = clsCrypto.Decrypt(PD)

				If Not IsLogin(UN, PD) Then
					ShowMessage("��سҵ�Ǩ�ͺ Username ����  Password ���١��ͧ���¤��")
				Else
					SwitchLoginPanel(True)
				End If
			End If
		End If
	End Sub
	Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogin.Click
		
		Dim UserID As String = Me.txtUserName.Text
		Dim Password As String = Me.txtPassword.Text

		If Not IsLogin(UserID, Password) Then
			ShowMessage("��سҵ�Ǩ�ͺ Username ����  Password ���١��ͧ���¤��")
		Else
			SwitchLoginPanel(True)
		End If
	End Sub
    Protected Sub btnLogout_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogout.Click
		SwitchLoginPanel(False)
		Me.objSessionVar.ModelUser = Nothing
        Response.Redirect("Main.aspx", True)
    End Sub

	Private Sub SwitchLoginPanel(ByVal Open As Boolean)
		If Open = True Then	'Login ����
			Me.lbUserName.Text = Me.objSessionVar.ModelUser.UserName
			Panel1.Visible = True
			Panel2.Visible = False
		Else '�ѧ����� Login
			Panel1.Visible = False
			Panel2.Visible = True
		End If
	End Sub
	Private Function IsLogin(ByVal userid As String, ByVal password As String) As Boolean
		Dim Engine As New UserEngine
		Dim resultIsLogin As Boolean
		resultIsLogin = Engine.Login(userid, password)
		Return resultIsLogin
	End Function

End Class
