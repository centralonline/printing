﻿
Partial Class Admin_ajDelete
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Complete As Boolean
        Dim CardID As String = Request("CardID")
        Dim Seq As String = Request("Seq")
        Dim objTextCard As New clsCardProvider

        If CardID = "" Or Seq = "" Then
            Response.Write("กรุณาเลือกรายการที่ต้องการลบด้วยค่ะ")
            Exit Sub
        End If

        Seq = Left(Seq, Seq.Length - 1)
        Dim lisSeq() As String = Seq.Split(",")

        For Count As Integer = 0 To (lisSeq.Length - 1)
            Complete = objTextCard.DeleteCardTemp(CardID, lisSeq(Count))
        Next

        If Complete Then
            Response.Write("ลบรายการเรียบร้อยแล้วค่ะ")
        Else
            Response.Write("Error!!!...กรุณาทำรายการอีกครั้งค่ะ")
        End If

    End Sub

End Class
