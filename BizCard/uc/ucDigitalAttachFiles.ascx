<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucDigitalAttachFiles.ascx.vb"
	Inherits="uc_ucAttachFiles" %>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td align="right">
			<asp:FileUpload ID="FileUpload1" runat="server" />
		</td>
	</tr>
	<tr>
		<td align="right">
			<asp:Button ID="btnAttach" runat="server" Text="Attach File" OnClick="btnAttach_Click" />
		</td>
	</tr>
	<tr>
		<td>
			<asp:DataList ID="DataList1" runat="server">
				<HeaderTemplate>
					<table border="0" width="100%">
						<tr>
							<td>
								Attachment
							</td>
							<td>
								Size
							</td>
							<td>
								Remove
							</td>
						</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td>
							Logo.jpg
						</td>
						<td>
							28KB
						</td>
						<td>
							X
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
					<tr>
						<td colspan="3" align="right">
							<asp:Label ID="lblTotal" runat="server" Text="Total 1.28 MB of 10.0 MB"></asp:Label>
						</td>
					</tr>
					</table>
				</FooterTemplate>
			</asp:DataList>
		</td>
	</tr>
</table>
