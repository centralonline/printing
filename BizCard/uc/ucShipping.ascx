﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucShipping.ascx.vb" Inherits="uc_ucShipping" %>
<p class="head-column">
    ที่อยู่จัดส่งสินค้า</p>
<table>
    <tr>
        <td style="width:150px;">
            รหัสลูกค้า:
        </td>
        <td style="width:200px;>
            <asp:Label ID="lbCustID" runat="server"></asp:Label>
        </td>
        <td style="width:150px;">
            ที่อยู่จัดส่ง:
        </td>
        <td style="width:200px;>
            <asp:Label ID="lbShipAddr1" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            ชื่อลูกค้า:
        </td>
        <td>
            <asp:Label ID="lbCustName" runat="server"></asp:Label>
        </td>
        <td>
        </td>
        <td>
            <asp:Label ID="lbShipAddr2" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
        เบอร์ติดต่อกลับ:
        </td>
        <td>
            <asp:Label ID="lbShipPhoneNo" runat="server"></asp:Label>
        </td>
        <td>
        </td>
        <td>
            <asp:Label ID="lbShipAddr3" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
        หมายเหตุ
        </td>
        <td>
            <asp:Label ID="lbShipRemark" runat="server"></asp:Label>
        </td>
        <td>
        </td>
        <td>
            <asp:Label ID="lbShipAddr4" runat="server"></asp:Label>
        </td>
    </tr>
</table>
