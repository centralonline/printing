﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucCategories.ascx.vb"
    Inherits="uc_ucCategories" %>
<asp:Panel ID="Panel2" runat="server">
    <div style="padding: 10px; border: 1px solid #ddd;">
    <%="" %>
        <table>
            <thead class="head-menu">
                <tr>
                    <th>
                        Category
                    </th>
                </tr>
            </thead>
            <tbody>
                <%  Dim Count As Integer = 0
                    For Each oCategories As clsCategories In listCategory
                        Dim Categories As String = listCategory.Item(Count).CategoryName.Replace("&", "^")%>
                <tr>
                    <%  Dim Highlight As String = ""
                        If listCategory.Item(Count).CategoryName.Replace("&", "^") = Request("Categories") Then
                            Highlight = "Style = 'background-color:#FFCC99'"
                        End If%>
                    <td <%=Highlight%>>
                        <a id="<%=listCategory.Item(Count).CategoryID%>" href="Templates.aspx?Categories=<%=Categories%>">
                            <%=listCategory.Item(Count).CategoryName%></a>
                    </td>
                </tr>
                <%  Count += 1
                Next%>
                <tr>
                <%
				Dim Highlight_All As String = ""
                If Request("Categories") = "" then 
					Highlight_All = "Style = 'background-color:#FFCC99'"
                %>
                 <%End if %>
                 <td <%=Highlight_All %>>
                        <a id="All" href="Templates.aspx">All</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Panel>
