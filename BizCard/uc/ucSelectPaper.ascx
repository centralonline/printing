﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucSelectPaper.ascx.vb"
	Inherits="uc_ucSelectPaper" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
	Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="ucHeadPanel1.ascx" TagName="ucHeadPanel1" TagPrefix="uc1" %>
<style type="text/css">
	.style12
	{
		width: 225px;
		height: 23px;
	}
	.style14
	{
		width: 225px;
		height: 22px;
	}
	.style15
	{
		height: 22px;
	}
	.style16
	{
		width: 102px;
	}
	.style20
	{
		width: 110px;
	}
	.style28
	{
		width: 505px;
	}
	.style29
	{
		width: 159px;
	}
	.style37
	{
		width: 168px;
	}
</style>
<style>
	.newspaper
	{
		-webkit-column-count: 3; /* Chrome, Safari, Opera */
		-moz-column-count: 3; /* Firefox */
		column-count: 3;
	}
</style>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
	<contenttemplate>
        <table style="width: 100%">
            <tr>
                <td style="height: 21px">
                    <asp:Panel ID="Panel1" runat="server" Width="100%">
                        <p class="head-column">
                            เลือกกระดาษที่ต้องการทำ</p>
                        <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False"
                            OnRowDataBound="GridView1_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:RadioButton ID="radPaper" runat="server" GroupName="xx" AutoPostBack="True"
                                            OnCheckedChanged="radPaper_CheckedChanged1" Checked="True" />
                                    </ItemTemplate>
                                    <ItemStyle Width="35px"></ItemStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="รหัสกระดาษ">
                                    <EditItemTemplate>
                                        <asp:TextBox runat="server" Text='<%# Bind("PaperID") %>' ID="TextBox2"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle Width="85px" HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Bind("PaperID") %>' ID="lbPaperID"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ชื่อกระดาษ">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemStyle Width="325px" HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lbPaperName" runat="server" Text='<%# eval("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ราคาปกติ">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                    <ItemStyle Width="85px" HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lbCost" runat="server" Text='<%# eval("Price") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ราคาโปรโมชั่น">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                                    <ItemStyle Width="100px" HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lbPromotion" runat="server" Text='<%# eval("Promotion") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
			</table>
	<table width="100%">
		<caption>
			<br />
			<tr>
				<td style="height: 21px; text-align: left" valign="top">
					<p class="head-column">
						บริการพิเศษ</p>
				</td>
			</tr>
		</caption>
		</table>
		<td>
		<table width="100%" style="border:1px; border-color:#da9">	
			<tr>
			<td class="style28">
			<table>
			<tr><td style="font-size:16px" class="style29" colspan="2"><u>บริการเคลือบบัตรพิเศษ</u></td></tr>
             <tr><td style="height:15px">&nbsp;</td></tr>
				<%--<caption>--%>
					<%--<tr>
						<td class="style32">
							<asp:RadioButton ID="radservice1" runat="server" AutoPostBack="True" 
								GroupName="angle" Text="" />
						</td>
                        <td class="style33">
							<asp:Label ID="lbServicePrice1" runat="server"></asp:Label>
                            <asp:HiddenField ID="lbServicePriceHidden1" runat="server"></asp:HiddenField>
						    บาท
                          </td>
						<td class="style34">
							<asp:RadioButton ID="radservice3" runat="server" AutoPostBack="True" 
								GroupName="angle" Text="" />
						</td>
						<td>
							<asp:Label ID="lbServicePrice3" runat="server"></asp:Label>
                              <asp:HiddenField ID="lbServicePriceHidden3" runat="server"></asp:HiddenField>
						    บาท</td>
					</tr>
					<tr>
						<td class="style32">
							<asp:RadioButton ID="radservice2" runat="server" AutoPostBack="True" 
								GroupName="angle" Text="" />
						</td>
                        <td class="style33">
							<asp:Label ID="lbServicePrice2" runat="server"></asp:Label>
                             <asp:HiddenField ID="lbServicePriceHidden2" runat="server"></asp:HiddenField>
						    บาท</td>
						<td class="style34">
							<asp:RadioButton ID="radservice4" runat="server" AutoPostBack="true" 
								GroupName="angle" Text="" />
						</td>
						<td>
							<asp:Label ID="lbServicePrice4" runat="server"></asp:Label>
                             <asp:HiddenField ID="lbServicePriceHidden4" runat="server"></asp:HiddenField>
						    บาท</td>
					</tr>
					<tr>
						<td class="style29" colspan="2">
							<asp:RadioButton ID="radservice5" runat="server" AutoPostBack="True" 
								Checked="True" GroupName="angle" Text="" />
						</td>
					</tr>--%>
                    <tr>
                    <td>
                    <div class="style28">
                     <asp:RadioButtonList ID="radDescription" runat="server" AutoPostBack="True" Width="200px" 
                                 ></asp:RadioButtonList>
                    </div>
                    </td>
                    </tr>

				<%--</caption>--%>
			</table>
            

			</td>                   
                    <td align="left" style="font-size:16px" class="style29" colspan="2"><u>รูปตัวอย่างมุมนามบัตร</u>
                        <br />
                        <img src="images/uc/IMG1.jpg" style="width:300px" />
                        <br />
                    </td>
               <%-- </caption>--%>
			</tr>
	</table>
	<table>
    <tr>
			<td class="style28">
			<table>
	<tr><td style="font-size:16px"><u>บริการตัดมุม</u></td><td colspan="2" style="font-size:16px">( เลือกได้มากกว่า 1 มุม )</td></tr>
    <tr><td style="height:15px"></td></tr>
		<tr>
			<td colspan="3">
				<asp:RadioButtonList ID="radAngle" runat="server" AutoPostBack="True">
                </asp:RadioButtonList>
			    </td>
            <td>
							&nbsp;</td>
		</tr>
		<tr>
			<td class="style16">
			</td>
			<td style="width: 100">
				<asp:CheckBox ID="CheckBox1" runat="server" Text="มุมขวาบน" 
                    AutoPostBack="True" />
			</td>
			<td class="style37">
				<asp:Label ID="Label7" runat="server" Text="ขนาด :   "></asp:Label>
				<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
					<asp:ListItem>--SELECT--</asp:ListItem>
					<asp:ListItem>1. มุม 3.5 mm</asp:ListItem>
					<asp:ListItem>2. มุม 6 mm</asp:ListItem>
					<asp:ListItem>3. มุม 10 mm</asp:ListItem>
					<asp:ListItem>4. มุมตัดเฉียง</asp:ListItem>
				</asp:DropDownList>
			</td>
           
		</tr>
		<tr>
			<td class="style16">
			</td>
			<td class="style20">
				<asp:CheckBox ID="CheckBox2" runat="server" Text="มุมขวาล่าง" 
                    AutoPostBack="True" />
			</td>
			<td class="style37">
				<asp:Label ID="Label8" runat="server" Text="ขนาด :   "></asp:Label>
				<asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True">
					<asp:ListItem>--SELECT--</asp:ListItem>
					<asp:ListItem>1. มุม 3.5 mm</asp:ListItem>
					<asp:ListItem>2. มุม 6 mm</asp:ListItem>
					<asp:ListItem>3. มุม 10 mm</asp:ListItem>
					<asp:ListItem>4. มุมตัดเฉียง</asp:ListItem>
				</asp:DropDownList>
			</td>
            
		</tr>
		<tr>
			<td class="style16">
			</td>
			<td class="style20">
				<asp:CheckBox ID="CheckBox3" runat="server" Text="มุมซ้ายบน" AutoPostBack="True" />
			</td>
			<td class="style37">
				<asp:Label ID="Label9" runat="server" Text="ขนาด :   "></asp:Label>
				<asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True">
					<asp:ListItem>--SELECT--</asp:ListItem>
					<asp:ListItem>1. มุม 3.5 mm</asp:ListItem>
					<asp:ListItem>2. มุม 6 mm</asp:ListItem>
					<asp:ListItem>3. มุม 10 mm</asp:ListItem>
					<asp:ListItem>4. มุมตัดเฉียง</asp:ListItem>
				</asp:DropDownList>
			</td>
            
		</tr>
		<tr>
			<td class="style16">
			</td>
			<td class="style20">
				<asp:CheckBox ID="CheckBox4" runat="server" Text="มุมซ้ายล่าง" AutoPostBack="True" />
			</td>
			<td class="style37">
				<asp:Label ID="Label10" runat="server" Text="ขนาด :   "></asp:Label>
				<asp:DropDownList ID="DropDownList4" runat="server" AutoPostBack="True">
					<asp:ListItem>--SELECT--</asp:ListItem>
					<asp:ListItem>1. มุม 3.5 mm</asp:ListItem>
					<asp:ListItem>2. มุม 6 mm</asp:ListItem>
					<asp:ListItem>3. มุม 10 mm</asp:ListItem>
					<asp:ListItem>4. มุมตัดเฉียง</asp:ListItem>
				</asp:DropDownList>
			</td>
            
		</tr>
		<tr>
			<td colspan="2">
				&nbsp;</td>
		</tr>
        </table>
            </td>              
                        <td align="left" class="style29" colspan="2" style="font-size:16px">
                            <u>รูปตัวอย่างขนาดมุมนามบัตร</u>
                            <br />
                            <img src="images/uc/viewPromotion.jpg" style="width:300px" />
                            <br />
                        </td>
                        <%-- </caption>--%>
        </tr>
	</table>
	</table>
    
	<table width="100%">
	<tr>
		<td colspan="3" style="height: 21px; text-align: left" valign="top">
			<p class="head-column">
				รายละเอียด</p>
			<table width="100%">
				<tr>
					<td style="width: 225px">
						รหัสกระดาษ :
					</td>
					<td>
						<asp:Label ID="lbPaperID" runat="server" Text="Label"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 22px;">
						ชื่อกระดาษ :
					</td>
					<td style="height: 22px">
						<asp:Label ID="lbPaperName" runat="server" Text="Label"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 22px;">
						ราคาต่อหน่วย :
					</td>
					<td style="height: 22px">
						<asp:Label ID="lbPrice" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px;">
						ราคาโปรโมชั่น :
					</td>
					<td>
						<asp:Label ID="lbPromotion" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						<asp:Label ID="Label1" runat="server" Text="บาท" Visible="False"></asp:Label>
					</td>
				</tr>
				<tr>
					<td class="style12">
						ราคาเคลือบต่อหน่วย :
					</td>
					<td>
						<asp:Label ID="lbangle" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						<asp:Label ID="lbsumangle" runat="server" Visible="false"></asp:Label>บาท&nbsp;
                        <asp:Label ID="radioview" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="width: 225px;">
						ราคาตัดมุม :
					</td>
					<td>
						<asp:Label ID="lbview" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						<asp:Label ID="lbsumview" runat="server" Visible="False"></asp:Label>บาท&nbsp;
						<asp:Label ID="lbview1" runat="server"></asp:Label>
						<asp:Label ID="lbright1" runat="server"></asp:Label>
						<asp:Label ID="lbview2" runat="server"></asp:Label>
						<asp:Label ID="lbright2" runat="server"></asp:Label>
						<asp:Label ID="lbview3" runat="server"></asp:Label>
						<asp:Label ID="lbleft1" runat="server"></asp:Label>
						<asp:Label ID="lbview4" runat="server"></asp:Label>
						<asp:Label ID="lbleft2" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 24px;">
						จำนวนทีสั่งทำ :
					</td>
					<td style="height: 24px">
						<asp:DropDownList ID="ddlNumber" runat="server" AutoPostBack="True" CssClass="textdd"
							OnSelectedIndexChanged="ddlNumber_SelectedIndexChanged">
							<asp:ListItem>100</asp:ListItem>
							<asp:ListItem>200</asp:ListItem>
							<asp:ListItem>300</asp:ListItem>
							<asp:ListItem>400</asp:ListItem>
							<asp:ListItem>500</asp:ListItem>
							<asp:ListItem>600</asp:ListItem>
							<asp:ListItem>700</asp:ListItem>
							<asp:ListItem>800</asp:ListItem>
							<asp:ListItem>900</asp:ListItem>
							<asp:ListItem>1000</asp:ListItem>
						</asp:DropDownList>
						ใบ <span style="color: #ff0033">** สั่งทำขั้นตํ่า 100 ใบ</span>
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 22px">
						รวมมูลค่าสินค้า(ก่อน vat) :
					</td>
					<td style="height: 22px">
						<asp:Label ID="lbVat" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td class="style14">
						ภาษีมูลค่าเพิ่ม 7% :
					</td>
					<td class="style15">
						<asp:Label ID="lbVat_card" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px">
						ค่าจัดส่ง :</td>
					<td>
						<asp:Label ID="lbDelivery" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr id="tr_sum" runat="server" visible="false">
					<td style="width: 225px">
						รวมมูลค่าสินค้า :</td>
					<td>
						<asp:Label ID="lbSum" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px">
						<strong>จำนวนเงินรวมทั้งสิ้น </strong>
					</td>
					<td>
						<asp:Label ID="lbSummary" runat="server" CssClass="hilight"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 21px;">
					</td>
					<td style="height: 21px">
					</td>
				</tr>
				<tr>
					<td colspan="2" style="height: 21px;">
						<b>*หมายเหตุ:</b><br />
						ยอดการสั่งซื้อ 499 บาทขึ้นไปบริการส่งฟรี<br />
						กรณียอดสั่งซื้อต่ำกว่า 499 บาท คิดค่าบริการจัดส่ง 50 บาทต่อใบสั่งซื้อ<br />

					</td>
				</tr>
				<tr><td><asp:Label ID="lbshow" runat="server" CssClass="hilight"></asp:Label></td></tr>
			</table>
		</td>
	</tr>
</table>
   </contenttemplate>
</asp:UpdatePanel>
<asp:HiddenField ID="hidSum" runat="server" />
<asp:HiddenField ID="hidVat" runat="server" />
<asp:HiddenField ID="hidSummary" runat="server" />
