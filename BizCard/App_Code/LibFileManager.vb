Imports Microsoft.VisualBasic
Imports System.Data
Imports CoreBaseII
Imports System.Drawing
Imports System.IO
Imports System.Collections.Generic

#Region "clsFilemanager"
Public Class clsFileManager
	'Private UserIDValue As String
	'Public Property UserID() As String
	'    Get
	'        Return UserIDValue
	'    End Get
	'    Set(ByVal value As String)
	'        UserIDValue = value
	'    End Set
	'End Property

	Private ImageIDValue As Integer
	Public Property ImageID() As Integer
		Get
			Return ImageIDValue
		End Get
		Set(ByVal value As Integer)
			ImageIDValue = value
		End Set
	End Property

	Private UserIDValue As String
	Public Property UserID() As String
		Get
			Return UserIDValue
		End Get
		Set(ByVal value As String)
			UserIDValue = value
		End Set
	End Property

	Private ImageNameValue As String
	Public Property ImageName() As String
		Get
			Return ImageNameValue
		End Get
		Set(ByVal value As String)
			ImageNameValue = value
		End Set
	End Property

	Private ImageDefaultValue As Char
	Public Property ImageDefault() As Char
		Get
			Return ImageDefaultValue
		End Get
		Set(ByVal value As Char)
			ImageDefaultValue = value
		End Set
	End Property
End Class
#End Region

#Region "clsImageSize"
Public Class clsImageSize
	Private WidthValue As Integer
	Public Property Width() As Integer
		Get
			Return WidthValue
		End Get
		Set(ByVal value As Integer)
			WidthValue = value
		End Set
	End Property

	Private HeightValue As Integer
	Public Property Height() As Integer
		Get
			Return HeightValue
		End Get
		Set(ByVal value As Integer)
			HeightValue = value
		End Set
	End Property

	Public Sub New()
		Me.Width = 100
		Me.Height = 100
	End Sub

	Public Sub New(ByVal iWidth As Integer, ByVal iHeight As Integer)
		Me.Width = iWidth
		Me.Height = iHeight
	End Sub
End Class
#End Region

#Region "clsFileManagerEngine"
Public Class clsFileManagerEngine
	Inherits clsBLBase

	'Public Function CountImageByUserID(ByVal UserID As String) As Integer
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    Return TempFileProvider.CountImageByUserID(UserID)
	'End Function

	'Public Function GetDefaultImageByUserID(ByVal UserID As String) As String
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    Return TempFileProvider.GetDefaultImageByUserID(UserID)
	'End Function
	'Public Function getDefaultImageByTeam(ByVal TeamCode As String) As DataTable
	'    Dim TempDT As DataTable = Nothing
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    TempDT = TempFileProvider.GetDefaultImageByTeam(TeamCode)
	'    Return TempDT
	'End Function
	'Public Function GetSummaryGroupByTeam(ByVal TeamCode As String) As DataTable 'Add By Surapon
	'    Dim TempDT As DataTable = Nothing
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    TempDT = TempFileProvider.GetSummaryGroupByTeam(TeamCode)
	'    Return TempDT
	'End Function

	'Public Function GetDefaultImageGroupByTeam() As DataSet
	'    Dim TempDS As DataSet = Nothing
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    TempDS = TempFileProvider.GetAllDefaultImageGroupByTeam
	'    Return TempDS
	'End Function

	'Public Function GetDefaultImageGroupByTeam2() As DataSet
	'    Dim TempDS As DataSet = Nothing
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    TempDS = TempFileProvider.GetAllDefaultImageGroupByTeam2
	'    Return TempDS
	'End Function

	Public Function CheckFileSize(ByVal File As HttpPostedFile, ByVal FileSizeKB As Integer) As clsResultII
		Dim TempResult As New clsResultII
        'Check File Size
        'GIFT User �駵�ͧ���������Ҵ File �� 20MB
        Dim MaxSize As Integer = (FileSizeKB * 1024) * 100 ' Exp 2MB
		If File.ContentLength > MaxSize Then
			TempResult.Flag = False
            TempResult.Message = "��Ҵ����ͧ����Թ  100 MB"
		End If
		Return TempResult
	End Function

	Public Function CheckImageFile(ByVal File As HttpPostedFile) As clsResultII
		Dim TempResult As New clsResultII
		Dim ext As String = File.FileName.Substring(File.FileName.LastIndexOf(".") + 1).ToLower()
		Dim FileType As New List(Of String)
		With FileType
			.Add("jpg")
			.Add("jpeg")
			.Add("tiff")
			.Add("tif")
			.Add("gif")
			.Add("bmp")
		End With

		For Each TypeFile As String In FileType
			If TypeFile = ext Then
				TempResult.Flag = True
				Exit For
			Else
				TempResult.Flag = False
				TempResult.Message = ext
			End If
		Next
		Return TempResult
	End Function
	Public Function CheckUploadFile(ByVal File As HttpPostedFile) As clsResultII
		Dim TempResult As New clsResultII
		Dim ext As String = File.FileName.Substring(File.FileName.LastIndexOf(".") + 1).ToLower()
		Dim FileType As New List(Of String)
        'Requiment ������ҵ�ͧ����� Word Excel Powerpoint  Version 2003
        'Requiment ������ҵ�ͧ����� Word Excel Powerpoint  Version 2007
        With FileType
            .Add("xlsx")
            .Add("docx")
            .Add("pptx")
            .Add("doc")
            .Add("xls")
            .Add("ppt")
            .Add("ai")
            .Add("eps")
            .Add("psd")
            .Add("pdf")
            .Add("jpg")
            .Add("jpeg")
            .Add("tiff")
            .Add("tif")
            .Add("gif")
            .Add("bmp")
        End With
		For Each TypeFile As String In FileType
			If TypeFile = ext Then
				TempResult.Flag = True
				Exit For
			Else
				TempResult.Flag = False
				TempResult.Message = ext
			End If
		Next
		Return TempResult
	End Function

	'Public Function GetImagesByUserID(ByVal UserID As String) As DataTable
	'    Dim TempDT As DataTable = Nothing
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    TempDT = TempFileProvider.GetImageByUserID(UserID)
	'    Return TempDT
	'End Function

	'Public Function GetImageByID(ByVal ImageID As String) As clsFileManager
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    Dim TempFile As clsFileManager = TempFileProvider.GetImageByID(ImageID)
	'    Return TempFile
	'End Function

	Public Function SetDefaultImage(ByVal ImageID As String) As clsResultII
		Dim TempResult As New clsResultII
		Dim TempFileProvider As New clsFileManagerProvider
		'TempFileProvider.SetDefaultImage(ImageID)
		Return TempResult
	End Function

	'Public Function DeleteImage(ByVal ImageID As Integer, ByVal strPath As String) As clsResultII
	'    Dim TempResult As New clsResultII
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    TempFileProvider.DeleteImage(ImageID, strPath)
	'    Return TempResult
	'End Function

	Public Function DeleteFile(ByVal strPath As String) As clsResultII
		Dim TempResult As New clsResultII
		Dim TempFileProvider As New clsFileManagerProvider
		TempFileProvider.DeleteFile(strPath)
		Return TempResult
	End Function

	Public Function UploadImage(ByVal File As HttpPostedFile, ByVal Path As String) As clsResultII
		Dim TempResult As New clsResultII
		Dim TempFileProvider As New clsFileManagerProvider

		TempResult.Message = TempFileProvider.InsertImage(File, Path)

		If TempResult.Message = "" Then
			TempResult.Flag = False
		Else
			TempResult.Flag = True
		End If
		Return TempResult
	End Function

	Public Function CopyFile(ByVal File As HttpPostedFile, ByVal strPath As String) As clsResultII
		Dim TempResult As New clsResultII
		Dim TempFileProvider As New clsFileManagerProvider
		Dim FileExtention As String = TempFileProvider.GetFileExtention(File)
		Dim FileName As String = clsMyUtil.RandomPassword(12) & FileExtention

		TempFileProvider.CopyFile(File, strPath & "\CardImages\UploadImage\UploadFiles\" & FileName)

		TempResult.Flag = True
		TempResult.Message = FileName
		Return TempResult
	End Function

	'Public Function AttachFile(ByVal File As HttpPostedFile, ByVal Path As String) As clsResultII
	'    Dim TempResult As New clsResultII
	'    Dim TempFileProvider As New clsFileManagerProvider
	'    TempResult = TempFileProvider.AttachFile(File, Path)
	'    Return TempResult
	'End Function

End Class

#End Region

#Region "clsFileManagerProvider"
Public Class clsFileManagerProvider
	Inherits clsBLBase

	'Public Function CountImageByUserID(ByVal UserID As String) As Integer
	'    Dim SqlStr As String = "select count(*) as ImageCount from tbwcuserimages where uUserid='" & UserID & "' "
	'    Dim Count As Integer = 0
	'    Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        Count = ObjDB.getDataSet(SqlStr, "ImageCount").Tables(0).Rows(0).Item(0)
	'    End Using
	'    Return Count
	'End Function

	'Public Function GetDefaultImageByUserID(ByVal UserID As String) As String
	'    Dim SqlStr As String = "SELECT ImageName FROM TBWCUserImages WHERE uUserID = '" & UserID & "'  AND ImageDefault = 1 "
	'    Dim TempString As String = "DEFAULT.GIF"
	'    Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        Dim TempDS As DataSet = ObjDB.getDataSet(SqlStr, "ImageDefaultUser")
	'        If TempDS.Tables.Count > 0 Then
	'            If TempDS.Tables(0).Rows.Count > 0 Then
	'                TempString = TempDS.Tables(0).Rows(0)("ImageName")
	'            End If
	'        End If
	'    End Using
	'    Return TempString
	'End Function

	'Public Function GetDefaultImageByTeam(ByVal TeamCode As String) As DataTable
	'    Dim TempDT As DataTable = Nothing
	'    Dim StrBdr As StringBuilder = Nothing

	'    StrBdr = New StringBuilder
	'    StrBdr.AppendLine("Select U.uUserID,U.uName,U.uSurname,ImageID,ISNULL(ImageName,'DEFAULT.GIF')as ImageName, u.uLevelID From TBWCUsers U ")
	'    StrBdr.AppendLine("LEFT OUTER JOIN TBWCUserImages T1 On U.uUserID = T1.uUserID ")
	'    StrBdr.AppendLine("Where u.TeamCode = '" & TeamCode & "' And ISNULL(T1.ImageDefault,1)=1 ")
	'    StrBdr.AppendLine("Order By u.uLevelID   ")

	'    Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        Dim TempDS As DataSet = ObjDB.getDataSet(StrBdr.ToString, "ImageDefaultTeam")
	'        If TempDS.Tables.Count > 0 Then
	'            TempDT = TempDS.Tables(0)
	'        End If
	'    End Using
	'    Return TempDT
	'End Function

	'Public Function GetAllDefaultImageGroupByTeam() As DataSet
	'    Dim TempDS As DataSet = Nothing
	'    Dim StrBdr As StringBuilder = Nothing
	'    Dim Relation1 As DataRelation = Nothing

	'    StrBdr = New StringBuilder
	'    StrBdr.AppendLine("Select Distinct(TeamName) From TBWCUsers Where uLevelID <> 'A' ")

	'    StrBdr.AppendLine("SELECT T1.uUserID, T1.uName, T1.uSurname, T1.uNickname, T1.TeamName,T1.TeamCode, ISNULL(T2.ImageName, 'DEFAULT.GIF') AS ImageName, T2.ImageDefault ")
	'    StrBdr.AppendLine("FROM TBWCUsers AS T1 LEFT OUTER JOIN ")
	'    StrBdr.AppendLine("TBWCUserImages AS T2 ON T1.uUserID = T2.uUserID AND ISNULL(T2.ImageDefault, 0) = 1 ")
	'    StrBdr.AppendLine("Where T1.uLevelID <> 'A' ")

	'    Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        TempDS = ObjDB.getDataSet(StrBdr.ToString, "UserTeam")
	'    End Using
	'    Relation1 = New DataRelation("UserTeam", TempDS.Tables(0).Columns("TeamName"), TempDS.Tables(1).Columns("TeamName"), True)
	'    Relation1.Nested = True
	'    TempDS.Relations.Add(Relation1)
	'    Return TempDS
	'End Function

	'Public Function GetAllDefaultImageGroupByTeam2() As DataSet
	'    Dim TempDS As DataSet = Nothing
	'    Dim StrBdr As StringBuilder = Nothing
	'    Dim Relation1 As DataRelation = Nothing

	'    StrBdr = New StringBuilder
	'    StrBdr.AppendLine("Select Distinct(TeamCode),TeamName From TBWCUsers Where uLevelID <> 'A' ")

	'    StrBdr.AppendLine("SELECT T1.uUserID, T1.uName, T1.uSurname, T1.uNickname, T1.TeamName,T1.TeamCode, ISNULL(T2.ImageName, 'DEFAULT.GIF') AS ImageName, T2.ImageDefault ")
	'    StrBdr.AppendLine("FROM TBWCUsers AS T1 LEFT OUTER JOIN ")
	'    StrBdr.AppendLine("TBWCUserImages AS T2 ON T1.uUserID = T2.uUserID AND ISNULL(T2.ImageDefault, 0) = 1 ")
	'    StrBdr.AppendLine("Where T1.uLevelID <> 'A' ")

	'    Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        TempDS = ObjDB.getDataSet(StrBdr.ToString, "UserTeam")
	'    End Using
	'    Relation1 = New DataRelation("UserTeam", TempDS.Tables(0).Columns("TeamCode"), TempDS.Tables(1).Columns("TeamCode"), True)
	'    Relation1.Nested = True
	'    TempDS.Relations.Add(Relation1)
	'    Return TempDS
	'End Function

	'Public Function GetImageByUserID(ByVal UserID As String) As DataTable
	'    Dim TempDT As DataTable = Nothing
	'    Dim SqlStr As String = Nothing
	'    SqlStr = "SELECT * From TBWCUserImages Where uUserID = '" & UserID & "' Order By ImageDefault DESC"
	'    Using objdb As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        Dim DS As DataSet = objdb.getDataSet(SqlStr, "Images")
	'        If DS.Tables.Count > 0 Then
	'            TempDT = DS.Tables(0)
	'        End If
	'    End Using
	'    Return TempDT
	'End Function

	'Public Function GetSummaryGroupByTeam(ByVal TeamCode As String) As DataTable  'Add By Surapon'
	'    Dim TempDT As DataTable = Nothing
	'    Dim StrBdr As StringBuilder = Nothing
	'    StrBdr = New StringBuilder

	'    With StrBdr
	'        .AppendLine("Select	u.teamcode,u.uUserID,u.uName,u.uSurname, ImageID, ISNULL(ImageName, 'DEFAULT.GIF')as ImageName, ")
	'        .AppendLine("		Isnull(UJ.CustCount,0)as 'CustCount' ,IsNull(u.CustReplyCount,0) As 'CustReplyCount'")
	'        .AppendLine("From	TBWCUsers u  Left Outer Join ")
	'        .AppendLine("	(")
	'        .AppendLine("		Select	TeamCode,T2.uUserID,Count(JobNo) As JobNo ,Count(*) as 'CustCount',Count(*) as 'CustReplyCount' ")
	'        .AppendLine("		From	TBWCJobs T1,TBWCUsers T2")
	'        .AppendLine("		Where	T1.CreateBy = T2.uUserID")
	'        .AppendLine("				And TeamCode='" & TeamCode & "'")
	'        .AppendLine("		Group by TeamCode,T2.uUserID")
	'        .AppendLine("		--Order by T2.uUserID")
	'        .AppendLine("		) UJ ")
	'        .AppendLine("")
	'        .AppendLine("	On u.uUserID = UJ.uUserID ")
	'        .AppendLine("	Left Outer Join TBWCUserImages T1 On u.uUserID = T1.uUserID ")
	'        .AppendLine("Where u.TeamCode='" & TeamCode & "' and ISNULL(T1.ImageDefault, 1) = 1")
	'    End With

	'    Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        Dim TempDS As DataSet = ObjDB.getDataSet(StrBdr.ToString(), "UserTeam")
	'        If TempDS.Tables.Count > 0 Then
	'            TempDT = TempDS.Tables(0)
	'        End If
	'    End Using
	'    Return TempDT
	'End Function

	'Public Function GetImageByID(ByVal ImageID As Integer) As clsFileManager
	'    Dim TempImage As clsFileManager = Nothing
	'    Dim SqlStr As String = Nothing

	'    SqlStr = "Select * From TBWCUserImages Where ImageID =" & ImageID
	'    Using ObjDb As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        ObjDb.SelectReQuery(SqlStr)
	'        If ObjDb.Go Then
	'            TempImage = New clsFileManager
	'            TempImage.ImageID = ObjDb.ReadField("ImageID")
	'            'TempImage.UserID = ObjDb.ReadField("uUserID")
	'            TempImage.ImageName = ObjDb.ReadField("ImageName")
	'            TempImage.ImageDefault = ObjDb.ReadField("ImageDefault")
	'        End If
	'    End Using
	'    Return TempImage
	'End Function

	Private Function GetImageSize(ByVal OldSize As clsImageSize, ByVal FixSize As clsImageSize) As clsImageSize
		Dim TempSize As clsImageSize = OldSize
		If OldSize.Height > FixSize.Height OrElse OldSize.Width > FixSize.Width Then
			If OldSize.Width > FixSize.Width Then
				TempSize.Height = Math.Ceiling((FixSize.Width * OldSize.Height) / OldSize.Width)
				TempSize.Width = FixSize.Width
			End If
			If TempSize.Height > FixSize.Height Then
				TempSize.Width = Math.Ceiling((FixSize.Height * TempSize.Width) / TempSize.Height)
				TempSize.Height = FixSize.Height
			End If
		End If
		Return TempSize
	End Function

	Public Function GetFileExtention(ByVal File As HttpPostedFile) As String
		Dim TempExt As String
		TempExt = Path.GetExtension(File.FileName)
		Return TempExt
	End Function

	Public Function InsertImage(ByVal File As HttpPostedFile, ByVal strPath As String) As String
		Dim FileExtention As String = Me.GetFileExtention(File)
		Dim ImgName As String = clsMyUtil.RandomPassword(12) & FileExtention
		Try
			Dim TempResult As New clsResultII
			Dim ImgThumb As New clsImageSize(100, 100) ' 35*51 
			Me.CreateImage(File, strPath & "\CardImages\UploadImage\Thumbnails\" & ImgName, ImgThumb) ' Thumbnails Images
			Me.CopyFile(File, strPath & "\CardImages\UploadImage\Currents\" & ImgName) ' Current Images
		Catch ex As Exception
			ImgName = ""
		End Try
		Return ImgName
	End Function

	'Public Sub DeleteImage(ByVal ImageID As Integer, ByVal strPath As String)
	'    Dim TempImage As New clsFileManager
	'    Dim SqlStr As String = Nothing
	'    TempImage = Me.GetImageByID(ImageID)
	'    Me.DeleteFile(strPath & "\member\400\" & TempImage.ImageName)
	'    Me.DeleteFile(strPath & "\member\150\" & TempImage.ImageName)
	'    Me.DeleteFile(strPath & "\member\70\" & TempImage.ImageName)
	'    SqlStr = "Delete From TBWCUserImages Where ImageID = " & ImageID
	'    Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
	'        ObjDB.ExecuteCommand(SqlStr, clsDBIII.eExecute.DeleteType)
	'    End Using
	'End Sub

	Public Sub DeleteFile(ByVal strPath As String)
		If System.IO.File.Exists(strPath) Then
			Dim s2 As New FileStream(strPath, FileMode.Open, FileAccess.ReadWrite, FileShare.None)
			's2.Dispose()
			's2.
			System.GC.Collect()
			'System.IO.File.Delete(strPath)
		End If
		'Kill("")
	End Sub

	'Public Function AttachFile(ByVal File As HttpPostedFile, ByVal strPath As String) As clsResultII
	'    Dim TempResult As New clsResultII
	'    Dim FileExtention As String = Me.GetFileExtention(File)
	'    Dim FileName As String = clsMyUtil.RandomPassword(5) & FileExtention
	'    Me.CopyFile(File, strPath & "\admin\attachfile\" & FileName)
	'    TempResult.Flag = True
	'    TempResult.Message = FileName
	'    Return TempResult
	'End Function

	Public Sub CopyFile(ByVal File As HttpPostedFile, ByVal strPath As String)
		Try
			File.SaveAs(strPath)
		Catch ex As Exception
		End Try
	End Sub

	'Public Sub CopyImage(ByVal File As HttpPostedFile, ByVal strpath As String)
	'    File.SaveAs(strpath)
	'End Sub

	Private Sub CreateImage(ByVal File As HttpPostedFile, ByVal strPath As String, ByVal FixSize As clsImageSize)
		Using uImg As Image = Image.FromStream(File.InputStream)
			Try
				Dim OldSize As New clsImageSize
				OldSize.Width = uImg.Width
				OldSize.Height = uImg.Height
				Dim NewSize As clsImageSize = Me.GetImageSize(OldSize, FixSize)
				Using uBitmap As Bitmap = New Bitmap(uImg, NewSize.Width, NewSize.Height)
					uBitmap.SetResolution(72, 72)
					uBitmap.Save(strPath)
				End Using
			Catch ex As Exception

			End Try
		End Using

	End Sub

	'Private Function ThumbnailCallback() As Boolean
	'    Return False
	'End Function

End Class
#End Region
