﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucCard.ascx.vb" Inherits="ucCard" %>
<asp:DataList ID="dlCard" runat="server" RepeatColumns="2" Width="350px">
    <ItemStyle VerticalAlign="Bottom" CssClass="topic1" />
    <ItemTemplate>
        <div style="padding: 10px;">
            <asp:HyperLink ID="HyperLink1" Width="260px" NavigateUrl='<%# "~/AddInformation.aspx?CardID=" & Eval("CardID") %>'
                runat="server">
                <%--<%#"เลือกใช้เทมเพลท" & Eval("CardName")%>--%>
                <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/CardImages/FullCard/" & Eval("templateImage") %>'
                    BorderWidth="1px" BorderColor="silver" BorderStyle="Solid" />
            </asp:HyperLink>
            <br />
            <asp:Image ID="Image2" ImageUrl="~/images/icondesign.gif" runat="server" />&nbsp;<span
                style="color: #c0282c;">
                <asp:Label ID="txtCardID" CssClass="des" runat="server" Text='<%# Eval("CardID") & " : " %>'></asp:Label>
                <asp:Label ID="txtCardName" CssClass="des" runat="server" Text='<%# Eval("CardName") %>'></asp:Label></span><br />
            <asp:Label ID="txtSize" runat="server" CssClass="des2" Text='<%# "ขนาดงาน : " & Eval("Height") & " X " & Eval("Width") & " mm" %>'></asp:Label>
            <asp:Label ID="txtOrderID" runat="server" Text='<%# eval("OrderID") %>' Font-Bold="True"
                CssClass="des2"></asp:Label><br />
            <asp:Label ID="txtCreateOn" runat="server" Text='<%# Eval("CreateOn") %>' CssClass="des2"></asp:Label>
        </div>
    </ItemTemplate>
</asp:DataList>
