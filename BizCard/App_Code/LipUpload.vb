﻿Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.Data
Imports System.Collections.Generic

Public Class clsPaperUpload
	Inherits clsBLBase

	Public Function SaveUploadFile(ByVal CardId As String, ByVal BGImage As String, ByVal Template As String) As String
		Return SaveBackground(CardId, BGImage)
	End Function

	Public Function GetUploadFileImage(ByVal PageNo As String) As List(Of clsCard)
		Dim listCard As New List(Of clsCard)
		listCard = GetUploadFile(PageNo)
		Return listCard
	End Function

	Public Function SaveTemplate(ByVal CardId As String, ByVal TempImage As String) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean = False

		With SqlStr
			.AppendLine("UPDATE TBBCCards ")
			.AppendLine(" SET TemplateImage = @TemplateImage,UpdateBy = @UserID,UpdateOn = getdate()")
			.AppendLine(" WHERE CardID = @CardId")

		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.PatametersAdd("@CardId", CardId)
			ObjDB.PatametersAdd("@TemplateImage", TempImage)
			ObjDB.PatametersAdd("@UserID", Me.objSessionVar.ModelUser.UserName)

			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using

		Return Complete

	End Function

	Public Function SaveBackground(ByVal CardId As String, ByVal BGImage As String) As Boolean

		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean = False

		With SqlStr
			.AppendLine("UPDATE TBBCCards ")
			.AppendLine(" SET BGImage = @BGImage,UpdateBy = @UserID,UpdateOn = getdate()")
			.AppendLine(" WHERE CardID = @CardId")

		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.PatametersAdd("@CardId", CardId)
			ObjDB.PatametersAdd("@BGImage", BGImage)
			ObjDB.PatametersAdd("@UserID", Me.objSessionVar.ModelUser.UserName)

			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using

		Return Complete
	End Function

	Public Function GetUploadFile(ByVal PageNo As String) As List(Of clsCard)

		Dim listCard As New List(Of clsCard)
		Dim Card As New clsCard
		Dim SqlStr As New StringBuilder
		Dim ds As New DataSet
		Dim WhereSearch As String = ""
		Dim WhereSearchCat As String = ""

		With SqlStr
			.AppendLine("DECLARE @PageNum AS INT;")
			.AppendLine("DECLARE @PageSize AS INT;")
			.AppendLine("DECLARE @PageTotal AS INT;")
			.AppendLine("DECLARE @RowCount AS INT;")
			.AppendLine("SET @PageNum = @PageNo;")
			.AppendLine("SET @PageSize = 10;")
			.AppendLine("Select @RowCount = Count(CardID)")
			.AppendLine("From   TBBCCards Where TmpFlag = 'Y' And CardActive = 'Y' AND BGImage = '' And TemplateImage = ''")
			.AppendLine("Select @PageTotal =  Floor(@RowCount / @PageSize );")
			.AppendLine("WITH CardRN AS")
			.AppendLine("(")
			.AppendLine("SELECT ROW_NUMBER() OVER(ORDER BY CreateON Desc) AS RowNum,*,@PageTotal as PageToTal")
			.AppendLine("From TBBCCards Where TmpFlag = 'Y' And CardActive = 'Y' AND BGImage = '' And TemplateImage = ''")
			.AppendLine(")")
			.AppendLine("")
			.AppendLine("SELECT *")
			.AppendLine("FROM   CardRN")
			.AppendLine("WHERE  RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 ")
			.AppendLine("       AND @PageNum * @PageSize")
			.AppendLine("ORDER  BY RowNum")
		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.PatametersAdd("@PageNo", PageNo)
			ds = ObjDB.getDataSet(SqlStr.ToString(), "TBBCCards")
		End Using

		If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			For Each aRow As DataRow In ds.Tables(0).Rows
				Card = New clsCard
				With Card
					.CardID = aRow("CardID")
					.CardName = aRow("CardName")
					.BGImage = aRow("BGImage")
					.templateImage = aRow("templateImage")
					.CardActive = aRow("CardActive")
					.CreateBy = aRow("CreateBy")
					.ToTalPage = aRow("PageToTal")
					listCard.Add(Card)
				End With
			Next
		End If
		Return listCard
	End Function

	Public Function GetShowImage(ByVal CardID As String) As clsCard
		Dim Image As New clsCard
		Dim SqlStr As String = "SELECT BGImage,TemplateImage FROM TBBCCards where CardID = '" & CardID & "'"

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr)
			While (ObjDB.Go)
				Image.templateImage = ObjDB.ReadField("TemplateImage")
				Image.BGImage = ObjDB.ReadField("BGImage")
			End While
		End Using

		Return Image
	End Function

	Public Function DeleteFileTemp(ByVal CardID As String) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean = False

		With SqlStr
			.AppendLine("UPDATE TBBCCards ")
			.AppendLine(" SET TemplateImage = '',UpdateBy = @UserID,UpdateOn = getdate()")
			.AppendLine(" WHERE CardID = @CardId")

		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.PatametersAdd("@CardId", CardID)
			ObjDB.PatametersAdd("@UserID", Me.objSessionVar.ModelUser.UserName)

			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using

		Return Complete

	End Function


	Public Function DeleteFileBG(ByVal CardID As String) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean = False

		With SqlStr
			.AppendLine("UPDATE TBBCCards ")
			.AppendLine(" SET BGImage = '',UpdateBy = @UserID,UpdateOn = getdate()")
			.AppendLine(" WHERE CardID = @CardId")

		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.PatametersAdd("@CardId", CardID)
			ObjDB.PatametersAdd("@UserID", Me.objSessionVar.ModelUser.UserName)

			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using

		Return Complete

	End Function

End Class


