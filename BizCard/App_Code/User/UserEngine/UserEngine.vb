﻿Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.Data
Imports System.Collections.Generic
Imports OfficeMate.Framework.Security.Cryptography
Imports System.Security.Cryptography
Imports OfficeMate.Framework.Data.Repositories
Imports OfficeMate.Framework.Data
Imports EnumTrendyPrint
Imports ModelUser
Imports ModelContact
Imports ModelInvoice
Imports ModelShipping
Imports ModelContactEmail

Namespace UserEngineTrendyPrint
	Public Class UserEngine
		Inherits clsBLBase

		Public Function Login(ByVal userid As String, ByVal password As String) As Boolean
			If IsUserInSystemTrendy(userid, password) Then
				Me.objSessionVar.ModelUser = LoadUserInformation(LoadUserGuid(userid))
				objSessionVar.ModelUser.UserInfo = eSystemType.Trendy
			Else
				Me.objSessionVar.ModelUser = Nothing
				Return False
			End If
			Return True
		End Function

		Public Function AdminLogin(ByVal userid As String, ByVal password As String) As Boolean
			Dim user As New ModelUser
			Dim Sqlstr As String = "Select T1.UserGuid,T1.UserID,T2.Password,CustID,Name,NickName,Mobile from DBWebOFM..TBUserInfo T1 inner join DBMaster..TBUsers T2 On T1.UserId = T2.UserID where T1.UserId = @UserId and IsAdmin = 'Yes'"
			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@UserID", userid)
				objDB.SelectReQuery(Sqlstr)
				If (objDB.Go) Then
					If IsValidPassword(password, objDB.ReadField("Password")) Then
						user.UserGuid = objDB.ReadField("UserGuid")
						user.UserID = objDB.ReadField("UserID")
						user.Password = objDB.ReadField("Password")
						user.CustID = objDB.ReadField("CustID")
						user.UserName = objDB.ReadField("Name")
						user.NickName = objDB.ReadField("NickName")
						user.Mobile = objDB.ReadField("Mobile")
						Me.objSessionVar.ModelUser = user
					End If
				End If
			End Using
			If Me.objSessionVar.ModelUser Is Nothing Then
				Return False
			Else
				Return True
			End If
		End Function

		Public Function RegisterUser(ByVal regisData As ModelRegisterData) As String
			If LoadUserGuid(regisData.Email) <> "" Then
				Return "Email นี้มีบัญชีสมาชิกอยู่ในระบบแล้วค่ะ"
			Else
				Return SaveUserRegister(regisData)
			End If
		End Function

		Public Function IsValidPassword(ByVal password As String, ByVal hashedPassword As String) As Boolean
			If Not String.IsNullOrEmpty(password) Then
				If New SaltedSha256PasswordHasher().CheckPassword(password, hashedPassword) Then
					Return True
				End If
			End If
			Return False
		End Function

		Public Function ChangePassword(ByVal userid As String, ByVal newpassword As String) As Boolean
			Dim SqlCommand As String = "UPDATE DBMaster..TBUsers SET Password = @hashedpassword WHERE UserId = @UserId"
			Dim hashedPassword As New SaltedSha256PasswordHasher
			Dim password As String = hashedPassword.HashPassword(newpassword)

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				With objDB
					Try
						.ParametersClear()
						.PatametersAdd("@hashedpassword", password)
						.PatametersAdd("@UserId", userid)
						Return (objDB.ExecuteCommand(SqlCommand.ToString, clsDBIII.eExecute.UpdateType))
					Catch ex As Exception
						Return False
					End Try
				End With
			End Using
		End Function

		Public Function EditUserInfo(ByVal ObjectUser As ModelUser) As Boolean 'ต้องแยกเก็บ ระหว่างUserinfoของเทรนดี่กับออฟฟิศเมท
			Dim Check As Boolean
			Dim SqlStr As String = ""

			If objSessionVar.ModelUser.UserInfo = eSystemType.Trendy Then
				SqlStr += "Update DBWEBOFM..TBUserInfo "
				SqlStr += "Set Name = @UserName, NickName = @NickName, Mobile = @Mobile, UpDateOn = getdate(), UpDateBy = @UserID "
				SqlStr += "Where UserGuid = @UserGuid "
			End If

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				With objDB
					Try
						.ParametersClear()
						.PatametersAdd("@UserID", objSessionVar.ModelUser.UserID)
						.PatametersAdd("@UserGuid", objSessionVar.ModelUser.UserGuid)
						.PatametersAdd("@UserName", ObjectUser.UserName)
						.PatametersAdd("@NickName", ObjectUser.NickName)
						.PatametersAdd("@Mobile", ObjectUser.Mobile)

						Check = objDB.ExecuteCommand(SqlStr, clsDBIII.eExecute.UpdateType)
						Check = True
						With objSessionVar.ModelUser
							.UserName = ObjectUser.UserName
							.NickName = ObjectUser.NickName
							.Mobile = ObjectUser.Mobile
						End With
					Catch ex As Exception
						Check = False
					End Try
				End With
			End Using
			Return Check
		End Function

		Public Function EditShipping(ByVal ObjectContect As ModelContact, ByVal ObjectInvoice As ModelInvoice, ByVal ObjectShipping As ModelShipping) As Boolean
			Dim Check As Boolean
			Dim Sqlstr As String = ""
			Dim SQLCommand As New StringBuilder

			With SQLCommand
				'---ที่อยู่ใบกำกับภาษี---
				.AppendLine("Update DBMaster..TBCustMaster")
				.AppendLine("Set	CustTName = @InvAddr1, InvAddr1 = @InvAddr1, InvAddr2 = @InvAddr2, InvAddr3 = @InvAddr3, InvAddr4 = @InvAddr4, UpDateOn = getdate()")
				.AppendLine("Where	CustID = @CustID")
				'---ข้อมูลผู้ติดต่อ---
				.AppendLine("Update	DBMaster..TBCustContact")
				.AppendLine("Set	ContactName = @ContactName, UpDateOn = getdate()")
				.AppendLine("Where	CustID = @CustID and ContactID = @ContactID")

				'.AppendLine("Update	DBMaster..TBCustomerPhone")
				'.AppendLine("Set	PhoneNo = @ContactPhoneNo, UpDateOn = getdate()")
				'.AppendLine("Where	CustID = @CustID and SequenceID = @ContactID and TypeNumber = 'PhoneOut' and TypeDataSource='Contact'")

				'.AppendLine("Update	DBMaster..TBCustomerPhone")
				'.AppendLine("Set	PhoneNo = @ContactMobileNo, UpDateOn = getdate()")
				'.AppendLine("Where	CustID = @CustID and SequenceID = @ContactID and TypeNumber = 'Mobile' and TypeDataSource='Contact'")

				'.AppendLine("Update	DBMaster..TBCustomerPhone")
				'.AppendLine("Set	PhoneNo = @ContactFaxNo, UpDateOn = getdate()")
				'.AppendLine("Where	CustID = @CustID and SequenceID = @ContactID and TypeNumber = 'FaxOut' and TypeDataSource='Contact'")

				'---ที่อยู่จัดส่ง---
				.AppendLine("Update	DBMaster..TBCustShipping")
				.AppendLine("Set	ShipAddr1 = @ShipAddr1, ShipAddr2 = @ShipAddr2, ShipAddr3 = @ShipAddr3, ShipAddr4 = @ShipAddr4, ShipProvince = @ShipProvince, ShipZipCode = @ShipZipCode, ShipRemark = @ShipRemark, UpDateOn = getdate()")
				.AppendLine("Where	CustID = @CustID and ShipID = @ShipID")

				'.AppendLine("Update	DBMaster..TBCustomerPhone")
				'.AppendLine("Set	PhoneNo = @ShipPhoneNo, UpDateOn = getdate()")
				'.AppendLine("Where	CustID = @CustID and SequenceID = @ShipID and TypeDataSource='Shipping'")
			End With

			Sqlstr = SQLCommand.ToString

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				With objDB
					Try
						.ParametersClear()
						.PatametersAdd("@CustID", objSessionVar.ModelUser.CustID)
						'---ที่อยู่ใบกำกับภาษี---
						.PatametersAdd("@InvAddr1", ObjectInvoice.InvAddr1)
						.PatametersAdd("@InvAddr2", ObjectInvoice.InvAddr2)
						.PatametersAdd("@InvAddr3", ObjectInvoice.InvAddr3)
						.PatametersAdd("@InvAddr4", ObjectInvoice.InvAddr4)
						'---ข้อมูลผู้ติดต่อ---
						.PatametersAdd("@ContactID", ObjectContect.ContactID)
						.PatametersAdd("@ContactName", ObjectContect.ContactName)
						'.PatametersAdd("@ContactMobileNo", ObjectContect.ContactMobileNo)
						'.PatametersAdd("@ContactPhoneNo", ObjectContect.ContactPhoneNo)
						'.PatametersAdd("@ContactFaxNo", ObjectContect.ContactFaxNo)
						'---ที่อยู่จัดส่ง---
						.PatametersAdd("@ShipID", ObjectShipping.ShipID)
						.PatametersAdd("@ShipAddr1", ObjectShipping.ShipAddr1)
						.PatametersAdd("@ShipAddr2", ObjectShipping.ShipAddr2)
						.PatametersAdd("@ShipAddr3", ObjectShipping.ShipAddr3)
						.PatametersAdd("@ShipAddr4", ObjectShipping.ShipAddr4)
						.PatametersAdd("@ShipProvince", ObjectShipping.ShipProvince)
						.PatametersAdd("@ShipZipCode", ObjectShipping.ShipZipCode)
						'.PatametersAdd("@ShipPhoneNo", ObjectShipping.ShipPhoneNo)
						.PatametersAdd("@ShipRemark", ObjectShipping.ShipRemark)

						Check = objDB.ExecuteCommand(Sqlstr, clsDBIII.eExecute.UpdateType)
						Check = True
						With objSessionVar.ModelUser
							.Invoice = ObjectInvoice
							.Contact = ObjectContect
							.Shipping.Item(0) = ObjectShipping
						End With
					Catch ex As Exception
						Check = False
					End Try
				End With
			End Using
			Return Check
		End Function

		Private Function IsUserInSystemTrendy(ByVal userid As String, ByVal password As String) As Boolean
			Dim SqlStr As String = "select TBUserInfo.UserGuid,TBUsers.Password from DBWebOFM..TBUserInfo inner join DBMaster..TBUsers on TBUserInfo.UserId = TBUsers.UserId where TBUserInfo.UserId = @UserId"
			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@UserID", userid)
				objDB.SelectReQuery(SqlStr)
				If (objDB.Go) Then
					Return IsValidPassword(password, objDB.ReadField("Password"))
				End If
			End Using
			Return False
		End Function

		Private Function LoadUserInformation(ByVal guid As String) As ModelUser
			Dim Sqlstr As String = ""
			Sqlstr = "Select T1.UserGuid,T1.UserID,T2.Password,T3.CustID,Name,NickName,Mobile,Isnull(T4.StoreId,'') as StoreId  "
			Sqlstr += "from DBWebOFM..TBUserInfo T1 inner join DBMaster..TBUsers T2 On T1.UserId = T2.UserId "
			Sqlstr += "inner join DBMaster..TBCustContactEmail T3 On T1.UserId = T3.ContactEmail "
			Sqlstr += "Left Join  DBBC..TBStoreUsers T4 ON T4.UserId = T1.UserID "
			Sqlstr += "where T1.UserGuid = @UserGuid"

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@UserGuid", guid)
				objDB.SelectReQuery(Sqlstr)

				Dim user As New ModelUser
				If (objDB.Go) Then
					user.UserGuid = objDB.ReadField("UserGuid")
					user.UserID = objDB.ReadField("UserID")
					user.Password = objDB.ReadField("Password")
					user.CustID = objDB.ReadField("CustID")
					user.UserName = objDB.ReadField("Name")
					user.NickName = objDB.ReadField("NickName")
					user.Mobile = objDB.ReadField("Mobile")
					user.StoreId = objDB.ReadField("StoreId")
					user.Contact = LoadUserContactInformation(user.CustID)
					user.Shipping = LoadUserShippingInformation(user.CustID)
					user.Invoice = LoadUserInvoiceInformation(user.CustID)
				End If
				Return user
			End Using
		End Function

		Private Function LoadUserContactInformation(ByVal custid As String) As ModelContact
			Dim Sqlstr As String = "Select ContactID,ContactName from DBMaster..TBCustContact  Where Custid = @Custid and IsDefault = 'Yes'"
			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@Custid", custid)
				objDB.SelectReQuery(Sqlstr)

				Dim Contact As New ModelContact
				While (objDB.Go)
					Contact.ContactID = objDB.ReadField("ContactID")
					Contact.ContactName = objDB.ReadField("ContactName")
					Contact.ContactMobileNo = LoadUserPhoneInformation(custid, Contact.ContactID, "Mobile")
					Contact.ContactPhoneNo = LoadUserPhoneInformation(custid, Contact.ContactID, "PhoneOut")
					Contact.ContactFaxNo = LoadUserPhoneInformation(custid, Contact.ContactID, "FaxOut")
					Contact.ContactEmail = LoadUserContactEmailInformation(custid, Contact.ContactID)
				End While
				Return Contact
			End Using
		End Function

		Private Function LoadUserContactEmailInformation(ByVal custid As String, ByVal contactid As String) As ModelContactEmail
			Dim Sqlstr As String = "select ContactEmail from  DBMaster..TBCustContactEmail Where CustID = @CustID and ContactID =@ContactID and IsDefaultEmail = 'Yes'"
			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@Custid", custid)
				objDB.PatametersAdd("@ContactID", contactid)
				objDB.SelectReQuery(Sqlstr)

				Dim ContactEmail As New ModelContactEmail
				While (objDB.Go)
					ContactEmail.ContactEmail = objDB.ReadField("ContactEmail")
				End While
				Return ContactEmail
			End Using
		End Function

		Private Function LoadUserShippingInformation(ByVal custid As String) As List(Of ModelShipping)
			Dim Sqlstr As String = "Select ShipID,ShipAddr1,ShipAddr2,ShipAddr3,ShipAddr4,ShipContactor,shipProvince,ShipZipCode,ShipRemark from DBMaster..TBCustShipping Where Custid = @Custid and IsDefault = 'Yes'"

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@Custid", custid)
				objDB.SelectReQuery(Sqlstr)


				Dim item As New ModelShipping
				Dim ListShipping As New List(Of ModelShipping)
				While (objDB.Go)
					item = New ModelShipping
					item.ShipID = objDB.ReadField("ShipID")
					item.ShipContactor = objDB.ReadField("ShipContactor")
					item.ShipAddr1 = objDB.ReadField("ShipAddr1")
					item.ShipAddr2 = objDB.ReadField("ShipAddr2")
					item.ShipAddr3 = objDB.ReadField("ShipAddr3")
					item.ShipAddr4 = objDB.ReadField("ShipAddr4")
					item.ShipPhoneNo = LoadUserPhoneInformation(custid, item.ShipID)
					item.ShipProvince = objDB.ReadField("ShipProvince")
					item.ShipZipCode = objDB.ReadField("ShipZipCode")
					item.ShipRemark = objDB.ReadField("ShipRemark")
					ListShipping.Add(item)
				End While
				Return ListShipping
			End Using
		End Function

		Private Function LoadUserPhoneInformation(ByVal CustId As String, ByVal SequenceID As String, Optional ByVal TypeNumber As String = "") As String
			Dim Sqlstr As String = ""

			If TypeNumber = "" Then
				Sqlstr = "Select PhoneNo,Extension from DBMaster..TBCustomerPhone where CustID=@CustID and SequenceID=@SequenceID and TypeDataSource='Shipping'"
			Else
				Sqlstr = "Select PhoneNo,Extension from DBMaster..TBCustomerPhone where CustID=@CustID and SequenceID=@SequenceID and TypeDataSource='Contact' and TypeNumber=@TypeNumber"
			End If

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@Custid", CustId)
				objDB.PatametersAdd("@SequenceID", SequenceID)
				objDB.PatametersAdd("@TypeNumber", TypeNumber)
				objDB.SelectReQuery(Sqlstr)

				Dim PhoneNo As String = ""
				While (objDB.Go)
					PhoneNo = objDB.ReadField("PhoneNo")
				End While
				Return PhoneNo
			End Using
		End Function

		Private Function LoadUserInvoiceInformation(ByVal custid As String) As ModelInvoice
			Dim Sqlstr As String = "Select InvAddr1,InvAddr2,InvAddr3,InvAddr4 From DBMaster..TBCustMaster  Where Custid = @Custid"

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@Custid", custid)
				objDB.SelectReQuery(Sqlstr)

				Dim Invoice As New ModelInvoice
				While (objDB.Go)
					Invoice.InvAddr1 = objDB.ReadField("InvAddr1")
					Invoice.InvAddr2 = objDB.ReadField("InvAddr2")
					Invoice.InvAddr3 = objDB.ReadField("InvAddr3")
					Invoice.InvAddr4 = objDB.ReadField("InvAddr4")
				End While
				Return Invoice
			End Using
		End Function

		Private Function LoadUserGuid(ByVal userid As String) As String
			Dim user_guid As String = ""
			Dim SqlStr As String = ""
			SqlStr = "select UserGuid From  DBWebOFM..TBUserInfo  where UserId = @UserId"
			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@UserID", userid)
				objDB.SelectReQuery(SqlStr)
				If (objDB.Go) Then
					user_guid = objDB.ReadField("UserGuid")
				End If
			End Using
			Return user_guid
		End Function
		Private Function SaveUserRegister(ByVal registerData As ModelRegisterData) As String
			Dim Check As String
			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				Dim SQLCommand As New StringBuilder
				Dim genGuid As String = Guid.NewGuid.ToString
				Dim hashedPassword As String = New SaltedSha256PasswordHasher().HashPassword(registerData.Password)
				Dim verifyKey As String = Me.GenerateVerifyKey(hashedPassword)

				Dim CustId As String = Me.InsertCustomerTemp(registerData)

				With SQLCommand

					.AppendLine("Insert Into DBWebOFM..TBUserInfo")
					.AppendLine("(UserGuid,CustId, UserId, Name, Nickname, Mobile, IsVerified, Verifykey, CreateBy,RegisterChannel)")
					.AppendLine("Values (@genGuid, @CustId, @UserId, @Name, @Nickname,@Mobile,'No',@verifykey,@UserId,@RegisterChannel)")
					.AppendLine("")


					.AppendLine("Insert Into DBMaster..TBUsers")
					.AppendLine("(UserId,Password,UserGuid)")
					.AppendLine("Values (@UserId,@Password,@genGuid)")
				End With

				With objDB
					Try
						.ParametersClear()
						.PatametersAdd("@UserId", registerData.Email)
						.PatametersAdd("@genGuid", genGuid)
						.PatametersAdd("@CustId", CustId)
						.PatametersAdd("@Name", registerData.FullName)
						.PatametersAdd("@Nickname", registerData.DisplayName)
						.PatametersAdd("@Mobile", registerData.MobilePhone)
						.PatametersAdd("@Password", hashedPassword)
						.PatametersAdd("@verifykey", verifyKey)
						.PatametersAdd("@RegisterChannel", "OfficematePrinting")
						Check = objDB.SelectReQuery(SQLCommand.ToString)
						Check = "True"
					Catch ex As Exception
						Check = "False"
					End Try
				End With
			End Using
			Return Check
		End Function

		Private Function GenerateVerifyKey(ByVal hashedPassword As String) As String
			Dim key As String = GenerateKey()
			Return key & hashedPassword
		End Function

		Private Function GenerateKey() As String
			Dim keyBytes As Byte() = New Byte(14) {}
			Dim rng As New RNGCryptoServiceProvider()
			rng.GetBytes(keyBytes)
			Return Convert.ToBase64String(keyBytes)
		End Function

		Private Function InsertCustomerTemp(ByVal registerData As ModelRegisterData) As String
			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				Dim Connection As New System.Data.SqlClient.SqlConnection
				Connection.ConnectionString = Me.objAppVar.DBConfigMas.ConnectionString
				Dim Repository As New CustomerRepository(Connection)
				Dim Customer As New Customer
				Dim Shipping As New CustomerShipping
				Dim Contact As New Contact

				With Customer
					.Name = registerData.FullName
					.InvoiceAddress.Item(0) = registerData.FullName
					.ActiveDate = DateTime.Now
					.CreateOn = DateTime.Now
					.VatPercentage = 7
				End With

				With Shipping
					.ShipAddress.Item(0) = registerData.FullName
					.Area = ""
					.Contactor = registerData.FullName
					.PhoneNo = registerData.MobilePhone
					.IsDefault = True
				End With

				With Contact
					.Name = registerData.FullName
					.MobileNumber = registerData.MobilePhone
					.Email = registerData.Email
					.IsSubscribeNewsletter = registerData.IsSubscribeNewsletter
					.IsDefault = True
				End With

				Repository.InsertCustomerTemp(Customer, Shipping, Contact, "system")

				Return Customer.Id
			End Using
		End Function

		Public Function CheckProvince(ByVal Province As String) As Integer
			Dim ProvinceType As Integer
			Dim Sqlstr As String = ""

			Sqlstr = "Select ProvinceType From DBMaster..cmProvince Where ProvinceID = @Province"

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@Province", Province)
				objDB.SelectReQuery(Sqlstr)
				If (objDB.Go) Then
					ProvinceType = objDB.ReadField("ProvinceType")
				End If
			End Using
			Return ProvinceType
		End Function

		Public Function ForgotPassword(ByVal UserID As String) As String
			Dim newPass As String = ""
			Dim TempUser As New ModelUser
			TempUser = Me.GetUserPreview(UserID)
			If TempUser.UserID IsNot Nothing Then
				newPass = Me.GeneratePassword(8)
				Me.ChangePassword(UserID, newPass)
				Return newPass
			End If
			Return newPass
		End Function

		Private Function GeneratePassword(ByVal PassLength As Integer) As String
			Dim allowedChars As String = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789"
			Dim randNum As New Random()
			Dim newPass As String = ""
			Dim allowedCharCount As Integer = allowedChars.Length

			For i As Integer = 0 To PassLength - 1
				newPass += allowedChars.Chars(CInt(Fix((allowedChars.Length) * randNum.NextDouble())))
			Next i

			Return newPass
		End Function

		Public Function GetUserPreview(ByVal userid As String) As ModelUser
			Dim TempUser As New ModelUser
			Dim Guid As String = LoadUserGuid(userid)
			If Guid <> "" Then
				TempUser = LoadUserInformation(Guid)
				TempUser.UserInfo = eSystemType.Trendy
			End If
			Return TempUser
		End Function

		Public Function SetNewsletter(ByVal UserID As String, ByVal CheckNews As Boolean) As String
			Dim ReceiveFlag As String = "No"
			If CheckNews Then
				ReceiveFlag = "Yes"
			End If
			Dim Check As String = ""
			Dim Sqlstr As String = ""
			If LoadUserGuid(UserID) <> "" Then
				Sqlstr = "Update DBMaster..TBCustContactEmail set SendNewsLetter=@ReceiveFlag, UpdateOn=GetDate() Where ContactEmail=@Email "
			End If
			Sqlstr += "Delete DBMaster..TBNewsLetter where Email = @Email "
			Sqlstr += "Insert Into DBMaster..TBNewsLetter(Email,Source,ReceiveFlag,CreateBy,CreateOn,UpdateBy,UpdateOn) Values (@Email,'PrintingSolution',@ReceiveFlag,@Email,GetDate(),@Email,GetDate())"

			Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfigMas)
				objDB.ParametersClear()
				objDB.PatametersAdd("@Email", UserID)
				objDB.PatametersAdd("@ReceiveFlag", ReceiveFlag)
				Try
					Check = objDB.ExecuteCommand(Sqlstr, clsDBIII.eExecute.UpdateType)
					Check = "True"
				Catch ex As Exception
					Check = "False"
				End Try
			End Using
			Return Check
		End Function

	End Class
End Namespace
