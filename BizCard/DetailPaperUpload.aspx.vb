
Partial Class DetailPaperUpload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.UcBar1.BarStep = 3
        Me.UcSelectPaper1.IsUpload = True

    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
        Dim PaperID As String = CType(Me.UcSelectPaper1.FindControl("lbPaperID"), Label).Text
        Dim Qty As String = CType(Me.UcSelectPaper1.FindControl("ddlNumber"), DropDownList).SelectedItem.Text
		Dim NetAmt As String = CType(Me.UcSelectPaper1.FindControl("lbVat"), Label).Text

		Dim OptionalDesc1 As String = Session("radioview")
		Dim OptionalPrice1 As String = Session("sumangle")
		Dim OptionalDesc2 As String = Session("Detailpromotion")
		Dim OptionalPrice2 As String = Session("sumview")

		Dim sumtotal As String = Session("sumtotal") '�Ҥ������͹���͡��ԡ������������
		Dim VatAmt As String = CType(Me.UcSelectPaper1.FindControl("lbVat_card"), Label).Text
        Dim TotAmt As String = CType(Me.UcSelectPaper1.FindControl("lbSummary"), Label).Text
        Dim DeliveryAmt As String = CType(Me.UcSelectPaper1.FindControl("lbDelivery"), Label).Text
        Dim lbview As String = CType(UcSelectPaper1.FindControl("lbview"), Label).Text
        Dim Check1 As String = CType(UcSelectPaper1.FindControl("Checkbox1"), CheckBox).Checked
        Dim Check2 As String = CType(UcSelectPaper1.FindControl("Checkbox2"), CheckBox).Checked
        Dim Check3 As String = CType(UcSelectPaper1.FindControl("Checkbox3"), CheckBox).Checked
        Dim Check4 As String = CType(UcSelectPaper1.FindControl("Checkbox4"), CheckBox).Checked
        Dim Dropdownlist1 As String = CType(UcSelectPaper1.FindControl("Dropdownlist1"), DropDownList).Text
        Dim Dropdownlist2 As String = CType(UcSelectPaper1.FindControl("Dropdownlist2"), DropDownList).Text
        Dim Dropdownlist3 As String = CType(UcSelectPaper1.FindControl("Dropdownlist3"), DropDownList).Text
		Dim Dropdownlist4 As String = CType(UcSelectPaper1.FindControl("Dropdownlist4"), DropDownList).Text
		Dim Type As String = "C"

        'Gift ������������´��ԡ�þ���� �Ѵ���
        Dim Detail As String = Session("Detailpromotion")


        Dim check As Boolean = True

        'check ��ͧ��õѴ���
        If lbview > "0.00" Then
            '��������͡������
            'Check = False
            If Check1 = False And Check2 = False And Check3 = False And Check4 = False Then
                Dim csm As ClientScriptManager = Page.ClientScript
                If (Not csm.IsStartupScriptRegistered(Me.GetType(), "ScriptKey1")) Then
                    Dim str As String = "alert('�س�ѧ��������͡��ԡ������������');"
                    csm.RegisterStartupScript(Me.GetType(), "ScriptKey1", str, True)
                    check = False
                End If
            End If
            '������͡��� ������������������´
            If (Check1 = True And Dropdownlist1 = "--SELECT--") Or (Check2 = True And Dropdownlist2 = "--SELECT--") Or (Check3 = True And Dropdownlist3 = "--SELECT--") Or (Check4 = True And Dropdownlist4 = "--SELECT--") Then
                Dim csm As ClientScriptManager = Page.ClientScript
                If (Not csm.IsStartupScriptRegistered(Me.GetType(), "ScriptKey1")) Then
                    Dim str As String = "alert('��س����͡��Ҵ����ѵ÷���ͧ��õѴ������¤��');"
                    csm.RegisterStartupScript(Me.GetType(), "ScriptKey1", str, True)
                    check = False
                End If
            End If
        End If

        If PaperID = "--�ѧ��������͡--" Then ''�礤�ҡ�����͡��Ҵ��д��
            Dim csm As ClientScriptManager = Page.ClientScript
            If (Not csm.IsStartupScriptRegistered(Me.GetType(), "ScriptKey1")) Then
                Dim str As String = "alert('�س�ѧ��������͡��д������Ѻ��觷ӹ���ѵ�');"
                csm.RegisterStartupScript(Me.GetType(), "ScriptKey1", str, True)
                check = False
            End If
        End If

        If check Then
			'Session("Condition") = PaperID + "^" + Qty + "^" + NetAmt + "^" + VatAmt + "^" + TotAmt + "^" + DeliveryAmt + "^" + sumangle + "^" + sumview + "^" + sumtotal + "^" + Type
			Session("Condition") = PaperID + "^" + Qty + "^" + NetAmt + "^" + VatAmt + "^" + TotAmt + "^" + DeliveryAmt + "^" + sumtotal + "^" + Type + "^" + OptionalPrice1 + "^" + OptionalDesc1 + "^" + OptionalPrice2 + "^" + OptionalDesc2

            Response.Redirect("ShippingAddressUploadNew.aspx")
        End If
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Response.Redirect("AttachFiles.aspx")
    End Sub

End Class
