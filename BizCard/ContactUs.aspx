﻿<%@ Register TagPrefix="cc1" Namespace="WebControlCaptcha" Assembly="WebControlCaptcha" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ContactUs.aspx.vb" Inherits="ContactUs" %>
<%@ Register Src="uc/ucFooter.ascx" TagName="ucFooter" TagPrefix="uc2" %>
<%@ Register Src="uc/ucLiveChat.ascx" TagName="ucLiveChat" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ติดต่อ Officemate Printing Solution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="สั่งทำนามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์ - Officemate Printing Solution" />
<meta name="keywords" content="นามบัตร, หัวจดหมาย, การ์ด, โปสเตอร์, ใบปลิว, ราคางานพิมพ์, Officemate Printing Solution" />
<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/text.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/trendyprint.css" />
<link rel="stylesheet" type="text/css" href="css/print.css" />

<script type="text/javascript" src="js/jsProductSearch.js"></script>
<script type="text/javascript" src="js/jsUtil.js"></script>
<script type="text/javascript" src="js/NoIEActivate.js"></script>
<script type="text/javascript" src="js/AC_RunActiveContent.js"></script>    
<script src="js/jquery-1.3.2.js" type="text/javascript"></script>
    
<style type="text/css">
        .column-register {text-align:left;}
		.clear { clear:both;}
        </style>
    
</head>
<body>
<div id="container" align="center">
<form id="form1" runat="server">
	<div style="padding: 30px; margin:auto; width: 920px; border-bottom: 1px solid #999;">    
    <a href="<%=ResolveUrl("~/main.aspx")%>" target="_self"><img src="images/logo-trendyprint.png" border="0" alt="Officemate Printing Solution" title="นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์" /></a>
    	<div style="float:right; width:100px; vertical-align:bottom; text-align:right;">< <a href="main.aspx">กลับหน้าแรก</a></div>
    
    </div>
    <div align="left" style="padding: 30px; margin:auto; width: 920px;"> 
   
  
    <h1>Contact Us</h1>
    <asp:Panel ID="panView" runat="server" meta:resourcekey="panViewResource1" Width="100%">
    <a href="javascript:void(0)" name="ContactForm"></a>
    <p>หากต้องการรับบริการพิมพ์หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว หรืออื่นๆ สามารถติดต่อได้ที่<br />
			Officemate Printing Solution 02-739-5555 ต่อ 2726 หรือ กรุณากรอกข้อความด้านล่างนี้<br />
			เจ้าหน้าที่จะทำการติดต่อกลับไปยังท่านค่ะ
	</p>
    <div class="column-register">
    <asp:Label ID="Label10" runat="server" meta:resourcekey="Label10Resource1" Text="ชื่อ : "></asp:Label>
    </div>
    <div class="column-register">
    <asp:TextBox ID="txtName" runat="server" CssClass="input_send" MaxLength="100" meta:resourcekey="txtNameResource1" Width="344px"></asp:TextBox>
    </div>
    <div class="column-register">
    <asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1" Text="e-Mail Address : "></asp:Label>
    </div>
    <div class="column-register">
    <asp:TextBox ID="txtMail" runat="server" CssClass="input_send" MaxLength="100" meta:resourcekey="txtMailResource1" Width="344px"></asp:TextBox>
    </div>
    <div class="column-register">
    <asp:Label ID="Label11" runat="server" meta:resourcekey="Label11Resource1" Text="เบอร์ติดต่อกลับ : "></asp:Label>
    </div>
    <div class="column-register">
    <asp:TextBox ID="txtTel" runat="server" CssClass="input_send" MaxLength="100" meta:resourcekey="txtTelResource1" Width="344px"></asp:TextBox>
    </div>
    
    <div class="column-register">
    <asp:Label ID="Label3" runat="server" meta:resourcekey="Label3Resource1" Text="ข้อความ : "></asp:Label>
    </div>
    <div class="column-register">
    <asp:TextBox ID="txtbodycontact" runat="server" CssClass="input_send" Height="72px"
    MaxLength="100" meta:resourcekey="txtbodycontactResource1" TextMode="MultiLine"
    Width="344px"></asp:TextBox><br />
    </div><br />

	<!-- Test Captcha-->
<%--
    <div class="validationDiv">
       <cc1:captchacontrol id="CaptchaControl1" runat="server" ></cc1:captchacontrol>
            &nbsp;
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
            <p></p>
			<br />
            <br />
    </div>--%>

  <!-- Test Captcha-->

    <div class="column-register" style="vertical-align:middle; padding-top:20px;">
    <asp:ImageButton ID="btnSend" runat="server" AlternateText="ส่งข้อความ" ImageUrl="images/btn/submit.png" meta:resourcekey="btnSendResource1"/>
    <asp:ImageButton ID="btnClear" runat="server" AlternateText="เคลียร์ข้อมูล" ImageUrl="images/btn/refresh.png" meta:resourcekey="btnClearResource1" />
    </div>
    </asp:Panel>
    </div>
	<br class="clear" />
	
	<div align="left" style="padding: 30px; margin:auto; width: 920px; border-top: 1px solid #aaa; border-bottom: 1px solid #aaa;">
	<h2>Related Links</h2>
	<a href="PriceList.aspx"><img src="images/view-pricelist.png" ></a>	
	<%--<a href="http://filezilla-project.org/download.php" target="_blank"><img src="images/load-filezilla.png" ></a>--%>
	</div>
</form>

<uc2:ucFooter ID="UcFooter1" runat="server" />  
<uc3:ucLiveChat ID="ucLiveChat1" runat="server" />
</div>

</body>
</html>
