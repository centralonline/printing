Imports System.Data
Imports EnumTrendyPrint

Partial Class UploadCard
    Inherits clsHTTPBase

    Private ThisSearchParams As clsCardSearchParam
    Public ThisPageNo As String = 0
    Public ThisPageSize As Integer = 10
    Private ThisSortBy As eCardSortBy = eCardSortBy.CreateOnDESC
    Private ThisOperation As String
    Private ThisCardType As eCardType = eCardType.Gallery

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.objSessionVar.ModelUser Is Nothing Then
            Me.Response.Redirect("Main.aspx?id=1")
            Exit Sub
        End If


        Me.ThisSearchParams = New clsCardSearchParam()
        Me.ThisSearchParams.SearchKey = ""
        Me.ThisSearchParams.CardType = ThisCardType
        Me.UcCard1.ThisType = "Upload"
        Me.selectEventHandler()

        If Page.IsPostBack = False Then
            Me.radReadyPrint.Checked = True
        End If
    End Sub

    Private Sub selectEventHandler()
        '--> !!!Only!!! : Set Value Form Request
        Dim TempOperation As String = Request("Operation")
        If (TempOperation Is Nothing) Then
            TempOperation = "ChangeQueryType"
        End If
        If (Request("SearchKey") IsNot Nothing) Then
            Me.ThisSearchParams.SearchKey = Request("SearchKey")
        End If
        If (Request("SortBy") IsNot Nothing) Then
            Me.ThisSortBy = Request("SortBy")
        End If
        If (Request("PageNo") IsNot Nothing) Then
            Me.ThisPageNo = Request("PageNo")
        End If
        If (Request("PageSize") IsNot Nothing) Then
            Me.ThisPageSize = Request("PageSize")
        End If

        '--> Select From Operation
        Select Case TempOperation
            Case "ChangeQueryType"
                Me.onQueryChange()
            Case "ChangeSortBy"
                Me.ThisPageNo = 0
                Me.onSortByChange()
            Case "ChangePage"
                Me.onPageChange()
            Case "ChangePageSize"
                Me.ThisPageNo = 0
                Me.onPageSizeChange()
        End Select

    End Sub

    Private Sub onPageChange()
        Me.showCard()
    End Sub

    Private Sub onPageSizeChange()
        Me.showCard()
    End Sub

    Private Sub onSortByChange()
        Me.showCard()
    End Sub

    Private Sub onQueryChange()
        Me.showCard()
    End Sub

    Private Sub showCard()
        Dim TempCardEngine As New clsCardEngine
        Dim DSCard As DataSet = TempCardEngine.GetCardBySearch(Me.ThisSearchParams, Me.ThisPageNo, Me.ThisPageSize, Me.ThisSortBy)
        If DSCard IsNot Nothing AndAlso DSCard.Tables(0).Rows.Count > 0 Then
            Me.BindCard(DSCard.Tables(0))
        End If
    End Sub

    Private Sub BindCard(ByVal DTCard As DataTable)
        Me.UcCard1.ThisDTCard = DTCard
        Me.UcCard1.ThisRepeat = 2
    End Sub

    Protected Sub btnSelectTemplate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSelectTemplate.Click
        Dim query As String = ""
        If Me.radReadyPrint.Checked = True Then
            query = "Y"
        Else
            query = "N"
        End If
        Response.Redirect("AttachFiles.aspx?Mode=" & query & "")
    End Sub

    Private Sub ShowMessage(ByVal Message As String, Optional ByVal RedirectUrl As String = "")
        Dim OwnerPage As Page

        OwnerPage = HttpContext.Current.Handler
        If OwnerPage IsNot Nothing Then
            Message = Message.Replace("'", "\")
            If RedirectUrl <> "" Then
                ScriptManager.RegisterStartupScript(OwnerPage, OwnerPage.GetType, "Error_Message", "alert('" & Message & "');document.location.href = '" & RedirectUrl & "'; ", True)
            Else
                ScriptManager.RegisterStartupScript(OwnerPage, OwnerPage.GetType, "Error_Message", "alert('" & Message & "');", True)
            End If

        End If

    End Sub
End Class
