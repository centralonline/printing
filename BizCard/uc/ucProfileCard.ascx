﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucProfileCard.ascx.vb" Inherits="uc_ucProfileCard" %>

<h1>ข้อมูลในนามบัตร</h1>
<asp:DataList ID="dlCardProfile" runat="server" CellPadding="4" ForeColor="#333333" Width="100%">
<ItemTemplate>
<asp:Label ID="lbFields" runat="server" Text='<%# Bind("FieldName") %>' Width="100px"></asp:Label>
<asp:Label ID="lbTextValue" runat="server" Text='<%# bind("TextValue") %>' Width="320px"></asp:Label>
</ItemTemplate>
<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
<SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
<AlternatingItemStyle BackColor="White" />
<ItemStyle BackColor="#EFF3FB" />
<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
</asp:DataList>
                    