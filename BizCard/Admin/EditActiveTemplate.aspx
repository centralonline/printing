﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPageAdmin.master" AutoEventWireup="false"
	CodeFile="EditActiveTemplate.aspx.vb" Inherits="Admin_EditActiveTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<link href="Css/AdminTool.css" rel="stylesheet" type="text/css" />
	<%=""%>
	<fieldset style="text-align: left">
		<legend><b>ค้นหานามบัตร</b></legend>
		<table>
			<tr>
				<td>
					<b>ค้นหาจากคำว่า: <span style="color: #0066FF">
						<%=SearchKey%></span></b>
				</td>
			</tr>
			<tr>
				<td>
					<input id="SearchKey" type="text" /><input id="Search" type="button" value="ค้นหา" /><input
						id="ShowAll" type="button" value="แสดงทั้งหมด" />
				</td>
			</tr>
			<tr>
				<td>
					<b>แสดงตาม categories</b>
				</td>
			</tr>
			<tr>
				<td>
					<select id="Categories">
						<option value="0">---เลือก Categories---</option>
						<%For Each item As clsCategories In listCategories
								If item.CategoryID = Cat Then%>
						<option value="<%=item.CategoryID %>" selected="selected">
							<%=item.CategoryName%></option>
						<%  End If%>
						<option value="<%=item.CategoryID %>">
							<%=item.CategoryName%></option>
						<%Next%>
					</select>
					<input id="ShowCat" type="button" value="แสดงผล" />
					<asp:Button ID="IsUpload" runat="server" Text="CheckUpload" />
				</td>
			</tr>
		</table>
	</fieldset>
	<% 
		If listCard IsNot Nothing AndAlso listCard.Count > 0 Then%>
	<table style="width: 70%">
		<tr>
			<%If listCard.Item(0).ToTalPage > 0 Then%>
			<td style="text-align: left">
				หน้าที่:<%=PageNo%>/<%=listCard.Item(0).ToTalPage%>
			</td>
			<td style="text-align: Right">
				goto:
				<%For i As Integer = 1 To listCard.Item(0).ToTalPage
						Dim Cat As String = ""
						If Request.QueryString("Cat") IsNot Nothing Then
							Cat = "&Cat=" & Request.QueryString("Cat")
				%>
				<span><a href=" <%=ResolveUrl("~/Admin/EditActiveTemplate.aspx?PageNo=" & i & Cat)%>">
					<%=i%></a></span>
				<%Else%>
				<span><a href="<%=ResolveUrl("~/Admin/EditActiveTemplate.aspx?PageNo=" & i )%>">
					<%=i%></a></span>
				<%End If
						Next%>
			</td>
			<%End If%>
		</tr>
	</table>
	<%End If%>
	<div id="dvShowCard" style="display: none;">
	</div>
	<%	 If listCard IsNot Nothing AndAlso listCard.Count > 0 Then%>
	<div class="datagrid" style="width: 1340px">
		<table id="tbCard">
			<thead>
				<tr style="background-color: #cfe">
					<th align="center" style="width: 100px">
						รหัสนามบัตร
					</th>
					<th align="center" style="width: 100px">
						ชื่อนามบัตร
					</th>
					<th align="center" style="width: 100px">
						ชื่อ Background
					</th>
					<th align="center" style="width: 100px">
						CardActive
					</th>
					<th align="center" style="width: 100px">
						สร้างโดย
					</th>
					<th align="center" style="width: 300px;">
						ดูตัวอย่าง
					</th>
					<th align="center" style="width: 100px">
						แก้ไขข้อมูล
					</th>
					<th align="center" style="width: 100px">
						Upload File
					</th>
					<th align="center" style="width: 100px">
						ตั้งค่าให้ UnActive
					</th>
					<th align="center" style="width: 100px">
						Copy ข้อมูล
					</th>
				</tr>
			</thead>
			<tbody>
				<%		Dim Count As Integer = 0
					For Each ObjCard As clsCard In listCard
				%>
				<tr style="background-color: #fff">
					<td align="center">
						<%=ObjCard.CardID%>
					</td>
					<td align="center">
						<%=ObjCard.CardName%>
					</td>
					<td align="center">
						<%=ObjCard.BGImage%>
					</td>
					<td align="center">
						<%=ObjCard.CardActive%>
					</td>
					<td align="center">
						<%=ObjCard.CreateBy%>
					</td>
					<td align="center" height="200px">
						<img alt="Card" style="cursor: pointer;" src="../CardImages/FullCard/<%=ObjCard.templateImage%>"
							id="<%=ObjCard.CardID%>" class="clsCardID" />
					</td>
					<td align="center">
						<input id="btnEdit_<%=ObjCard.CardID%>" type="button" value="Edit" class="clsEdit"
							style="width: 80px" />
					</td>
					<td align="center">
						<input id="btnUploads_<%=ObjCard.CardID%>" type="button" value="Uploads File" class="clsUpload"
							style="width: 90px" />
					</td>
					<td align="center">
						<input id="btnUnActive_<%=ObjCard.CardID%>" type="button" value="UnActive" class="clsUnActive"
							style="width: 80px" />
					</td>
					<td align="center">
						<input id="btnCopy_<%=ObjCard.CardID%>" type="button" value="Copy" class="clsCopy"
							style="width: 80px" />
					</td>
				</tr>
				<%
					Count += 1
					   Next%>
			</tbody>
		</table>
	</div>
	<%Else%>
	[No Data]
	<%End If%>
	<script language="javascript" type="text/javascript">

		$("#dvShowCard").dialog({
			title: "This Card",
			bgiframe: true,
			autoOpen: false,
			height: 500,
			width: 500,
			resizable: false,
			modal: true
		});

		$("#Search").click(function () {
			var key = $("#SearchKey").val();
			if (key == "") {
				alert("ใส่เงื่อนไขค้นหาด้วยค่ะ");
			}
			else {
				window.location = 'EditActiveTemplate.aspx?PageNo=1&SearchKey=' + key;
			}
		});

		$("#ShowAll").click(function () {
			window.location = 'EditActiveTemplate.aspx?PageNo=1';

		});

		$("#ShowCat").click(function () {
			var cat = $("#Categories").val();
			window.location = 'EditActiveTemplate.aspx?PageNo=1&Cat=' + cat;
		});


		$(function () {
			$("#dvShowCard").bind("dialogclose", function () {
				window.location.reload();
			});

			$(".clsCardID").click(function () {
				var CardID = this.id;
				$("#dvShowCard").dialog("open");
				$("#dvShowCard").load('ShowCard.aspx?CardID=' + CardID);
			});

			$(".clsEdit").click(function () {
				var ID = this.id;
				var CardID = "";
				var CardID = ID.split("_");
				window.location = ('EditTemplate.aspx?Type=Edit&CardID=' + CardID[1]);
			});

			$(".clsCopy").click(function () {
				var ID = this.id;
				var CardID = "";
				var CardID = ID.split("_");

				var answer = confirm("คุณต้องการ copy ข้อมูลการ์ดใบนี้?");
				if (answer) {
					$.ajax({
						type: "POST",
						url: "CopyTemplate.aspx",
						data: { "CardID": CardID[1] },
						success: function (msg) {
							window.location = ('EditTemplate.aspx?Type=Edit&CardID=' + msg);
						}
					});

				}
			});

			$(".clsUnActive").click(function () {
				var ID = this.id;
				var CardID = "";
				var CardID = ID.split("_");
				var Active = "N"
				$.ajax({
					type: "POST",
					url: "ajManageFlagCard.aspx",
					data: { "CardID": CardID[1], "Active": Active },
					success: function (msg) {
						alert(msg);
						window.location = "EditActiveTemplate.aspx";
					}
				});
			});

			$(".clsUpload").click(function () {
				var ID = this.id;
				var CardID = "";
				var CardID = ID.split("_");
				window.location = ('AdminUploadFile.aspx?Type=Upload&CardID=' + CardID[1]);
			});
		});


	</script>
</asp:Content>
