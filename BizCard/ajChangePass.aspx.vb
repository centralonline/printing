﻿Imports CoreBaseII
Imports OfficeMate.Framework.Security.Cryptography
Imports UserEngineTrendyPrint
Partial Class ajChangePass
  Inherits clsHTTPBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		Dim Repo As New UserEngine
		Dim UserID As String = objSessionVar.ModelUser.UserID
		Dim password_fromDB As String = objSessionVar.ModelUser.Password
		Dim password_fromUserKey As String = Request("OldPass")
		Dim newpassword As String = Request("NewPass")


		If Repo.IsValidPassword(password_fromUserKey, password_fromDB) Then
			If (Repo.ChangePassword(UserID, newpassword)) Then
				Repo.Login(UserID, newpassword)
				Response.Write("บันทึกข้อมูลเรียบร้อยแล้วค่ะ")
			Else
				Response.Write("Error!!! กรุณาลองใหม่อีกครั้งค่ะ")
			End If
		Else
			Response.Write("กรุณากรอกรหัสผ่านปัจจุบันให้ถูกต้องด้วยค่ะ")
			Exit Sub
		End If

	End Sub

End Class
