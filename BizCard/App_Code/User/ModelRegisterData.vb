﻿Imports Microsoft.VisualBasic
Imports ModelShipping
Imports ModelInvoice
Imports ModelContact
Imports System.Collections.Generic

Public Class ModelRegisterData

	Private EmailValue As String
	Public Property Email() As String
		Get
			Return EmailValue
		End Get
		Set(ByVal value As String)
			EmailValue = value
		End Set
	End Property

	Private ConfirmEmailValue As String
	Public Property ConfirmEmail() As String
		Get
			Return ConfirmEmailValue
		End Get
		Set(ByVal value As String)
			ConfirmEmailValue = value
		End Set
	End Property

	Private PasswordValue As String
	Public Property Password() As String
		Get
			Return PasswordValue
		End Get
		Set(ByVal value As String)
			PasswordValue = value
		End Set
	End Property

	Private ConfirmPasswordValue As String
	Public Property ConfirmPassword() As String
		Get
			Return ConfirmPasswordValue
		End Get
		Set(ByVal value As String)
			ConfirmPasswordValue = value
		End Set
	End Property

	Private FullNameValue As String
	Public Property FullName() As String
		Get
			Return FullNameValue
		End Get
		Set(ByVal value As String)
			FullNameValue = value
		End Set
	End Property

	Private DisplayNameValue As String
	Public Property DisplayName() As String
		Get
			Return DisplayNameValue
		End Get
		Set(ByVal value As String)
			DisplayNameValue = value
		End Set
	End Property

	Private MobilePhoneValue As String
	Public Property MobilePhone() As String
		Get
			Return MobilePhoneValue
		End Get
		Set(ByVal value As String)
			MobilePhoneValue = value
		End Set
	End Property

    Private IsAcceptedAgreementValue As Boolean
    Public Property IsAcceptedAgreement() As Boolean
        Get
            Return IsAcceptedAgreementValue
        End Get
        Set(ByVal value As Boolean)
            IsAcceptedAgreementValue = value
        End Set
    End Property

    Private IsSubscribeNewsletterValue As Boolean
    Public Property IsSubscribeNewsletter() As Boolean
        Get
            Return IsSubscribeNewsletterValue
        End Get
        Set(ByVal value As Boolean)
            IsSubscribeNewsletterValue = value
        End Set
    End Property

    Private InvoiceValue As ModelInvoice
    Public Property Invoice() As ModelInvoice
        Get
            Return InvoiceValue
        End Get
        Set(ByVal value As ModelInvoice)
            InvoiceValue = value
        End Set
    End Property

    Private ContactValue As ModelContact
    Public Property Contact() As ModelContact
        Get
            Return ContactValue
        End Get
        Set(ByVal value As ModelContact)
            ContactValue = value
        End Set
    End Property

    Private ShippingValue As List(Of ModelShipping)
    Public Property Shipping() As List(Of ModelShipping)
        Get
            Return ShippingValue
        End Get
        Set(ByVal value As List(Of ModelShipping))
            ShippingValue = value
        End Set
    End Property

End Class
