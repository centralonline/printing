﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucTop.ascx.vb" Inherits="uc_ucTop" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Panel ID="Panel3" runat="server" Visible="False" Width="100%" >
	<div class="alert" style="background-color:#ff9999">
คุณยังไม่ได้ทำการ Login ค่ะ
</div>
</asp:Panel>

<table style="width:100%">
	<tr>
		<td align="left">
			<div id="logo" align="left">
			<a href="/main.aspx">
			
				<img src="<%=ResolveUrl("~/Admin/images/logo-trendyprint.png")%>" class = "png" style="width: 250px; height: 51px;" alt=""/></a></div>
		</td>
		<td align="right">
			<asp:Panel ID="Panel1" runat="server" Height="24px" Visible="False" Width="100%" CssClass="Tlogin">
				<span class="Tlogin">UserName:</span>
				<asp:Label ID="lbUserName" runat="server" Text="" CssClass="Tlogin"></asp:Label>
				<asp:ImageButton ID="btnLogout" ImageUrl="~/images/logout.jpg" runat="server" Style="height: 20px" />
			</asp:Panel>
			<asp:Panel ID="Panel2" runat="server" Height="24px" Visible="False" Width="100%" CssClass="Tlogin">
				<p style="padding-right: 20px; vertical-align: middle;">
					<img src="../images/t-login.png" style="padding-right: 10px; vertical-align: middle;" class="png" width="43" height="17" alt="" />
					<asp:TextBox ID="txtUserName" runat="server" CssClass="Tloginborder" value="enter your email" onclick="if (this.value == 'enter your email') { this.value=''; } " onBlur="if (this.value == '') { this.value='enter your email'; } "></asp:TextBox>
					password
					<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="Tloginborder2" Width="128px"></asp:TextBox>
					<asp:ImageButton ID="btnLogin" ImageUrl="~/images/login.jpg" runat="server" />
				</p>
			</asp:Panel>
		</td>
	</tr>
</table>

<div style="width:100%">
	<asp:Panel ID="Panel4" runat="server" Height="24px" Visible="False" Width="100%">
		<div align="left" class="chromestyle" id="chromemenu">
			<ul>
				<li><a href="#" rel="dropmenu1">Order</a></li>
				<li><a href="#" rel="dropmenu2">Template</a></li>
			</ul>
		</div>
		<div align="left" id="dropmenu1" class="dropmenudiv">
		
			<a href="<%=ResolveUrl("~/Admin/ManageFile.aspx") %>">Manage File</a>
			<a href="<%=ResolveUrl("~/Admin/ViewOrder.aspx") %>">View Order</a>
			
		</div>
		<div align="left" id="dropmenu2" class="dropmenudiv">
		
			<a href="<%=ResolveUrl("~/Admin/AddTemplate.aspx") %>">Add New Template</a>
			<a href="<%=ResolveUrl("~/Admin/ManageFlagCard.aspx") %>">Manage FlagCard</a>
			<a href="<%=ResolveUrl("~/Admin/EditActiveTemplate.aspx") %>">Edit Active Template</a>
			
		</div>
		<script type="text/javascript">
			cssdropdown.startchrome("chromemenu")
		</script>
	</asp:Panel>
</div>