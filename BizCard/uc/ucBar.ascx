﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucBar.ascx.vb" Inherits="uc_ucBar" %>

<div style="font-size: 80%; padding:10px; border: 1px solid #da9; margin-bottom:20px;" class="v-middle">
<asp:Image ID="imgStep1" runat="server" ImageUrl="~/images/uc/num11.gif"  />
<asp:Label ID="Label1" runat="server" Text="เลือกรูปแบบสิ่งพิมพ์ที่ต้องการ"></asp:Label>

<asp:Image ID="ImageButton1" runat="server" ImageUrl="~/images/uc/forward.gif"  />

<asp:Image ID="imgStep2" runat="server" ImageUrl="~/images/uc/num21.gif" />
<asp:Label ID="Label2" runat="server" Text="ใส่ข้อมูลลงในแบบฟอร์มให้ครบถ้วน"></asp:Label>

<asp:Image ID="Image1" runat="server" ImageUrl="~/images/uc/forward.gif" />

<asp:Image ID="imgStep3" runat="server" ImageUrl="~/images/uc/num31.gif" />
<asp:Label ID="Label3" runat="server" Text="เลือกกระดาษ ระบุจำนวนสั่งทำ และสถานที่จัดส่ง"></asp:Label>

<asp:Image ID="Image2" runat="server" ImageUrl="~/images/uc/forward.gif" />

<asp:Image ID="imgStep4" runat="server" ImageUrl="~/images/uc/num41.gif" />
<asp:Label ID="Label4" runat="server" Text="ยืนยันการสั่งทำ"></asp:Label>
</div>