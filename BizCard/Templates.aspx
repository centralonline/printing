﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Templates.aspx.vb" Inherits="Templates" Title="เลือกรูปแบบนามบัตรจาก Template trendyprint.net" %>

<%@ Register Src="uc/ucCategories.ascx" TagName="ucCategories" TagPrefix="uc4" %>
<%@ Register Src="uc/ucBar.ascx" TagName="ucBar" TagPrefix="uc3" %>
<%@ Register Src="uc/ucCard.ascx" TagName="ucCard" TagPrefix="uc2" %>
<%@ Register Src="uc/ucPaging.ascx" TagName="ucPaging" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
    <div style="margin: auto;">
        <img src="images/h-make-card01.png" class="png" alt="ทำนามบัตร" title="ทำนามบัตร" /><br />
    </div>
    <div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
        <span style="font-size: 80%; padding: 20px 0px 20px 0px;">< ต้องการกลับไปเลือก <a
            href="UpLoadCard.aspx">ทำนามบัตรโดยการ Upload ไฟล์</a></span><br />
        <br />
        <uc3:ucBar ID="UcBar1" runat="server" />
        <div style="float: left;">
            <uc4:ucCategories ID="UcCategories1" runat="server" />
        </div>
        <div style="float: left; padding: 20px; width: 700px;">
            <uc1:ucPaging ID="UcPaging1" runat="server" />
            <asp:Label ID="Label1" runat="server" Text="Label" Font-Bold="True" Font-Size="11pt"
                ForeColor="MediumSeaGreen"></asp:Label><br />
            <br />
            <uc2:ucCard ID="UcCard1" runat="server" />
            <br />
            <uc1:ucPaging ID="UcPaging2" runat="server" />
        </div>
        <br class="clear" />
        <div id="btn-card" align="left">
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/DesignCard.aspx" class="btn-face2"><< Previous </asp:HyperLink>
        </div>
    </div>
</asp:Content>
