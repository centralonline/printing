<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucFileUploaded.ascx.vb" Inherits="uc_ucFileUploaded" %>

<asp:DataList ID="dlImgFields" runat="server" RepeatColumns="4" Width="100%">
    <ItemStyle VerticalAlign="Bottom" />
    <ItemTemplate>        
    <div style="width:200px; align:left; float:left; padding-bottom:20px;">
                    <asp:HiddenField ID="hidImgUrl" runat="server" Value='<%# Eval("imageUrl") %>' />
                    <asp:HiddenField ID="hidContentType" Value='<%#Eval("ContentType") %>' runat="server" />
                    <asp:HiddenField ID="hidReadyToPrint" runat="server" Value='<%# eval("ReadyToPrint") %>' />
                    <asp:HiddenField ID="hidPreviousName" runat="server" Value='<%# eval("PreviousName") %>' />
                    <asp:Image ID="imgUploaded" runat="server" ImageUrl='<%# "~/CardImages/UploadImage/Thumbnails/" & Eval("imageUrl") %>' /><br /><br />
	      <asp:CheckBox ID="chkSelect" runat="server" />
                    <asp:HyperLink ID="hlFile" runat="server" Target="_blank" NavigateUrl='<%# "~/CardImages/UploadImage/Currents/" & Eval("imageUrl") %>'><%#Eval("PreviousName")%></asp:HyperLink><br />                  
                    <asp:Label ID="Label2" runat="server" Text=<%# Eval("CreateOn") %>></asp:Label><br class="clear">
    </div>
    </ItemTemplate>    
</asp:DataList>

