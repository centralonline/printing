
Partial Class uc_ucCustomer
    Inherits clsUCBase

    Public ReadOnly Property InvAddr1() As String
        Get
            Return lbInvAddr1.Text
        End Get
    End Property

    Public ReadOnly Property InvAddr2() As String
        Get
            Return lbInvAddr2.Text
        End Get
    End Property

    Public ReadOnly Property InvAddr3() As String
        Get
            Return lbInvAddr3.Text
        End Get
    End Property

    Public ReadOnly Property InvAddr4() As String
        Get
            Return lbInvAddr4.Text
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.objSessionVar.ModelUser IsNot Nothing Then
            lbCustID.Text = Me.objSessionVar.ModelUser.CustID
            lbInvAddr1.Text = Me.objSessionVar.ModelUser.Invoice.InvAddr1
            lbInvAddr2.Text = Me.objSessionVar.ModelUser.Invoice.InvAddr2
            lbInvAddr3.Text = Me.objSessionVar.ModelUser.Invoice.InvAddr3
            lbInvAddr4.Text = Me.objSessionVar.ModelUser.Invoice.InvAddr4
        End If
    End Sub
End Class
