﻿
Partial Class Admin_ajManageFlagCard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Complete As Boolean
        Dim CardID As String = Request("CardID")
        Dim Active As String = Request("Active")
        Dim objProvider As New clsCardProvider

        If CardID = "" Then
            Response.Write("กรุณาเลือก Card ด้วยค่ะ")
            Exit Sub
        End If

        Complete = objProvider.ActiveCard(CardID, Active)

        If Complete Then
            Response.Write("UpDate Flag เรียบร้อยแล้วค่ะ")
        Else
            Response.Write("Error!!!...กรุณาทำรายการอีกครั้งค่ะ")
        End If

    End Sub

End Class
