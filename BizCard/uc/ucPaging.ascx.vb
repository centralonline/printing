Imports System.Data
Imports System.Collections.Generic

Partial Class ucPaging
    Inherits clsUCBase

#Region "Properties"

    Private PGCountValue As Integer
    Public Property PGCount() As Integer
        Get
            Return PGCountValue
        End Get
        Set(ByVal value As Integer)
            PGCountValue = value
        End Set
    End Property

    Private PageNoValue As Integer
    Public Property PageNo() As Integer
        Get
            Return PageNoValue
        End Get
        Set(ByVal value As Integer)
            PageNoValue = value
        End Set
    End Property

    Private PageSizeValue As Integer
    Public Property PageSize() As Integer
        Get
            Return PageSizeValue
        End Get
        Set(ByVal value As Integer)
            PageSizeValue = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            '--> Add Event handler
        End If
        Me.refreshTotalPage(PGCount)
    End Sub

    Private Sub refreshTotalPage(ByVal TotalPG As Integer)
        Dim TempTotalPage As Integer = 1
        If (Me.PageSize = 0) Then
            Me.dlPageNo.DataSource = Nothing
        Else
            TempTotalPage = Math.Ceiling(TotalPG / Me.PageSize)
            Dim DTPage As New DataTable
            DTPage.Columns.Add("PageNo")
            DTPage.Columns.Add("PageText")
            Dim DRPage As DataRow
            For i As Integer = 0 To TempTotalPage - 1
                DRPage = DTPage.NewRow()
                DRPage("PageNo") = i
                DRPage("PageText") = i + 1
                DTPage.Rows.Add(DRPage)
            Next
            If (Me.PageNo > 0) Then
                DRPage = DTPage.NewRow()
                DRPage("PageNo") = Me.PageNo - 1
                DRPage("PageText") = "Prev"
                DTPage.Rows.InsertAt(DRPage, 0)
            End If
            If (Me.PageNo < TempTotalPage - 1) Then
                DRPage = DTPage.NewRow()
                DRPage("PageNo") = Me.PageNo + 1
                DRPage("PageText") = "Next"
                DTPage.Rows.Add(DRPage)
            End If

            If DTPage.Rows.Count > 0 Then
                Me.dlPageNo.DataSource = DTPage
                Me.dlPageNo.DataBind()

                Me.lbPageNo.Visible = True
                Me.lbGoto.Visible = True
                Me.txtPageNo.Visible = True
            Else
                Me.lbPageNo.Visible = False
                Me.lbGoto.Visible = False
                Me.txtPageNo.Visible = False
            End If

        End If

        Me.txtPageNo.Text = Me.PageNo + 1 & " / " & TempTotalPage
        Me.dlPageNo.SelectedIndex = Me.PageNo

    End Sub

    Protected Sub dlPageNo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlPageNo.ItemDataBound
        If (e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.SelectedItem) Then
            Dim RV As DataRowView = e.Item.DataItem
            If (RV("PageText") <> "Prev" AndAlso RV("PageText") <> "Next" AndAlso RV("PageNo") = Me.PageNo) Then
                Dim lblPageNo As New Label
                lblPageNo.Font.Bold = True
                lblPageNo.Text = RV("PageText")
                e.Item.Controls.Add(lblPageNo)
            Else
                Dim btnChangePage As New LinkButton
                btnChangePage.Attributes.Add("onclick", "ChangePage('" & RV("PageNo") & "'); return false;")
                btnChangePage.Text = RV("PageText")
                e.Item.Controls.Add(btnChangePage)
            End If
        End If

    End Sub

End Class
