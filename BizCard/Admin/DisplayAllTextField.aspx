﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DisplayAllTextField.aspx.vb"
    Inherits="Admin_DisplayAllTextField" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <script language="javascript" type="text/javascript">
    $(document).ready(function() {
    
        $("#btnDelete").click(function(){
        var Seq = "";
        var CardID = "<%=objSessionVar.ThisCardID%>";
        $(".clsDelete:checked").each(function(){ 
        Seq += $(this).attr("value") + ","; 
        })
        $.ajax({
        type: "POST",
        url: "ajDelete.aspx",
        data: {"CardID":CardID, "Seq":Seq},
        success: function(msg) {
        alert(msg);
        window.location = "AddTemplate.aspx";
        }
        });
        });
     });
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <%  If ObjTextCard IsNot Nothing AndAlso ObjTextCard.Count > 0 Then%>
    <div>
        <b>แสดง ข้อมูล/ตำแหน่งของข้อมูล ที่ต้องการเพิ่มบน Card ทั้งหมด</b>
        <table id="tbOrder" class="tableOrder" style="width: 100%">
            <thead>
                <tr style="background-color: #cfe">
                    <th align="center" style="width: 100px">
                        ลำดับ
                    </th>
                    <th align="center" style="width: 100px">
                        ชื่อ<br/>คอลัมน์
                    </th>
                    <th align="center" style="width: 100px">
                        ข้อความ<br/>Guide
                    </th>
                    <th align="center" style="width: 100px">
                        แกน Y
                    </th>
                    <th align="center" style="width: 100px">
                        แกน X
                    </th>
                    <%--<th align="center" style="width: 100px">
                        zIndex
                    </th>--%>
                    <%--<th align="center" style="width: 100px">
                        BackColor
                    </th>--%>
                    <th align="center" style="width: 100px">
                        ForeColor
                    </th>
                    <th align="center" style="width: 100px">
                        ชื่อ Font
                    </th>
                    <th align="center" style="width: 100px">
                        Font<br />
                        Size
                    </th>
                    <th align="center" style="width: 100px">
                        Font<br />
                        Bold
                    </th>
                    <th align="center" style="width: 100px">
                        Font<br />
                        Italic
                    </th>
                    <th align="center" style="width: 100px">
                        Font<br />
                        Underline
                    </th>
                    <th align="center" style="width: 100px">
                        Right<br />
                        2Left
                    </th>
                    <th align="center" style="width: 100px">
                        Align<br />
                        Center
                    </th>
                    <th align="center" style="width: 100px">
                        Delete
                    </th>
                </tr>
            </thead>
            <tbody>
                <%  Dim Count As Integer = 0
                    For Each ObjectTextFields As clsCardTextField In ObjTextCard
                %>
                <tr style="background-color: #fff" align="center">
                    <td align="center">
                        <%=ObjectTextFields.Seq%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.FieldName%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.TextValue%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.PositionTop%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.PositionLeft%>
                    </td>
                    <%--<td align="center">
                        <%=ObjectTextFields.zIndex%>
                    </td>--%>
                    <%--<td align="center">
                        <%=ObjectTextFields.BackColor%>
                    </td>--%>
                    <td align="center">
                        <%=ObjectTextFields.ForeColor%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.FontFamily%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.FontSize%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.FontBold%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.FontItalic%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.FontUnderline%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.Right2Left%>
                    </td>
                    <td align="center">
                        <%=ObjectTextFields.AlignCenter%>
                    </td>
                    <td align="center">
                        <input id="Checkbox<%=Count%>" type="checkbox" class="clsDelete" value="<%=ObjectTextFields.Seq%>" />
                    </td>
                </tr>
                <%
                    Count += 1
                Next%>
            </tbody>
        </table>
        <br />
        <div align="Right">
            <input id="btnDelete" type="button" value="ลบข้อมูลบน Card" style="width: 120px;"/>
        </div>
    </div>
    <%Else%>
    [No Data]
    <%End If%>
    </form>
</body>
</html>
