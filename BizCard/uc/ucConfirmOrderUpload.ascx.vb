Imports System.Collections.Generic
Partial Class uc_ucConfirmOrderUpload
    Inherits clsUCBase

    Private OrderIDValue As String
    Public Property OrderID() As String
        Get
            Return OrderIDValue
        End Get
        Set(ByVal value As String)
            OrderIDValue = value
        End Set
    End Property
    Private CardIDValue As String
    Public Property CardID() As String
        Get
            Return CardIDValue
        End Get
        Set(ByVal value As String)
            CardIDValue = value
        End Set
    End Property
    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        MyBase.OnInit(e)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            Dim clsOrder As New clsOrderEngine
            Dim listOrder As New List(Of clsOrder)
            Dim oCard As New clsCard 'oUCard

           
            Me.btnPrint.Attributes.Add("onclick", "window.print();")
            listOrder = clsOrder.GetInfoOrders(Me.OrderID)


            Dim ClsCardEn As New clsCardEngine
            Dim clsCards As New clsCard

            clsCards = ClsCardEn.GetCardByID(Me.CardID)
            Me.lbCardID.Text = clsCards.CardID
            Me.lbCardName.Text = clsCards.CardName

            If Request.QueryString("CardID") <> "" Then
                Dim clsCardEngine As New clsCardEngine
                Dim cardID As String = Request.QueryString("CardID")
                Dim clsCard As New clsCard
                clsCard = clsCardEngine.GetCardByID(cardID)

                Me.dlUploadFile.DataSource = clsCard.ImageFields
                Me.dlUploadFile.DataBind()
            End If
            


            Me.lbOrderID.Text = listOrder.Item(0).OrderID
            Me.lbDateOrder.Text = listOrder.Item(0).OrderDate
            Me.lbPaperName.Text = listOrder.Item(0).PaperName
            Me.lbPrice.Text = listOrder.Item(0).PaperPrice
            Me.lbNumOrder.Text = listOrder.Item(0).Qty
            Me.lbTotal.Text = listOrder.Item(0).NetAmt
            Me.lbVat.Text = listOrder.Item(0).VatAmt
            Me.lbSum.Text = listOrder.Item(0).TotAmt
            lbTypePayment.Text = listOrder.Item(0).PaymentCode
            Me.lbAddress1.Text = listOrder.Item(0).ShippingAddr1
            Me.lbAddress2.Text = listOrder.Item(0).ShippingAddr2
            Me.lbAddress3.Text = listOrder.Item(0).ShippingAddr3
            Me.lbCustID.Text = listOrder.Item(0).CustID
            'Me.lbCustOld.Text = ""
            'If listOrder.Item(0).CustOld <> "" Then
            '    Me.lbCustOld.Text = "(" & listOrder.Item(0).CustOld & ")"
            'End If
            Me.lbRemark.Text = listOrder.Item(0).Description

			'Me.lbContact.Text = Me.objSessionVar.User.UserName
			'Me.lbCustomer.Text = Me.objSessionVar.User.UserName
			'Me.lbPhone1.Text = Me.objSessionVar.User.PhoneNo
			'Me.lbPhone2.Text = Me.objSessionVar.User.PhoneNo
			'Me.lbFax1.Text = Me.objSessionVar.User.FaxNo
			'Me.lbFax2.Text = Me.objSessionVar.User.FaxNo

         

        End If

    End Sub
End Class
