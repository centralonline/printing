Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.Data
Imports System.Collections.Generic

Public Class clsPaper
#Region "Create Property Fields"
    Private PaperIDValue As String
    Public Property PaperID() As String
        Get
            Return PaperIDValue
        End Get
        Set(ByVal value As String)
            PaperIDValue = value
        End Set
    End Property

    Private DescriptionValue As String
    Public Property Description() As String
        Get
            Return DescriptionValue
        End Get
        Set(ByVal value As String)
            DescriptionValue = value
        End Set
    End Property

    Private PriceValue As Decimal
    Public Property Price() As Decimal
        Get
            Return PriceValue
        End Get
        Set(ByVal value As Decimal)
            PriceValue = value
        End Set
    End Property


    Private PriceForSystemValue As String
    Public Property PriceForSystem() As String
        Get
            Return PriceForSystemValue
        End Get
        Set(ByVal value As String)
            PriceForSystemValue = value
        End Set
    End Property

    Private PriceDisplayValue As String
    Public ReadOnly Property PriceDisplay() As String
        Get
            If Price = 0 Then
                Return "0"
            ElseIf Price > 0 And Price < 1 Then
                Return Price.ToString("#,##0.0")
            Else
                Dim result = Split(Price, ".")
                If result(1) IsNot Nothing And result(1) > 0 Then
                    Return Price.ToString("#,##0.00")
                End If
                Return Price.ToString("#,##0")
            End If
            Return PriceDisplayValue
        End Get
    End Property


    Private PriceDescriptionValue(Description) As String
    Public ReadOnly Property PriceDescription() As String
        Get

            If Price = 0 Then
                Return Description
            else
                Return (Description) + " " + (PriceDisplay.ToString()) + " �ҷ"
            End If


        End Get

    End Property


	Private SeqValue As String
	Public Property Seq() As String
		Get
			Return SeqValue
		End Get
		Set(ByVal value As String)
			SeqValue = value
		End Set
	End Property


    Private PromotionValue As Decimal
    Public Property Promotion() As Decimal
        Get
            Return PromotionValue
        End Get
        Set(ByVal value As Decimal)
            PromotionValue = value
        End Set
    End Property


    Private statusValue As String
    Public Property status() As String
        Get
            Return statusValue
        End Get
        Set(ByVal value As String)
            statusValue = value
        End Set
    End Property
    'Pragaydaw Chalermboon
    'Createby 26/6/2014
    Private SizeValue As String
    Public Property Size() As String
        Get
            Return SizeValue
        End Get
        Set(ByVal value As String)
            SizeValue = value
        End Set
    End Property

    Private TypenameValue As String
    Public Property Typename() As String
        Get
            Return TypenameValue
        End Get
        Set(ByVal value As String)
            TypenameValue = value
        End Set
    End Property
    Private ListPaperValue As String
    Public Property ListPaper() As String
        Get
            Return ListPaperValue
        End Get
        Set(ByVal value As String)
            ListPaperValue = value
        End Set
    End Property

#End Region
End Class

'Gift ���� select data  Digital  Inkjet Card
'CreateBy 26/6/2014
Public Class clsPaperEngine
    Inherits clsBLBase
    Public Function GetListPaperdigital(ByVal Type As String) As List(Of clsPaper)
        Dim ListPaper As New List(Of clsPaper)
        Dim TempPaperProvider As New ClsPaperSize()
        ListPaper = TempPaperProvider.Getddlsize(Type)
        Me.objSessionVar.Paper = ListPaper

        Return ListPaper
    End Function

    Public Function GetListPapersize(ByVal size As String, ByVal Type As String, Optional ByVal isUpload As Boolean = False) As List(Of clsPaper)
        Dim namepaper As New List(Of clsPaper)
        Dim TempPaperProvidersize As New ClsNamePaper()
        namepaper = TempPaperProvidersize.Getddlnamesize(size, Type, isUpload)
        Me.objSessionVar.Paper = namepaper

        Return namepaper
    End Function

    Public Function GetListPaperDecription(ByVal ddlname As String, ByVal size As String, ByVal Type As String) As List(Of clsPaper)
        Dim namepaperDescription As New List(Of clsPaper)
        Dim TempPaperProviderDecription As New ClsNamePaper()
        namepaperDescription = TempPaperProviderDecription.GetddlnameDecription(ddlname, size, Type)
        Me.objSessionVar.Paper = namepaperDescription
        Return namepaperDescription
    End Function

    Public Function GetOptionPrice(ByVal size As String, ByVal optionsizepaper As String) As List(Of clsPaper)
        Dim OptionPrice As New List(Of clsPaper)
        Dim TempPaperOptionPrice As New ClsNamePaper()
        OptionPrice = TempPaperOptionPrice.GetddlOptionPrice(size, optionsizepaper)
        Me.objSessionVar.Paper = OptionPrice

        Return OptionPrice
    End Function

    Public Function GetService(ByVal Card As String, ByVal Cover As String) As List(Of clsPaper)
        Dim serviceprice As New List(Of clsPaper)
        Dim TempPaperOptionPrice As New ClsNamePaper()
        serviceprice = TempPaperOptionPrice.GetService(Card, Cover)
        Me.objSessionVar.Paper = serviceprice

        Return serviceprice
    End Function

    Public Function GetAngle(ByVal Card As String, ByVal Angel As String) As List(Of clsPaper)
        Dim AnglePrice As New List(Of clsPaper)
        Dim TempPaperOptionPrice As New ClsNamePaper()
        AnglePrice = TempPaperOptionPrice.GetService(Card, Angel)
        Me.objSessionVar.Paper = AnglePrice

        Return AnglePrice
    End Function
End Class

Public Class ClsPaperSize
    Inherits clsBLBase
    Public Function Getddlsize(ByVal Type As String) As List(Of clsPaper)
        Dim ListPaper As New List(Of clsPaper)
        Dim SqlStr As New StringBuilder

        SqlStr.AppendLine("select distinct Size")
        SqlStr.AppendLine(" FROM TBBCPapers")
        SqlStr.AppendLine(" where Type = '" & Type & "'")
        SqlStr.AppendLine("order by Size DESC")

        Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            objDB.ParametersClear()
            objDB.SelectReQuery(SqlStr.ToString())

            While (objDB.Go)
                Dim ddl As New clsPaper
                ddl.ListPaper = objDB.ReadField("Size")
                ddl.Size = objDB.ReadField("Size")
                ListPaper.Add(ddl)
            End While
            Return ListPaper
        End Using
    End Function

End Class

Public Class ClsNamePaper
    Inherits clsBLBase
    Public Function Getddlnamesize(ByVal size As String, ByVal Type As String, Optional ByVal isUpload As Boolean = False) As List(Of clsPaper)
        Dim namepaper As New List(Of clsPaper)
        Dim SqlStr As New StringBuilder

        SqlStr.AppendLine("SELECT  PaperID, Description, Price,PricePromotion, status,size")
        SqlStr.AppendLine("FROM TBBCPapers")
        SqlStr.AppendLine("where Type ='" & Type & "'")
        If Not String.IsNullOrEmpty(Type) Then
            SqlStr.AppendLine("and size = '" & size & "'")
        End If
        Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            objDB.SelectReQuery(SqlStr.ToString())

            While (objDB.Go)
                Dim Paper As New clsPaper
                Paper.ListPaper = objDB.ReadField("Description")
                Paper.Description = objDB.ReadField("Description")
                Paper.PaperID = objDB.ReadField("PaperID")
                Paper.Price = objDB.ReadField("Price")
                Paper.status = objDB.ReadField("status")
                Paper.Promotion = objDB.ReadField("PricePromotion")
                namepaper.Add(Paper)
            End While
            Return namepaper
        End Using
    End Function

    Public Function GetddlnameDecription(ByVal ddlname1 As String, ByVal size As String, ByVal Type As String) As List(Of clsPaper)
        Dim namepaperDecription As New List(Of clsPaper)
        Dim SqlStr As New StringBuilder

        SqlStr.AppendLine("SELECT  PaperID, Description, Price,PricePromotion, status,size,Type")
        SqlStr.AppendLine("FROM TBBCPapers")
        SqlStr.AppendLine(" where Type ='" & Type & "'")
        SqlStr.AppendLine("and size = '" & size & "'")
        SqlStr.AppendLine("and PaperID = '" & ddlname1 & "'")
        Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            objDB.SelectReQuery(SqlStr.ToString())

            While (objDB.Go)
                Dim name As New clsPaper
                name.PaperID = objDB.ReadField("PaperID")
                name.Price = objDB.ReadField("Price")
                name.status = objDB.ReadField("status")
                name.Promotion = objDB.ReadField("PricePromotion")
                name.Typename = objDB.ReadField("Type")

                namepaperDecription.Add(name)
            End While
            Return namepaperDecription
        End Using
    End Function

    Public Function GetddlOptionPrice(ByVal size As String, ByVal optionsizepaper As String) As List(Of clsPaper) '��ԡ������������
        Dim OptionPrice As New List(Of clsPaper)
        Dim Sqlstr As New StringBuilder
		Sqlstr.AppendLine("SELECT sizepaper, Description , Price, Pid ")
        Sqlstr.AppendLine("FROM TBBCOptionPrice")
        Sqlstr.AppendLine("Where sizepaper = '" & size & "'")

        If optionsizepaper <> String.Empty And optionsizepaper <> "0.00" Then
            Sqlstr.AppendLine("AND Pid = '" & optionsizepaper & "'")
        End If

        Sqlstr.AppendLine("Order By Pid asc")

        Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            ObjDB.SelectReQuery(Sqlstr.ToString())

            While (ObjDB.Go)
                Dim ddloption As New clsPaper
                ddloption.Size = ObjDB.ReadField("sizepaper")
                ddloption.Description = ObjDB.ReadField("Description")
                ddloption.Price = ObjDB.ReadField("Price")
                ddloption.Seq = ObjDB.ReadField("Pid")

                OptionPrice.Add(ddloption)
            End While
            Return OptionPrice

        End Using
    End Function


    Public Function GetService(ByVal Card As String, ByVal Cover As String) As List(Of clsPaper) '��ԡ���������������ͺ����� Card
        Dim serviceprice As New List(Of clsPaper)
        Dim Sqlstr As New StringBuilder
		Sqlstr.AppendLine("SELECT Description, Price ,Pid FROM TBBCOptionPrice ")
        Sqlstr.AppendLine("where PrintingType ='" & Card & "'")
        Sqlstr.AppendLine("and OptionType = '" & Cover & "'")

        Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            ObjDB.SelectReQuery(Sqlstr.ToString())
            While (ObjDB.Go)
                Dim radservice As New clsPaper
                radservice.Description = ObjDB.ReadField("Description")
                radservice.Price = ObjDB.ReadField("Price")
                radservice.PriceForSystem = ObjDB.ReadField("Price") + "_" + ObjDB.ReadField("Description")
                serviceprice.Add(radservice)
            End While
            Return serviceprice
        End Using
    End Function

    Public Function GetAnglePrice(ByVal Card As String, ByVal Angle As String) As List(Of clsPaper)
        Dim AnglePrice As New List(Of clsPaper)
        Dim Sqlstr As New StringBuilder
        Sqlstr.AppendLine("SELECT Description, Price ,PID FROM TBBCOptionPrice ")
        Sqlstr.AppendLine("where PrintingType ='" & Card & "'")
        Sqlstr.AppendLine("and OptionType = '" & Angle & "'")

        Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            ObjDB.SelectReQuery(Sqlstr.ToString())
            Dim index As Integer = 0
            While (ObjDB.Go)
                Dim radservice As New clsPaper
                radservice.Description = ObjDB.ReadField("Description")
                radservice.Price = ObjDB.ReadField("Price")
                radservice.PriceForSystem = ObjDB.ReadField("Price") + "_" + index.ToString()
                index += 1
                AnglePrice.Add(radservice)
            End While
            Return AnglePrice
        End Using
    End Function
End Class






