﻿Imports System.Net.Mail
Imports CoreBaseII

Partial Class ContactUs

	Inherits clsHTTPBase

	Private ThisName As String
	Private ThiseMail As String
	Private ThisTel As String
	Private ThisMessage As String

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		If objSessionVar.ModelUser IsNot Nothing Then
			Me.txtName.Text = objSessionVar.ModelUser.UserName
			Me.txtMail.Text = objSessionVar.ModelUser.UserID
			Me.txtTel.Text = objSessionVar.ModelUser.Mobile


		End If
		'CaptchaControl1.CaptchaWidth = 200
		'CaptchaControl1.CaptchaLength = 5

		SetHtmlPage()
		BindValueToProperty()

	End Sub
	Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSend.Click

		'If (CaptchaControl1.IsValid) Then
		If ValidateData() Then
			If objAppVar.SiteName = "Y" Then
                clsMailService.SendMail("printing@officemate.co.th", "OfficeMate Online System", "printing@officemate.co.th", "Contact From " & Me.txtName.Text & " Tel. " & Me.txtTel.Text, clsMailService.toOFMFormat("https://printing.officemate.co.th/", Me.createMailMsg))
			Else
                clsMailService.SendMail("printing@officemate.co.th", "OfficeMate Offline System", "panupan.ja@officemate.co.th", "Contact From " & Me.txtName.Text & " Tel. " & Me.txtTel.Text, clsMailService.toOFMFormat("https://printing.officemate.co.th/", Me.createMailMsg))
			End If
			Me.txtName.Text = ""
			Me.txtMail.Text = ""
			Me.txtTel.Text = ""
			Me.txtbodycontact.Text = ""
			MessageBox.Show("ระบบทำการส่งข้อความของท่านเรียบร้อยแล้ว...กรุณารอการติดต่อจากเจ้าหน้าที่ค่ะ")
		End If

		'End If

	End Sub
	Private Function createMailMsg() As String
		Dim TempStrMail As String

		Using MailStr As New clsStringBuilder
			With MailStr
				.AppendLine(MyLang.Show("ผู้ติดต่อ: ", "Contact: "))
				.AppendLine(txtName.Text & " <BR><BR> ")
				.AppendLine("e-Mail Address: ")
				.AppendLine(txtMail.Text & "<BR><br>")
				.AppendLine(MyLang.Show("เบอร์โทรติดต่อกลับ: ", "Phone No: "))
				.AppendLine(txtTel.Text & "<BR><br>")
				.AppendLine(MyLang.Show("ข้อความ: ", "Message: "))
				.AppendLine(txtbodycontact.Text & "<BR><BR>")
			End With

			TempStrMail = MailStr.ToString
		End Using

		Return TempStrMail
	End Function
	Private Function EmailValid(ByVal eMail As String) As Boolean
		Dim emailRegex As New System.Text.RegularExpressions.Regex("\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*")
		If emailRegex.IsMatch(eMail) = True Then
			Return True
		Else
			Return False
		End If
	End Function
	Private Function ValidateData() As Boolean
		If (Me.ThisName.Trim() = "") Then
			MessageBox.Show(MyLang.Show("กรุณากรอกชื่อผู้ติดต่อ", "Please fill contact."))
			Return False
		End If

		If (Me.ThiseMail.Trim() = "") Then
			MessageBox.Show(MyLang.Show("กรุณากรอก e-Mail Address", "Please fill e-Mail Address."))
			Return False
		Else

			Dim strEmail As String = txtMail.Text

			If Not EmailValid(strEmail) Then
				MessageBox.Show(MyLang.Show("Format e-Mail ไม่ถูก Format กรุณาตรวจสอบความถูกต้อง", "e-Mail format is incorrect, please try again."))
				Return False
			End If

		End If

		If (Me.ThisTel.Trim() = "") Then
			MessageBox.Show(MyLang.Show("กรุณากรอกหมายเลขโทรศัพท์ผู้ส่ง", "Please fill receiver's phone no."))
			Return False
		End If
		If (Me.ThisMessage.Trim() = "") Then
			MessageBox.Show(MyLang.Show("กรุณากรอกข้อความ", "Please fill message."))
			Return False
		End If

		Return True
	End Function
	Private Sub SetHtmlPage()
		Me.Header.Title = MyLang.Show("OfficeMate: ติดต่อบริษัทออฟฟิศเมท", "OfficeMate: Contact")
		Dim meta As New HtmlMeta
		meta.Attributes.Add("name", "keywords")
		meta.Attributes.Add("content", "Contact, ติดต่อ")
		Header.Controls.Add(meta)
	End Sub
	Private Sub BindValueToProperty()
		Me.ThisName = Me.txtName.Text
		Me.ThiseMail = Me.txtMail.Text
		Me.ThisTel = Me.txtTel.Text
		Me.ThisMessage = Me.txtbodycontact.Text
	End Sub
End Class
