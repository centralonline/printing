﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	CodeFile="MyAccount.aspx.vb" Inherits="MyAccount" Title="My Account" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<script type="text/javascript">
		$(function () {

			$("#txtCheckUser").val("<%=objSessionVar.ModelUser%>");
			if ($("#txtCheckUser").val() == "") {
				alert("กรุณา Login ด้วยค่ะ")
				window.location.href = "Main.aspx";
				return false;
			}

			var url = '<%=ResolveUrl("~/MyProfile.aspx")%>';
			$.post(url, function (data) { if (data != "") { $('#dvProfileView').html(data); } })

			$("#EditProfile").click(function () {
				var url = '<%=ResolveUrl("~/EditShipping.aspx")%>';
				$.post(url, function (data) { if (data != "") { $('#dvProfileView').html(data); } })
			});

			$("#ChangePass").click(function () {
				var url = '<%=ResolveUrl("~/ChangePass.aspx")%>';
				$.post(url, function (data) { if (data != "") { $('#dvProfileView').html(data); } })
			});

			$("#ViewProfile").click(function () {
				var url = '<%=ResolveUrl("~/MyProfile.aspx")%>';
				$.post(url, function (data) { if (data != "") { $('#dvProfileView').html(data); } })
			});

		});
	</script>
	<div style="margin: auto;">
		<img src="images/h-my-account.png" class="png" alt="My Account" title="My Account" /><br />
	</div>
	<div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<div style="width: 100px; margin-right: 20px; float: left; padding: 10px;">
			<b>บัญชีผู้ใช้</b><br />
			<br />
			<span id="ViewProfile" style="font-size: 12px; color: #888; cursor: pointer;">ข้อมูลส่วนตัว</span><br />
			<span id="EditProfile" style="font-size: 12px; color: #888; cursor: pointer;">ข้อมูลจัดส่งสินค้า</span><br />
			<span id="ChangePass" style="font-size: 12px; color: #888; cursor: pointer;">เปลี่ยนรหัสผ่าน</span><br />
		</div>
		<div id="dvProfileView" style="width: 750px; float: left; padding: 10px;">
		</div>
		<input id="txtCheckUser" type="hidden" />
		<br class="clear" />
	</div>
</asp:Content>
