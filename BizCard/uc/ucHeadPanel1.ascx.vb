
Partial Class ucHeadPanel1
    Inherits clsUCBase

    Public Sub New()
        Me.TableWidth = 100
        Me.PanelName = ""
    End Sub

    Private PanelNameValue As String
    Public Property PanelName() As String
        Get
            Return PanelNameValue
        End Get
        Set(ByVal value As String)
            PanelNameValue = value
        End Set
    End Property

    Private TableWidthValue As Integer
    Public Property TableWidth() As Integer
        Get
            Return TableWidthValue
        End Get
        Set(ByVal value As Integer)
            TableWidthValue = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lblString.Text = PanelName
        Me.Table1.Width = Me.TableWidth
    End Sub

End Class
