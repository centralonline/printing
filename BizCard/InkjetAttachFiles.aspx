﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	EnableEventValidation="False" CodeFile="InkjetAttachFiles.aspx.vb" Inherits="AttachFiles"
	Title="Attach File เพื่อทำการสั่งพิมพ์ Officemate Printing Solution" %>

<%@ Register Src="uc/ucFileUploaded.ascx" TagName="ucFileUploaded" TagPrefix="uc2" %>
<%@ Register Src="uc/ucBar.ascx" TagName="ucBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<div style="margin: auto;">
		<img src="images/h-make-card01.png" class="png" alt="ทำนามบัตร" title="ทำนามบัตร" /><br />
	</div>
	<div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<uc1:ucBar ID="UcBar1" runat="server" />
		<h1>
			บริการสั่งพิมพ์สิ่งพิมพ์ออนไลน์ | ประเภท Inkjet</h1>
		<%--<div id="dvShowDetail" style="padding-bottom: 20px;" runat="server">
			<p>
				กรุณาเลือกรูปแบบรายละเอียดเพิ่มเติมของการทำ Art Work นามบัตรของท่าน</p>
			<p style="font-weight: bold;">
				1. ประเภทธุรกิจ</p>
			<asp:TextBox ID="txtBizType" runat="server" Width="250px" MaxLength="100"></asp:TextBox>
			<p>
				&nbsp;</p>
			<p style="font-weight: bold;">
				2.โทนของนามบัตร</p>
			<asp:RadioButtonList ID="radTone" runat="server" RepeatDirection="Horizontal" Width="100%">
				<asp:ListItem Selected="True">ทันสมัย</asp:ListItem>
				<asp:ListItem>เป็นทางการ</asp:ListItem>
				<asp:ListItem>น่ารัก</asp:ListItem>
				<asp:ListItem Value="อื่นๆ">อื่นๆ</asp:ListItem>
			</asp:RadioButtonList>
			อื่นๆกรุณาระบุ
			<asp:TextBox ID="txtOtherTone" runat="server" MaxLength="50"></asp:TextBox>
			<p>
				&nbsp;</p>
			<p style="font-weight: bold;">
				3.โทนสีของนามบัตร</p>
			<asp:RadioButtonList ID="radToneColor" runat="server" RepeatColumns="3" RepeatDirection="Horizontal"
				Width="100%">
				<asp:ListItem Selected="True">สดใส</asp:ListItem>
				<asp:ListItem>เข้มขรึม</asp:ListItem>
				<asp:ListItem>จัดจ้าน</asp:ListItem>
				<asp:ListItem>หวาน</asp:ListItem>
				<asp:ListItem>พาสเทล</asp:ListItem>
				<asp:ListItem>อื่นๆ</asp:ListItem>
			</asp:RadioButtonList>
			อื่นๆกรุณาระบุ
			<asp:TextBox ID="txtOtherColorTone" runat="server" MaxLength="50"></asp:TextBox>
		</div>--%>
		<div style="width: 450; border: 1px solid #ddd; padding: 10px;">
			<p class="head-column">
				วิธีที่ 1 อัพโหลดไฟล์ที่ต้องการจัดพิมพ์ โดยการ Attach File</p>
			<p>
				กรณีที่มีไฟล์งานหรือโลโก้ ขนาดไม่เกิน 100 MB</p>
			<div class="btn-face2">
				File :
				<asp:FileUpload ID="FileUpload1" runat="server" Width="350px" />
				<asp:Button ID="btnAttach" runat="server" Text="Attach File" class="btn-face" />
			</div>
			<asp:DataList ID="dlImage" runat="server" Width="400px">
				<HeaderTemplate>
					<table border="0" width="100%">
						<tr style="background: #ccc; padding: 5px;">
							<td>
								ชื่อไฟล์
							</td>
							<td>
								ขนาดไฟล์
							</td>
							<td>
								ประเภทไฟล์
							</td>
							<td>
								Remove
							</td>
						</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr style="background: #eee; padding: 5px;">
						<td>
							<asp:Label ID="lbPreviousName" runat="server" Text='<%#Eval("PreviousName") %>'></asp:Label>
						</td>
						<td>
							<asp:Label ID="lbImageSize" runat="server" Text='<%#Eval("ImageSize") %>'></asp:Label>
							<asp:HiddenField ID="hidSeq" runat="server" Value='<%#Eval("Seq")  %>' />
							<asp:HiddenField ID="hidPositionTop" runat="server" Value='<%#Eval("PositionTop") %>' />
							<asp:HiddenField ID="hidPositionLeft" runat="server" Value='<%#Eval("PositionLeft") %>' />
							<asp:HiddenField ID="hidzIndex" runat="server" Value='<%#Eval("zIndex") %>' />
							<asp:HiddenField ID="hidwidth" runat="server" Value='<%#Eval("width") %>' />
							<asp:HiddenField ID="hidHeight" runat="server" Value='<%#Eval("Height") %>' />
							<asp:HiddenField ID="hidCreateBy" runat="server" Value='<%#Eval("CreateBy") %>' />
							<asp:HiddenField ID="hidCreateOn" runat="server" Value='<%#Eval("CreateOn") %>' />
							<asp:HiddenField ID="hidUpdateBy" runat="server" Value='<%#Eval("UpdateBy") %>' />
							<asp:HiddenField ID="hidUpdateOn" runat="server" Value='<%#Eval("UpdateOn") %>' />
							<asp:HiddenField ID="hidImageUrl" runat="server" Value='<%#Eval("imageUrl") %>' />
							<asp:HiddenField ID="hidReadyToPrint" runat="server" Value='<%#Eval("Ready2Print") %>' />
						</td>
						<td>
							<asp:Label ID="LabelType" runat="server"></asp:Label>
						</td>
						<td>
							<asp:ImageButton ID="btnDelete" ImageUrl="~/images/delete.gif" runat="server" CommandArgument='<%# Container.ItemIndex %>'
								CommandName="DeleteImage" />
						</td>
					</tr>
				</ItemTemplate>
				<SeparatorTemplate>
				</SeparatorTemplate>
				<FooterTemplate>
					</table>
				</FooterTemplate>
			</asp:DataList>
			<p>
				<asp:Label ID="txtTotalSize" runat="server" Text="0.0 MB"></asp:Label></p>
			<%--<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
				<p class="head-column">
					วิธีที่ 2 อัพโหลดไฟล์ที่ต้องการจัดพิมพ์ โดยการ FTP</p>
				<p>
					กรณีที่มีไฟล์งานหรือโลโก้ ขนาดใหญ่กว่า 100MB ซึ่งหลังจากทำการสั่งซื้อเรียบร้อยแล้ว
					จะมี e-Mail แจ้งข้อมูลที่ใช้ในการ FTP ไฟล์งานของคุณค่ะ</p>
				<p>
					<a href="HowtoOrder.aspx#FTP" target="_blank">อ่านวิธีการ FTP</a></p>
				<div class="btn-face2">
					<asp:CheckBox ID="cbUploadFTP" runat="server" />
					หากท่านต้องการอัพโหลดไฟล์ที่มีขนาดเกิน 100MB กรุณาเลือกหัวข้อนี้ เพื่ออัพโหลดไฟล์ผ่าน
					FTP</div>
				<br />
			</div>--%>
		</div>
		<div id="dvDetailDesign" runat="server" style="border: 1px solid #ddd; padding: 10px;
			margin-top: 10px; clear: both;">
			<div style="font-size: 80%;">
				<h3>
					คำแนะนำสำหรับการสั่งทำ</h3>
				<li>1.ขนาดของนามบัตรมาตรฐานคือ 5.5 x 9 ซม. ถ้าคุณต้องการขนาดที่แตกต่างจากมาตรฐาน บริษัทฯ
					ขอสงวนสิทธิ์ในการคิดต่าใช้จ่ายเพิ่มในการจัดพิมพ์</li>
				<li>2.หากชิ้นงานมีการระบุค่าสีพิเศษ เช่น spot Color หรือ Pantone ควรระบุค่าสีเป็นค่า
					CMYK มายังบริษัทฯ เพื่อให้สีของชิ้นงานใกล้เคียงกับที่ต้องการมากที่สุด</li>
				<li>3.ไฟล์ภาพอื่นๆ ที่ใช้ประกอบนามบัตร สามารถส่งเป็นไฟล์ pdf , jpg , tif , ai , eps
					, psd , word , excel , ppt ที่มีความละเอียดไม่ต่ำกว่า 300 dpi</li>
				<li>4.ไฟล์ภาพที่สแกนส่งมาต้องไม่มีรอยใดๆ ทั้งสิ้น</li>
				<li>5.ควรทำการครีเอท Text ให้เป็น Outline (Vector) เพื่อป้องกันการไม่อ่าน Text หรือจะทำการ
					Save Fonts มาด้วยก็ได้</li>
				<li>6.กรณีที่นามบัตรเป็นพื้นสี หรือรูปภาพอยู่ติดขอบด้านใดด้านหนึ่ง หรือทุกด้าน ขอให้ทำการขยายขอบชิ้นงาน
					เผื่อการตัดตกออกไปข้างละ 3 มิล เช่น ขนาดนามบัตรมาตรฐาน 5.5 x 9 ซม. ดังนั้นชิ้นงานที่ท่านควรส่งมาจะเป็นขนาด
					6.1 x 9.6 ซม.</li>
			</div>
		</div>
		<div id="dvDetailPrint" runat="server" style="border: 1px solid #ddd; padding: 10px;
			margin-top: 10px; clear: both;">
			<div style="font-size: 80%;">
				<h3>
					คำแนะนำสำหรับการสั่งทำ</h3>
				<li>1.ไฟล์งาน ที่เป็นโลโก้ สามารถส่งได้ทุกประเภทไฟล์ เช่น ai , jpg , pdf เป็นต้น</li>
				<li>2.ไฟล์ข้อมูลชื่อ ที่อยู่ เบอร์โทรศัพท์ สามารถส่งเป็นไฟล์ word หรือ excel</li>
				<li>3.ไฟล์ภาพอื่นๆ ที่ใช้ประกอบนามบัตร สามารถส่งเป็นไฟล์ pdf , jpg , tif ที่มีความละเอียดไม่ต่ำกว่า
					300 dpi</li>
				<li>4.ในกรณีที่มีตัวอย่างของนามบัตร ขอให้ทำการแนบไฟล์มาเป็นไฟล์ภาพ เพื่อเป็นตัวอย่างในการขึ้นแบบ</li>
				<li>5.ขนาดของนามบัตรมาตรฐานคือ 5.5 x 9 ซม. ถ้าคุณต้องการขนาดที่แตกต่างจากมาตรฐาน บริษัทฯ
					ขอสงวนสิทธิ์ในการคิดค่าใช้จ่ายเพิ่มในการจัดพิมพ์</li>
				<li>6.หากชิ้นงานมีการระบุค่าสีพิเศษ เช่น spot Color หรือ Pantone ควรระบุค่าสีเป็นค่า
					CMYK มายังบริษัทฯ เพื่อให้สีของชิ้นงานใกล้เคียงกับที่ต้องการมากที่สุด</li>
			</div>
		</div>
		<div id="dvReadyPrint" runat="server" style="border: 1px solid #ddd; padding: 10px;
			margin-top: 10px; font-size: 80%; margin-bottom: 10px;">
			<b>ข้อตกลง</b><br />
			รูปแบบนามบัตรที่รับไฟล์ ที่พร้อมจัดพิมพ์ เป็นการจัดทำนามบัตร จากไฟล์ที่คุณ Upload
			มาโดยไม่มีการแก้ไขใดๆ ทั้งสิ้น โดยไฟล์งานจะต้องเป็นไฟล์ที่สมบูรณ์แล้ว ทั้งค่าสี
			และความคมชัดของภาพต่างๆ ดังนั้นคุณภาพของสี และความคมชัดของงาน จึงขึ้นอยู่กับไฟล์ต้นฉบับที่
			Upload มายังบริษัทฯ ในกรณีที่ไฟล์งานที่ Upload มา ไม่สมบูรณ์ หรือไม่พร้อมในการจัดพิมพ์
			ทางบริษัทฯ จะมีการติดต่อและแจ้งกลับไปยังท่านอีกครั้ง
			<br />
			<br />
			<span style="padding-top: 20px;">* กรุณาตรวจสอบความถูกต้องของไฟล์งาน ก่อน Upload มายังบริษัทฯ</span><br />
			<br />
			<div id="dvOfmDesign" runat="server" style="font-size: 80%;">
				รูปแบบนามบัตร ที่รับไฟล์เพื่อให้ออฟฟิศเมทดีไซน์ก่อนจัดพิมพ์ เป็นการจัดทำนามบัตร
				โดยที่ทางบริษัทฯ จะนำไฟล์ต่างๆ ที่คุณ Up Load มา ทั้งไฟล์โลโก้ รูปภาพ ข้อความ มาดำเนินการออกแบบ
				จัดวางไฟล์งานต่างๆ ให้ทั้งหมด โดยก่อนการจัดพิมพ์ จะมีเจ้าหน้าที่ของบริษัทฯ ติดต่อกลับไปยังท่าน
				เพื่อส่งแบบให้ตรวจสอบความถูกต้องของชิ้นงาน พร้อมแจ้งราคากับท่าน เพื่อเป็นการยืนยันความถูกต้องก่อนการจัดพิมพ์<br />
			</div>
		</div>
		<div style="border: 1px solid #ddd; background-color: #eee; padding: 10px; margin-top: 10px;">
			<b>ในกรณีที่ท่านออกแบบด้วยโปรแกรม Illustrator หรือ Photoshop</b><br />
			ท่านสามารถ Download Template ขนาดมาตรฐานพร้อมพิมพ์ได้ที่นี่<br />
			<br />
			<img src="images/icon/illus.gif" style="vertical-align: middle;">
			<a href="images/template.ai">illustrator template</a><br />
			<img src="images/icon/illus.gif" style="vertical-align: middle;">
			<a href="images/template.psd">photoshop template</a><br />
		</div>
		<div style="background-color: #fff; font-size: 80%; margin-top: 10px;">
			<p style="background-color: #bbb; padding: 10px; color: #fff">
				<b>ไฟล์ที่ทำการอัพโหลดก่อนหน้านี้</b>
				<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/FileUploaded.aspx">ดูทั้งหมด</asp:HyperLink>
			</p>
			<uc2:ucFileUploaded ID="UcFileUploaded1" runat="server" Visible="false" />
			<br class="clear">
			<%--<asp:Label ID="txtFileCount" runat="server" Text="txtFileCount"></asp:Label>--%>
			<br />
		</div>
		<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
			<div id="btn-card" align="left">
				<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/InkjetUploadCard.aspx" class="btn-face2"><< Previous </asp:HyperLink>
				<asp:ImageButton ID="Button1" ImageUrl="~/images/next_03.gif" runat="server" Style="float: right;" />
			</div>
		</div>
	</div>
</asp:Content>
