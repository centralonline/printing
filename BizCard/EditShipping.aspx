﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditShipping.aspx.vb" Inherits="EditShipping" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Edit Shipping - นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์</title>
	<style type="text/css">
		.style1
		{
			color: #FF0000;
		}
		.column-register
		{
			text-align: left;
		}
		.clear
		{
			clear: both;
		}
	</style>
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			// ส่วนข้อมูลผู้ติดต่อ
			$("#txtContactName").val("<%=objSessionVar.ModelUser.Contact.ContactName%>");
			$("#txtContactMobileNo").val("<%=objSessionVar.ModelUser.Contact.ContactMobileNo%>");
			$("#txtContactPhoneNo").val("<%=objSessionVar.ModelUser.Contact.ContactPhoneNo%>");
			$("#txtContactFaxNo").val("<%=objSessionVar.ModelUser.Contact.ContactFaxNo%>");
			//ส่วนที่อยู่ใบกำกับภาษี
			$("#txtInvAddr1").val("<%=objSessionVar.ModelUser.Invoice.InvAddr1%>");
			$("#txtInvAddr2").val("<%=objSessionVar.ModelUser.Invoice.InvAddr2%>");
			$("#txtInvAddr3").val("<%=objSessionVar.ModelUser.Invoice.InvAddr3%>");
			$("#txtInvAddr4").val("<%=objSessionVar.ModelUser.Invoice.InvAddr4%>");
			//ส่วนข้อมูลสถานที่จัดส่ง
			$("#txtShipPhoneNo").val("<%=objSessionVar.ModelUser.Shipping.Item(0).ShipPhoneNo%>");
			$("#txtShipAddr1").val("<%=objSessionVar.ModelUser.Shipping.Item(0).ShipAddr1%>");
			$("#txtShipAddr2").val("<%=objSessionVar.ModelUser.Shipping.Item(0).ShipAddr2%>");
			$("#txtShipAddr3").val("<%=objSessionVar.ModelUser.Shipping.Item(0).ShipAddr3%>");
			$("#dropdownProvince").val("<%=objSessionVar.ModelUser.Shipping.Item(0).ShipProvince%>");
			$("#txtShipZipCode").val("<%=objSessionVar.ModelUser.Shipping.Item(0).ShipZipCode%>");
			$("#txtShipRemark").val("<%=objSessionVar.ModelUser.Shipping.Item(0).ShipRemark%>");

			$("#btnSave").click(function () {
				// เช็คข้อมูลผู้ติดต่อ
				var ContactName = $("#txtContactName").val();
				if (ContactName == "") {
					alert("กรุณาระบุชื่อ-นามสกุลด้วยค่ะ")
					return false;
				}
				var ContactMobileNo = $("#txtContactMobileNo").val();
				if (ContactMobileNo == "") {
					alert("กรุณาระบุเบอร์มือถือด้วยค่ะ")
					return false;
				}
				//เช็คที่อยู่ใบกำกับภาษี
				var InvAddr1 = $("#txtInvAddr1").val();
				if (InvAddr1 == "") {
					alert("กรุณาระบุที่อยู่ใบกำกับภาษีด้วยค่ะ")
					return false;
				}
				//เช็คข้อมูลสถานที่จัดส่ง
				var ShipPhoneNo = $("#txtShipPhoneNo").val();
				if (ShipPhoneNo == "") {
					alert("กรุณาระบุเบอร์ติดต่อกลับด้วยค่ะ")
					return false;
				}
				var ShipAddr1 = $("#txtShipAddr1").val();
				if (ShipAddr1 == "") {
					alert("กรุณาระบุชื่อผู้ด้วยค่ะ")
					return false;
				}
				var ShipAddr2 = $("#txtShipAddr2").val();
				if (ShipAddr2 == "") {
					alert("กรุณาระบุที่อยู่จัดส่งด้วยค่ะ")
					return false;
				}
				var ShipZipCode = $("#txtShipZipCode").val();
				if (ShipZipCode == "") {
					alert("กรุณาระบุรหัสไปรษณีย์ด้วยค่ะ")
					return false;
				}
				//ส่งค่าอื่นๆ
				var ContactPhoneNo = $("#txtContactPhoneNo").val();
				var ContactFaxNo = $("#txtContactFaxNo").val();
				var InvAddr2 = $("#txtInvAddr2").val();
				var InvAddr3 = $("#txtInvAddr3").val();
				var InvAddr4 = $("#txtInvAddr4").val();
				var ShipAddr3 = $("#txtShipAddr3").val();
				var ShipProvince = $("#dropdownProvince").attr("value");
				var ShipRemark = $("#txtShipRemark").val();

				$.ajax({
					type: 'POST',
					url: "ajCheckEditShipping.aspx",
					data: { "ContactName": ContactName, "ContactMobileNo": ContactMobileNo, "ContactPhoneNo": ContactPhoneNo, "ContactFaxNo": ContactFaxNo, "InvAddr1": InvAddr1, "InvAddr2": InvAddr2, "InvAddr3": InvAddr3, "InvAddr4": InvAddr4, "ShipPhoneNo": ShipPhoneNo, "ShipAddr1": ShipAddr1, "ShipAddr2": ShipAddr2, "ShipAddr3": ShipAddr3, "ShipProvince": ShipProvince, "ShipZipCode": ShipZipCode, "ShipRemark": ShipRemark },
					success: function (msg) {
						alert(msg);
						if (msg == 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ') {
							window.location.href = "MyAccount.aspx";
						}
					}
				});

			});

		});
            
	</script>
</head>
<body>
	<form id="EditShippingForm" runat="server">
	<div align="left" style="margin: auto; width: 700px;">
		<h1>
			ที่อยู่ในการติดต่อและจัดส่งสินค้า</h1>
		<div id="ctl00_BodyCenter_panAccount" style="">
			<div class="column-register">
			</div>
			<div class="column-register">
				<b><font color="#ff6600"><span id="ctl00_BodyCenter_Label1">ข้อมูลผู้ติดต่อ</span></font></b><br />
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>ชื่อ-นามสกุล</b>
			</div>
			<div class="column-register">
				<input name="txtContactName" id="txtContactName" style="width: 200px;" type="text"
					maxlength="200" />
				<span class="style1">*</span>&nbsp;
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>เบอร์มือถือ</b>
			</div>
			<div class="column-register">
				<input name="txtContactMobileNo" id="txtContactMobileNo" style="width: 200px;" type="text"
					maxlength="10" />
				<span class="style1">*</span>
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>เบอร์โทรศัพท์</b>
			</div>
			<div class="column-register">
				<input name="txtContactPhoneNo" id="txtContactPhoneNo" style="width: 200px;" type="text"
					maxlength="10" />
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>เบอร์แฟ็กซ์</b>
			</div>
			<div class="column-register">
				<input name="txtContactFaxNo" id="txtContactFaxNo" style="width: 200px;" type="text"
					maxlength="10" />
			</div>
			<br class="clear" />
			<div class="column-register">
				<b><span id="ctl00_BodyCenter_Label2">e-Mail Address</span></b><br />
			</div>
			<div class="column-register">
				<asp:Label ID="lbEmail" runat="server"></asp:Label>
			</div>
			<br class="clear" />
		</div>
		<hr />
		<div id="ctl00_BodyCenter_panTax">
			<div class="column-register">
			</div>
			<div class="column-register">
				<b><font color="#ff6600">ข้อมูลสำหรับการออกใบกำกับภาษี</font></b><br />
			</div>
			<br class="clear" />
			<div class="column-register">
				<input name="txtInvAddr1" id="txtInvAddr1" style="width: 200px;" type="text" maxlength="55" />&nbsp;<span
					class="style1">*</span>
				<br />
				<br class="clear" />
				<input name="txtInvAddr2" id="txtInvAddr2" style="width: 200px;" type="text" maxlength="55" />
				<br />
				<br class="clear" />
				<input name="txtInvAddr3" id="txtInvAddr3" style="width: 200px;" type="text" maxlength="55" />
				<br />
				<br class="clear" />
				<input name="txtInvAddr4" id="txtInvAddr4" style="width: 200px;" type="text" maxlength="55" />
				<br />
				<br class="clear" />
			</div>
		</div>
		<hr />
		<div id="ctl00_BodyCenter_panBilling">
			<div class="column-register">
			</div>
			<div class="column-register">
				<b><font color="#ff6600">ข้อมูลสถานที่จัดส่งสินค้า</font></b><br />
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>ชื่อ-นามสกุล</b>
			</div>
			<div class="column-register">
				<input name="txtShipName" id="txtShipAddr1" style="width: 200px;" type="text" maxlength="55" />&nbsp;<span
					class="style1">*</span>
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>เบอร์ติดต่อกลับ</b>
			</div>
			<div class="column-register">
				<input name="txtShipPhoneNo" id="txtShipPhoneNo" style="width: 200px;" type="text"
					maxlength="10" />&nbsp;<span class="style1">*</span>
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>ที่อยู่จัดส่ง</b>
			</div>
			<div class="column-register">
				<input name="txtShipAddr2" id="txtShipAddr2" style="width: 200px;" type="text" maxlength="55"
					class="required maxlength" />
				<span class="style1">*</span><br />
				<br class="clear" />
				<input name="txtShipAddr3" id="txtShipAddr3" style="width: 200px;" type="text" maxlength="55"
					class="maxlength" />
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>จังหวัด</b>
			</div>
			<div class="column-register">
				<select name="dropdownProvince" id="dropdownProvince" style="width: 100px;">
					<option value="กาญจนบุรี">กาญจนบุรี</option>
					<option selected="selected" value="กรุงเทพฯ">กรุงเทพฯ</option>
					<option value="กระบี่">กระบี่</option>
					<option value="กำแพงเพชร">กำแพงเพชร</option>
					<option value="กาฬสินธุ์">กาฬสินธุ์</option>
					<option value="ขอนแก่น">ขอนแก่น</option>
					<option value="จันทบุรี">จันทบุรี</option>
					<option value="ฉะเชิงเทรา">ฉะเชิงเทรา</option>
					<option value="ชัยนาท">ชัยนาท</option>
					<option value="ชุมพร">ชุมพร</option>
					<option value="ชัยภูมิ">ชัยภูมิ</option>
					<option value="เชียงใหม่">เชียงใหม่</option>
					<option value="เชียงราย">เชียงราย</option>
					<option value="ชลบุรี">ชลบุรี</option>
					<option value="ตาก">ตาก</option>
					<option value="ตรัง">ตรัง</option>
					<option value="ตราด">ตราด</option>
					<option value="หนองคาย">หนองคาย</option>
					<option value="นนทบุรี">นนทบุรี</option>
					<option value="น่าน">น่าน</option>
					<option value="นครปฐม">นครปฐม</option>
					<option value="นครพนม">นครพนม</option>
					<option value="หนองบัวลำภู">หนองบัวลำภู</option>
					<option value="นครนายก">นครนายก</option>
					<option value="นราธิวาส">นราธิวาส</option>
					<option value="นครสวรรค์">นครสวรรค์</option>
					<option value="นครศรีธรรมราช">นครศรีธรรมราช</option>
					<option value="นครราชสีมา">นครราชสีมา</option>
					<option value="บุรีรัมย์">บุรีรัมย์</option>
					<option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์</option>
					<option value="ปราจีนบุรี">ปราจีนบุรี</option>
					<option value="ปัตตานี">ปัตตานี</option>
					<option value="ปทุมธานี">ปทุมธานี</option>
					<option value="พังงา">พังงา</option>
					<option value="พิจิตร">พิจิตร</option>
					<option value="เพชรบุรี">เพชรบุรี</option>
					<option value="พิษณุโลก">พิษณุโลก</option>
					<option value="พัทลุง">พัทลุง</option>
					<option value="เพชรบูรณ์">เพชรบูรณ์</option>
					<option value="พะเยา">พะเยา</option>
					<option value="แพร่">แพร่</option>
					<option value="ภูเก็ต">ภูเก็ต</option>
					<option value="มหาสารคาม">มหาสารคาม</option>
					<option value="แม่ฮ่องสอน">แม่ฮ่องสอน</option>
					<option value="มุกดาหาร">มุกดาหาร</option>
					<option value="ยะลา">ยะลา</option>
					<option value="ยโสธร">ยโสธร</option>
					<option value="ระนอง">ระนอง</option>
					<option value="ราชบุรี">ราชบุรี</option>
					<option value="ระยอง">ระยอง</option>
					<option value="ร้อยเอ็ด">ร้อยเอ็ด</option>
					<option value="ลพบุรี">ลพบุรี</option>
					<option value="ลำปาง">ลำปาง</option>
					<option value="ลำพูน">ลำพูน</option>
					<option value="เลย">เลย</option>
					<option value="ศรีสะเกษ">ศรีสะเกษ</option>
					<option value="สระแก้ว">สระแก้ว</option>
					<option value="สงขลา">สงขลา</option>
					<option value="สมุทรสงคราม">สมุทรสงคราม</option>
					<option value="สุราษฎร์ธานี">สุราษฎร์ธานี</option>
					<option value="สตูล">สตูล</option>
					<option value="สุโขทัย">สุโขทัย</option>
					<option value="สุรินทร์">สุรินทร์</option>
					<option value="สมุทรปราการ">สมุทรปราการ</option>
					<option value="สุพรรณบุรี">สุพรรณบุรี</option>
					<option value="สระบุรี">สระบุรี</option>
					<option value="สกลนคร">สกลนคร</option>
					<option value="สมุทรสาคร">สมุทรสาคร</option>
					<option value="สิงห์บุรี">สิงห์บุรี</option>
					<option value="อุตรดิตถ์">อุตรดิตถ์</option>
					<option value="อ่างทอง">อ่างทอง</option>
					<option value="อุทัยธานี">อุทัยธานี</option>
					<option value="อำนาจเจริญ">อำนาจเจริญ</option>
					<option value="อุบลราชธานี">อุบลราชธานี</option>
					<option value="อยุธยา">อยุธยา</option>
					<option value="อุดรธานี">อุดรธานี</option>
				</select>&nbsp; <span class="style1">*</span>
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>รหัสไปรษณีย์</b>
			</div>
			<div class="column-register">
				<input name="txtShipZipCode" id="txtShipZipCode" style="width: 200px;" type="text"
					class="required digits" minlength="5" maxlength="5" />&nbsp; <span class="style1">*</span>
			</div>
			<br class="clear" />
			<div class="column-register">
				<b>หมายเหตุ</b>
			</div>
			<div class="column-register">
				<input name="txtShipRemark" id="txtShipRemark" style="width: 200px;" type="text"
					maxlength="200" />
			</div>
			<br class="clear" />
		</div>
		<div class="column-register" style="border-top: solid 1px #999999; padding-top: 20px;">
			<input id="btnSave" type="button" name="btnSave" value="Save" class="btn-face" />
		</div>
		<br class="clear" />
	</div>
	</form>
</body>
</html>
