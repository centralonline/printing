﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucConfirmOrderUpload.ascx.vb" Inherits="uc_ucConfirmOrderUpload" %>
<script type="text/javascript">
function printpage()
{
window.print();
report.printing.leftMargin = 9;
}
</script>

<link href="../Css/print.css" rel="stylesheet" type="text/css" />
<table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <img src="images/b_gallery_top_left.gif" /></td>
                <td style="background-image: url(images/b_gallery_top.gif); width: 562px">
                    <img src="images/b_gallery_top.gif" /></td>
                <td>
                    <img src="images/b_gallery_top_right.gif" /></td>
            </tr>
            <tr>
                <td style="background-image: url(images/b_gallery_left.gif)">
                    <img src="images/b_gallery_left.gif" /></td>
                <td valign="top">
                    <table style="width: 248px">
                        <tr>
                            <td colspan="3">
                            <div class="Page" align="left">
<!-- -->
<table width="550" border="0" cellpadding="5" cellspacing="0" bgcolor="#ffffff">
    <tr>
        <td colspan="3">&nbsp;<asp:Image ID="Image1" runat="server"  ImageUrl="~/images/logo_ofm_head.gif" /></td>
    </tr>
    <tr>
        <td colspan="3">เลขที่ใบเสนอราคาของท่านคือ&nbsp;<asp:Label ID="lbOrderID" runat="server" Text="Label" ></asp:Label></td>        
    </tr>
    <tr>
        <td colspan="3">วันที่ทำการสั่งซื้อ :&nbsp;<asp:Label ID="lbDateOrder" runat="server" Text="Label" ></asp:Label></td>
    </tr>
    <tr>
        <td>รหัสลูกค้า:&nbsp;<asp:Label ID="lbCustID" runat="server" Text="Label"></asp:Label>
        </td>        
        <td colspan="2">
            <strong>สถานที่จัดส่ง:</strong></td>        
    </tr>
    <tr>
        <td>ชื่อผู้ติดต่อ :&nbsp;<asp:Label ID="lbContact" runat="server" Text="Label"></asp:Label></td>
        <td>เบอร์โทรศัพท์ :&nbsp;<asp:Label ID="lbPhone1" runat="server" Text="Label"></asp:Label></td>        
        <td><asp:Label ID="lbAddress1" runat="server" Text="Label"></asp:Label></td>
    </tr>
    <tr>
        <td>
            <strong>&nbsp;</strong></td>
        <td>เบอร์แฟ็กซ์:&nbsp;<asp:Label ID="lbFax1" runat="server" Text="Label" ></asp:Label></td>
        <td><asp:Label ID="lbAddress2" runat="server" Text="Label"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="3"><hr />
        </td>
    </tr>
    <tr>
        <td>ชื่อผู้รับสินค้า :&nbsp;<asp:Label ID="lbCustomer" runat="server" Text="Label" ></asp:Label></td>
        <td>เบอร์โทรศัพท์:&nbsp;<asp:Label ID="lbPhone2" runat="server" Text="Label"></asp:Label></td>
        <td><asp:Label ID="lbAddress3" runat="server" Text="Label" ></asp:Label></td>
    </tr>
    <tr>
        <td></td>
        <td>เบอร์แฟ็กซ์:&nbsp;<asp:Label ID="lbFax2" runat="server" Text="Label"></asp:Label></td>
        <td>รูปแบบการชำระเงิน:&nbsp;<asp:Label ID="lbTypePayment" runat="server" Text="Label"></asp:Label></td>
    </tr>
</table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">

<table width="550" border="1" cellpadding="5" cellspacing="0" bordercolor="gainsboro" bgcolor="#ffffff" align="center">
    <tr>
        <td colspan="4" align="left">สรุปรายการสินค้าที่สั่งซื้อ</td>
    </tr>
    <tr>
        <td align="center"><strong>&nbsp;รายละเอียด</strong></td>
        <td align="center"><strong>&nbsp;ราคากระดาษ/หน่วย</strong></td>
        <td align="center"><strong>&nbsp;จำนวนที่สั่งซื้อ(ใบ)</strong></td>
        <td align="center"><strong>&nbsp;รวม</strong></td>
    </tr>
    <tr>
        <td align="left">&nbsp;รหัสนามบัตร:&nbsp;<asp:Label ID="lbCardID" runat="server" Text="Label"></asp:Label></td>
        <td align="left"><asp:Label ID="lbPrice" runat="server" Text="Label"></asp:Label></td>
        <td align="left"><asp:Label ID="lbNumOrder" runat="server" Text="Label"></asp:Label></td>
        <td align="left"><asp:Label ID="lbTotal" runat="server" Text="Label"></asp:Label></td>
    </tr>
    <tr>
        <td align="left">
            &nbsp;ชื่อนามบัตร:&nbsp;<asp:Label ID="lbCardName" runat="server" Text="Label"></asp:Label></td>
        <td colspan="2" align="right">&nbsp;ภาษี (7%)</td>
        <td align="left"><asp:Label ID="lbVat" runat="server" Text="Label"></asp:Label></td>
    </tr>
    <tr>
        <td align="left">&nbsp;ชนิดกระดาษ:&nbsp;
            <asp:Label ID="lbPaperName" runat="server" Text="Label"></asp:Label></td>
        <td colspan="2" align="right">&nbsp;ราคารวม (บาท)</td>
        <td align="left"><asp:Label ID="lbSum" runat="server" Text="Label"></asp:Label></td>
    </tr>
</table>
<!-- -->
<table width="550" border="0" cellpadding="0" cellspacing="0" align="center"> 
    <tr>
        <td colspan="3" style="height: 20px" align="left">รูปแบบนามบัตรที่สั่งทำ :&nbsp;</td>        
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:DataList ID="dlUploadFile" runat="server"  RepeatColumns="3">
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/CardImages/UploadImage/Thumbnails/" & Eval("imageUrl") %>' />
                </ItemTemplate>
            </asp:DataList></td>
    </tr>
    <tr>
        <td colspan="3" align="left">หมายเหตุ :&nbsp;<asp:Label ID="lbRemark" runat="server" Text="Label"></asp:Label></td>
    </tr> 
    <tr>
        <td align="right" colspan="3">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="right">
            <div id="xxxx" class="noprint" align="right">
                <asp:HyperLink ID="btnPrint" runat="server" ImageUrl="~/images/icon_pri.gif"></asp:HyperLink>&nbsp;
                <asp:HyperLink ID="btnExit" runat="server" ImageUrl="~/images/print_ex.gif" NavigateUrl="~/Main.aspx"></asp:HyperLink>                       
            </div>
        </td>
    </tr>
    <tr>
        <td align="right" colspan="3">
        </td>
    </tr>
</table>

                            </td>
                        </tr>
                    </table>
                </td>
                <td style="background-image: url(images/b_gallery_right.gif)">
                    <img src="images/b_gallery_right.gif" /></td>
            </tr>
            <tr>
                <td>
                    <img src="images/b_gallery_botomleft.gif" /></td>
                <td style="background-image: url(images/b_gallery_bottom.gif); width: 562px">
                    <img src="images/b_gallery_bottom.gif" /></td>
                <td>
                    <img src="images/b_gallery_bottom_right.gif" /></td>
            </tr>
        </table>
