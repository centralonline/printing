﻿Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.IO
Imports System.Data
Imports System.Collections.Generic
Imports System.Drawing
Imports System.Drawing.Text
Imports System.Drawing.Imaging
Imports EnumTrendyPrint

''Pragaydaw Chalermboon
'' Create By 29/07/2015

#Region "clsDisplayCategory"
Public Class clsDisplayCategory

	Private CategoryIDValue As String
	Public Property CategoryID() As String
		Get
			Return CategoryIDValue
		End Get
		Set(ByVal value As String)
			CategoryIDValue = value
		End Set
	End Property


	Private CategoryNameValue As String
	Public Property CategoryName() As String
		Get
			Return CategoryNameValue
		End Get
		Set(ByVal value As String)
			CategoryNameValue = value
		End Set
	End Property

	Private ImageURLValue As String
	Public Property ImageURL() As String
		Get
			Return ImageURLValue
		End Get
		Set(ByVal value As String)
			ImageURLValue = value
		End Set
	End Property

	Private CategoryTitleValue As String
	Public Property CategoryTitle() As String
		Get
			Return CategoryTitleValue
		End Get
		Set(ByVal value As String)
			CategoryTitleValue = value
		End Set
	End Property

	Private StatusValue As String
	Public Property Status() As String
		Get
			Return StatusValue
		End Get
		Set(ByVal value As String)
			StatusValue = value
		End Set
	End Property

	Private UpdateByValue As String
	Public Property UpdateBy() As String
		Get
			Return UpdateByValue
		End Get
		Set(ByVal value As String)
			UpdateByValue = value
		End Set
	End Property

	Private UpdateOnValue As String
	Public Property UpdateOn() As String
		Get
			Return UpdateOnValue
		End Get
		Set(ByVal value As String)
			UpdateOnValue = value
		End Set
	End Property

End Class
#End Region

#Region "clsDisplayDetail"
Public Class clsDisplayDetail

	Private DisplayIDValue As String
	Public Property DisplayID() As String
		Get
			Return DisplayIDValue
		End Get
		Set(ByVal value As String)
			DisplayIDValue = value
		End Set
	End Property

	Private DisplayNameValue As String
	Public Property DisplayName() As String
		Get
			Return DisplayNameValue
		End Get
		Set(ByVal value As String)
			DisplayNameValue = value
		End Set
	End Property

	Private MinPriceValue As String
	Public Property MinPrice() As String
		Get
			Return MinPriceValue
		End Get
		Set(ByVal value As String)
			MinPriceValue = value
		End Set
	End Property

	Private MaxPriceValue As String
	Public Property MaxPrice() As String
		Get
			Return MaxPriceValue
		End Get
		Set(ByVal value As String)
			MaxPriceValue = value
		End Set
	End Property

	Private SizeValue As String
	Public Property Size() As String
		Get
			Return SizeValue
		End Get
		Set(ByVal value As String)
			SizeValue = value
		End Set
	End Property

	Private ImageURLValue As String
	Public Property ImageURL() As String
		Get
			Return ImageURLValue
		End Get
		Set(ByVal value As String)
			ImageURLValue = value
		End Set
	End Property

	Private OrderURLValue As String
	Public Property OrderURL() As String
		Get
			Return OrderURLValue
		End Get
		Set(ByVal value As String)
			OrderURLValue = value
		End Set
	End Property
End Class
#End Region

#Region "clsDisplayPaper"
Public Class clsDisplayPaper

	Private PaperIDValue As String
	Public Property PaperID() As String
		Get
			Return PaperIDValue
		End Get
		Set(ByVal value As String)
			PaperIDValue = value
		End Set
	End Property
End Class
#End Region

#Region "clsDisplayEngine"
Public Class clsDisplayEngine
	Public Function GetImage(ByVal PaperID As String) As DataSet
		Dim TempImage As New clsDisplayProvider
		Return TempImage.GetImgDisplayCategory()
	End Function

	Public Function GetImgDisplayDetail(ByVal PaperID As String, ByVal DisplayID As String) As DataSet
		Dim TempDetail As New clsDisplayProvider
		Return TempDetail.GetDisplayDetail(PaperID, DisplayID)
	End Function

	Public Function GetDisplayPreview(ByVal Url As String, ByVal sPath As String, Optional ByVal Zoom As Double = 1) As Bitmap
		Dim TempPreview As New clsDisplayProvider
		Return TempPreview.GenerateDisplayPreview(Url, sPath, Zoom)
	End Function

End Class
#End Region


#Region "clsDisplayProvider"
Public Class clsDisplayProvider
	Inherits clsBLBase

	Public Function GetImgDisplayCategory() As DataSet
		Dim SqlStr As New StringBuilder
		Dim ItemDisplayCategory As DataSet
		With SqlStr
			.AppendLine("SELECT T1.CategoryID,T1.CategoryName,ImageURL,T1.CategoryTitle,T2.PaperID From TBBCDisplayCategory T1")
			.AppendLine("INNER  JOIN TBBCDisplayPaper T2 ON T1.CategoryID = T2.CategoryID ORDER BY T2.CategoryID ASC")
		End With
		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ItemDisplayCategory = objDB.getDataSet(SqlStr.ToString, "tb1")
		End Using
		Return ItemDisplayCategory
	End Function

	Public Function GetDisplayDetail(ByVal PaperID As String, ByVal DisplayID As String) As DataSet
		Dim where As String = ""
		Dim SqlStr As New StringBuilder
		Dim ItemListdata = New DataSet
		If PaperID <> "" Then
			where = "  WHERE T2.PaperID = '" & PaperID & "'"
		End If
		If DisplayID <> "" Then
			where = "  where T1.CategoryID = '" & DisplayID & "'"
		End If
		With SqlStr
			.AppendLine(" SELECT T1.DisplayID, T1.CategoryID, T1.DisplayName, T1.MinPrice, T1.MaxPrice, T1.Size, T1.ImageURL, T1.OrderURL")
			.AppendLine(" FROM TBBCDisplayDetail T1")
			.AppendLine(" INNER JOIN TBBCDisplayPaper T2")
			.AppendLine(" ON T1.CategoryID = T2.CategoryID " & where)
		End With
		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ItemListdata = objDB.getDataSet(SqlStr.ToString, "tb1")
		End Using
		Return ItemListdata
	End Function


	Public Function GenerateDisplayPreview(ByVal Url As String, ByVal sPath As String, Optional ByVal Zoom As Double = 1) As Bitmap
		' Cretae New Bitmap

		Dim oDpi As Int16 = 72
		Dim oBitmap As Bitmap

		oBitmap = New Bitmap(clsMyUtil.mm2Px(90 * Zoom, oDpi), clsMyUtil.mm2Px(55 * Zoom, oDpi))
		Dim oGraphic As Graphics = Graphics.FromImage(oBitmap)
		Dim oColor As Color = Color.White

		Dim oBrush As New SolidBrush(oColor)
		oGraphic.FillRectangle(oBrush, 0, 0, clsMyUtil.mm2Px(90 * Zoom, oDpi), clsMyUtil.mm2Px(55 * Zoom, oDpi))
		oGraphic.TextRenderingHint = TextRenderingHint.AntiAliasGridFit
		oBitmap.SetResolution(oDpi, oDpi)

		oGraphic = Me.SetImageFields(oGraphic, Url, sPath, oDpi, Zoom)

		Return oBitmap
	End Function

	Private Function SetImageFields(ByVal pGraphic As Graphics, ByVal Url As String, ByVal sPath As String, ByVal oDpi As Integer, Optional ByVal Zoom As Double = 1) As Graphics
		Dim oGraphic As Graphics = pGraphic

		'img Background

		''Dim sBgImage As String = sPath & "\CardImages\FullCard\" & Url.Substring(22)
		Dim sBgImage As String = sPath & Url
		Dim oBgImage As New Bitmap(sBgImage)
		oGraphic.DrawImage(oBgImage, 0, 0, clsMyUtil.mm2Px(90 * Zoom, oDpi), clsMyUtil.mm2Px(55 * Zoom, oDpi))


		Return oGraphic
	End Function

End Class

#End Region




