Imports Microsoft.VisualBasic

Public Class clsHTTPBase
    Inherits Page

    Protected objSessionVar As clsSessionVarWC
    Protected objAppVar As clsAppVarWC

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.objAppVar = HttpContext.Current.Application("objAppVar")
        Me.objSessionVar = HttpContext.Current.Session("objSessionVar")
        Me.CheckSessionUser()
    End Sub

    Protected thisTheme As String
    Protected Overrides Sub InitializeCulture()
        MyBase.InitializeCulture()
    End Sub

    Public Overrides Property StyleSheetTheme() As String
        Get
            Return Theme
        End Get
        Set(ByVal value As String)
        End Set
    End Property

    Protected Sub CheckSessionUser()

    End Sub

	Public Shared Sub ShowMessage(ByVal Message As String, Optional ByVal RedirectUrl As String = "")
		Dim OwnerPage As Page
		OwnerPage = HttpContext.Current.Handler
		If OwnerPage IsNot Nothing Then
			Message = Message.Replace("'", "\")
			If RedirectUrl <> "" Then
				ScriptManager.RegisterStartupScript(OwnerPage, OwnerPage.GetType, "Error_Message", "alert('" & Message & "');document.location.href = '" & RedirectUrl & "'; ", True)
			Else
				ScriptManager.RegisterStartupScript(OwnerPage, OwnerPage.GetType, "Error_Message", "alert('" & Message & "');", True)
			End If
		End If
	End Sub

End Class
