﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports ModelContactEmail
Public Class ModelContact
#Region "Create Property Fields"

	Private ContactIDValue As String
	Public Property ContactID() As String
		Get
			Return ContactIDValue
		End Get
		Set(ByVal value As String)
			ContactIDValue = value
		End Set
	End Property

	Private ContactNameValue As String
	Public Property ContactName() As String
		Get
			Return ContactNameValue
		End Get
		Set(ByVal value As String)
			ContactNameValue = value
		End Set
	End Property

	Private ContactMobileNoValue As String
	Public Property ContactMobileNo() As String
		Get
			Return ContactMobileNoValue
		End Get
		Set(ByVal value As String)
			ContactMobileNoValue = value
		End Set
	End Property

	Private ContactPhoneNoValue As String
	Public Property ContactPhoneNo() As String
		Get
			Return ContactPhoneNoValue
		End Get
		Set(ByVal value As String)
			ContactPhoneNoValue = value
		End Set
	End Property

	Private ContactFaxNoValue As String
	Public Property ContactFaxNo() As String
		Get
			Return ContactFaxNoValue
		End Get
		Set(ByVal value As String)
			ContactFaxNoValue = value
		End Set
	End Property

    Private ContactEmailValue As ModelContactEmail
    Public Property ContactEmail() As ModelContactEmail
        Get
            Return ContactEmailValue
        End Get
        Set(ByVal value As ModelContactEmail)
            ContactEmailValue = value
        End Set
    End Property
#End Region
End Class


