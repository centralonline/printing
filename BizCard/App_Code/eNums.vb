Imports Microsoft.VisualBasic

Namespace EnumTrendyPrint
	Public Enum eCardSortBy
		CreateOnDESC = 0
		CreateOnASC = 1
		CardNameDESC = 2
		CardNameASC = 3
	End Enum

	Public Enum eCardType
		Gallery = 1
		Templete = 2
	End Enum

	Public Enum eSystemType
		Trendy = 1
		OFM = 2
	End Enum
End Namespace
