﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MailFormat.aspx.vb" Inherits="MailFormat"
	Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Mail Contact</title>
	<style type="text/css">
		body
		{
			margin-left: 0px;
			margin-top: 0px;
			margin-right: 0px;
			margin-bottom: 0px;
			color: #4d4d46;
			font-size: 13px;
		}
		.red
		{
			color: #FF0000;
			font-weight: bold;
		}
		a
		{
			color: #0277c4;
			text-decoration: none;
		}
		a:hover
		{
			color: #0099FF;
			text-decoration: underline;
		}
	</style>
</head>
<body style="color: black; font-family: 'Microsoft Sans Serif'; background-color: white;
	font-size: 11pt;">
	<div style="padding: 10px; background-color: #f8f8f8; width: 780px">
		<div>
			<img alt="" src="<%=ResolveUrl("/images/logo.gif")%>" /></div>
		<div style="background-color: #000080; height: 5px">
		</div>
		<div style="background-color: #445C8C; padding: 5px; height: 13px">
		</div>
		<div style="background-color: #efefef;">
			<div>
				<div style="display: inline; background-color: #4d4d46; color: #dadada; padding: 3px 60px 3px 20px;
					font-size: 14px;">
					Contact Us</div>
			</div>
			<div style="padding: 30px">
				[MailBody]
			</div>
		</div>
		<div style="background-color: #445C8C; padding: 5px; padding: 5px; height: 13px">
		</div>
		<div style="background-color: #000080; height: 5px">
		</div>
		<div style="background-color: #f8f8f8; font-size: 13px; color: #666666; padding: 10px 0 10px 30px">
			บริษัท ซีโอแอล จำกัด ( มหาชน )<br />
			24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ 10250<br />
			OfficeMate Contact Center: Tel: 02-739-5555, Fax: 02-763-5555<br />
			เวลาทำการ จันทร์ - ศุกร์ 8:30 - 18:00 น.<br />
			© สงวนลิขสิทธิ์ เว็บไซต์ <a href="http://www.Officemate.co.th" target="_blank">officemate.co.th</a>
			โดย <a href="http://www.officemate.co.th" target="_blank">OfficeMate</a></div>
	</div>
</body>
</html>
