Imports System.Collections.Generic
Imports System.Data

Partial Class uc_ucFileUploaded
    Inherits clsUCBase


    Private ThisImageFieldsValue As DataTable
    Public Property ThisImageFields() As DataTable
        Get
            Return ThisImageFieldsValue
        End Get
        Set(ByVal value As DataTable)
            ThisImageFieldsValue = value
        End Set
    End Property

    Private ThisRepeatValue As Integer
    Public Property ThisRepeat() As Integer
        Get
            Return ThisRepeatValue
        End Get
        Set(ByVal value As Integer)
            ThisRepeatValue = value
        End Set
    End Property
    Private imageUrlValue As String
    Public Property ThisImageUrl() As String
        Get
            Return imageUrlValue
        End Get
        Set(ByVal value As String)
            imageUrlValue = value
        End Set
    End Property

    Private previousNameValue As String
    Public Property ThisPreviousName() As String
        Get
            Return previousNameValue
        End Get
        Set(ByVal value As String)
            previousNameValue = value
        End Set
    End Property
    Private ReadyToPrintValue As String
    Public Property ReadyToPrint() As String
        Get
            Return ReadyToPrintValue
        End Get
        Set(ByVal value As String)
            ReadyToPrintValue = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.dlImgFields.RepeatColumns = Me.ThisRepeat
            Me.dlImgFields.DataSource = Me.ThisImageFields
            Me.dlImgFields.DataBind()
        End If
    End Sub

    Protected Sub dlImgFields_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlImgFields.ItemDataBound
        Dim FileType() As String = HttpContext.Current.Application("objFileType") ' from global
        Dim FileTypeImage() As String = HttpContext.Current.Application("objFileTypeImage") ' from global

        Dim hidImgUrl As HiddenField = CType(e.Item.FindControl("hidImgUrl"), HiddenField)
        Dim imgUploaded As Image = CType(e.Item.FindControl("imgUploaded"), Image) '�ٻ����ʴ�
        Dim hlFile As HyperLink = CType(e.Item.FindControl("hlFile"), HyperLink) 'link �Դ���
        Dim hidReadyToPrint As HiddenField = CType(e.Item.FindControl("hidReadyToPrint"), HiddenField)
        Dim hidPreviousName As HiddenField = CType(e.Item.FindControl("hidPreviousName"), HiddenField)
        Dim hidContentType As HiddenField = CType(e.Item.FindControl("hidContentType"), HiddenField)

        '��Ǩ�ͺ������Ҿ

        If hidContentType IsNot Nothing Then
            For ft As Int16 = 0 To FileType.Length - 1
                If hidContentType.Value.Equals(FileType(ft)) = True Then
                    If imgUploaded IsNot Nothing Then
                        imgUploaded.ImageUrl = "../images/" & FileTypeImage(ft)
                        hlFile.NavigateUrl = "~/CardImages/UploadImage/UploadFiles/" & hidImgUrl.Value
                    End If
                End If
            Next
        End If

    End Sub
End Class
