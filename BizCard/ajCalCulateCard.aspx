﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ajCalCulateCard.aspx.vb"
	Inherits="ajCalCulateCard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
</head>
<body>
	<form id="form1" runat="server">
	<div>
		<div>
			<div style="padding: 10px;background: #91c3f4;font-size: 100%;color: #fff;font-weight: bold;">
				รายการที่คุณเลือกคำนวณราคา Poster
			</div>
			<table>
				<tr>
					<th style="text-align:center;">
						ลำดับ
					</th>
					<th style="text-align:center;">
						รายการ
					</th>
					<th style="text-align:center;">
						ราคา
					</th>
					<th style="text-align:center;">
						จำนวน
					</th>
					<th style="text-align:center;">
						รวม
					</th>
				</tr>
				<% 
					Dim total As Decimal = 0
					Dim sumQty As Integer = 0
					Dim count As Integer = 0

					For Each posters As String In split%>
				<tr>
					<%						
						Dim poster As String() = (posters.Split(","))
						Dim description As String = poster(0)
						Dim price As String = poster(1)
						Dim qty As String = poster(2)
						Dim sum As String = poster(3)
							
					%>
					<td style="text-align: center;">
						<%count += 1%>
						<%=count%>
					</td>
					<td>
						<%=price%>
					</td>
					<td style="text-align: center;">
						<%=qty%>
					</td>
					<td style="text-align: right;">
						<%=sum%>
						<%sumQty += sum%>
					</td>
					<td style="text-align: right;">
						<%=(qty * sum).ToString("#,##.00")%>
						<%total += qty * sum%>
					</td>
				</tr>
				<%Next%>
				<tr>
					<td colspan="2" style="text-align: right;font-weight: bold;">
						รวม
					</td>
					<td>
					</td>
					<td style="text-align: right;font-size:14px;font-weight: bold;">
						<%=sumQty%>
					</td>
					<td style="text-align: right;font-size:14px;font-weight: bold;">
						<%=total.ToString("#,##.00")%>
					</td>
				</tr>
			</table>
		</div>
	</div>
	</form>
</body>
</html>
