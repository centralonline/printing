<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucLiveChat.ascx.vb" Inherits="uc_ucLiveChat" %>
<script type="text/javascript">

	var _gaq = _gaq || [];

	_gaq.push(['_setAccount', 'UA-11520015-2']);

	_gaq.push(['_trackPageview']);


	(function () {

		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

	})();

</script>
<style type="text/css">
	#clicktochat
	{
		position: fixed;
		bottom: 0;
		right: 0;
	}
	#clicktochat input
	{
		border: none;
	}
	#randomvote
	{
		position: fixed;
		bottom: 0;
		left: 10;
	}
	#randomvote img
	{
		border: none;
	}
</style>
<div id="clicktochat" style="width: 240px; height: 35px; display: none;">
	<img alt="LiveChat" src="<%=ResolveUrl("../images/livechat.jpg")%>" width="240" height="35"
		style="cursor: pointer" id="img-clicktochat" />
	<%	Dim Check As Boolean = False
		Dim UserId As String = ""
		Dim UserName As String = ""
		If objSessionVar.ModelUser IsNot Nothing Then
			Check = True
			UserId = objSessionVar.ModelUser.UserID
			UserName = objSessionVar.ModelUser.UserName
		End If%>
	<script type="text/javascript">

		function utf8_encode(string) {
			string = string.replace(/\r\n/g, "\n");
			var utftext = "";
			for (var n = 0; n < string.length; n++) {
				var c = string.charCodeAt(n);
				if (c < 128) {
					utftext += String.fromCharCode(c);
				}
				else if ((c > 127) && (c < 2048)) {
					utftext += String.fromCharCode((c >> 6) | 192);
					utftext += String.fromCharCode((c & 63) | 128);
				}
				else {
					utftext += String.fromCharCode((c >> 12) | 224);
					utftext += String.fromCharCode(((c >> 6) & 63) | 128);
					utftext += String.fromCharCode((c & 63) | 128);
				}
			}
			return utftext;
		}

		var Url = "";
		var Check = '<%=Check%>';
		if (Check) {
			var UserId = '<%=UserId%>';
			Url = "http://livechat.officemate.co.th/webchat/TrendyPrint/ChatAndCoBrowse.htm?Emailaddress=" + UserId;
		}
		else {
			Url = "http://livechat.officemate.co.th/webchat/TrendyPrint/ChatAndCoBrowse.htm";
		}

		$(function () {
			$("#clicktochat").show();
			$("#img-clicktochat").click(function () { window.open(Url, "livechat", "fullscreen=no,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=415,left=100,top=100,screenX=100,screenY=100"); });
		});
	</script>
</div>
