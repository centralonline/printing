﻿Imports Microsoft.VisualBasic

Public Class ModelContactEmail
#Region "Create Property Fields"

	Private ContactEmailValue As String
	Public Property ContactEmail() As String
		Get
			Return ContactEmailValue
		End Get
		Set(ByVal value As String)
			ContactEmailValue = value
		End Set
	End Property

#End Region
End Class


