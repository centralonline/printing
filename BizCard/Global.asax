<%@ Application Language="VB" %>
<script RunAt="server">

	Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
		' Code that runs on application startup
		Dim objAppVar As New clsAppVarWC
		Application("objAppVar") = objAppVar
        
		Dim objFileType() As String = {"text/plain", "application/pdf", "application/msword", "application/vnd.ms-excel"}
		Dim objFileTypeImage() As String = {"iconText.jpg", "iconPDF.gif", "iconWord.gif", "iconExcel.gif"}
		Application("objFileType") = objFileType
		Application("objFileTypeImage") = objFileTypeImage
	End Sub
    
	Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
		' Code that runs on application shutdown
	End Sub
        
	Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
		' Code that runs when an unhandled error occurs
		' Code that runs when an unhandled error occurs
		Dim Msg As String = ""
		'--> write parameters
		Dim Req As HttpRequest = HttpContext.Current.Request
		Dim aKey, aValue As String
		Dim Params As String = ""
		For Each aKey In Req.Params.Keys
			aValue = Req(aKey)
			If (aKey = "ALL_HTTP") Then
				Exit For
			End If
			Params += aKey & " = " & aValue & "<br/>"
		Next
		Msg += Params + "<br/>"
        '--> write exception stack trace
        'Response.Redirect("~/GenericError.htm")
	End Sub

	Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
		' Code that runs when a new session is started
		Dim objAppVar As clsAppVarWC = Application("objAppVar")
		If (objAppVar Is Nothing) Then
			objAppVar = New clsAppVarWC()
			Application("objAppVar") = objAppVar
		End If
		Dim objSessionVar As New clsSessionVarWC(objAppVar)
		Session("objSessionVar") = objSessionVar
		'--> Revise Application Variable
		Application.Lock()
		objAppVar.onSessionStart()
		Application.UnLock()
	End Sub
	Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
		Dim objAppVar As clsAppVarWC = Application("objAppVar")
		Application.Lock()
		objAppVar.onSessionStop()
		Application.UnLock()
	End Sub
       
</script>
