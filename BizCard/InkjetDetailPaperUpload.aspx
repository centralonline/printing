﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	CodeFile="InkjetDetailPaperUpload.aspx.vb" Inherits="DetailPaperUpload" Title="เลือกกระดาษ ระบุจำนวนสั่งทำ" %>

<%@ Register Src="uc/ucBar.ascx" TagName="ucBar" TagPrefix="uc1" %>
<%@ Register Src="uc/ucInkjetSelectPaper.ascx" TagName="ucInkjetSelectPaper" TagPrefix="uc2" %>
<%@ Register Src="uc/ucProduct.ascx" TagName="ucProduct" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<div style="margin: auto;">
		<img src="images/h-make-card01.png" class="png" alt="ทำนามบัตร" title="ทำนามบัตร" /><br />
	</div>
	<div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<uc1:ucBar ID="UcBar1" runat="server" />
		<uc2:ucInkjetSelectPaper ID="UcInkjetSelectPaper1" runat="server" />
		<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
			<div id="btn-card" align="left">
				<asp:LinkButton ID="LinkButton1" runat="server" class="btn-face2"><< กลับไปหน้า Attach Files</asp:LinkButton>
				<asp:ImageButton ID="btnNext" runat="server" ImageUrl="~/images/next_03.gif" Style="float: right;" />
			</div>
		</div>
</asp:Content>
