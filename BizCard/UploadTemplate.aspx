<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UploadTemplate.aspx.vb" Inherits="UploadTemplateCard" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 936px">
            <tr>
                <td colspan="2">
                    Upload File: &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;<asp:FileUpload ID="FileUpload1" runat="server" />
                    <asp:Button ID="btnUpload" runat="server" Height="24px" Text="Upload" Width="104px" /></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="height: 26px" colspan="2">
                    BG Image: &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                    <asp:FileUpload ID="BGUpload" runat="server" /></td>
                <td style="height: 26px">
                </td>
            </tr>
            <tr>
                <td style="height: 26px" colspan="2">
                    Simple Image: &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
                    <asp:FileUpload ID="FullUpload" runat="server" />
                    <asp:Button ID="btnUploadImage" runat="server" Text="UploadImage" Width="104px" /></td>
                <td style="height: 26px">
                </td>
            </tr>
            <tr>
                <td style="height: 26px" colspan="2">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>
                <td style="height: 26px">
                </td>
            </tr>
            <tr>
                <td style="width: 95px; height: 26px">
                </td>
                <td style="width: 168px; height: 26px; text-align: center">
                    </td>
                <td style="height: 26px">
                </td>
            </tr>
            <tr>
                <td style="height: 24px;" colspan="2">
                    Select File: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;<asp:DropDownList ID="ddlFileInfo" runat="server" Width="232px">
                    </asp:DropDownList></td>
                <td style="height: 24px">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    WorkSheetname:&nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="DropDownList1" runat="server" Width="232px" />
                    <asp:Button ID="btnExcel" runat="server" Text="Display Excel" Width="104px" /></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 95px; height: 21px;">
                </td>
                <td style="width: 168px; height: 21px;">
                </td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 21px">
                    <asp:Panel ID="Panel1" runat="server" Height="300px" ScrollBars="Auto" Width="100%" Visible="False">
                    <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#E7E7FF"
                        BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Size="Small" GridLines="Horizontal"
                        Width="928px">
                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="height: 21px" colspan="2">
                    Description: &nbsp; &nbsp; &nbsp;&nbsp;<asp:TextBox ID="txtDescription" runat="server" Width="304px"></asp:TextBox></td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 23px; height: 21px">
                    <asp:Button ID="btnInsert" runat="server" Text="Insert" Width="80px" /></td>
                <td style="width: 168px; height: 21px">
                </td>
                <td style="height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 95px; height: 22px">
        <asp:HiddenField ID="hdfName" runat="server" />
                </td>
                <td style="width: 168px; height: 22px">
                </td>
                <td style="height: 22px">
                </td>
            </tr>
        </table>
                <td style="width: 361px">
                </td>
    
    </div>
    </form>
</body>
</html>
