Imports Microsoft.VisualBasic

Public Class clsUCBase
    Inherits System.Web.UI.UserControl

    Protected objSessionVar As clsSessionVarWC
    Protected objAppVar As clsAppVarWC

    Public Sub New()
        Me.objAppVar = HttpContext.Current.Application("objAppVar")
        Me.objSessionVar = HttpContext.Current.Session("objSessionVar")
	End Sub


	Public Shared Sub ShowMessage(ByVal Message As String, Optional ByVal RedirectUrl As String = "")
		Dim OwnerPage As Page
		OwnerPage = HttpContext.Current.Handler
		If OwnerPage IsNot Nothing Then
			Message = Message.Replace("'", "\")
			If RedirectUrl <> "" Then
				ScriptManager.RegisterStartupScript(OwnerPage, OwnerPage.GetType, "Error_Message", "alert('" & Message & "');document.location.href = '" & RedirectUrl & "'; ", True)
			Else
				ScriptManager.RegisterStartupScript(OwnerPage, OwnerPage.GetType, "Error_Message", "alert('" & Message & "');", True)
			End If
		End If
	End Sub

End Class
