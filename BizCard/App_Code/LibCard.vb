Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.IO
Imports System.Data
Imports System.Collections.Generic
Imports System.Drawing
Imports System.Drawing.Text
Imports System.Drawing.Imaging
Imports EnumTrendyPrint


#Region "clsCard"
Public Class clsCard

	Private CardIDValue As String
	Public Property CardID() As String
		Get
			Return CardIDValue
		End Get
		Set(ByVal value As String)
			CardIDValue = value
		End Set
	End Property

	Private CardNameValue As String
	Public Property CardName() As String
		Get
			Return CardNameValue
		End Get
		Set(ByVal value As String)
			CardNameValue = value
		End Set
	End Property

	Private DescriptionValue As String
	Public Property Description() As String
		Get
			Return DescriptionValue
		End Get
		Set(ByVal value As String)
			DescriptionValue = value
		End Set
	End Property

	Private BGColorValue As String
	Public Property BGColor() As String
		Get
			Return BGColorValue
		End Get
		Set(ByVal value As String)
			BGColorValue = value
		End Set
	End Property

	Private BGImageValue As String
	Public Property BGImage() As String
		Get
			Return BGImageValue
		End Get
		Set(ByVal value As String)
			BGImageValue = value
		End Set
	End Property

	Private templateImageValue As String
	Public Property templateImage() As String
		Get
			Return templateImageValue
		End Get
		Set(ByVal value As String)
			templateImageValue = value
		End Set
	End Property

	Private TmpFlagValue As Char
	Public Property TmpFlag() As Char
		Get
			Return TmpFlagValue
		End Get
		Set(ByVal value As Char)
			TmpFlagValue = value
		End Set
	End Property

	Private WidthValue As Integer
	Public Property Width() As Integer
		Get
			Return WidthValue
		End Get
		Set(ByVal value As Integer)
			WidthValue = value
		End Set
	End Property

	Private HeightValue As Integer
	Public Property Height() As Integer
		Get
			Return HeightValue
		End Get
		Set(ByVal value As Integer)
			HeightValue = value
		End Set
	End Property

	Private UploadFlagValue As Char
	Public Property UploadFlag() As Char
		Get
			Return UploadFlagValue
		End Get
		Set(ByVal value As Char)
			UploadFlagValue = value
		End Set
	End Property

	Private ownerValue As String
	Public Property owner() As String
		Get
			Return ownerValue
		End Get
		Set(ByVal value As String)
			ownerValue = value
		End Set
	End Property

	Private CreateByValue As String
	Public Property CreateBy() As String
		Get
			Return CreateByValue
		End Get
		Set(ByVal value As String)
			CreateByValue = value
		End Set
	End Property

	Private CreateOnValue As DateTime
	Public Property CreateOn() As DateTime
		Get
			Return CreateOnValue
		End Get
		Set(ByVal value As DateTime)
			CreateOnValue = value
		End Set
	End Property

	Private UpdateByValue As String
	Public Property UpdateBy() As String
		Get
			Return UpdateByValue
		End Get
		Set(ByVal value As String)
			UpdateByValue = value
		End Set
	End Property

	Private UpdateOnValue As DateTime
	Public Property UpdateOn() As DateTime
		Get
			Return UpdateOnValue
		End Get
		Set(ByVal value As DateTime)
			UpdateOnValue = value
		End Set
	End Property

	Private UserIDValue As String
	Public Property UserID() As String
		Get
			Return UserIDValue
		End Get
		Set(ByVal value As String)
			UserIDValue = value
		End Set
	End Property

	Private TextFieldsValue As List(Of clsCardTextField)
	Public Property TextFields() As List(Of clsCardTextField)
		Get
			Return TextFieldsValue
		End Get
		Set(ByVal value As List(Of clsCardTextField))
			TextFieldsValue = value
		End Set
	End Property

	Private ImageFieldsValue As List(Of clsCardImageField)
	Public Property ImageFields() As List(Of clsCardImageField)
		Get
			Return ImageFieldsValue
		End Get
		Set(ByVal value As List(Of clsCardImageField))
			ImageFieldsValue = value
		End Set
	End Property

	Private CategoryValue As String
	Public Property Category() As String
		Get
			Return CategoryValue
		End Get
		Set(ByVal value As String)
			CategoryValue = value
		End Set
	End Property

	Private CardActiveValue As String
	Public Property CardActive() As String
		Get
			Return CardActiveValue
		End Get
		Set(ByVal value As String)
			CardActiveValue = value
		End Set
	End Property



	Private ToTalPageValue As String
	Public Property ToTalPage() As String
		Get
			Return ToTalPageValue
		End Get
		Set(ByVal value As String)
			ToTalPageValue = value
		End Set
	End Property



End Class
#End Region

#Region "clsCategories"
Public Class clsCategories

	Private CategoryIDValue As Integer
	Public Property CategoryID() As Integer
		Get
			Return CategoryIDValue
		End Get
		Set(ByVal value As Integer)
			CategoryIDValue = value
		End Set
	End Property

	Private CategoryNameValue As String
	Public Property CategoryName() As String
		Get
			Return CategoryNameValue
		End Get
		Set(ByVal value As String)
			CategoryNameValue = value
		End Set
	End Property

	Private SeqValue As Integer
	Public Property Seq() As Integer
		Get
			Return SeqValue
		End Get
		Set(ByVal value As Integer)
			SeqValue = value
		End Set
	End Property

	Private TypeValue As String
	Public Property Type() As String
		Get
			Return TypeValue
		End Get
		Set(ByVal value As String)
			TypeValue = value
		End Set
	End Property

End Class
#End Region

#Region "clsFont"
Public Class clsFontFamily

	Private FontNameValue As String
	Public Property FontName() As String
		Get
			Return FontNameValue
		End Get
		Set(ByVal value As String)
			FontNameValue = value
		End Set
	End Property

End Class
#End Region

#Region "clsCardTextField"
Public Class clsCardTextField

	Private CardIDValue As String
	Public Property CardID() As String
		Get
			Return CardIDValue
		End Get
		Set(ByVal value As String)
			CardIDValue = value
		End Set
	End Property

	Private SeqValue As Integer
	Public Property Seq() As Integer
		Get
			Return SeqValue
		End Get
		Set(ByVal value As Integer)
			SeqValue = value
		End Set
	End Property

	Private FieldNameValue As String
	Public Property FieldName() As String
		Get
			Return FieldNameValue
		End Get
		Set(ByVal value As String)
			FieldNameValue = value
		End Set
	End Property

	Private TextValueValue As String
	Public Property TextValue() As String
		Get
			Return TextValueValue
		End Get
		Set(ByVal value As String)
			TextValueValue = value
		End Set
	End Property

	Private PositionTopValue As Double
	Public Property PositionTop() As Double
		Get
			Return PositionTopValue
		End Get
		Set(ByVal value As Double)
			PositionTopValue = value
		End Set
	End Property

	Private PositionLeftValue As Double
	Public Property PositionLeft() As Double
		Get
			Return PositionLeftValue
		End Get
		Set(ByVal value As Double)
			PositionLeftValue = value
		End Set
	End Property

	Private zIndexValue As Integer
	Public Property zIndex() As Integer
		Get
			Return zIndexValue
		End Get
		Set(ByVal value As Integer)
			zIndexValue = value
		End Set
	End Property

	Private BackColorValue As String
	Public Property BackColor() As String
		Get
			Return BackColorValue
		End Get
		Set(ByVal value As String)
			BackColorValue = value
		End Set
	End Property

	Private ForeColorValue As String
	Public Property ForeColor() As String
		Get
			Return ForeColorValue
		End Get
		Set(ByVal value As String)
			ForeColorValue = value
		End Set
	End Property

	Private FontFamilyValue As String
	Public Property FontFamily() As String
		Get
			Return FontFamilyValue
		End Get
		Set(ByVal value As String)
			FontFamilyValue = value
		End Set
	End Property

	Private FontSizeValue As String
	Public Property FontSize() As String
		Get
			Return FontSizeValue
		End Get
		Set(ByVal value As String)
			FontSizeValue = value
		End Set
	End Property

	Private CreateByValue As String
	Public Property CreateBy() As String
		Get
			Return CreateByValue
		End Get
		Set(ByVal value As String)
			CreateByValue = value
		End Set
	End Property

	Private CreateOnValue As DateTime
	Public Property CreateOn() As DateTime
		Get
			Return CreateOnValue
		End Get
		Set(ByVal value As DateTime)
			CreateOnValue = value
		End Set
	End Property

	Private UpdateByValue As String
	Public Property UpdateBy() As String
		Get
			Return UpdateByValue
		End Get
		Set(ByVal value As String)
			UpdateByValue = value
		End Set
	End Property

	Private UpdateOnValue As DateTime
	Public Property UpdateOn() As DateTime
		Get
			Return UpdateOnValue
		End Get
		Set(ByVal value As DateTime)
			UpdateOnValue = value
		End Set
	End Property

	Private FontBoldValue As String
	Public Property FontBold() As String
		Get
			Return FontBoldValue
		End Get
		Set(ByVal value As String)
			FontBoldValue = value
		End Set
	End Property

	Private FontItalicValue As String
	Public Property FontItalic() As String
		Get
			Return FontItalicValue
		End Get
		Set(ByVal value As String)
			FontItalicValue = value
		End Set
	End Property

	Private FontUnderlineValue As String
	Public Property FontUnderline() As String
		Get
			Return FontUnderlineValue
		End Get
		Set(ByVal value As String)
			FontUnderlineValue = value
		End Set
	End Property

	Private Right2LeftValue As String
	Public Property Right2Left() As String
		Get
			Return Right2LeftValue
		End Get
		Set(ByVal value As String)
			Right2LeftValue = value
		End Set
	End Property

	Private AlignCenterValue As String
	Public Property AlignCenter() As String
		Get
			Return AlignCenterValue
		End Get
		Set(ByVal value As String)
			AlignCenterValue = value
		End Set
	End Property

End Class
#End Region

#Region "clsCardImageField"
Public Class clsCardImageField

	Private CardIDValue As String
	Public Property CardID() As String
		Get
			Return CardIDValue
		End Get
		Set(ByVal value As String)
			CardIDValue = value
		End Set
	End Property

	Private SeqValue As Integer
	Public Property Seq() As Integer
		Get
			Return SeqValue
		End Get
		Set(ByVal value As Integer)
			SeqValue = value
		End Set
	End Property

	Private imageUrlValue As String
	Public Property imageUrl() As String
		Get
			Return imageUrlValue
		End Get
		Set(ByVal value As String)
			imageUrlValue = value
		End Set
	End Property

	Private PositionTopValue As Double
	Public Property PositionTop() As Double
		Get
			Return PositionTopValue
		End Get
		Set(ByVal value As Double)
			PositionTopValue = value
		End Set
	End Property

	Private PositionLeftValue As Double
	Public Property PositionLeft() As Double
		Get
			Return PositionLeftValue
		End Get
		Set(ByVal value As Double)
			PositionLeftValue = value
		End Set
	End Property

	Private zIndexValue As Integer
	Public Property zIndex() As Integer
		Get
			Return zIndexValue
		End Get
		Set(ByVal value As Integer)
			zIndexValue = value
		End Set
	End Property

	Private widthValue As Integer
	Public Property width() As Integer
		Get
			Return widthValue
		End Get
		Set(ByVal value As Integer)
			widthValue = value
		End Set
	End Property

	Private HeightValue As Integer
	Public Property Height() As Integer
		Get
			Return HeightValue
		End Get
		Set(ByVal value As Integer)
			HeightValue = value
		End Set
	End Property

	Private CreateByValue As String
	Public Property CreateBy() As String
		Get
			Return CreateByValue
		End Get
		Set(ByVal value As String)
			CreateByValue = value
		End Set
	End Property

	Private CreateOnValue As DateTime
	Public Property CreateOn() As DateTime
		Get
			Return CreateOnValue
		End Get
		Set(ByVal value As DateTime)
			CreateOnValue = value
		End Set
	End Property

	Private UpdateByValue As String
	Public Property UpdateBy() As String
		Get
			Return UpdateByValue
		End Get
		Set(ByVal value As String)
			UpdateByValue = value
		End Set
	End Property

	Private UpdateOnValue As DateTime
	Public Property UpdateOn() As DateTime
		Get
			Return UpdateOnValue
		End Get
		Set(ByVal value As DateTime)
			UpdateOnValue = value
		End Set
	End Property

	Private ImageSizeValue As Integer
	Public Property ImageSize() As Integer
		Get
			Return ImageSizeValue
		End Get
		Set(ByVal value As Integer)
			ImageSizeValue = value
		End Set
	End Property

	Private PreviousNameValue As String
	Public Property PreviousName() As String
		Get
			Return PreviousNameValue
		End Get
		Set(ByVal value As String)
			PreviousNameValue = value
		End Set
	End Property

	Private ContentTypeValue As String
	Public Property ContentType() As String
		Get
			Return ContentTypeValue
		End Get
		Set(ByVal value As String)
			ContentTypeValue = value
		End Set
	End Property

	Private Ready2PrintValue As String
	Public Property Ready2Print() As String
		Get
			Return Ready2PrintValue
		End Get
		Set(ByVal value As String)
			Ready2PrintValue = value
		End Set
	End Property
End Class
#End Region

#Region "clsCardEngine"
Public Class clsCardEngine
	Public Function GetCardByID(ByVal CardID As String) As clsCard
		Dim TempCardProvider As New clsCardProvider
		Return TempCardProvider.GetCardByID(CardID)
	End Function

	Public Function GetCardBySearch(ByVal SearchParams As clsCardSearchParam, ByVal PageNo As Integer, ByVal PageSize As Integer, ByVal SortBy As eCardSortBy) As DataSet
		Dim TempCardProvider As New clsCardProvider
		Return TempCardProvider.GetCardBySearch(SearchParams, PageNo, PageSize, SortBy)
	End Function

	Public Function GetCardByCustCreated(ByVal SearchParams As clsCardSearchParam, ByVal PageNo As Integer, ByVal PageSize As Integer, ByVal SortBy As eCardSortBy) As DataSet
		Dim TempCardProvider As New clsCardProvider
		Return TempCardProvider.GetCardByCustCreated(SearchParams, PageNo, PageSize, SortBy)
	End Function

	Public Function GeneratePreview(ByVal oCard As clsCard, ByVal sPath As String, Optional ByVal Zoom As Double = 1) As Bitmap
		Dim TempCardProvider As New clsCardProvider
		Return TempCardProvider.GeneratePreview(oCard, sPath, Zoom)
	End Function

	Public Function GetListImageByUser(ByVal UserID As String, Optional ByVal MaxSize As Integer = 0) As DataTable
		Dim TempCardProvider As New clsCardProvider
		Return TempCardProvider.GetListImageByUser(UserID, MaxSize)
	End Function

	Public Function GetListCardTextFields(ByVal CardID As String) As List(Of clsCardTextField)
		Dim ListCls As New List(Of clsCardTextField)
		Dim clsProvider As New clsCardProvider
		ListCls = clsProvider.GetListCardTextFields(CardID)
		Return ListCls
	End Function

	Public Function SaveCard(ByVal oCard As clsCard, ByVal obitmap As Bitmap, ByVal strRootPath As String) As String
		Dim TempCardProvider As New clsCardProvider
		Return TempCardProvider.SaveCard(oCard, obitmap, strRootPath)
	End Function

	'������� GetoCard ��㹡���Ѿഷ FlagCard
	Public Function GetoCardForAdmin(ByVal CardID As String) As clsCard
		Dim TempCardProvider As New clsCardProvider
		Return TempCardProvider.GetoCardForAdmin(CardID)
	End Function
	'�������
	'CreateBy 06/01/2011
	'Objective For Admin
	Public Function EditTextFields(ByVal ObjTextFields As clsCardTextField) As Boolean
		Dim CardProvider As New clsCardProvider
		Dim Complete As Boolean = CardProvider.EditTextFields(ObjTextFields)
		Return Complete
	End Function
	'�������
	'CreateBy 06/01/2011
	'Objective For Admin
	Public Function EditCardInfo(ByVal ObjCard As clsCard) As Boolean
		Dim CardProvider As New clsCardProvider
		Dim Complete As Boolean = CardProvider.EditCardInfo(ObjCard)
		Return Complete
	End Function
	'�������
	'CreateBy 07/01/2011
	'Objective For Admin
	Public Function EditImageFields(ByVal ObjImageFields As clsCardImageField) As Boolean
		Dim CardProvider As New clsCardProvider
		Dim Complete As Boolean = CardProvider.EditImageFields(ObjImageFields)
		Return Complete
	End Function

	Public Function GetListCategories() As List(Of clsCategories)
		Dim CardProvider As New clsCardProvider
		Return CardProvider.GetListCategories()
	End Function

	Public Function GetListFontFamily() As List(Of clsFontFamily)
		Dim CardProvider As New clsCardProvider
		Return CardProvider.GetListFontFamily()
	End Function

	Public Function AddNewRowTextFileds(ByVal CardID As String) As Boolean
		Dim CardProvider As New clsCardProvider
		Return CardProvider.AddNewRowTextFileds(CardID)
	End Function

	Public Function DeleteRowTextFileds(ByVal CardID As String, ByVal Seq As String) As Boolean
		Dim CardProvider As New clsCardProvider
		Return CardProvider.DeleteRowTextFileds(CardID, Seq)
	End Function


End Class
#End Region

#Region "clsCardProvider"
Public Class clsCardProvider
	Inherits clsBLBase

	Public Function GetCardByID(ByVal CardID As String) As clsCard
		Dim oCard As clsCard = Nothing
		Dim SqlStr As String = "Select * From TBBCCards Where CardID = '" & CardID & "' and CardActive= 'Y' "

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr)
			If ObjDB.Go Then
				oCard = New clsCard
				With oCard
					.CardID = CardID
					.CardName = ObjDB.ReadField("CardName")
					.Description = ObjDB.ReadField("Description")
					.BGColor = ObjDB.ReadField("BGColor")
					.BGImage = ObjDB.ReadField("BGImage")
					.templateImage = ObjDB.ReadField("templateImage")
					.TmpFlag = ObjDB.ReadField("TmpFlag")
					.Width = ObjDB.ReadField("Width")
					.Height = ObjDB.ReadField("Height")
					' CountOrder �����
					.UploadFlag = ObjDB.ReadField("UploadFlag")
					.owner = ObjDB.ReadField("owner")
					.CreateBy = ObjDB.ReadField("CreateBy")
					.CreateOn = ObjDB.ReadField("CreateOn")
					.UpdateBy = ObjDB.ReadField("UpdateBy")
					.UpdateOn = ObjDB.ReadField("UpdateOn")
					.UserID = ObjDB.ReadField("UserID")
					' Text and Image
					.TextFields = Me.GetListCardTextFields(CardID)
					.ImageFields = Me.GetListCardImageFields(CardID)
				End With
			End If
		End Using

		Return oCard
	End Function

	Public Function GetCardBySearch(ByVal SearchParams As clsCardSearchParam, ByVal PageNo As Integer, ByVal PageSize As Integer, ByVal SortBy As eCardSortBy) As DataSet
		Dim SqlStr As New StringBuilder()

		SqlStr.AppendLine("-------��ͧ���ͷ����� row ��������͹ : #ListOfCard ------- ")
		SqlStr.AppendLine("Select * Into #ListOfCard From ")
		SqlStr.AppendLine("( ")
		SqlStr.AppendLine("Select *,'xxx' as OrderID From TBBCCards ")
		SqlStr.AppendLine("Where " & SearchParams.AdvanceSearchSqlWhereStatement & " and CardActive = 'Y'")
		SqlStr.AppendLine(") Table1 ")
		SqlStr.AppendLine("--------��� #ListOfPID �ҷ� Paging------- ")
		SqlStr.AppendLine("Select * From (Select Row_Number() Over (Order By " & Me.GetCardSortBy(SortBy) & ") as 'RunningCount',*  from #ListOfCard ")
		SqlStr.AppendLine(") T ")
		SqlStr.AppendLine("Where RunningCount Between ").Append((PageNo * PageSize) + 1).Append(" and ").AppendLine(PageNo * PageSize + PageSize)
		SqlStr.AppendLine("Select 'CardCount' as 'name',Count(*) as 'value' From #ListOfCard ")
		SqlStr.AppendLine("--------------- ")
		SqlStr.AppendLine("drop table #ListOfCard ")

		Dim TempDS As DataSet
		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			TempDS = objDB.getDataSet(SqlStr.ToString, "tb1")
		End Using

		If (TempDS.Tables.Count = 0) Then
			Return Nothing
		End If

		Return TempDS
	End Function
	Public Function GetCardByCustCreated(ByVal SearchParams As clsCardSearchParam, ByVal PageNo As Integer, ByVal PageSize As Integer, ByVal SortBy As eCardSortBy) As DataSet
		Dim SqlStr As New StringBuilder()

		SqlStr.AppendLine("-------��ͧ���ͷ����� row ��������͹ : #ListOfCard ------- ")
		SqlStr.AppendLine("Select * Into #ListOfCard From ")
		SqlStr.AppendLine("( ")
		SqlStr.AppendLine("select isnull(T1.OrderID,'') as OrderID,T2.* from TBBCCards T2 left  join TBBCorders T1  on T1.CardID=T2.CardID  ")
		SqlStr.AppendLine("Where " & SearchParams.AdvanceSearchSqlWhereStatement & " and T2.CardActive = 'Y'")
		SqlStr.AppendLine(") Table1 ")
		SqlStr.AppendLine("--------��� #ListOfPID �ҷ� Paging------- ")
		SqlStr.AppendLine("Select * From (Select Row_Number() Over (Order By " & Me.GetCardSortBy(SortBy) & ") as 'RunningCount',*  from #ListOfCard ")
		SqlStr.AppendLine(") T ")
		SqlStr.AppendLine("Where RunningCount Between ").Append((PageNo * PageSize) + 1).Append(" and ").AppendLine(PageNo * PageSize + PageSize)
		SqlStr.AppendLine("Select 'CardCount' as 'name',Count(*) as 'value' From #ListOfCard ")
		SqlStr.AppendLine("--------------- ")
		SqlStr.AppendLine("drop table #ListOfCard ")

		Dim TempDS As DataSet
		Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)

			Try
				TempDS = objDB.getDataSet(SqlStr.ToString, "tb1")

			Catch ex As Exception
				
				Throw ex
			End Try

		End Using

		If (TempDS.Tables.Count = 0) Then
			Return Nothing
		End If

		Return TempDS
	End Function

	Public Function GeneratePreview(ByVal oCard As clsCard, ByVal sPath As String, Optional ByVal Zoom As Double = 1) As Bitmap
		' Cretae New Bitmap


		Dim oDpi As Int16 = 72
		Dim oBitmap As Bitmap

		oBitmap = New Bitmap(clsMyUtil.mm2Px(oCard.Width * Zoom, oDpi), clsMyUtil.mm2Px(oCard.Height * Zoom, oDpi))
		Dim oGraphic As Graphics = Graphics.FromImage(oBitmap)
		Dim oColor As Color = Color.White

		Dim oBrush As New SolidBrush(oColor)
		oGraphic.FillRectangle(oBrush, 0, 0, clsMyUtil.mm2Px(oCard.Height * Zoom, oDpi), clsMyUtil.mm2Px(oCard.Height * Zoom, oDpi))
		oGraphic.TextRenderingHint = TextRenderingHint.AntiAliasGridFit
		oBitmap.SetResolution(oDpi, oDpi)

		' Draw Image
		oGraphic = Me.SetImageFields(oGraphic, oCard, sPath, oDpi, Zoom)

		' Draw Text
		oGraphic.TextRenderingHint = TextRenderingHint.AntiAlias '���� ���ź�����ѡ��� text :: AA
		oGraphic = Me.SetTextFields(oGraphic, oCard, oDpi, Zoom)

		' Draw WaterMark
		'oGraphic = Me.SetWaterMark(oGraphic, oCard, sPath, oDpi, Zoom)

		'' ������ dispose �� save �Ҿ ����� (bitmap.save)
		oGraphic.Dispose()

		Return oBitmap
	End Function

	Public Function SaveCard(ByVal oCard As clsCard, ByVal oBitmap As Bitmap, ByVal strRootPath As String) As String
		Dim CardID As String = Nothing
		Dim SqlStr As New StringBuilder
		Dim ListTextName As New List(Of String)
		Dim ListTextValue As New List(Of String)
		Dim ThisUser As String = Me.objSessionVar.ModelUser.UserID



		Dim i As Int16 = 0

		With SqlStr
			.AppendLine("-- generate CardID ")
			.AppendLine("declare @CardID as varchar (20) ")
			.AppendLine("Exec spc_GetMaxIDInc 'TBBCCards','CardID','BC','BC','','', @CardID output ")
			.AppendLine(" ")


			If oCard.UploadFlag = "N" Then

				For Each aText As clsCardTextField In oCard.TextFields
					i += 1
					ListTextName.Add(aText.FieldName)
					ListTextValue.Add(aText.TextValue)

					.AppendLine("Insert Into TBBCTextFields ")
					.AppendLine("(CardID,Seq,FieldName,TextValue,PositionTop,PositionLeft,zIndex,BackColor,ForeColor,FontFamily, ")
					.AppendLine("FontSize,CreateBy,CreateOn,UpdateBy,UpdateOn,FontBold,FontItalic,FontUnderline,Right2Left,AlignCenter) ")
					.AppendLine("Values ")
					.AppendLine("(@CardID," & i & ",@FieldName" & i & ",@TextValue" & i & "," & aText.PositionTop & "," & aText.PositionLeft & "," & aText.zIndex & ",'" & aText.BackColor & "','" & aText.ForeColor & "','" & aText.FontFamily & "', ")
					.AppendLine("'" & aText.FontSize & "','" & ThisUser & "',@DTNow,'" & ThisUser & "',@DTNow,'" & aText.FontBold & "','" & aText.FontItalic & "','" & aText.FontUnderline & "','" & aText.Right2Left & "','" & aText.AlignCenter & "') ")
					.AppendLine(" ")
				Next

			End If

			i = 0


			If oCard.ImageFields IsNot Nothing Then

				For Each aImage As clsCardImageField In oCard.ImageFields
					i += 1

					.AppendLine("Insert Into TBBCImageFields ")
					.AppendLine("(CardID,Seq,imageUrl,PositionTop,PositionLeft,zIndex,width,Height,CreateBy,CreateOn, ")
					.AppendLine("UpdateBy,UpdateOn,ImageSize,PreviousName,ContentType,ReadyToPrint) ")
					.AppendLine("Values ")
					.AppendLine("(@CardID," & i & ",'" & aImage.imageUrl & "'," & aImage.PositionTop & "," & aImage.PositionLeft & "," & aImage.zIndex & "," & aImage.width & "," & aImage.Height & ",'" & ThisUser & "',@DTNow, ")
					.AppendLine("'" & ThisUser & "',@DTNow," & aImage.ImageSize & ",'" & aImage.PreviousName & "','" & aImage.ContentType & "','" & aImage.Ready2Print & "') ")
					.AppendLine(" ")
				Next

			End If

			.AppendLine("-- Insert Card Detail ")
			.AppendLine("Insert Into TBBCCards  ")
			.AppendLine("(CardID,CardName,Description,BGColor,BGImage,templateImage,TmpFlag,Width,Height, ")
			.AppendLine("CountOrder,UploadFlag,owner,CreateBy,CreateOn,UpdateBy,UpdateOn,UserID,Category,CardActive) ")
			.AppendLine("Values ")
			.AppendLine("(@CardID,@CardName,@Description,@BGColor,@BGImage,@CardID+'.jpg',@TmpFlag,@Width,@Height, ")
			.AppendLine("@CountOrder,@UploadFlag,@owner,@CreateBy,@DTNow,@UpdateBy,@DTNow,@UserID,@Category,@CardActive) ")
			.AppendLine("select @CardID as CardID ")

		End With
		'oBitmap.Save("")
        '' save Image to thumbnail 


        Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            ObjDB.ParametersClear()
            ObjDB.PatametersAdd("@DTNow", MyDateTime.ToEnUsYMDHM(Now))
            If (oCard.CardName Is Nothing) Then
                oCard.CardName = ""
            End If
            If (oCard.Description Is Nothing) Then
                oCard.Description = ""
            End If
            If (oCard.BGColor Is Nothing) Then
                oCard.BGColor = ""
            End If
            If (oCard.BGImage Is Nothing) Then
                oCard.BGImage = ""
            End If
            If (oCard.owner Is Nothing) Then
                oCard.owner = "ofm"
            End If
            Dim flag As String = oCard.UploadFlag.ToString
            If flag = "" Then
                flag = "N"
            End If

            ObjDB.PatametersAdd("@CardName", oCard.CardName)
            ObjDB.PatametersAdd("@Description", oCard.Description)
            ObjDB.PatametersAdd("@BGColor", oCard.BGColor)
            ObjDB.PatametersAdd("@BGImage", oCard.BGImage)
            'ObjDB.PatametersAdd("@templateImage", "@CardID" & ".jpg")
            'ObjDB.PatametersAdd("@TmpFlag", flag)
            ObjDB.PatametersAdd("@TmpFlag", "N")
            ObjDB.PatametersAdd("@Width", oCard.Width)
            ObjDB.PatametersAdd("@Height", oCard.Height)
            ObjDB.PatametersAdd("@CountOrder", "")
            'ObjDB.PatametersAdd("@UploadFlag", oCard.UploadFlag)
            ObjDB.PatametersAdd("@UploadFlag", flag)
            ObjDB.PatametersAdd("@owner", oCard.owner)
            ObjDB.PatametersAdd("@CreateBy", ThisUser)
            'ObjDB.PatametersAdd("@CreateOn", MyDateTime.ToEnUsDMYHM(oCard.CreateOn))
            'ObjDB.PatametersAdd("@CreateOn", oCard.CreateOn)
            ObjDB.PatametersAdd("@UpdateBy", ThisUser)
            'ObjDB.PatametersAdd("@UpdateOn", oCard.UpdateOn)
            'ObjDB.PatametersAdd("@UpdateOn", MyDateTime.ToEnUsDMYHM(oCard.UpdateOn))
            ObjDB.PatametersAdd("@UserID", Me.objSessionVar.ModelUser.UserID)
            'ObjDB.PatametersAdd("@Category", oCard.Category)
            ObjDB.PatametersAdd("@Category", "")
            ObjDB.PatametersAdd("@CardActive", "Y")

            '' ** =========== Add Parameter =================
            i = 0 ' Text Name
            For Each aParam As String In ListTextName
                i += 1
                ObjDB.PatametersAdd("@FieldName" & i, aParam)
            Next
            i = 0 ' Text Value
            For Each aParam As String In ListTextValue
                i += 1
                ObjDB.PatametersAdd("@TextValue" & i, aParam)
            Next

            Try
                ObjDB.SelectClose()
                ObjDB.OpenConnWithBeginTran()

                ObjDB.SelectReQuery(SqlStr.ToString)
                If ObjDB.Go Then
                    CardID = ObjDB.ReadField("CardID")

                    If oBitmap IsNot Nothing Then
                        Dim oBitmapZ As System.Drawing.Bitmap = System.Web.HttpContext.Current.Session("oBitmapZ")
                        Dim strSavePath As String = strRootPath & "\CardImages\FullCard\" & CardID & ".jpg"
                        Dim strSavePathFull As String = strRootPath & "\CardImages\FullSizes\" & CardID & ".jpg"
                        oBitmap.Save(strSavePath) ' save �ٻ �����template
                        oBitmapZ.Save(strSavePathFull) ' save �ٻ ����ʴ�
                    End If
                    ' Last
                End If
                ObjDB.CloseConnWithCommitTran()
            Catch ex As Exception
                If ObjDB.OnTransaction Then
                    ObjDB.CloseConWithRollbackTran()
                End If
            Finally
                ObjDB.SelectClose()
            End Try
        End Using

        Return CardID
    End Function

	Public Function GetListCardTextFields(ByVal CardID As String) As List(Of clsCardTextField)
		Dim gTextFields As New List(Of clsCardTextField)
		Dim SqlStr As String = "Select * From TBBCTextFields Where CardID = '" & CardID & "' Order By zIndex"

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr)
			While (ObjDB.Go)
				Dim oTextField As New clsCardTextField
				With oTextField
					.CardID = CardID
					.Seq = ObjDB.ReadField("Seq")
					.FieldName = ObjDB.ReadField("FieldName")
					.TextValue = ObjDB.ReadField("TextValue")
					.PositionTop = ObjDB.ReadField("PositionTop")
					.PositionLeft = ObjDB.ReadField("PositionLeft")
					.zIndex = ObjDB.ReadField("zIndex")
					.BackColor = ObjDB.ReadField("BackColor")
					.ForeColor = ObjDB.ReadField("ForeColor")
					.FontFamily = ObjDB.ReadField("FontFamily")
					.FontSize = ObjDB.ReadField("FontSize")
					.CreateBy = ObjDB.ReadField("CreateBy")
					.CreateOn = ObjDB.ReadField("CreateOn")
					.UpdateBy = ObjDB.ReadField("UpdateBy")
					.UpdateOn = ObjDB.ReadField("UpdateOn")
					.FontBold = ObjDB.ReadField("FontBold")
					.FontItalic = ObjDB.ReadField("FontItalic")
					.FontUnderline = ObjDB.ReadField("FontUnderline")
					.Right2Left = ObjDB.ReadField("Right2Left")
					.AlignCenter = ObjDB.ReadField("AlignCenter")
				End With

				gTextFields.Add(oTextField)
			End While
		End Using

		Return gTextFields
	End Function

	Public Function GetListCardImageFields(ByVal CardID As String) As List(Of clsCardImageField)
		Dim gImageFields As New List(Of clsCardImageField)
		Dim SqlStr As String = "Select * From TBBCImageFields Where CardID = '" & CardID & "' Order By zIndex"

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr)
			While (ObjDB.Go)
				Dim oImageField As New clsCardImageField
				With oImageField
					.CardID = CardID
					.Seq = ObjDB.ReadField("Seq")
					.imageUrl = ObjDB.ReadField("imageUrl")
					.PositionTop = ObjDB.ReadField("PositionTop")
					.PositionLeft = ObjDB.ReadField("PositionLeft")
					.zIndex = ObjDB.ReadField("zIndex")
					.width = ObjDB.ReadField("width")
					.Height = ObjDB.ReadField("Height")
					.CreateBy = ObjDB.ReadField("CreateBy")
					.CreateOn = ObjDB.ReadField("CreateOn")
					.UpdateBy = ObjDB.ReadField("UpdateBy")
					.UpdateOn = ObjDB.ReadField("UpdateOn")
					.ImageSize = ObjDB.ReadField("ImageSize")
					.PreviousName = ObjDB.ReadField("PreviousName")
					.ContentType = ObjDB.ReadField("ContentType")
					.Ready2Print = ObjDB.ReadField("ReadyToPrint")
				End With

				gImageFields.Add(oImageField)
			End While
		End Using

		Return gImageFields
	End Function
	Public Function GetListImageByUser(ByVal UserID As String, Optional ByVal MaxSize As Integer = 0) As DataTable
		Dim TempDT As DataTable = Nothing
		Dim SqlStr As String = ""
		Dim TopOption As String = ""
		If MaxSize <> 0 Then
			TopOption = " Top " & MaxSize & " "
		End If

		SqlStr = "Select " & TopOption & "i.* From TBBCImagefields i Inner Join TBBCCards c "
		SqlStr &= "On i.CardID= c.CardID where c.UploadFlag='Y' And i.CreateBy='" & UserID & "' and i.Imagesize <> '0' and c.CardActive= 'Y'"

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			Dim TempDS As DataSet = ObjDB.getDataSet(SqlStr, "Images")
			If TempDS.Tables.Count > 0 Then
				TempDT = TempDS.Tables(0)
			End If
		End Using
		Return TempDT
	End Function

	Public Function CopyTemplate(ByVal CardID As String) As String
		Dim SqlStr As New StringBuilder
		With SqlStr
			.AppendLine("declare @CardID as varchar (20) ")
			.AppendLine("Exec spc_GetMaxIDInc 'TBBCCards','CardID','BC','BC','','', @CardID output ")
			.AppendLine("Select @CardID as CardID")
			.AppendLine("----------������Card----------")
			.AppendLine("Insert Into TBBCCards (CardID,CardName,Description,BGColor,BGImage,templateImage,TmpFlag,Width,Height,")
			.AppendLine("            			CountOrder,UploadFlag,owner,CreateBy,CreateOn,UpdateBy,UpdateOn,UserID,Category,CardActive)")
			.AppendLine("select @CardID,CardName,Description,BGColor,BGImage,templateImage,TmpFlag,Width,Height,")
			.AppendLine("CountOrder,UploadFlag,owner,CreateBy,CreateOn,UpdateBy,UpdateOn,UserID,Category,'N' from TBBCCards Where CardID = '" & CardID & "'")
			.AppendLine("----------������Logo----------")
			.AppendLine("Insert Into TBBCImageFields (CardID,Seq,imageUrl,PositionTop,PositionLeft,zIndex,width,Height,CreateBy,CreateOn,")
			.AppendLine("            				UpdateBy,UpdateOn,ImageSize,PreviousName,ContentType,ReadyToPrint)")
			.AppendLine("select @CardID,Seq,imageUrl,PositionTop,PositionLeft,zIndex,width,Height,CreateBy,CreateOn,")
			.AppendLine("       UpdateBy,UpdateOn,ImageSize,PreviousName,ContentType,ReadyToPrint  from TBBCImageFields Where CardID = '" & CardID & "'")
			.AppendLine("----------�����Ţ�ͤ�����Card----------")
			.AppendLine("Insert Into TBBCTextFields (CardID,Seq,FieldName,TextValue,PositionTop,PositionLeft,zIndex,BackColor,ForeColor,FontFamily,")
			.AppendLine("            				FontSize,CreateBy,CreateOn,UpdateBy,UpdateOn,FontBold,FontItalic,FontUnderline,Right2Left,AlignCenter)")
			.AppendLine("select @CardID,Seq,FieldName,TextValue,PositionTop,PositionLeft,zIndex,BackColor,ForeColor,FontFamily,")
			.AppendLine("FontSize,CreateBy,CreateOn,UpdateBy,UpdateOn,FontBold,FontItalic,FontUnderline,Right2Left,AlignCenter  from TBBCTextFields Where CardID = '" & CardID & "'")
		End With
		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
			End With
			Try
				ObjDB.SelectReQuery(SqlStr.ToString)
				If ObjDB.Go Then
					CardID = ObjDB.ReadField("CardID")
				End If
			Catch ex As Exception
				CardID = ""
			End Try
		End Using
		Return CardID
	End Function
	'�������
	'CreateBy 15/11/2010
	'Objective For Admin
	Public Function SaveTemplate(ByVal Card As clsCard, ByVal Logo As clsCardImageField, ByVal TextFieldCardID As String) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean = False

		With SqlStr
			.AppendLine("declare @CardID as varchar (20) ")
			.AppendLine("Exec spc_GetMaxIDInc 'TBBCCards','CardID','BC','BC','','', @CardID output ")
			.AppendLine("Select @CardID as CardID")
			.AppendLine("----------������Card----------")
			.AppendLine("Insert Into TBBCCards (CardID,CardName,Description,BGColor,BGImage,templateImage,TmpFlag,Width,Height,")
			.AppendLine("						CountOrder,UploadFlag,owner,CreateBy,CreateOn,UpdateBy,UpdateOn,UserID,Category,CardActive)")
			.AppendLine("Values (@CardID,@CardName,'Officemate',@BGColor,@BGImage,@templateImage,'Y',@Width,@Height,")
			.AppendLine("		@CountOrder,'N','ofm',@CreateBy,getdate(),@UpdateBy,getdate(),'ofm',@Category,'N')")
			.AppendLine("----------������Logo----------")
			.AppendLine("Insert Into TBBCImageFields (CardID,Seq,imageUrl,PositionTop,PositionLeft,zIndex,width,Height,CreateBy,CreateOn,")
			.AppendLine("							UpdateBy,UpdateOn,ImageSize,PreviousName,ContentType,ReadyToPrint)")
			.AppendLine("Values (@CardID,1,@IimageUrl,@IPositionTop,@IPositionLeft,@IzIndex,@Iwidth,@IHeight,@CreateBy,getdate(),")
			.AppendLine("		@UpdateBy,getdate(),@IImageSize,@IPreviousName,'image/pjpeg','N')")
			.AppendLine("----------�����Ţ�ͤ�����Card----------")
			.AppendLine("Insert Into TBBCTextFields (CardID,Seq,FieldName,TextValue,PositionTop,PositionLeft,zIndex,BackColor,ForeColor,FontFamily,")
			.AppendLine("							FontSize,CreateBy,CreateOn,UpdateBy,UpdateOn,FontBold,FontItalic,FontUnderline,Right2Left,AlignCenter)")
			.AppendLine("select @CardID as CardID,Seq,FieldName,TextValue,PositionTop,PositionLeft,zIndex,BackColor,ForeColor,")
			.AppendLine("		FontFamily,FontSize,CreateBy,CreateOn,UpdateBy,UpdateOn,FontBold,FontItalic,FontUnderline,Right2Left,AlignCenter ")
			.AppendLine("		From TBBCTextFieldTemp ")
			.AppendLine("		where CardID = @TextFieldCardID")
			.AppendLine("DELETE FROM TBBCTextFieldTemp")
			.AppendLine("WHERE  CardID = @TextFieldCardID")

		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@CreateBy", Me.objSessionVar.ModelUser.UserName)
				.PatametersAdd("@UpdateBy", Me.objSessionVar.ModelUser.UserName)
				'TBBCCards
				.PatametersAdd("@CardName", Card.CardName)
				.PatametersAdd("@BGColor", Card.BGColor)
				.PatametersAdd("@BGImage", Card.BGImage)
				.PatametersAdd("@templateImage", Card.templateImage)
				.PatametersAdd("@Width", Card.Width)
				.PatametersAdd("@Height", Card.Height)
				.PatametersAdd("@CountOrder", "")
				.PatametersAdd("@Category", Card.Category)
				'TBBCImageFields
				.PatametersAdd("@IimageUrl", Logo.imageUrl)
				.PatametersAdd("@IPositionTop", Logo.PositionTop)
				.PatametersAdd("@IPositionLeft", Logo.PositionLeft)
				.PatametersAdd("@IzIndex", Logo.zIndex)
				.PatametersAdd("@Iwidth", Logo.width)
				.PatametersAdd("@IHeight", Logo.Height)
				.PatametersAdd("@IImageSize", Logo.ImageSize)
				.PatametersAdd("@IPreviousName", Logo.PreviousName)
				.PatametersAdd("@TextFieldCardID", TextFieldCardID)
			End With
			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using
		Return Complete
	End Function
	'�������
	'CreateBy 15/13/2010
	'Objective For Admin
	Public Function GetActiveCard(ByVal PageNo As String, Optional ByVal SearchKey As String = "", Optional ByVal Cat As String = "") As List(Of clsCard)
		Dim listCard As New List(Of clsCard)
		Dim Card As New clsCard
		Dim SqlStr As New StringBuilder
		Dim ds As New DataSet
		Dim WhereSearch As String = ""
		Dim WhereSearchCat As String = ""

		If Not String.IsNullOrEmpty(SearchKey) Then
			WhereSearch = " and  CardID like '%" & SearchKey.ToUpper & "%'"
		End If

		If Not String.IsNullOrEmpty(Cat) And Cat <> "0" Then
			WhereSearchCat = " and Category in (Select CategoryName From TBBCCategory Where CategoryID = '" & Cat & "' )"
		End If

		With SqlStr
			.AppendLine("DECLARE @PageNum AS INT;")
			.AppendLine("DECLARE @PageSize AS INT;")
			.AppendLine("DECLARE @PageTotal AS INT;")
			.AppendLine("DECLARE @RowCount AS INT;")
			.AppendLine("SET @PageNum = @PageNo;")
			.AppendLine("SET @PageSize = 10;")
			.AppendLine("Select @RowCount = Count(CardID)")
			.AppendLine("From   TBBCCards Where TmpFlag = 'Y' And CardActive = 'Y'")
			.AppendLine(WhereSearch)
			.AppendLine(WhereSearchCat)
			.AppendLine("Select @PageTotal =  Floor(@RowCount / @PageSize );")
			.AppendLine("WITH CardRN AS")
			.AppendLine("(")
			.AppendLine("SELECT ROW_NUMBER() OVER(ORDER BY CreateON Desc) AS RowNum,*,@PageTotal as PageToTal")
			.AppendLine("From TBBCCards Where TmpFlag = 'Y' And CardActive = 'Y'")
			.AppendLine(WhereSearch)
			.AppendLine(WhereSearchCat)
			.AppendLine(")")
			.AppendLine("")
			.AppendLine("SELECT *")
			.AppendLine("FROM   CardRN")
			.AppendLine("WHERE  RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 ")
			.AppendLine("       AND @PageNum * @PageSize")
			.AppendLine("ORDER  BY RowNum")
		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.PatametersAdd("@PageNo", PageNo)
			ds = ObjDB.getDataSet(SqlStr.ToString(), "TBBCCards")
		End Using

		If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			For Each aRow As DataRow In ds.Tables(0).Rows
				Card = New clsCard
				With Card
					.CardID = aRow("CardID")
					.CardName = aRow("CardName")
					.BGImage = aRow("BGImage")
					.templateImage = aRow("templateImage")
					.CardActive = aRow("CardActive")
					.CreateBy = aRow("CreateBy")
					.ToTalPage = aRow("PageToTal")
					listCard.Add(Card)
				End With
			Next
		End If
		Return listCard
	End Function
	'�������
	'CreateBy 15/11/2010
	'Objective For Admin
	Public Function GetUnActiveCard(ByVal PageNo As String) As List(Of clsCard)
		Dim listCard As New List(Of clsCard)
		Dim Card As New clsCard
		Dim SqlStr As New StringBuilder
		Dim ds As New DataSet

		With SqlStr
			.AppendLine("DECLARE @PageNum AS INT;")
			.AppendLine("DECLARE @PageSize AS INT;")
			.AppendLine("DECLARE @PageTotal AS INT;")
			.AppendLine("DECLARE @RowCount AS INT;")
			.AppendLine("SET @PageNum = @PageNo;")
			.AppendLine("SET @PageSize = 10;")
			.AppendLine("Select @RowCount = Count(CardID)")
			.AppendLine("		From TBBCCards Where TmpFlag = 'Y' And CardActive = 'N';")
			.AppendLine("Select @PageTotal =  Floor(@RowCount / @PageSize );")
			.AppendLine("WITH   CardRN AS")
			.AppendLine("(")
			.AppendLine("SELECT ROW_NUMBER() OVER(ORDER BY CardID) AS RowNum,*,@PageTotal as PageToTal")
			.AppendLine("From TBBCCards Where TmpFlag = 'Y' And CardActive = 'N'")
			.AppendLine(")")
			.AppendLine("SELECT *")
			.AppendLine("FROM   CardRN")
			.AppendLine("WHERE  RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 ")
			.AppendLine("       AND @PageNum * @PageSize")
			.AppendLine("ORDER BY CardID")
		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.PatametersAdd("@PageNo", PageNo)
			ds = ObjDB.getDataSet(SqlStr.ToString(), "TBBCCards")
		End Using

		If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			For Each aRow As DataRow In ds.Tables(0).Rows
				Card = New clsCard
				With Card
					.CardID = aRow("CardID")
					.CardName = aRow("CardName")
					.BGImage = aRow("BGImage")
					.templateImage = aRow("templateImage")
					.CardActive = aRow("CardActive")
					.CreateBy = aRow("CreateBy")
					.ToTalPage = aRow("PageToTal")
					listCard.Add(Card)
				End With
			Next
		End If
		Return listCard
	End Function
	'�������
	'CreateBy 15/11/2010
	'Objective For Admin
	Public Function GetoCardForAdmin(ByVal CardID As String) As clsCard
		Dim oCard As clsCard = Nothing
		Dim SqlStr As String = "Select * From TBBCCards Where CardID = '" & CardID & "'"

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr)
			If ObjDB.Go Then
				oCard = New clsCard
				With oCard
					.CardID = CardID
					.CardName = ObjDB.ReadField("CardName")
					.Description = ObjDB.ReadField("Description")
					.BGColor = ObjDB.ReadField("BGColor")
					.BGImage = ObjDB.ReadField("BGImage")
					.templateImage = ObjDB.ReadField("templateImage")
					.TmpFlag = ObjDB.ReadField("TmpFlag")
					.Width = ObjDB.ReadField("Width")
					.Height = ObjDB.ReadField("Height")
					' CountOrder �����
					.UploadFlag = ObjDB.ReadField("UploadFlag")
					.owner = ObjDB.ReadField("owner")
					.CreateBy = ObjDB.ReadField("CreateBy")
					.CreateOn = ObjDB.ReadField("CreateOn")
					.UpdateBy = ObjDB.ReadField("UpdateBy")
					.UpdateOn = ObjDB.ReadField("UpdateOn")
					.UserID = ObjDB.ReadField("UserID")
					.Category = ObjDB.ReadField("Category")
					.CardActive = ObjDB.ReadField("CardActive")
					' Text and Image
					.TextFields = Me.GetListCardTextFields(CardID)
					.ImageFields = Me.GetListCardImageFields(CardID)
				End With
			End If
		End Using

		Return oCard
	End Function
	'�������
	'CreateBy 06/01/2011
	'Objective For Admin
	Public Function EditTextFields(ByVal ObjTextFields As clsCardTextField) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean = False

		With SqlStr
			.AppendLine("Update	TBBCTextFields")
			.AppendLine("Set	FieldName=@FieldName,TextValue=@TextValue,PositionTop=@PositionTop,")
			.AppendLine("		PositionLeft=@PositionLeft,zIndex=@zIndex,BackColor=@BackColor,")
			.AppendLine("		ForeColor=@ForeColor,FontFamily=@FontFamily,")
			.AppendLine("		FontSize=@FontSize,UpdateBy=@UpdateBy,UpdateOn=getdate(),")
			.AppendLine("		FontBold=@FontBold,FontItalic=@FontItalic,FontUnderline=@FontUnderline,")
			.AppendLine("		Right2Left=@Right2Left,AlignCenter=@AlignCenter")
			.AppendLine("Where	CardID=@CardID and Seq=@Seq")
		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@CardID", ObjTextFields.CardID)
				.PatametersAdd("@Seq", ObjTextFields.Seq)
				'�����ŷ����
				.PatametersAdd("@FieldName", ObjTextFields.FieldName)
				.PatametersAdd("@TextValue", ObjTextFields.TextValue)
				.PatametersAdd("@PositionTop", ObjTextFields.PositionTop)
				.PatametersAdd("@PositionLeft", ObjTextFields.PositionLeft)
				.PatametersAdd("@zIndex", ObjTextFields.zIndex)
				.PatametersAdd("@BackColor", ObjTextFields.BackColor)
				.PatametersAdd("@ForeColor", ObjTextFields.ForeColor)
				.PatametersAdd("@FontFamily", ObjTextFields.FontFamily)
				.PatametersAdd("@FontSize", ObjTextFields.FontSize)
				.PatametersAdd("@UpdateBy", Me.objSessionVar.ModelUser.UserName)
				.PatametersAdd("@FontBold", ObjTextFields.FontBold)
				.PatametersAdd("@FontItalic", ObjTextFields.FontItalic)
				.PatametersAdd("@FontUnderline", ObjTextFields.FontUnderline)
				.PatametersAdd("@Right2Left", ObjTextFields.Right2Left)
				.PatametersAdd("@AlignCenter", ObjTextFields.AlignCenter)
			End With
			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using

		Return Complete
	End Function
	'�������
	'CreateBy 06/01/2011
	'Objective For Admin
	Public Function EditCardInfo(ByVal ObjCard As clsCard) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean = False

		With SqlStr
			.AppendLine("Update	TBBCCards")
			.AppendLine("Set	CardName=@CardName,BGImage=@BGImage,BGColor=@BGColor,")
			.AppendLine("       TemplateImage=@TemplateImage,Width=@Width,Height=@Height,")
			.AppendLine("		Category=@Category,UpdateBy=@UpdateBy,UpdateOn=getdate()")
			.AppendLine("Where	CardID=@CardID")

		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@CardID", ObjCard.CardID)
				'�����ŷ����
				.PatametersAdd("@CardName", ObjCard.CardName)
				.PatametersAdd("@BGColor", ObjCard.BGColor)
				.PatametersAdd("@BGImage", ObjCard.BGImage)
				.PatametersAdd("@TemplateImage", ObjCard.templateImage)
				.PatametersAdd("@Width", ObjCard.Width)
				.PatametersAdd("@Height", ObjCard.Height)
				.PatametersAdd("@Category", ObjCard.Category)
				.PatametersAdd("@UpdateBy", Me.objSessionVar.ModelUser.UserName)
			End With
			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using

		Return Complete
	End Function
	'�������
	'CreateBy 07/01/2011
	'Objective For Admin
	Public Function EditImageFields(ByVal ObjImageFields As clsCardImageField) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean = False

		With SqlStr
			.AppendLine("Update	TBBCImageFields")
			.AppendLine("Set	imageUrl=@imageUrl,PositionTop=@PositionTop,")
			.AppendLine("		PositionLeft=@PositionLeft,zIndex=@zIndex,")
			.AppendLine("		Width=@Width,Height=@Height,")
			.AppendLine("		ImageSize=@ImageSize,PreviousName=@PreviousName,")
			.AppendLine("		UpdateBy=@UpdateBy,UpdateOn=getdate()")
			.AppendLine("Where	CardID=@CardID")
		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@CardID", ObjImageFields.CardID)
				'�����ŷ����
				.PatametersAdd("@imageUrl", ObjImageFields.imageUrl)
				.PatametersAdd("@PositionTop", ObjImageFields.PositionTop)
				.PatametersAdd("@PositionLeft", ObjImageFields.PositionLeft)
				.PatametersAdd("@zIndex", ObjImageFields.zIndex)
				.PatametersAdd("@Width", ObjImageFields.width)
				.PatametersAdd("@Height", ObjImageFields.Height)
				.PatametersAdd("@ImageSize", ObjImageFields.ImageSize)
				.PatametersAdd("@PreviousName", ObjImageFields.PreviousName)
				.PatametersAdd("@UpdateBy", Me.objSessionVar.ModelUser.UserName)
			End With
			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using

		Return Complete
	End Function

	''' <summary>
	''' Surapon Kunsri
	''' CreateBy 11/11/2010
	''' Objective For Admin
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	''' 

	Public Function ActiveCard(ByVal CardID As String, ByVal Active As String) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean

		With SqlStr
			.AppendLine("Update TBBCCards Set CardActive =@Active Where CardID =@CardID")
		End With
		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@CardID", CardID)
				.PatametersAdd("@Active", Active)
				Try
					Complete = ObjDB.SelectReQuery(SqlStr.ToString)
					Complete = True
				Catch ex As Exception
					Complete = False
				End Try
			End With
		End Using
		Return Complete
	End Function

	Public Function DeleteCardTemp(ByVal CardID As String, ByVal Seq As String) As Boolean
		Dim SqlStr As New StringBuilder
		Dim Complete As Boolean

		With SqlStr
			.AppendLine("Delete TBBCTextFieldTemp Where CardID =@CardID and Seq =@Seq")
		End With
		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@Seq", Seq)
				.PatametersAdd("@CardID", CardID)
			End With
			Try
				Complete = ObjDB.SelectReQuery(SqlStr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using

		Return Complete
	End Function

	Public Function AddNewRowTextFileds(ByVal CardID As String) As Boolean

		Dim Complete As Boolean
		Dim Sqlstr As New StringBuilder

		With Sqlstr
			.AppendLine("Declare @MaxID varchar(10)")
			.AppendLine("Select @MaxID = max(Seq+1) FROM TBBCTextFields Where  CardID = @CardID")
			.AppendLine("Insert into TBBCTextFields")
			.AppendLine("Select  Top 1  CardID,@MaxID,'' as FieldName ,'' as TextValue, 0 as PositionTop , 0 as Positionleft,")
			.AppendLine("@MaxID As zIndex,BackColor,ForeColor,FontFamily,Fontsize,")
			.AppendLine("@Createby,getdate(),@UpdateBy,getdate(),FontBold,FontItalic,FontUnderLine,Right2Left,AlignCenter FROM TBBCTextFields")
			.AppendLine("Where   CardID = @CardID")

		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@CardID", CardID)
				.PatametersAdd("@CreateBy", Me.objSessionVar.ModelUser.UserName)
				.PatametersAdd("@UpdateBy", Me.objSessionVar.ModelUser.UserName)
			End With
			Try
				Complete = ObjDB.SelectReQuery(Sqlstr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using
		Return Complete
	End Function
	Public Function DeleteRowTextFileds(ByVal CardID As String, ByVal Seq As String) As Boolean

		Dim Complete As Boolean
		Dim Sqlstr As New StringBuilder

		With Sqlstr
			.AppendLine("Delete TBBCTextFields Where  CardID = @CardID and Seq = @Seq")
			.AppendLine("")
			.AppendLine("DECLARE @SEQ_CODE varchar(10) ")
			.AppendLine("DECLARE @CARDID_CODE varchar(10)")
			.AppendLine("DECLARE @ROW_INDEX varchar(10)")
			.AppendLine("DECLARE tempID CURSOR FOR")
			.AppendLine("")
			.AppendLine("select  CardID,Seq, ROW_NUMBER() OVER(ORDER BY Seq Asc)as RowIndex from TBBCTextFields Where  CardID = @CardID")
			.AppendLine("OPEN tempID")
			.AppendLine("")
			.AppendLine("FETCH NEXT FROM tempID INTO @CARDID_CODE ,@SEQ_CODE,@ROW_INDEX -- �Ӥ�� CardID,Seq� table ������ �����")
			.AppendLine("WHILE (@@FETCH_STATUS = 0) -- ǹ�ٻ")
			.AppendLine("BEGIN")
			.AppendLine("")
			.AppendLine("UPDATE TBBCTextFields SET Seq =@ROW_INDEX , zIndex = @ROW_INDEX  Where CardID = @CardID and  Seq = @SEQ_CODE")
			.AppendLine("FETCH NEXT FROM tempID INTO @CARDID_CODE ,@SEQ_CODE,@ROW_INDEX -- loop ����ͧ")
			.AppendLine("")
			.AppendLine("END")
			.AppendLine("CLOSE tempID")
			.AppendLine("DEALLOCATE tempID")
		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@CardID", CardID)
				.PatametersAdd("@Seq", Seq)
			End With
			Try
				Complete = ObjDB.SelectReQuery(Sqlstr.ToString)
				Complete = True
			Catch ex As Exception
				Complete = False
			End Try
		End Using
		Return Complete
	End Function


	Public Function AddTempTextFields(ByVal TextField As clsCardTextField) As String
		Dim cardID As String = ""
		Dim SqlStr As New StringBuilder
		With SqlStr
			.AppendLine("declare @CardID as varchar (20) ")
			If TextField.CardID = "" Then
				.AppendLine("Exec spc_GetMaxIDInc 'TBBCTextFieldTemp','CardID','BC','BC','','', @CardID output ")
			Else
				.AppendLine("Set @CardID = '" & TextField.CardID & "' ")
			End If
			.AppendLine("select @CardID  as CardID")
			.AppendLine("Insert Into TBBCTextFieldTemp ")
			.AppendLine("(CardID,Seq,FieldName,TextValue,PositionTop,PositionLeft,zIndex,BackColor,ForeColor,FontFamily, ")
			.AppendLine("FontSize,CreateBy,CreateOn,UpdateBy,UpdateOn,FontBold,FontItalic,FontUnderline,Right2Left,AlignCenter) ")
			.AppendLine("Values( ")
			.AppendLine("@CardID,@Seq,@FieldName,@TextValue,@PositionTop,@PositionLeft,@zIndex,@BackColor,@ForeColor,@FontFamily, ")
			.AppendLine("@FontSize,@CreateBy,getdate(),@UpdateBy,getdate(),@FontBold,@FontItalic,@FontUnderline,@Right2Left,@AlignCenter)")
		End With


		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			With ObjDB
				.ParametersClear()
				.PatametersAdd("@Seq", TextField.Seq)
				.PatametersAdd("@FieldName", TextField.FieldName)
				.PatametersAdd("@TextValue", TextField.TextValue)
				.PatametersAdd("@PositionTop", TextField.PositionTop)
				.PatametersAdd("@PositionLeft", TextField.PositionLeft)
				.PatametersAdd("@zIndex", TextField.zIndex)
				.PatametersAdd("@BackColor", TextField.BackColor)
				.PatametersAdd("@ForeColor", TextField.ForeColor)
				.PatametersAdd("@FontFamily", TextField.FontFamily)
				.PatametersAdd("@FontSize", TextField.FontSize)
				.PatametersAdd("@CreateBy", TextField.CreateBy)
				.PatametersAdd("@UpdateBy", TextField.UpdateBy)
				.PatametersAdd("@FontBold", TextField.FontBold)
				.PatametersAdd("@FontItalic", TextField.FontItalic)
				.PatametersAdd("@FontUnderline", TextField.FontUnderline)
				.PatametersAdd("@Right2Left", TextField.Right2Left)
				.PatametersAdd("@AlignCenter", TextField.AlignCenter)
				ObjDB.SelectReQuery(SqlStr.ToString)
				If ObjDB.Go Then
					cardID = ObjDB.ReadField("CardID")
					objSessionVar.ThisCardID = cardID
				End If
			End With
		End Using
		Return cardID
	End Function
	Public Function GetCardTemp(ByVal CardID As String) As clsCard
		Dim card As New clsCard
		Dim SqlStr As String = "Select * From TBBCCards Where CardID = '" & CardID & "' and CardActive = 'Y' "

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr)
			If ObjDB.Go Then
				card = New clsCard
				With card
					.CardID = CardID
					.CardName = ObjDB.ReadField("CardName")
					.Description = ObjDB.ReadField("Description")
					.BGColor = ObjDB.ReadField("BGColor")
					.BGImage = ObjDB.ReadField("BGImage")
					.templateImage = ObjDB.ReadField("templateImage")
					.TmpFlag = ObjDB.ReadField("TmpFlag")
					.Width = ObjDB.ReadField("Width")
					.Height = ObjDB.ReadField("Height")
					' CountOrder �����
					.UploadFlag = ObjDB.ReadField("UploadFlag")
					.owner = ObjDB.ReadField("owner")
					.CreateBy = ObjDB.ReadField("CreateBy")
					.CreateOn = ObjDB.ReadField("CreateOn")
					.UpdateBy = ObjDB.ReadField("UpdateBy")
					.UpdateOn = ObjDB.ReadField("UpdateOn")
					.UserID = ObjDB.ReadField("UserID")
					' Text and Image
					.TextFields = Me.GetListCardTempTextFields(CardID)
					.ImageFields = Me.GetListCardTempImageFields(CardID)
				End With
			End If
		End Using
		Return card
	End Function

	Public Function GetListCardTempTextFields(ByVal CardID As String) As List(Of clsCardTextField)
		Dim gTextFields As New List(Of clsCardTextField)
		Dim SqlStr As String = "Select * From TBBCTextFieldTemp Where CardID = '" & CardID & "' Order By zIndex"

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr)
			While (ObjDB.Go)
				Dim oTextField As New clsCardTextField
				With oTextField
					.CardID = CardID
					.Seq = ObjDB.ReadField("Seq")
					.FieldName = ObjDB.ReadField("FieldName")
					.TextValue = ObjDB.ReadField("TextValue")
					.PositionTop = ObjDB.ReadField("PositionTop")
					.PositionLeft = ObjDB.ReadField("PositionLeft")
					.zIndex = ObjDB.ReadField("zIndex")
					.BackColor = ObjDB.ReadField("BackColor")
					.ForeColor = ObjDB.ReadField("ForeColor")
					.FontFamily = ObjDB.ReadField("FontFamily")
					.FontSize = ObjDB.ReadField("FontSize")
					.CreateBy = ObjDB.ReadField("CreateBy")
					.CreateOn = ObjDB.ReadField("CreateOn")
					.UpdateBy = ObjDB.ReadField("UpdateBy")
					.UpdateOn = ObjDB.ReadField("UpdateOn")
					.FontBold = ObjDB.ReadField("FontBold")
					.FontItalic = ObjDB.ReadField("FontItalic")
					.FontUnderline = ObjDB.ReadField("FontUnderline")
					.Right2Left = ObjDB.ReadField("Right2Left")
					.AlignCenter = ObjDB.ReadField("AlignCenter")
				End With
				gTextFields.Add(oTextField)
			End While
		End Using

		Return gTextFields
	End Function
	Private Function GetListCardTempImageFields(ByVal CardID As String) As List(Of clsCardImageField)
		Dim gImageFields As New List(Of clsCardImageField)
		Dim SqlStr As String = "Select * From TBBCImageFieldsTemp Where CardID = '" & CardID & "' Order By zIndex"

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr)
			While (ObjDB.Go)
				Dim oImageField As New clsCardImageField
				With oImageField
					.CardID = CardID
					.Seq = ObjDB.ReadField("Seq")
					.imageUrl = ObjDB.ReadField("imageUrl")
					.PositionTop = ObjDB.ReadField("PositionTop")
					.PositionLeft = ObjDB.ReadField("PositionLeft")
					.zIndex = ObjDB.ReadField("zIndex")
					.width = ObjDB.ReadField("width")
					.Height = ObjDB.ReadField("Height")
					.CreateBy = ObjDB.ReadField("CreateBy")
					.CreateOn = ObjDB.ReadField("CreateOn")
					.UpdateBy = ObjDB.ReadField("UpdateBy")
					.UpdateOn = ObjDB.ReadField("UpdateOn")
					.ImageSize = ObjDB.ReadField("ImageSize")
					.PreviousName = ObjDB.ReadField("PreviousName")
					.ContentType = ObjDB.ReadField("ContentType")
					.Ready2Print = ObjDB.ReadField("ReadyToPrint")
				End With

				gImageFields.Add(oImageField)
			End While
		End Using

		Return gImageFields
	End Function

	'���˹�� Admin
	Public Function GetListCategories() As List(Of clsCategories)
		Dim SqlStr As New StringBuilder
		Dim ItemCategories As New List(Of clsCategories)
		With SqlStr
			.AppendLine("Select CategoryID,CategoryName,Seq,Type")
			.AppendLine("From   TBBCCategory")
			.AppendLine("Order  By Seq")
		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr.ToString())
			While (ObjDB.Go)
				Dim oCategories As New clsCategories
				With oCategories
					.CategoryID = ObjDB.ReadField("CategoryID")
					.CategoryName = ObjDB.ReadField("CategoryName")
					.Seq = ObjDB.ReadField("Seq")
					.Type = ObjDB.ReadField("Type")
				End With

				ItemCategories.Add(oCategories)
			End While
		End Using

		Return ItemCategories
	End Function

	Public Function GetListFontFamily() As List(Of clsFontFamily)
		Dim SqlStr As New StringBuilder
		Dim ItemFontFamily As New List(Of clsFontFamily)
		With SqlStr
			.AppendLine("Select FontName")
			.AppendLine("From   TBBCFont")
			.AppendLine("Order  By FontName")
		End With

		Using ObjDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
			ObjDB.SelectReQuery(SqlStr.ToString())
			While (ObjDB.Go)
				Dim oFontFamily As New clsFontFamily
				With oFontFamily
					.FontName = ObjDB.ReadField("FontName")
				End With

				ItemFontFamily.Add(oFontFamily)
			End While
		End Using

		Return ItemFontFamily
	End Function


	Private Function SetTextFields(ByVal pGraphic As Graphics, ByVal oCard As clsCard, ByVal oDpi As Integer, Optional ByVal Zoom As Double = 1) As Graphics
		Dim oGraphic As Graphics = pGraphic
		Dim fBold As Byte = 0
		Dim fItalic As Byte = 0
		Dim fUnderline As Byte = 0
		Dim FontStyle As FontStyle = Nothing
		Dim sF As StringFormat

		For Each oTextField As clsCardTextField In oCard.TextFields

			If (oTextField.FontBold = "Y") Then fBold = 1 Else fBold = 0
			If (oTextField.FontItalic = "Y") Then fItalic = 2 Else fItalic = 0
			If (oTextField.FontUnderline = "Y") Then fUnderline = 4 Else fUnderline = 0
			FontStyle = fBold Or fItalic Or fUnderline

			sF = New StringFormat

			Dim TextPositionLeft As Single = clsMyUtil.mm2Pxs(oTextField.PositionLeft, Zoom, oDpi)
			Dim TextPositionTop As Single = clsMyUtil.mm2Pxs(oTextField.PositionTop, Zoom, oDpi)

			If oTextField.Right2Left = "Y" Then	' �Դ���
				TextPositionLeft = clsMyUtil.mm2Pxs((oCard.Width - oTextField.PositionLeft) * Zoom, oDpi) ' ���Ѻ�Ҩҡ�ҧ���᷹
				sF.Alignment = StringAlignment.Far
			End If

			If oTextField.AlignCenter = "Y" Then sF.Alignment = StringAlignment.Center ' �ʴ���ͤ����ç��ҧ �¡�˹�᡹ X �繨ش��觡�ҧ

			Dim oFont As New Font(oTextField.FontFamily, oTextField.FontSize * Zoom, FontStyle, GraphicsUnit.Pixel)

			Dim oBrush As New SolidBrush(clsMyUtil.GetColorFromHtml(oTextField.ForeColor))

			oGraphic.DrawString(oTextField.TextValue, oFont, oBrush, TextPositionLeft, TextPositionTop, sF)

		Next

		Return oGraphic
	End Function

	Private Function SetImageFields(ByVal pGraphic As Graphics, ByVal oCard As clsCard, ByVal sPath As String, ByVal oDpi As Integer, Optional ByVal Zoom As Double = 1) As Graphics
		Dim oGraphic As Graphics = pGraphic

		'img Background
		Dim sBgImage As String = sPath & "\CardImages\BGCard\" & oCard.BGImage
		Dim oBgImage As New Bitmap(sBgImage)
		oGraphic.DrawImage(oBgImage, 0, 0, clsMyUtil.mm2Px(oCard.Width * Zoom, oDpi), clsMyUtil.mm2Px(oCard.Height * Zoom, oDpi))

		' img Logo and Other
		For Each oImageField As clsCardImageField In oCard.ImageFields
			If oImageField.imageUrl <> "" Then
				Dim sImage As String = sPath & "\CardImages\UploadImage\Thumbnails\" & oImageField.imageUrl
				Dim oImage As New Bitmap(sImage)

				'Dim oPoint As New Point(oImageField.PositionLeft, oImageField.PositionTop)
				Dim x As clsImageSize = clsMyUtil.GetImageSize(New clsImageSize(oImage.Width, oImage.Height), New clsImageSize(oImageField.width, oImageField.Height))

				'oGraphic.DrawImage(oImage, clsMyUtil.mm2Px(oImageField.PositionLeft * Zoom, oDpi), clsMyUtil.mm2Px(oImageField.PositionTop * Zoom, oDpi), clsMyUtil.mm2Px(oImageField.width * Zoom, oDpi), clsMyUtil.mm2Px(oImageField.Height * Zoom, oDpi)) '// (img,left,top,width,hight)
				oGraphic.DrawImage(oImage, clsMyUtil.mm2Pxs(oImageField.PositionLeft * Zoom, oDpi), clsMyUtil.mm2Pxs(oImageField.PositionTop * Zoom, oDpi), clsMyUtil.mm2Pxs(x.Width * Zoom, oDpi), clsMyUtil.mm2Pxs(x.Height * Zoom, oDpi))
			End If
		Next

		Return oGraphic
	End Function

	Private Function SetWaterMark(ByVal pGraphic As Graphics, ByVal oCard As clsCard, ByVal sPath As String, ByVal oDpi As Int16, Optional ByVal Zoom As Double = 1) As Graphics
		Dim oGraphic As Graphics = pGraphic

		'img Watermark  �����ٻ PNG ��������� tranparent
		Dim sWmImage As String = sPath & "\CardImages\BGCard\pattern.png"
		Dim oWmImage As New Bitmap(sWmImage)

		oGraphic.DrawImage(oWmImage, 0, 0, clsMyUtil.mm2Px(oCard.Width * Zoom, oDpi), clsMyUtil.mm2Px(oCard.Height * Zoom, oDpi))

		Return oGraphic
	End Function

	Private Function GetCardSortBy(ByVal SortBy As eCardSortBy) As String
		Select Case SortBy
			Case eCardSortBy.CreateOnDESC
				Return "CreateOn DESC"
			Case eCardSortBy.CreateOnASC
				Return "CreateOn ASC"
			Case eCardSortBy.CardNameDESC
				Return "CardName DESC"
			Case eCardSortBy.CardNameASC
				Return "CardName ASC"
			Case Else
				Throw New Exception("eCardSortBy incorrect")
		End Select
	End Function


	Protected Overrides Sub Finalize()
		MyBase.Finalize()
	End Sub
End Class
#End Region

#Region "clsCardSearchParam"
Public Class clsCardSearchParam
	Inherits clsBLBase

	Private ThisMyParams As MyParams
	Private SearchKeyValue As String
	Public Property SearchKey() As String
		Get
			Return SearchKeyValue
		End Get
		Set(ByVal value As String)
			Me.ThisMyParams = New MyParams(MyURLChars.UnReplaceMyOperator(value))
			SearchKeyValue = value
		End Set
	End Property

	Public Property CardType() As String
		Get
			Return ThisMyParams.GetValue("CardType")
		End Get
		Set(ByVal value As String)
			ThisMyParams.SetValue("CardType", value)
		End Set
	End Property

	Public Property Categories() As String
		Get
			Return ThisMyParams.GetValue("Categories")
		End Get
		Set(ByVal value As String)
			ThisMyParams.SetValue("Categories", value)
		End Set
	End Property

	Public ReadOnly Property AdvanceSearchSqlWhereStatement() As String
		Get
			Dim SqlStr As String = ""

			Select Case Me.CardType
				Case "1" ' Gallery
					If objSessionVar.ModelUser IsNot Nothing Then
						SqlStr += " UserID = '" & Me.objSessionVar.ModelUser.UserID & "' And UploadFlag='N' "
					End If
				Case "2" ' Templete
					SqlStr += " TmpFlag = 'Y' "
				Case Else
					SqlStr += " 1=1 "
			End Select

			If Me.CardType = "2" And Me.Categories IsNot Nothing Then
				SqlStr += " and category = '" & Me.Categories & " ' "
			Else
				SqlStr += ""
			End If

			If SqlStr = "" Then
				SqlStr = "1=0"
			End If
			Return SqlStr
		End Get
	End Property
End Class
#End Region