﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" CodeFile="DesignCard.aspx.vb" Inherits="DesignCard" title="นามบัตร" %>

<%@ Register Src="uc/ucCard.ascx" TagName="ucCard" TagPrefix="uc2" %>
<%@ Register Src="uc/ucHeadPanel1.ascx" TagName="ucHeadPanel1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" Runat="Server">
    <div style="margin:auto;">
    <img src="images/h-make-card01.png" class="png" alt="ทำนามบัตร" title="ทำนามบัตร" /><br />
    </div>
    <div style="padding: 30px; background-color:#fff; margin:auto; width: 920px; ">
    	
    	<div style="background-color:#fff; float:left; width:600px; ">
    	<h1>บริการสั่งพิมพ์นามบัตรออนไลน์</h1>
    	
        
        	<div style="width: 450; border: 1px solid #ddd; padding:10px;">
        	<p class="head-column">เริ่มต้นสั่งพิมพ์นามบัตรออนไลน์</p> 
    
    		<div id="btn-card" style="vertical0align:middle;">
    		<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/btn/select01.jpg" PostBackUrl="~/Templates.aspx"  /> 
		<img src="images/btn/select00.jpg" alt="" />
    		<a href = "UpLoadCard.aspx"><img src="images/btn/select02.jpg"></a>
        		</div>
    
    		<span style="font-size: 80%; padding-top:20px;">* กรุณาตรวจสอบตัวอย่างนามบัตรของคุณที่แสดงบนหน้าเว็บ ก่อนยืนยันการสั่งทำ</span>
        	</div>
        </div>
        <div style="width:300px; background-color:#fff; float:right;" align="center">
        <img src="images/make-card01.jpg" alt="นามบัตร" title="นามบัตร" />
        <p style="font-size: 80%; padding-top:20px;" align="center">
       	<img src="images/icon/help.gif" class="v-middle" /> <a href="HowtoOrder.aspx">วิธีสั่งทำนามบัตร</a><br /><br />
	 <a href="http://filezilla-project.org/download.php" target="_blank"><img src="images/load-filezilla.png"></a>
        </p>
        </div><br class="clear">
    </div>
    <div id="dvViewGalley" runat="server" style="padding: 30px; background-color:#fff; margin: 20px auto; width: 920px; clear:both;">
    <div style="background-color:#fff; float:left; font-weight:bold;"><b>My Gallery</b></div><br /><br />
   	<uc2:ucCard ID="UcCard1" runat="server" /><br /><br />
    
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Gallerys.aspx">More</asp:HyperLink>
    </div>                                                      
</asp:Content>

