Imports System.Collections.Generic
Imports CoreBaseII

Partial Class frmShippingAddressUploadNew
	Inherits clsHTTPBase


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If (Not IsPostBack) Then
			Me.UcBar1.BarStep = 3
			btnConfirmOrder.Attributes.Add("onclick", "javascript:confirmOrderUpload();")

			If Session("Condition") IsNot Nothing Then
				Dim str As String = Session("Condition")

			End If

		End If

	End Sub

	Protected Sub btnConfirmOrder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnConfirmOrder.Click
		Dim clsOrderEngine As New clsOrderEngine
		Dim OrderID As String
		Dim Parameter As String = ""
		Dim CardIDResult As String = Nothing
		Dim Order As New clsOrder

		If Me.CheckBox1.Checked = True Then

			If Me.hd1.Value = "1" Then	  ' 1 = ��觷�
				If Session("Condition") IsNot Nothing Then
					CardIDResult = Me.saveCard()
					Dim str As String = Session("Condition")
					Dim strArray() As String = Split(str, "^")

					Dim Details() As String = Nothing
					Dim detail As String

					detail = Session("Detail")
					Details = detail.Split(",")

					Order.CustID = objSessionVar.ModelUser.CustID
					Order.Type = strArray(7)
					Order.Qty = strArray(1)
					Order.PaperID = strArray(0)
					Order.NetAmt = strArray(2)
					Order.VatAmt = strArray(3)
					Order.DesiignAmt = "0"
					Order.TotAmt = strArray(4)
					Order.PaymentCode = Me.ucPayMents1.PaymentCode
					Order.PaymentType = Me.ucPayMents1.PaymentType
					Order.ShippingSeqNo = 1
					Order.ContactorSeqNo = 2
					Order.CardID = CardIDResult
					Order.Image1 = 1
					Order.Image2 = 2
					Order.CreateBy = objSessionVar.ModelUser.UserID
					Order.UpdateBy = objSessionVar.ModelUser.UserID
					Order.DeliveryAmt = strArray(5)
					Order.Description = Me.txtRemark.Text
					Order.ShippingAddr1 = Me.ucShipping1.ShipAddr1
					Order.ShippingAddr2 = Me.ucShipping1.ShipAddr2
					Order.ShippingAddr3 = Me.ucShipping1.ShipAddr3
					Order.ShippingAddr4 = Me.ucShipping1.ShipAddr4
					Order.ShippingAddr5 = ""

					Order.BusinessType = Details(0)
					Order.CardStyle = Details(1)
					Order.ColorShade = Details(2)

					Order.TotalAmt = strArray(6)
					Order.OptionalPrice1 = strArray(8)
					Order.OptionalDesc1 = strArray(9)
					Order.OptionalPrice2 = strArray(10)
					Order.OptionalDesc2 = strArray(11)



					OrderID = clsOrderEngine.InsertOrder(Order)
					Dim clsCards As clsCard = Session("oCard")
					Dim urlLogo As String = ""

					If clsCards.ImageFields IsNot Nothing Then
						If clsCards.ImageFields.Count > 0 Then
							urlLogo = clsCards.ImageFields.Item(0).imageUrl.ToString()
						End If
					End If

					Dim sizeCard As String = clsCards.Height & " X " & clsCards.Width
					If CardIDResult IsNot Nothing AndAlso OrderID IsNot Nothing Then
						Session("oBitmap") = Nothing
						Session("oCard") = Nothing
						Session("ImgAtt") = Nothing
					End If

					'If objSessionVar.ModelUser.ActionUpload = True Then
					'Dim Result As New clsResultII
					'Result = clsOrderEngine.UpdateFTPfile(OrderID, Order.CreateBy)

					'If Result.Flag = False Then
					'	objSessionVar.ModelUser.ActionUpload = False

					'	Response.Redirect("ConfirmOrderUploadNew.aspx?Order=" & OrderID & "&FTPUpLoad=N" & "&CardID=" & CardIDResult & "&CardSize=" & sizeCard & "&UrlLogo=" & urlLogo)
					'Else
					'	Response.Redirect("ConfirmOrderUploadNew.aspx?Order=" & OrderID & "&CardID=" & CardIDResult & "&CardSize=" & sizeCard & "&UrlLogo=" & urlLogo)

					'End If	
					'End If

					'�� UploadFile
					If clsCards.ImageFields IsNot Nothing Then
						If Session("Typename") = "Inkjet" Then
							Session.Remove("Typename")
							Response.Redirect("inkjetConfirmOrderUploadNew.aspx?Order=" & OrderID & "&CardID=U" & CardIDResult & "&CardSize=" & sizeCard & "&UrlLogo=" & urlLogo)
						Else
							Response.Redirect("ConfirmOrderUploadNew.aspx?Order=" & OrderID & "&CardID=U" & CardIDResult & "&CardSize=" & sizeCard & "&UrlLogo=" & urlLogo)
						End If
					Else
						If Session("Typename") = "Inkjet" Then
							Session.Remove("Typename")
							Response.Redirect("inkjetConfirmOrderUploadNew.aspx?Order=" & OrderID & "&CardUpload=N" & "&CardID=" & CardIDResult & "&CardSize=" & sizeCard & "&UrlLogo=" & urlLogo)
						Else
							Response.Redirect("ConfirmOrderUploadNew.aspx?Order=" & OrderID & "&CardUpload=N" & "&CardID" & CardIDResult & "&CardSize=" & sizeCard & "&UrlLogo=" & urlLogo)
						End If
					End If

				End If
			End If
		End If




		If Me.hd1.Value = "2" Then	'2 = �������� Save �����
			CardIDResult = Me.saveCard()
			Response.Redirect("Main.aspx")
		End If

	End Sub

	Public Function saveCard() As String
		Dim oCard As clsCard = Nothing
		Dim oCardOld As clsCard = Nothing
		Dim oBitmap As System.Drawing.Bitmap = Session("oBitmap")
		Dim ThumbnailName As String = Nothing
		Dim CardEngine As New clsCardEngine
		Dim CardIDResult As String = Nothing

		If Session("oCard") IsNot Nothing Then
			oCard = Session("oCard")
			If Session("oCardOld") IsNot Nothing Then
				Dim newImages As List(Of clsCardImageField) = Session("oCardOld")
				For Each newImage As clsCardImageField In newImages
					oCard.ImageFields.Add(newImage)
				Next
			End If

			CardIDResult = CardEngine.SaveCard(oCard, oBitmap, Server.MapPath("."))

		End If
		Return CardIDResult
	End Function

End Class
