﻿Imports System.Collections.Generic
Imports CoreBaseII

Partial Class Admin_DisplayAllTextField
    Inherits clsHTTPBase
    Public ObjTextCard As New List(Of clsCardTextField)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim CardID As String = Request("CardID")
        Dim clsTextCard As New clsCardProvider
        ObjTextCard = clsTextCard.GetListCardTempTextFields(CardID)

    End Sub

End Class
