﻿
Partial Class Admin_ajEditTextField
    Inherits clsHTTPBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'TBBCTextFields
        Dim CardID As String = Request("CardID")
        Dim Seq As String = Request("Seq")
        Dim FieldName As String = Request("FieldName")
        Dim TextValue As String = Request("TextValue")
        Dim PositionTop As String = Request("PositionTop")
        Dim PositionLeft As String = Request("PositionLeft")
        Dim zIndex As String = Request("zIndex")
        Dim BackColor As String = Request("BackColor")
        Dim ForeColor As String = Request("ForeColor")
        Dim FontFamily As String = Request("FontFamily")
        Dim FontSize As String = Request("FontSize")
        Dim FontBold As String = Request("FontBold")
        Dim FontItalic As String = Request("FontItalic")
        Dim FontUnderline As String = Request("FontUnderline")
        Dim Right2Left As String = Request("Right2Left")
        Dim AlignCenter As String = Request("AlignCenter")
        Dim ObjectTextFields As New clsCardTextField

        With ObjectTextFields
            .CardID = CardID
            .Seq = Seq
            .FieldName = FieldName
            .TextValue = TextValue
            .PositionTop = PositionTop
            .PositionLeft = PositionLeft
            .zIndex = zIndex
            .BackColor = BackColor
            .ForeColor = ForeColor
            .FontFamily = FontFamily
            .FontSize = FontSize
            .CreateBy = objSessionVar.ModelUser.UserName
            .UpdateBy = objSessionVar.ModelUser.UserName
            .FontBold = FontBold
            .FontItalic = FontItalic
            .FontUnderline = FontUnderline
            .Right2Left = Right2Left
            .AlignCenter = AlignCenter
        End With

        Dim LibCard As New clsCardEngine
        Dim Complete As Boolean = LibCard.EditTextFields(ObjectTextFields)

        If Complete = False Then
            Response.Write("Error!!! กรุณาลองใหม่อีกครั้งค่ะ")
        Else
            Response.Write("แก้ไขข้อมูลเรียบร้อยแล้วค่ะ")
        End If

    End Sub
End Class
