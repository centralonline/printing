Imports Microsoft.VisualBasic
Imports CoreBaseII
Imports System.IO
Imports System.Data
Imports System.Collections.Generic



Public Class clsUploadTemplateEngine
    Public Sub InsertUploadTemplateEngine(ByVal Parameter As String)
        Dim clsUploadProvider As New clsUploadTemplateProvider
        clsUploadProvider.InsertUploadTemplateProvider(Parameter)
    End Sub
    Public Sub InsertUploadTBBCTextFieldsEngine(ByVal Parameter As String)
        Dim clsUploadProvider As New clsUploadTemplateProvider
        clsUploadProvider.InsertUploadTBBCTextFieldsProvider(Parameter)
    End Sub
    Public Sub InsertUploadTBBCImageFieldsEngine(ByVal Parameter As String)
        Dim clsUploadProvider As New clsUploadTemplateProvider
        clsUploadProvider.InsertUploadTBBCImageFieldsProvider(Parameter)
    End Sub
    Public Function GetMaxID() As String
        Dim maxid As String
        Dim clsUploadProvider As New clsUploadTemplateProvider
        maxid = clsUploadProvider.GetMaxID()
        Return maxid
    End Function
End Class

Public Class clsUploadTemplateProvider
    Inherits clsBLBase
    Public Sub InsertUploadTemplateProvider(ByVal Parameter As String)
        Dim SqlStr As New StringBuilder
        Dim CARDID As String = Nothing

        SqlStr.AppendLine("Insert Into TBBCCards Values(")
        SqlStr.AppendLine("@Parameter")
        SqlStr.AppendLine(")")

        Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            objDB.ParametersClear()
            SqlStr = SqlStr.Replace("@Parameter", Parameter)
            objDB.SelectReQuery(SqlStr.ToString())
        End Using
    End Sub
    Public Sub InsertUploadTBBCTextFieldsProvider(ByVal Parameter As String)
        Dim SqlStr As New StringBuilder
        Dim CARDID As String = Nothing

        SqlStr.AppendLine("Insert Into TBBCTextFields Values(")
        SqlStr.AppendLine("@Parameter")
        SqlStr.AppendLine(")")

        Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            objDB.ParametersClear()
            SqlStr = SqlStr.Replace("@Parameter", Parameter)
            objDB.SelectReQuery(SqlStr.ToString())
        End Using
    End Sub
    Public Sub InsertUploadTBBCImageFieldsProvider(ByVal Parameter As String)
        Dim SqlStr As New StringBuilder
        Dim CARDID As String = Nothing

        SqlStr.AppendLine("Insert Into TBBCImageFields Values(")
        SqlStr.AppendLine("@Parameter")
        SqlStr.AppendLine(")")

        Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            objDB.ParametersClear()
            SqlStr = SqlStr.Replace("@Parameter", Parameter)
            objDB.SelectReQuery(SqlStr.ToString())
        End Using
    End Sub
    Public Function GetMaxID() As String
        Dim TempDS As New DataSet
        Dim MaxID As String = ""
        Dim SqlStr As New StringBuilder

        SqlStr.AppendLine("declare @CARDID varchar (12)")
        SqlStr.AppendLine("Exec spc_GetMaxIDInc 'TBBCCards','CARDID','BC','CARD','','', @CARDID output ")
        SqlStr.AppendLine("select @CARDID as CARDID")

        Using objDB As clsDBIII = clsDBIII.GetInstance(Me.objAppVar.DBConfig)
            objDB.ExecuteCommand(SqlStr.ToString, clsDBIII.eExecute.SelectType)
            While (objDB.Go)
                MaxID = objDB.ReadField("CARDID")
            End While
        End Using
        Return MaxID
    End Function

End Class
