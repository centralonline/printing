Imports System.Drawing
Imports System.Drawing.Text
Imports System.Drawing.Imaging

Partial Class FileSender
    Inherits System.Web.UI.Page
    Dim Path As String = Server.MapPath(".")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim CardEngine As New clsCardEngine
        Dim oBitmap As Bitmap = Session("oBitmap")
        If oBitmap IsNot Nothing Then
            'oBitmap.Save(Response.OutputStream, ImageFormat.Gif)
            Dim ici As ImageCodecInfo = GetImageCodec("image/jpeg")
            Dim eps As New EncoderParameters(1)
            eps.Param(0) = New EncoderParameter(Encoder.Quality, 120) 'or whatever other quality value you want
            oBitmap.Save(Response.OutputStream, ici, eps)
        End If
    End Sub

    Private Function GetImageCodec(ByVal mimetype As String) As ImageCodecInfo
        For Each ici As ImageCodecInfo In ImageCodecInfo.GetImageEncoders()
            If (ici.MimeType = mimetype) Then
                Return ici
            End If
        Next
        Return Nothing
    End Function
End Class
