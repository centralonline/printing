Imports System.Web

Public Class clsSecurity

    Public Shared Sub validateParam(ByVal Request As HttpRequest, ByVal Response As HttpResponse)
        ''--> Check risk param
        'Dim TempRiskChars As String() = {"'", """", "--", "/*", "*/"}
        'Dim Req As HttpRequest = HttpContext.Current.Request
        'Dim aKey, aValue, aChar As String
        'Dim NoneSecureText As String = Nothing
        'For Each aKey In Req.Params.Keys
        '    aValue = Req(aKey).ToLower()
        '    If (aKey = "ALL_HTTP") Then
        '        Exit For
        '    End If
        '    For Each aChar In TempRiskChars
        '        If (aValue.IndexOf(aChar) <> -1) Then
        '            NoneSecureText += aKey & " = " & aValue & "<br/>"
        '        End If
        '    Next
        'Next
        'If (Not NoneSecureText Is Nothing) Then
        '    sendNotifyMail("Security Alert from B2C", NoneSecureText, Request)
        '    Response.Redirect("Detector.htm")
        'End If
    End Sub

    Public Shared Sub validateSql(ByVal SqlStr As String)
        'SqlStr = SqlStr.ToLower()
        ''--> Check risk sql command
        'Dim TempRiskCom As String() = {"drop", "alter"}
        'Dim aCom As String
        'For Each aCom In TempRiskCom
        '    If (SqlStr.IndexOf(aCom) <> -1) Then
        '        sendNotifyMail(SqlStr, Nothing)
        '        Throw New Exception("MySecurity")
        '    End If
        'Next
    End Sub

    Public Shared Sub sendNotifyMail(ByVal Subject As String, ByVal SqlStr As String, ByVal Request As HttpRequest)
        Dim Req As HttpRequest
        If (Request Is Nothing) Then
            Req = HttpContext.Current.Request
        Else
            Req = Request
        End If

        Dim Session As SessionState.HttpSessionState = HttpContext.Current.Session
        Dim Application As HttpApplicationState = HttpContext.Current.Application

        Dim TempBody As String = ""
        TempBody += "****�����Ţͧ����ͧ���кء�ء <br/>"
        TempBody += "User Agent : " & Req.UserAgent & "<br/>"
        TempBody += "Host Address : " & Req.UserHostAddress & "<br/>"
        TempBody += "Host Name : " & Req.UserHostName & "<br/>"
        TempBody += "Http Method : " & Req.HttpMethod & "<br/>"
        TempBody += "Absolute Path : " & Req.Url.AbsoluteUri & "<br/>"
        TempBody += "Browser Version : " & Req.Browser.Version & "<br/>"
        TempBody += "File Path : " & Req.FilePath & "<br/>"
        If (Req.UrlReferrer IsNot Nothing AndAlso Req.UrlReferrer.AbsoluteUri IsNot Nothing) Then
            TempBody += "Absolute Referer : " & Req.UrlReferrer.AbsoluteUri & "<br/>"
        Else
            TempBody += "Absolute Referer : Null<br/>"
        End If
        TempBody += " Alert Date : " & Now.ToShortDateString() & "<br/>"
        TempBody += " Alert Time : " & Now.ToLongTimeString() & "<br/>"
        TempBody += "<br/>"

        TempBody += "****<b>Monitor Text</b> : <br/>" & SqlStr & "<br/>"
        TempBody += " <br/>"

        If (Session IsNot Nothing) Then
            Dim objSessionVar As clsSessionVarWC = Session("objSessionVar")
            If (objSessionVar IsNot Nothing) Then
                Dim objUser As ModelUser = objSessionVar.ModelUser
                If (objUser IsNot Nothing) Then
                    TempBody += "**** Login infomation " & "<br/>"
                    TempBody += " Login Name : " & objUser.UserID & "<br/>"
                    TempBody += " Name : " & objUser.UserName & "<br/>"
                    TempBody += "<br/>"
                End If
            End If
        End If


		clsMailService.SendMail("monitorserver@officemate.co.th", "Officemate Printing Solution", "monitorserver@officemate.co.th", Subject, TempBody)

    End Sub

End Class
