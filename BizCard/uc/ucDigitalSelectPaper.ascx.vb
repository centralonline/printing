Imports System.Collections.Generic
Partial Class uc_ucDigitalSelectPaper
    Inherits System.Web.UI.UserControl
    Private PaperIDValue As String
    Private _test As String

    Private IsUploadValue As Boolean
    Public Property IsUpload() As String
        Get
            Return IsUploadValue
        End Get
        Set(ByVal value As String)
            IsUploadValue = value
        End Set
    End Property

    Private listPaperValue As String
    Public Property listPaper() As String
        Get
            Return listPaperValue
        End Get
        Set(ByVal value As String)
            listPaperValue = value
        End Set
    End Property
    Private ddlsizetextValue As String
    Public Property ddlsize() As String
        Get
            Return ddlsizetextValue
        End Get
        Set(ByVal value As String)
            ddlsizetextValue = value
        End Set
    End Property
    Private PaperValue As Integer
    Public Property PapersizeID() As Integer
        Get
            Return PaperValue
        End Get
        Set(ByVal value As Integer)
            PaperValue = value
        End Set
    End Property

    Private PaperSizeValue As String
    Public Property PaperSize() As String
        Get
            Return PaperSizeValue
        End Get
        Set(ByVal value As String)
            PaperSizeValue = value
        End Set
    End Property

    Private TypeSystemValue As String
    Public Property TypeSystem() As String
        Get
            Return TypeSystemValue
        End Get
        Set(ByVal value As String)
            TypeSystemValue = value
        End Set
    End Property

    'gift ����������㹡�äԴ��Һ�ԡ���Թ���
    Private orderValue As String
    Public Property order() As String
        Get
            Return orderValue
        End Get
        Set(ByVal value As String)
            orderValue = value
        End Set
    End Property

    Private sumpromotionValue As Double
    Public Property sumpromotion() As Double
        Get
            Return sumpromotionValue
        End Get
        Set(ByVal value As Double)
            sumpromotionValue = value
        End Set
    End Property

    Private sumpriceValue As Double
    Public Property sumprice() As Double
        Get
            Return sumpriceValue
        End Get
        Set(ByVal value As Double)
            sumpriceValue = value
        End Set
    End Property

    Private sumserviceValue As Double
    Public Property sumservice() As Double
        Get
            Return sumserviceValue
        End Get
        Set(ByVal value As Double)
            sumserviceValue = value
        End Set
    End Property

    Private sumValue As Double
    Public Property sum() As Double
        Get
            Return sumValue
        End Get
        Set(ByVal value As Double)
            sumValue = value
        End Set
    End Property
    Private SummaryValue As Double
    Public Property Summary() As Double
        Get
            Return SummaryValue
        End Get
        Set(ByVal value As Double)
            SummaryValue = value
        End Set
    End Property
    Private PPaperIDValue As String
    Public Property PaperID() As String
        Get
            Return PPaperIDValue
        End Get
        Set(ByVal value As String)
            PPaperIDValue = value
        End Set
    End Property
    Private NetAmtValue As String
    Public Property NetAmt() As String
        Get
            Return NetAmtValue
        End Get
        Set(ByVal value As String)
            NetAmtValue = value
        End Set
    End Property
    Private sumviewValue As String
    Public Property sumview() As String
        Get
            Return sumviewValue
        End Get
        Set(ByVal value As String)
            sumviewValue = value
        End Set
    End Property
    Private papernameValue As String
    Public Property papername() As String
        Get
            Return papernameValue
        End Get
        Set(ByVal value As String)
            papernameValue = value
        End Set
    End Property
    Private OptionsizepaperValue As String
    Public Property Optionsizepaper() As String
        Get
            Return OptionsizepaperValue
        End Get
        Set(ByVal value As String)
            OptionsizepaperValue = value
        End Set
    End Property


    'Gift ������ԡ����ҹ Digital 
    'Createby 26/6/2014
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then

            Dim clsGetListPaper As New clsPaperEngine
            Dim listPaper As New List(Of clsPaper)
            listPaper = clsGetListPaper.GetListPaperdigital("Poster")
            ddlsizetext.Items.Clear()

            Me.ddlsizetext.DataSource = listPaper
            Me.ddlsizetext.DataTextField = "Size"
            Me.ddlsizetext.DataValueField = "Size"
            Me.ddlsizetext.DataBind()
            Optionsizepaper = 0
            txtQuantity.Text = ""
            clear()
            BindOptionPrice(GetDataPaperByOptionPrice(ddlsizetext.Text))
        Else
            PaperSize = ddlsizetext.SelectedValue
            papername = ddlnametext.SelectedValue

            If txtQuantity.Text = "" Then
                clear()
            Else
                Getcost()
            End If
        End If
     
    End Sub

    Protected Sub ddlsizetext_Load(ByVal sender As Object, e As System.EventArgs) Handles ddlsizetext.Load
        BindDataPaperBySize(GetDataPaperBySize(ddlsizetext.Text))
        Try
            If PaperSize = ddlsizetext.SelectedValue Then
                ddlnametext.SelectedValue = papername

            Else
                ddlnametext.SelectedIndex = -1
                ddloption.SelectedIndex = -1
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlsizetext_SelectedIndexChanged(ByVal sender As Object, e As System.EventArgs) Handles ddlsizetext.SelectedIndexChanged 'Droupdown ������ҹ

        txtQuantity.Text = ""
        BindDataPaperBySize(GetDataPaperBySize(ddlsizetext.Text))

        ddloption.SelectedIndex = -1 ' ��ԡ������������

        BindOptionPrice(GetDataPaperByOptionPrice(ddlsizetext.Text))

        clear()
    End Sub

    Protected Sub ddlname1_Load(sender As Object, e As System.EventArgs) Handles ddlnametext.Load
        GetDatapaperByDecription(ddlnametext.Text)
        If txtQuantity.Text = "" Then
            clear()
        Else
            Getcost()
        End If

    End Sub

    Protected Sub ddlname1_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlnametext.SelectedIndexChanged 'Droupdown ��������ʴ�
        GetDatapaperByDecription(ddlnametext.Text)
        txtQuantity.Text = ""
		clear()

		'modify
		ddloption.SelectedIndex = -1
		If ddloption.Text = "0.00" Then
			Me.lbQty.Text = "0.00"
		Else
			GetDataPaperByOptionPrice(ddlsizetext.Text)
		End If
		'GetDataPaperByOptionPrice(ddloption.Text)

    End Sub

    Private Sub BindDataPaperBySize(ByVal papername As List(Of clsPaper))
        Me.ddlnametext.Items.Clear()
        Me.ddlnametext.DataSource = papername
        Me.ddlnametext.DataValueField = "PaperID"
        Me.ddlnametext.DataTextField = "Description"
        Me.ddlnametext.DataBind()

        Me.ddlnametext.SelectedIndex = -1
    End Sub

    Private Function GetDataPaperBySize(ByVal paperSize As String) As List(Of clsPaper)
        Dim clsGetListPaper As New clsPaperEngine
        Dim namepaperA3 As New List(Of clsPaper)

        ddlnametext.Items.Clear()
        namepaperA3 = clsGetListPaper.GetListPapersize(ddlsizetext.Text, "Poster")
        Return namepaperA3
    End Function

    Private Function GetDatapaperByDecription(ByVal Description As String) As List(Of clsPaper)
        Dim clsGetddlnameDecription As New clsPaperEngine
        Dim namepaperDecription As New List(Of clsPaper)
        namepaperDecription = clsGetddlnameDecription.GetListPaperDecription(ddlnametext.Text, ddlsizetext.Text, "Poster")
        Me.lbPaperID.Text = namepaperDecription.Item(0).PaperID
        Me.lbPrice.Text = namepaperDecription.Item(0).Price
        Me.lbPromotion.Text = namepaperDecription.Item(0).Promotion
        Session("Typename") = namepaperDecription.Item(0).Typename

        Return namepaperDecription

    End Function


    '��ԡ������������ ���ͺ�ѹ ���ͺ��ҹ
    Protected Sub ddloption_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddloption.SelectedIndexChanged
        'Modify
        If ddloption.Text = "0.00" Then
            Me.lbQty.Text = "0.00"
        Else
            GetDataPaperByOptionPrice(ddlsizetext.Text)
        End If

        If txtQuantity.Text = "" Then
            clear()
        Else
            Getcost()
        End If
    End Sub

    Private Sub BindOptionPrice(ByVal Optionsizepaper As List(Of clsPaper))
        ddloption.Items.Clear()
        Me.ddloption.DataSource = Optionsizepaper
        Me.ddloption.DataValueField = "Seq"
        Me.ddloption.DataTextField = "Description"
        Me.ddloption.DataBind()
        ddloption.Items.Insert(0, (New ListItem("������͡��ԡ�����ͺ", "0.00")))
        Me.lbQty.Text = ddloption.Items(0).Value
        ddloption.SelectedIndex = -1
    End Sub

    Private Function GetDataPaperByOptionPrice(ByVal paperSize As String) As List(Of clsPaper)
        Dim clsGetListPaper As New clsPaperEngine
        Dim optionprice As New List(Of clsPaper)
        optionprice = clsGetListPaper.GetOptionPrice(ddlsizetext.Text, ddloption.Text)
        If optionprice.Count > 0 Then
            Me.lbQty.Text = optionprice.Item(0).Price
        End If
        Return optionprice
    End Function

    Protected Sub txtQuantity_TextChanged(sender As Object, e As System.EventArgs) Handles txtQuantity.TextChanged
        If txtQuantity.Text = "" Then
            clear()
        Else
            Getcost()
        End If
    End Sub

    Private Sub clear()
        lbVat.Text = "0.00"
        lbVat_card.Text = "0.00"
        lbDelivery.Text = "0.00"
        lbSummary.Text = "0.00"
    End Sub

    Private Sub Getcost() '�ӹǹ�Ҥ�

        Me.order = txtQuantity.Text
        If lbPromotion.Text <> "0" Then
            Dim Promotion As Double = lbPromotion.Text
            Dim Price As Double = lbPrice.Text
            Dim service As Double = lbQty.Text '�Ҥ����ͺ���˹��� 
            Me.sumprice = Format((Price * order), "#,##0.00")
            Me.sumservice = Format((service * order), "#,##0.00")
            Me.sum = Format((sumprice + sumservice), "#,##0.00")
            Me.lbVat_card.Text = Format((Me.sum * 7) / 107, "#,##0.00")
            Me.lbVat.Text = (Me.sum - lbVat_card.Text)
            Dim Vat As Double = lbVat.Text
            Dim Vat_card As Double = lbVat_card.Text
            Me.Summary = Format((Vat + Vat_card), "#,##0.00")

        Else

            Dim Price As Double = lbPrice.Text
            Dim service As Double = lbQty.Text '�Ҥ����ͺ���˹��� 
            Me.sumprice = Format((Price * order), "#,##0.00")
            Me.sumservice = Format((service * order), "#,##0.00")
            Me.sum = Format(sumprice + sumservice, "#,##0.00")
            Me.lbVat_card.Text = Format((Me.sum * 7) / 107, "#,##0.00")
            Me.lbVat.Text = (Me.sum - lbVat_card.Text)

            Dim Vat As Double = lbVat.Text
            Dim Vat_card As Double = lbVat_card.Text
            Me.Summary = Format((Vat + Vat_card), "#,##0.00")

        End If

        If lbVat.Text < 500 Then
            lbDelivery.Text = "50.00"

            Me.lbSummary.Text = Format(((Me.Summary * 1) + 50), "#,##0.00")
        Else
			lbDelivery.Text = "0.00"

            Me.lbSummary.Text = Format(((Me.Summary * 1)), "#,##0.00")
        End If

        Me.sumview = Format((0), "#,##0.00")
        Me.NetAmt = 0
        Me.Session("NetAmt") = Me.sum
        Me.Session("TotAmt") = Me.lbSummary.Text
        Me.Session("lbVat_Card") = Me.lbVat_card.Text
        Me.Session("lbDelivery") = Me.lbDelivery.Text
        Me.Session("lbVat") = Me.lbVat.Text
        Me.Session("sumtotal") = Format((Me.sumprice), "#,##0.00")
        Me.Session("sumangle") = Format((Me.sumservice), "#,##0.00")
        Me.Session("sumview") = Me.sumview

    End Sub

End Class




