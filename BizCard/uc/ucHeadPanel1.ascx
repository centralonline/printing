<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucHeadPanel1.ascx.vb" Inherits="ucHeadPanel1" %>

<asp:Table ID="Table1" runat="server" Width="460px" CellPadding="0" CellSpacing="0">
    <asp:TableRow runat="server">
        <asp:TableCell Width="6px" runat="server">
            <asp:Image ID="TDLeft" runat="server" ImageUrl="~/Images/uc/Left.gif" />
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left" runat="server" CssClass="ucHeadCenter">
            <asp:Panel ID="TDCenter" runat="server">               
                <asp:Label ID="lblString" runat="server" Text="Text Panel Name"></asp:Label>
            </asp:Panel>
        </asp:TableCell>  
        <asp:TableCell Width="6px" runat="server">
            <asp:Image ID="TDRight" runat="server" ImageUrl="~/Images/uc/Right.gif" />
        </asp:TableCell> 
    </asp:TableRow>
</asp:Table>