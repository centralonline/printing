﻿
function checkEmail(fieldValue){ //reference to email field passed as argument

//var fieldValue = emField.value // store field's entire value in variable

// Begin Valid Email Address Tests

//if field is not empty
if(fieldValue != ""){ 
var atSymbol = 0

//loop through field value string
for(var a = 0; a < fieldValue.length; a++){ 

//look for @ symbol and for each @ found, increment atSymbol variable by 1
if(fieldValue.charAt(a) == "@"){ 
atSymbol++
}

}

// if more than 1 @ symbol exists
if(atSymbol > 1){ 
// then cancel and don't submit form
alert("Please Enter A Valid Email Address") 
return false
}

// if 1 @ symbol was found, and it is not the 1st character in string
if(atSymbol == 1 && fieldValue.charAt(0) != "@"){ 
//look for period at 2nd character after @ symbol 
var period = fieldValue.indexOf(".",fieldValue.indexOf("@")+2) 

// "." immediately following 1st "." ? 
var twoPeriods = (fieldValue.charAt((period+1)) == ".") ? true : false 

//if period was not found OR 2 periods together OR field contains less than 5 characters OR period is in last position
if(period == -1 || twoPeriods || fieldValue.length < period + 2 || fieldValue.charAt(fieldValue.length-1)=="."){
// then cancel and don't submit form
alert("Please Enter A Valid Email Address") 
return false
}

}
// no @ symbol exists or it is in position 0 (the first character of the field)
else{ 
// then cancel and don't submit form
alert("Please Enter A Valid Email Address")
return false 
}
}
// if field is empty
else{ 
// then cancel and don't submit form
alert("Please Enter A Valid Email Address")
return false 
}

//all tests passed, submit form
//alert("VALID EMAIL ADDRESS!")
return true
}

