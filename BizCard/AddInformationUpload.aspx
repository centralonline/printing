﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddInformationUpload.aspx.vb" Inherits="AddInformationUpload" title="ใส่ข้อมูลลงในแบบฟอร์ม" %>

<%@ Register Src="uc/ucBar.ascx" TagName="ucBar" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" Runat="Server">
<div style="margin:auto;">
<img src="images/h-make-card01.png" class="png" alt="ทำนามบัตร" title="ทำนามบัตร" /><br />
</div>
<div style="padding: 30px; background-color:#fff; margin:auto; width: 920px;">
<uc1:ucBar ID="UcBar1" runat="server" />
<h1>ข้อมูลในนามบัตร</h1>
	<div>
    <asp:DataList ID="dlText" runat="server" Width="300px">
    <HeaderTemplate >
    <table>
    </HeaderTemplate>
    <ItemTemplate>
    <tr>
    <td style="white-space: nowrap; padding-right:20px;">
    <asp:Label ID="Label1" runat="server" Text='<%# Eval("FieldName") %>'></asp:Label>
    </td>
    <td>
    <asp:TextBox ID="TextBox1" runat="server" Text='<%#Eval("TextValue") %>' Width="200px"></asp:TextBox>
    </ItemTemplate>
    <FooterTemplate>
    </table>
    </FooterTemplate>
    <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Right" />
    </asp:DataList>
    </div>
<h2>AttachFiles</h2>
<div>
<asp:DataList ID="dlImage" runat="server">
<HeaderTemplate>
<div>
</HeaderTemplate>                    
<ItemTemplate>                        
<asp:HiddenField ID="hidContentType" Value='<%#Eval("ContentType") %>' runat="server" />
<asp:HiddenField ID="hidUrl" Value='<%#Eval("imageUrl") %>' runat="server" />
<asp:HiddenField ID="hidImageSize" Value='<%#Eval("ImageSize") %>' runat="server" />
<asp:HiddenField ID="hidReadyToPrint" runat="server" Value='<%#Eval("Ready2Print") %>' />
<asp:Image ID="imgUpload" runat="server" ImageUrl='<%# "~/CardImages/UploadImage/Thumbnails/" & Eval("imageUrl") %>' /><br /><br />
<asp:Image ID="Image2" runat="server" ImageUrl="~/images/attach.gif" />
<asp:Label ID="txtImgName" runat="server" Text='<%#Eval("PreviousName") %>' Font-Size="11px"></asp:Label><br /><br />
</ItemTemplate>                   
<FooterTemplate>
</div>
</FooterTemplate>
</asp:DataList>
</div>
<div style="background-color:#fff; margin:auto; clear:both; padding-top:20px;">
	<div id="btn-card" align="left">
	<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/UploadCard.aspx" class="btn-face2"><< กลับไปหน้า My Gallery</asp:HyperLink>
	<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/AttachFiles.aspx" class="btn-face2"><< กลับไปหน้า Attach Files</asp:HyperLink>
	<asp:ImageButton ID="btnNext" runat="server" ImageUrl="~/images/next_03.gif" style="float:right;" />
    </div>
</div>
</asp:Content>

