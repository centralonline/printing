
Partial Class uc_ucHead1
    Inherits clsUCBase

    Public Enum eColor
        Green = 0
        Red = 1
    End Enum

    Private ThisWidthValue As Integer
    Public Property ThisWidth() As Integer
        Get
            Return ThisWidthValue
        End Get
        Set(ByVal value As Integer)
            ThisWidthValue = value
        End Set
    End Property

    Private ThisNameValue As String
    Public Property ThisName() As String
        Get
            Return ThisNameValue
        End Get
        Set(ByVal value As String)
            If value Is Nothing OrElse value = "" Then
                value = "&nbsp;"
            End If
            ThisNameValue = value
        End Set
    End Property

    Private ThisColorValue As eColor
    Public Property ThisColor() As eColor
        Get
            Return ThisColorValue
        End Get
        Set(ByVal value As eColor)
            ThisColorValue = value
        End Set
    End Property


    Public Sub New()
        Me.ThisColor = eColor.Green
        Me.ThisWidth = 100
        Me.ThisName = ""
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim imgLeft As Image = CType(Me.FindControl("imgLeft"), Image)
        Dim imgRight As Image = CType(Me.FindControl("imgRight"), Image)
        Dim c1 As TableCell = CType(Me.FindControl("c1"), TableCell)

        Select Case Me.ThisColor
            Case eColor.Green
                imgLeft.ImageUrl = "~/images/tab_green1.gif"
                imgRight.ImageUrl = "~/images/tab_green2.gif"
                c1.BackColor = System.Drawing.ColorTranslator.FromHtml("#74745f")
            Case eColor.Red
                imgLeft.ImageUrl = "~/images/tab_red1.gif"
                imgRight.ImageUrl = "~/images/tab_red2.gif"
                c1.BackColor = System.Drawing.ColorTranslator.FromHtml("#c0282c")
            Case Else
                imgLeft.ImageUrl = "~/images/tab_green1.gif"
                imgRight.ImageUrl = "~/images/tab_green2.gif"
                c1.BackColor = System.Drawing.ColorTranslator.FromHtml("#74745f")
        End Select

        Dim txtName As Label = CType(Me.FindControl("txtName"), Label)

        Me.Table1.Width = Me.ThisWidth
        txtName.Text = Me.ThisName
    End Sub
    
End Class
