﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" CodeFile="HowtoOrder.aspx.vb" Inherits="HowtoOrder" title="วิธีการใช้งาน trendyprint.net" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" Runat="Server">
<div style="margin:auto;">
<img src="images/h-HowtoOrder.png" class="png" alt="HowtoOrder" title="HowtoOrder" /><br />
</div>
<div style="padding: 30px; background-color:#fff; margin:auto; width: 920px;">

<p class="head-column">1. เมื่อ Log in ในเว็บ PrintingSolution แล้วคุณสามารถคลิกเลือกเข้าสั่งทำนามบัตร ดังภาพที่แสดง</p>
<p><img src="images/how/01.jpg"></p>

<p class="head-column">2. คุณสามารถเลือกรูปแบบนามบัตร ได้ 2 แบบ คือ</p>
<p>2.1 เลือกจาก My Gallery นามบัตรที่เคยทำ</p>
<p>2.2 คลิกปุ่ม  <img src="images/btn4_03.jpg" style="vertical-align:middle;" /> เพื่อเลือกรูปแบบนามบัตรที่ต้องการ</p>
<p><img src="images/how/02.jpg"></p>

<p class="head-column">3. เลือกนามบัตร</p>
<p>3.1 กรณีที่เลือกจาก My Gallery (หัวข้อ 2.1) สามารถเลือกจากนามบัตรเดิมที่เคยทำมาก่อนได้เช่นกันจะเข้าหน้า หัวข้อที่ 4</p>
<p>3.2 กรณีที่เลือกรูปแบบนามจาก Template (หัวข้อ 2.2) คุณสามารถเลือกแบบนามบัตรตามที่ต้องการจาก Categories ต่างๆ ของนามบัตร</p>
<p><img src="images/how/03.jpg"></p>

<p class="head-column">4. จากการเลือกในหัวข้อที่ 2.1 หรือ 2.2 จะเข้าหน้าการกรอกข้อมูลในนามบัตร รวมถึงกรณีที่มี Logo ในนามบัตรนั้นก็สามารถจะแนบหรือไม่ก็ได้</p>
<p><img src="images/how/04.jpg"></p>

<p class="head-column">5. หน้าถัดมาจะเป็นการเลือกกระดาษที่ต้องการ  รวมถึงจำนวนของนามบัตรที่สั่งทำ</p>
<p><img src="images/how/05.jpg"></p>

<p class="head-column">6. เลือกสถานที่จัดส่งของคุณ  รวมทั้งการชำระเงิน  กรณีต้องการหมายเหตุใดๆที่ต้องการบอกทางออฟฟิศเมทก็สามารถแจ้งได้ในช่อง “หมายเหตุลูกค้า”</p>
<p><img src="images/how/06.jpg"></p>

<p class="head-column">7. หน้าสุดท้ายจะสรุปข้อมูลการสั่งทำนามบัตรทั้งหมดของคุณ  โดยสามารถสั่งพิมพ์ข้อมูลนี้ได้</p>
<a name="FTP"></a>
<p class="head-column">8. ขั้นตอนการ FTP Files</p>
<p>เมื่อได้ทำใบสั่งซื้อเรียบร้อยแล้ว ระบบจะทำการส่งเมล์แจ้ง account ในการ FTP ไฟล์ของคุณเข้ามาให้กับทางออฟฟิศเมทเพื่อจัดทำต่อไป</p>
<p>ซึ่งวิธี FTP File แบ่งเป็น 2 แบบ ดังนี้</p>
<p>8.1. ใช้โปรแกรม Filezilla</p>
<p><img src="images/how/08-1.jpg"></p>
<p><u>รูปที่ 1</u></p>
<p>
<ul>
<li>1.	คลิกเลือกปุ่มการจัดการ</li>
<li>2.	ใส่ hosting ที่ได้จากการแจ้งผ่านเมล์ใบสั่งซื้อ</li>
<li>3.	ใส่ account ในการใช้งาน</li>
<li>4.	ใส่รหัสผ่าน</li>
<li>5.	ใส่ account อีกครั้ง</li>
<li>6.	คลิกปุ่ม “เชื่อมต่อ” เพื่อ FTP Files ให้กับทางออฟฟิศเมท</li>
</ul>
</p>
<p><img src="images/how/08-2.jpg"></p>
<p><u>รูปที่ 2</u></p>
<p><ul>
<li>7.	คลิกเมาส์ที่ไฟล์ เลือก “อัพโหลด”</li>
<li>8.	ด้านขวาจะเป็นที่เก็บไฟล์งานของคุณ พร้อมที่จะให้ออฟฟิศเข้าไปดึงไฟล์ และดำเนินการต่อ</li>
</ul>
</p>
<a name="yousendit"></a>
<p>8.2 FTP ผ่าน Folder </p>
<p>1. เข้า Directory หรือ Folder ใดก็ได้ ในตัวอย่างคือ Drive C:</p>
<p><img src="images/how/FTP-File01.png"></p>
<p>2. จากนั้น พิมพ์ หรือ Copy URL ทีได้รับแจ้งจากเมลในส่วนของ Address Bar</p>
<p><img src="images/how/FTP-File02.png"></p>
<p>3. จะขึ้น หน้าต่างให้ Log on โดย</p>
<p>User Name : UserID ที􀃉เมลแจ้ง</p>
<p>Password : รหัสผ่านที􀃉เมลแจ้ง</p>
<p><img src="images/how/FTP-File03.png"></p>
<p>4. แสดงการเข้าถึง Folder จากที่ได้ผ่านการ Log on เข้ามา</p>
<p><img src="images/how/FTP-File04.png"></p>
<p>5. ทำการสร้าง Folder ตามชือ􀃉 ของเลขทีใ􀃉 บสัง􀃉 ซื􀃊อที􀃉ได้จากการสร้างในเว็บ PrintingSolution</p>
<p><img src="images/how/FTP-File05.png"></p>
<p><img src="images/how/FTP-File5.2.png"></p>
<p>6. โยนไฟล์ Art work ทีไ􀃉 ด้จากลูกค้า เข้า Folder ถือเป็นขัน􀃊 ตอนสุดท้ายในการส่งงานเข้า Folder</p>
<p><img src="images/how/FTP-File06.png"></p>

<%--<p style="border-top: 1px solid #999; padding-top:20px; font-weight:bold;">วิธีส่งไฟล์งานพิมพ์ผ่าน <img src="images/how/logo-yousendit.gif" style="vertical-align:bottom;"></p>
<p>
<ul>
<li>1. 	เข้าเว็บไซต์ <a href="http://www.yousendit.com" target="_blank">www.yousendit.com</a></li>
<li>2. 	กรอกข้อมูลในช่อง "Send a file" ตามรายการดังนี้
	<ul>
	<li>a) From: กรอกอีเมลของท่าน</li>
    <li>b) To: <b>printing@officemate.co.th</b></li>
    <li>c) คลิกที่ปุ่ม <img src="images/how/select-file.gif" style="vertical-align:middle;"> เพื่อเลือกไฟล์ที่ต้องการส่ง (ไฟล์ขนาดไม่เกิน 100MB.)</li>
    <li>d) Subject: กรอกข้อมูลว่า <b>"สั่งงานพิมพ์ trendyprint.net"</b></li>
    <li>e) Message: แจ้งรายละเอียดของงาน จำนวนที่ต้องการ และลงรายละเอียดของท่าน</li>
    <li>f) คลิกปุ่ม <img src="images/how/send-it.gif" style="vertical-align:middle;"> และรอจนไฟล์อัพโหลดครบ 100%</li>
	</ul>
    <p><b>เพียงแค่นี้ก็เสร็จสิ้นขั้นตอน หลังจากนั้นทางเจ้าหน้าที่จะติดต่อกลับ เพื่อยืนยันการสั่งงานพิมพ์อีกครั้ง</b></p>
</li>
</ul>
</p>--%>
<p style="border-top: 1px solid #999; padding-top:20px;"><b>ลิงค์ที่เกี่ยวข้อง</b></p>
<p>
<ul>
<li><a href="FAQ.aspx">คำถามพบบ่อย (FAQ)</a></li>
<li><a href="ContactUs.aspx">ติดต่อเรา</a></li>
</ul>
</p>
    	
</div>   	
</asp:Content>

