﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucConfirmOrderUploadPreview.ascx.vb"
	Inherits="uc_ucConfirmOrderUploadPreview" %>
<%@ Register Src="ucHead1.ascx" TagName="ucHead1" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Untitled Document</title>
</head>
<body>
	<div align="left">
		<table align="center" border="0">
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="0" style="width: 700px">
						<tr>
							<td style="width: 260px; height: 70px;">
								<asp:Image ID="Image1" runat="server" ImageUrl="../images/logo-trendyprint.png" />
							</td>
							<td style="width: 440px; height: 70px;">
								<p style="font-family: Tahoma; color: #666666; font-size: 11px;">
									<b>บริษัท ซีโอแอล จำกัด (มหาชน)</b><br />
									24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ 10250
									<br />
									โทร : 02-739-5555(120 สาย) แฟ็กซ์ : 02-763-5555(30 สาย)<br />
									Website : www.officemate.co.th, e-Mail : <a href="printing@officemate.co.th">printing@officemate.co.th</a>
								</p>
							</td>
						</tr>
					</table>
					<table width="680" border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								<br />
								<strong>เลขที่ใบสั่งทำของท่านคือ </strong>
								<asp:Label ID="lbOrderID" runat="server" Text="lbOrderID" Font-Bold="True"></asp:Label>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								วันที่ทำการสั่ง :
								<asp:Label ID="lbDateOrder" runat="server" Text="lbDateOrder"></asp:Label><br />
							</td>
						</tr>
						<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
							<td bgcolor="#CCCCCC" style="font-family: Tahoma; color: #666666; font-size: 11px">
								<table border="0" cellspacing="1" cellpadding="5" style="width: 700px">
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td width="70px" bgcolor="#FFFFFF">
											รหัสลูกค้า:
										</td>
										<td width="220px" bgcolor="#FFFFFF">
											<span class="hilight">
												<asp:Label ID="lbCustID" runat="server" Text="lbCustID"></asp:Label>
											</span>
										</td>
										<td width="70px" bgcolor="#FFFFFF">
											ชื่อลูกค้า:
										</td>
										<td width="260px" bgcolor="#FFFFFF">
											<asp:Label ID="lbCustName" runat="server" Text="lbCustName"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
											เบอร์มือถือ:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbMobile" runat="server" Text="lbMobile"></asp:Label>
										</td>
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
							<td bgcolor="#FFFFFF" style="font-family: Tahoma; color: #666666; font-size: 11px">
								&nbsp;
							</td>
						</tr>
						<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
							<td bgcolor="#CCCCCC" style="font-family: Tahoma; color: #666666; font-size: 11px">
								<table border="0" cellspacing="1" cellpadding="5" style="width: 700px">
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td width="70px" bgcolor="#FFFFFF">
											สถานที่จัดส่ง:
										</td>
										<td width="220px" bgcolor="#FFFFFF">
											<asp:Label ID="lbAddress1" runat="server" Text="lbAddress1"></asp:Label>
										</td>
										<td width="70px" bgcolor="#FFFFFF">
											ผู้รับสินค้า:
										</td>
										<td width="260px" bgcolor="#FFFFFF">
											<asp:Label ID="lbContactName" runat="server" Text="lbContactName"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbAddress2" runat="server" Text="lbAddress2"></asp:Label>
										</td>
										<td bgcolor="#FFFFFF">
											อีเมล์:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbContactEmail" runat="server" Text="lbContactEmail"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbAddress3" runat="server" Text="lbAddress3"></asp:Label>
										</td>
										<td bgcolor="#FFFFFF">
											เบอร์มือถือ:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbContactMobileNo" runat="server" Text="lbContactMobileNo"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbAddress4" runat="server" Text="lbAddress4"></asp:Label>
										</td>
										<td bgcolor="#FFFFFF">
											เบอร์โทร:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbContactPhoneNo" runat="server" Text="lbContactPhoneNo"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
										</td>
										<td bgcolor="#FFFFFF">
											เบอร์แฟกซ์:
										</td>
										<td bgcolor="#FFFFFF">
											<asp:Label ID="lbContactFaxNo" runat="server" Text="lbContactFaxNo"></asp:Label>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								<br />
								<strong>สรุปรายการที่สั่งทำ:</strong>
							</td>
						</tr>
						<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
							<td bgcolor="#CCCCCC" style="font-family: Tahoma; color: #666666; font-size: 11px">
								<table width="100%" border="0" cellspacing="1" cellpadding="5">
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td bgcolor="#C9E4C9">
											<div align="center">
												<strong>รายละเอียด</strong></div>
										</td>
										<td bgcolor="#C9E4C9">
											<div align="center">
												ราคา/หน่วย<br />
												บาท</div>
										</td>
										<td bgcolor="#C9E4C9">
											<div align="center">
												จำนวนที่สั่งทำ<br />
												(ใบ)</div>
										</td>
										<td bgcolor="#C9E4C9">
											<div align="center">
												จำนวนเงิน<br />
												(บาท)</div>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											<%If Type = "C" Then%>
											รหัสกระดาษ:
											<%Else%>
											รหัสวัสดุ :
											<%End If%>
											<asp:Label ID="lbPaperID" runat="server" Text="lbPaperID"></asp:Label>
										</td>
										<td align="right" bgcolor="#FFFFFF" width="100px">
											<asp:Label ID="lbPrice" runat="server" Text="lbPrice"></asp:Label>
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbNumOrder" runat="server" Text="lbNumOrder"></asp:Label>
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbTotal" runat="server" Text="lbTotal"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											<%If Type = "C" Then%>
											ชนิดกระดาษ:
											<%Else%>
											ชนิดวัสดุ:
											<%End If%>
											<asp:Label ID="lbPaperName" runat="server" Text="lbPaperName"></asp:Label>
										</td>
										<td bgcolor="#ffffff" colspan="2" style="text-align: right">
										</td>
										<td bgcolor="#ffffff" style="text-align: right">
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											<%If Type = "C" Then%>
											บริการตัดมุม:
											<%Else%>
											ประเภทงาน:
											<%End If%>
											<asp:Label ID="OptionalDesc2" runat="server" Text="OptionalDesc2"></asp:Label>
										</td>
										<td colspan="2" align="right" bgcolor="#FFFFFF">
											<%If Type = "C" Then%>
											ค่าบริการ:
											<%End If%>
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<%If Type = "C" Then%>
											<asp:Label ID="OptionalPrice2" runat="server" Text="OptionalPrice2"></asp:Label>
											<%End If%>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											<%If Type = "C" Then%>
											บริการเคลือบ:
											<%Else%>
											ขนาดที่ต้องการ:
											<%End If%>
											<asp:Label ID="OptionalDesc1" runat="server" Text="OptionalDesc1"></asp:Label>
										</td>
										<td colspan="2" align="right" bgcolor="#FFFFFF">
											<%If Type = "C" Then%>
											ค่าบริการ:
											<%End If%>
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<%If Type = "C" Then%>
											<asp:Label ID="OptionalPrice1" runat="server" Text="OptionalPrice1"></asp:Label>
											<%End If%>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
											การชำระเงิน :
											<asp:Label ID="lbPayMentCode" runat="server" Text="lbPayMentCode"></asp:Label>
										</td>
										<td bgcolor="#ffffff" colspan="2" style="text-align: right; height: 25px;">
											ราคาสุทธิ (ก่อน vat) :
										</td>
										<td align="right" bgcolor="#ffffff" style="width: 100px; height: 25px;">
											<asp:Label ID="lbPriceExcVat" runat="server" Text="lbPriceExcVat"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
										</td>
										<td colspan="2" align="right" bgcolor="#FFFFFF">
											ภาษีมูลค่าเพิ่ม 7% :
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbVat" runat="server" Text="lbVat"></asp:Label>
										</td>
									</tr>
									<tr>
										<td align="left" bgcolor="#FFFFFF">
										</td>
										<td colspan="2" bgcolor="#FFFFFF" align="right">
											ค่าจัดส่ง :
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px;">
											<asp:Label ID="lbDelivery" runat="server" Text="lbDelivery"></asp:Label>
										</td>
									</tr>
									<tr style="font-size: 11px; color: #666666; font-family: Tahoma">
										<td align="left" bgcolor="#FFFFFF" style="height: 23px">
										</td>
										<td align="right" colspan="2" bgcolor="#FFFFFF" style="height: 23px">
											จำนวนเงินรวมทั้งหมด:
										</td>
										<td align="right" bgcolor="#FFFFFF" style="width: 100px; height: 23px;">
											<asp:Label ID="lbSum" runat="server" Text="lbSum"></asp:Label>
										</td>
									</tr>
									<tr>
										<td bgcolor="#FFFFFF" colspan="4">
											<div align="right">
												*ราคานี้ยังไม่รวมค่าออกแบบ หรือค่าบริการอื่นๆหากมีจะแจ้งให้ทราบภายหลังในทันที</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#CCCCCC" style="font-family: Tahoma; color: #666666; font-size: 11px">
								<div class="box-general">
									<asp:Label ID="lbDetailFTP" runat="server" Text=""></asp:Label></div>
							</td>
						</tr>
						<tr>
							<td style="font-size: 11px; color: #666666; font-family: Tahoma">
								<strong>ไฟล์ที่แนบ:</strong>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
								<asp:DataList ID="dlUploadFile" runat="server" BackColor="White" BorderColor="#336666"
									BorderStyle="Double" BorderWidth="3px" CellPadding="3" GridLines="Both" RepeatColumns="1"
									RepeatDirection="Horizontal" Width="100%">
									<FooterStyle BackColor="White" ForeColor="#000066" />
									<SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
									<ItemTemplate>
										<table width="100%">
											<tr>
												<td style="font-size: 11px; color: #666666; font-family: Tahoma; width: 100px;">
													<asp:Label ID="Label3" runat="server" Text='<%# eval("PreviousName") %>'></asp:Label>
												</td>
												<td style="font-size: 11px; color: #666666; font-family: Tahoma; width: 150px;">
													<asp:Label ID="Label5" runat="server"></asp:Label>
												</td>
											</tr>
										</table>
										<asp:HiddenField ID="hidReadyToPrint" runat="server" Value='<%# eval("Ready2Print") %>' />
									</ItemTemplate>
									<ItemStyle ForeColor="#666666" HorizontalAlign="Left" Font-Size="Small" />
									<HeaderTemplate>
										<table width="100%">
											<tr>
												<td style="font-size: 11px; color: #666666; font-family: Tahoma">
													ชื่อไฟล์
												</td>
												<td style="width: 3px">
												</td>
												<td style="font-size: 11px; color: #666666; font-family: Tahoma">
													ประเภทไฟล์
												</td>
											</tr>
										</table>
									</HeaderTemplate>
									<HeaderStyle BackColor="#C9E4C9" Font-Bold="True" ForeColor="DimGray" />
								</asp:DataList>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px; height: 90px;" valign="top">
								<br />
								<strong>หมายเหตุลูกค้า:</strong><br />
								<asp:TextBox ID="lbDescription" runat="server" ReadOnly="True" TextMode="MultiLine"
									Height="80px" Width="500px"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td style="font-family: Tahoma; color: #666666; font-size: 11px">
							</td>
						</tr>
						<tr>
							<td style="font-size: 11px; color: #666666; font-family: Tahoma; height: 17px">
								<br />
								<strong>Link File Attach:</strong>
								<asp:DataList ID="dlAttachFile" runat="server" BackColor="White" BorderColor="#336666"
									BorderStyle="Double" BorderWidth="3px" CellPadding="3" GridLines="Both" RepeatColumns="1"
									RepeatDirection="Horizontal" Width="100%" ShowHeader="False">
									<FooterStyle BackColor="White" ForeColor="#000066" />
									<SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
									<ItemTemplate>
										<table width="100%">
											<tr>
												<td colspan="2" style="font-size: 11px; color: #666666; font-family: Tahoma; height: 18px">
													Link File Attach:<asp:Label ID="Label3" runat="server" Text='<%# eval("PreviousName") %>'></asp:Label>
												</td>
											</tr>
											<tr>
												<td style="font-size: 11px; color: #666666; font-family: Tahoma; height: 18px;" colspan="2">
													<asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
												</td>
											</tr>
										</table>
										<asp:HiddenField ID="hidimgUrl" runat="server" Value='<%# eval("imageUrl") %>' />
										<asp:HiddenField ID="hidContentType" runat="server" Value='<%# eval("ContentType") %>' />
									</ItemTemplate>
									<ItemStyle ForeColor="#666666" HorizontalAlign="Left" Font-Size="Small" />
									<HeaderTemplate>
										<table width="100%">
											<tr>
												<td style="font-size: 11px; color: #666666; font-family: Tahoma">
													ชื่อไฟล์
												</td>
												<td style="width: 3px">
												</td>
												<td style="font-size: 11px; color: #666666; font-family: Tahoma">
													ประเภทไฟล์
												</td>
											</tr>
										</table>
									</HeaderTemplate>
									<HeaderStyle BackColor="#C9E4C9" Font-Bold="True" ForeColor="DimGray" />
								</asp:DataList>
							</td>
						</tr>
						<tr>
							<td style="font-size: 11px; color: #666666; font-family: Tahoma; height: 17px">
								<br />
								<strong>Link&nbsp;ใบ RO:</strong><br />
								<asp:Label ID="lbLinkRO" runat="server" Text=""></asp:Label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
