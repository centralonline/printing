﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucInkjetSelectPaper.ascx.vb"
    Inherits="uc_ucInkjetSelectPaper" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="ucHeadPanel1.ascx" TagName="ucHeadPanel1" TagPrefix="uc1" %>
<style type="text/css">
    .style12
    {
        width: 225px;
        height: 23px;
    }
    .style14
    {
        width: 225px;
        height: 22px;
    }
    .style15
    {
        height: 22px;
    }
    .style16
    {
        height: 24px;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
    <contenttemplate>
	<table width="100%">
	<tr>
		<td style="height: 21px; text-align: left" valign="top">
			<p class="head-column">
				รายละเอียด</p>
			<table width="100%">
            <tr>
            <td style="width: 225px">
                ประเภทงาน :
            </td>
            <td>
            
                <asp:DropDownList ID="ddlType" runat="server"  AutoPostBack="true" 
                    AppendDataBoundItems="True" Height="20px" Width="100px" />
            </td>
            </tr>
				<tr>
					<td style="width: 225px; height: 22px;">
						ประเภทวัสดุ :
					</td>
					<td style="height: 22px">
						<asp:DropDownList ID="ddlTypename" runat="server" Height="23px" Width="300px" 
                            AutoPostBack="True" >
					        <asp:ListItem>--SELECT--</asp:ListItem>
                        </asp:DropDownList>
					</td>
				</tr>
                <tr>
					<td style="width: 225px">
						รหัสวัสดุ :
					</td>
					<td>
						<asp:Label ID="lbPaperID" runat="server" Text="Label"></asp:Label>
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 22px;">
                    	<asp:Label ID="lbQtyPrice" runat="server" Text="Label">
						ราคาต่อ 1 ตารางเมตร :</asp:Label>
                        <asp:Label ID="lbQtyDisplay" runat="server" Text="Label">
						ราคาต่อ 1 ชิ้น :</asp:Label>
					</td>
					<td style="height: 22px">
						<asp:Label ID="lbPrice" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px;">
						ราคาโปรโมชั่น :
					</td>
					<td>
						<asp:Label ID="lbPromotion" runat="server" CssClass="hilight" Text="Label"></asp:Label>
						<asp:Label ID="Label1" runat="server" Text="บาท" Visible="False"></asp:Label>
					</td>
				</tr>

				<tr>
					<td class="style12">
						ขนาดที่ต้องการ :
                        </td>
					<td>
                        กว้าง&nbsp;&nbsp;
						<asp:TextBox ID="txtwidth" runat="server" Width="51px" AutoPostBack="True"></asp:TextBox>
                            &nbsp;CM
                            <asp:Label ID="lbwidth" runat="server" ForeColor="Red" Text="*"></asp:Label>
                            </td>
					
				</tr>
                <tr>
                <td class="style12">
                </td>
                <td >
                    ยาว&nbsp;&nbsp;&nbsp;&nbsp; 
                <asp:TextBox ID="txtheight" runat="server" Width="51px" AutoPostBack="True"></asp:TextBox>
                            &nbsp;CM
                    <asp:Label ID="lbheight" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                
                </tr>

				<tr>
					
                    <td class="style16">
                        จำนวนที่สั่งทำ :</td>
                        <td class="style16">
                            <asp:TextBox ID="txtQuantity" runat="server" Width="51px" AutoPostBack="True"></asp:TextBox>
                            &nbsp;ใบ <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label></td>
						
				</tr>
                <tr>
					<td style="width: 225px;">
						ราคาเฉลี่ยต่อชิ้น :
					</td>
					<td>
						<asp:Label ID="lbQty" runat="server" CssClass="hilight" Text=""></asp:Label>
                        บาท</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 22px">
						รวมมูลค่าสินค้า(ก่อน vat) :
					</td>
					<td style="height: 22px">
						<asp:Label ID="lbVat" runat="server" CssClass="hilight" Text=""></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td class="style14">
						ภาษีมูลค่าเพิ่ม 7% :
					</td>
					<td class="style15">
						<asp:Label ID="lbVat_card" runat="server" CssClass="hilight" Text=""></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px">
						ค่าจัดส่ง
					</td>
					<td>
						<asp:Label ID="lbDelivery" runat="server" CssClass="hilight" Text=""></asp:Label>
						บาท
					</td>
				</tr>

				<tr>
					<td style="width: 225px">
						<strong>จำนวนเงินรวมทั้งสิ้น </strong>
					</td>
					<td>
						<asp:Label ID="lbSummary" runat="server" CssClass="hilight"></asp:Label>
						บาท
					</td>
				</tr>
				<tr>
					<td style="width: 225px; height: 21px;">
					</td>
					<td style="height: 21px">
					</td>
				</tr>
				<tr>
					<td colspan="2" style="height: 21px;">
						<b>*หมายเหตุ:</b><br />
                        ไม่รวมค่าบริการติดตั้งชิ้นงานตารางเมตรละ 70 บาท และค่าเดินทาง 15 บาท/กม.<br />
                        กรณีมีการออกแบบหรือจัดทำอาร์ทเวิคสามารถสอบถามราคาได้ที่พนักงาน<br />
                        ยอดการสั่งซื้อ 499 บาทขึ้นไปบริการส่งฟรี<br />
                        กรณียอดสั่งซื้อต่ำกว่า 499 บาท คิดค่าบริการจัดส่ง 50 บาทต่อใบสั่งซื้อ<br />


					</td>
				</tr>
			</table>
            </td>
            </tr>
            </table>
   </contenttemplate>
</asp:UpdatePanel>
<asp:HiddenField ID="hidSum" runat="server" />
<asp:HiddenField ID="hidVat" runat="server" />
<asp:HiddenField ID="hidSummary" runat="server" />
