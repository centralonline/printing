﻿<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPageAdmin.master" AutoEventWireup="false"
	CodeFile="ManageFile.aspx.vb" Inherits="Admin_ManageFile" Title="Admin ManageFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<link href="Css/AdminTool.css" rel="stylesheet" type="text/css" />
	<fieldset style="padding: 30px; background-color: #fff; margin: auto; width: 1600px">
		<h3 style="width:1480px" class="head-column">
			Manage File</h3>
		<div class="datagrid" style="width: 1500px">
			<table id="tbOrder" class="tableOrder" style="width: 100%">
				<thead>
					<tr style="background-color: #cfe">
						<th align="center" style="width: 100px">
							FolderName
						</th>
						<th align="center" style="width: 100px">
							UserAccount
						</th>
						<th align="center" style="width: 100px">
							Password
						</th>
						<th align="center" style="width: 100px">
							OrderID
						</th>
						<th align="center" style="width: 100px">
							CreateOn
						</th>
						<th align="center" style="width: 100px">
							CreateBy
						</th>
						<th align="center" style="width: 100px">
							UpdateOn
						</th>
						<th align="center" style="width: 100px">
							UpdateBy
						</th>
						<th align="center" style="width: 100px">
							เปิดสิทธิ<br />
							การใช้งาน
						</th>
					</tr>
				</thead>
				<tbody>
					<%	 Dim Count As Integer = 0
						For Each ObjOrder As clsOrder In listOrder
					%>
					<tr style="background-color: #fff">
						<td align="center">
							<%=ObjOrder.FolderName%>
						</td>
						<td align="center">
							<%=ObjOrder.UserAccount%>
						</td>
						<td align="center">
							<%=ObjOrder.Password%>
						</td>
						<td align="center">
							<%=ObjOrder.OrderID%>
						</td>
						<td align="center">
							<%=ObjOrder.CreateOn%>
						</td>
						<td align="center">
							<%=ObjOrder.CreateBy%>
						</td>
						<td align="center">
							<%=ObjOrder.UpdateOn%>
						</td>
						<td align="center">
							<%=ObjOrder.UpdateBy%>
						</td>
						<td align="center">
							<input id="Checkbox<%=Count%>" type="checkbox" class="clsDelete" value="<%=ObjOrder.FolderName%>" />
						</td>
					</tr>
					<%
						Count += 1
						   Next%>
				</tbody>
			</table>
		</div>
		<div align="right" style="width: 1500px">
			<br />
			<input id="btnSave" type="button" value="Save" style="width: 80px;" />
			<br />
		</div>
	</fieldset>
	<script language="javascript" type="text/javascript">

		$(function () {
			$("#btnSave").click(function () {
				var FolderName = "";
				$(".clsDelete:checked").each(function () {
					FolderName += "'" + $(this).attr("value") + "',";
				})
				$.ajax({
					type: "POST",
					url: "ajManageFlagFile.aspx",
					data: { "FolderName": FolderName },
					success: function (msg) {
						alert(msg);
						window.location = "ManageFile.aspx";
					}
				});
			});
		});
	</script>
</asp:Content>
