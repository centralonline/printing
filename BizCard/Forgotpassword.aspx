﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ForgotPassword.aspx.vb" Inherits="forgotpassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Forgot Password - นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link href="Css/BCCss.css" rel="stylesheet" type="text/css" />
<link href="Css/design.css" rel="stylesheet" type="text/css" />
<link href="Css/print.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
       var _gaq = _gaq || [];
           _gaq.push(['_setAccount', 'UA-11520015-3']);
           _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();
    </script>    
    <script language="javascript">
        function newLocation(){
            window.location.href = "DesignCard.aspx";
        }
    
    </script>
<style type="text/css">
.style1 { color: #FF0000;}
.column-register {text-align:left;}
.clear { clear:both;}
</style>
</head>
<body>

<div align="center">
    <div style="width:800px; vertical-align:bottom; padding:20px 0px;" align="left">    
    <a href="main.aspx" target="_self"><img src="images/logo-trendyprint.png" border="0" alt="trendyprint.net" title="นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์" /></a>
    	<div style="float:right; width:100px; vertical-align:bottom; text-align:right;">< <a href="main.aspx">กลับหน้าแรก</a></div>
    
    </div>
    <div align="left" style="width:800px; border-top:solid 1px #999999; padding-top:10px;">
    	<form id="form1" runat="server">
		<div>
		<asp:Label ID="labText1" runat="server" Text="Label"><font color="#ff6600"><b>ลืมรหัสผ่าน</b></font></asp:Label><p>
		<asp:Label ID="labText2" runat="server" Text="Label">กรุณากรอก e-Mail Address ของท่าน แล้วคลิ๊ก "Send"</asp:Label><br />
		<asp:Label ID="labText3" runat="server" Text='ระบบจะทำการส่ง "รหัสผ่าน" ไปยัง e-Mail ของท่านค่ะ'></asp:Label><br /><br />
		<%--<p style="height: 32px; width: 446px">--%>
		<asp:Label ID="labText4" runat="server" Text="Label"><b>e-Mail Address</b> :</asp:Label><br /><br />
		<asp:TextBox ID="txtEmail" runat="server" CssClass="input_contact" Width="240px"></asp:TextBox>
		<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
		ErrorMessage="No Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator><br />
		<asp:Label ID="labError" runat="server" Width="300px"></asp:Label>
		&nbsp;<br /><br />
		<asp:Button ID="btnSend" runat="server" Text="Send" style="background-color:#0054a6; padding: 5px; font: Tahoma; font-size: 13px; color: #ffffff; font-weight: bold; cursor: pointer; border: 0px; width:100px; " /><br />
		</div>
		</form>
    </div>
</div>
</body>
</html>
