﻿Imports System.Data
Imports EnumTrendyPrint
Imports System.Collections.Generic
Imports System.IO

Partial Class ucDisplay
	Inherits clsUCBase

	Private DsImageValue As DataSet
	Public Property DsImage() As DataSet
		Get
			Return DsImageValue
		End Get
		Set(ByVal value As DataSet)
			DsImageValue = value
		End Set
	End Property

	Private DisplayIDValue As String
	Public Property DisplayID() As String
		Get
			Return DisplayIDValue
		End Get
		Set(ByVal value As String)
			DisplayIDValue = value
		End Set
	End Property

	Private DsDetailValue As DataSet
	Public Property DsDetail() As DataSet
		Get
			Return DsDetailValue
		End Get
		Set(ByVal value As DataSet)
			DsDetailValue = value
		End Set
	End Property

	Private PaperIDValue As String
	Public Property PaperID() As String
		Get
			Return PaperIDValue
		End Get
		Set(ByVal value As String)
			PaperIDValue = value
		End Set
	End Property

	Private UrlValue As String
	Public Property Url() As String
		Get
			Return UrlValue
		End Get
		Set(ByVal value As String)
			UrlValue = value
		End Set
	End Property

	Private txtPaperValue As String
	Public Property txtPaper() As String
		Get
			Return txtPaperValue
		End Get
		Set(ByVal value As String)
			txtPaperValue = value
		End Set
	End Property



	Protected Sub Page_Load(sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If IsPostBack = False Then
			Dim TempDisplayEngine As New clsDisplayEngine
			DsImage = TempDisplayEngine.GetImage(PaperID)

			Me.dlDisplay.RepeatColumns = 3
			Me.dlDisplay.DataSource = DsImage.Tables(0)
			Me.dlDisplay.DataBind()

			If Session("PaperID") <> "" Then
				PaperID = Session("PaperID").ToString()
				DisplayData(PaperID, Me.DisplayID)
				Session("PaperID") = ""
			End If
		End If
	End Sub

	Protected Sub dlDisplay_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlDisplay.ItemDataBound

		CType(e.Item.FindControl("txtCatID"), Label).Visible = False
		CType(e.Item.FindControl("txtCatName"), Label).Visible = False
		CType(e.Item.FindControl("txtPaperID"), Label).Visible = False

	End Sub

	Protected Sub Item_Command(sender As Object, e As DataListCommandEventArgs)

		Session("Url") = CType(e.Item.FindControl("Image1"), Image).ImageUrl
		Me.DisplayID = CType(e.Item.FindControl("txtCatID"), Label).Text
		Me.txtPaper = CType(e.Item.FindControl("txtPaperID"), Label).Text


		GetPriview()
		DisplayData(PaperID, Me.DisplayID)

	End Sub

	Private Sub DisplayData(ByVal PaperID As String, ByVal DisplayID As String)
		If PaperID <> "" Then
			txtFieldValue.Text = Me.PaperID
		End If
		If DisplayID <> "" Then
			txtFieldValue.Text = Me.txtPaper
		End If

		Dim TempDisplay As New clsDisplayEngine
		DsDetail = TempDisplay.GetImgDisplayDetail(PaperID, DisplayID)
		Me.dlDetail.DataSource = DsDetail.Tables(0)
		Me.dlDetail.DataBind()
		If DsDetail.Tables(0).Rows.Count() = 0 Then
			lbError.Text = "ไม่มีรหัสกระดาษนี้ในระบบค่ะ"
			Session("oBitmapZ") = ""
			Me.imgDisplay.Visible = False
		Else
			GetPriview()
		End If

	End Sub


	Private Sub GetPriview()
		If PaperID <> "" Then
			Me.Url = DsDetail.Tables(0).Rows(0)(6).ToString()
			Session("Url") = ""
		End If

		If DisplayID <> "" Then
			Me.Url = Session("Url")
		End If

		Dim Path As String = Server.MapPath(".")
		Dim TempDisplayEngine As New clsDisplayEngine
		Session("oBitmapZ") = TempDisplayEngine.GetDisplayPreview(Me.Url, Path, 1.5)

		Me.imgDisplay.ImageUrl = "~/FileSenderZ.aspx"
		Me.imgDisplay.Visible = True
		lbError.Text = ""

	End Sub

	Protected Sub btnPurchase_Click(sender As Object, e As System.EventArgs) Handles btnPurchase.Click

		Me.PaperID = txtFieldValue.Text
		DisplayData(PaperID, DisplayID)

	End Sub

	Protected Sub LinkPaperID_Click(sender As Object, e As System.EventArgs) Handles LinkFile.Click
		Me.PaperID = txtFieldValue.Text
		If Me.PaperID.Substring(0, 2) = "SC" Then
			Response.Redirect("DetailPaper.aspx")
		ElseIf Me.PaperID.Substring(0, 2) = "SD" Then
			Response.Redirect("DigitalDetailPaperUpload.aspx")
		ElseIf Me.PaperID.Substring(0, 2) = "SJ" Then
			Response.Redirect("DetailAllpaper.aspx")
		End If

	End Sub
End Class