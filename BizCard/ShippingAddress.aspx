﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
	CodeFile="ShippingAddress.aspx.vb" Inherits="frmShippingAddress" Title="ระบุสถานที่จัดส่ง" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
	Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="uc/ucShipping.ascx" TagName="ucShipping" TagPrefix="uc1" %>
<%@ Register Src="uc/ucConditionCard.ascx" TagName="ucConditionCard" TagPrefix="uc12" %>
<%@ Register Src="uc/ucBar.ascx" TagName="ucBar" TagPrefix="uc11" %>
<%@ Register Src="uc/ucProduct.ascx" TagName="ucProduct" TagPrefix="uc7" %>
<%@ Register Src="uc/ucProfileCard.ascx" TagName="ucProfileCard" TagPrefix="uc9" %>
<%@ Register Src="uc/ucProductUpload.ascx" TagName="ucProductUpload" TagPrefix="uc10" %>
<%@ Register Src="uc/UcCustomer.ascx" TagName="UcCustomer" TagPrefix="uc5" %>
<%@ Register Src="uc/ucCard.ascx" TagName="ucCard" TagPrefix="uc6" %>
<%@ Register Src="uc/ucContact.ascx" TagName="ucContact" TagPrefix="uc2" %>
<%@ Register Src="uc/ucPayMents.ascx" TagName="ucPayMents" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<script language="javascript" type="text/javascript">
		function confirmOrder() {
			var chk = document.getElementById("ctl00_ctMasterCenter_CheckBox1").checked;
			//alert(chk);
			if (chk == false) {    // ยังไม่ได้  Check ยืนยันการสั่งทำนามบัตร
				var response3 = confirm("คุณยังไม่ได้ยืนยันการสั่งทำนามบัตรต้องการยืนยันหรือไม่?");

				if (response3 == false) {
					//alert("TEST");
					var response2 = confirm("คุณต้องการเก็บรูปแบบนามบัตรที่ทำไว้ใน Gallery หรือไม่ ?");
					if (response2 == true) {
						document.getElementById("ctl00_ctMasterCenter_hd1").value = "2";
						//   alert(document.getElementById("ctl00_ctMasterCenter_hd1").value);
					}
				}

			}
			if (chk == true) {

				//alert("xcccxxxxx");
				var response1 = confirm("คุณต้องการยืนยันการสั่งทำหรือไม่?");
				if (response1 == false) {
					var response4 = confirm("คุณต้องการเก็บรูปแบบนามบัตรที่ทำไว้ใน Gallery หรือไม่ ?");
					if (response4 == true) {
						document.getElementById("ctl00_ctMasterCenter_hd1").value = "2";
						//alert(document.getElementById("ctl00_ctMasterCenter_hd1").value);
					}
				}
				else {
					document.getElementById("ctl00_ctMasterCenter_hd1").value = "1";
					//alert(document.getElementById("ctl00_ctMasterCenter_hd1").value);
				}

			}

		}

		function limitCharsLength(Object, MaxLen) {
			if (Object.value.length > MaxLen - 1) {
				Object.value = Object.value.substring(0, MaxLen);
			}
		}
 
	</script>
	<div style="margin: auto;">
		<img src="images/h-make-card01.png" class="png" alt="ทำนามบัตร" title="ทำนามบัตร" /><br />
	</div>
	<div style="padding: 30px; background-color: #fff; margin: auto; width: 920px;">
		<uc11:ucBar ID="UcBar1" runat="server" />
		<h1>
			บริการสั่งพิมพ์นามบัตรออนไลน์ | ประเภท Template Card</h1>
		<table>
			<tr>
				<td style="width: 40%;">
					<div class="box-general" style="height: 200px;">
						<uc2:ucContact ID="ucContact1" runat="server" />
					</div>
				</td>
				<td style="width: 40%;">
					<div class="box-general" style="height: 200px;">
						<uc5:UcCustomer ID="ucCustomer1" runat="server" />
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="box-general" style="height: 200px;">
						<uc1:ucShipping ID="ucShipping1" runat="server" />
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="box-general">
						<uc4:ucPayMents ID="ucPayMents1" runat="server" />
					</div>
				</td>
			</tr>
		</table>
		<br />
		<b>หมายเหตุลูกค้า:</b> ท่านสามารถระบุข้อความที่ต้องการแจ้งถึงออฟฟิศเมทได้ที่ช่องหมายเหตุนี้ค่ะ<br />
		<asp:TextBox ID="txtRemark" runat="server" Width="360px" Height="80" onkeypress="javascript: return limitCharsLength(this,250);"
			onblur="javascript:return limitCharsLength(this, 250)" onchange="javascript : return limitCharsLength(this, 250)"
			TextMode="MultiLine"></asp:TextBox>
		<br />
		
		<br />
		<div class="box-general" style="font-size: 80%;">
			<uc12:ucConditionCard ID="UcConditionCard1" runat="server" />
			<div align="center">
				<asp:CheckBox ID="CheckBox1" runat="server" Text="ยืนยันการสั่งทำนามบัตร" ForeColor="Red"
					class="btn-face2" Width="450px" />
			</div>
		</div>
		<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
			<div id="btn-card" align="left">
				<asp:HyperLink ID="HyperLink2" NavigateUrl="~/DetailPaper.aspx" runat="server" class="btn-face2">&lt;&lt; 
                เลือกกระดาษและจำนวนสั่งทำใหม่</asp:HyperLink>
				<asp:ImageButton ID="btnConfirmOrder" ImageUrl="~/images/next_03.gif" runat="server"
					Style="float: right;" />
				<asp:HiddenField ID="hd1" runat="server" />
			</div>
		</div>
	</div>
</asp:Content>
