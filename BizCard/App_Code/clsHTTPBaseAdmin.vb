﻿Imports Microsoft.VisualBasic

Public Class clsHTTPBaseAdmin
    Inherits Page

    Protected objSessionVar As clsSessionVarWC
    Protected objAppVar As clsAppVarWC

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.objAppVar = HttpContext.Current.Application("objAppVar")
        Me.objSessionVar = HttpContext.Current.Session("objSessionVar")
        Me.CheckSessionUser()
    End Sub

    Protected thisTheme As String
    Protected Overrides Sub InitializeCulture()
        'setTheme()
        MyBase.InitializeCulture()
    End Sub

    Public Overrides Property StyleSheetTheme() As String
        Get
            Return Theme
        End Get
        Set(ByVal value As String)
        End Set
    End Property

    Protected Sub CheckSessionUser()
        If Me.objSessionVar.ModelUser Is Nothing Then
            Response.Redirect("Main.aspx")
        End If
    End Sub

End Class
