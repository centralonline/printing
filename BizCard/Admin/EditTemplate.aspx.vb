﻿Imports System.Collections.Generic

Partial Class Admin_EditTemplate
    Inherits clsHTTPBaseAdmin
    Public lisCard As New clsCard
    Public listCategory As New List(Of clsCategories)
    Public listFontFamily As New List(Of clsFontFamily)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim CardEngine As New clsCardEngine
        Dim CardID As String = Request("CardID")

        If CardID = "" Then
            Response.Redirect("ManageFlagCard.aspx")
            Exit Sub
        End If

        lisCard = CardEngine.GetoCardForAdmin(CardID)
        listCategory = CardEngine.GetListCategories()
        listFontFamily = CardEngine.GetListFontFamily()

    End Sub

End Class
