Imports System.Diagnostics
Imports System.Security.Cryptography
Imports System.Text
Imports System.IO

Public Class clsCryptoUtil

    '8 bytes randomly selected for both the Key and the Initialization Vector
    'the IV is used to encrypt the first block of text so that any repetitive 
    'patterns are not apparent
    Private Shared KEY_64() As Byte = {42, 16, 93, 156, 78, 4, 218, 32}
    Private Shared IV_64() As Byte = {55, 103, 246, 79, 36, 99, 167, 3}

    '24 byte or 192 bit key and IV for TripleDES
    Private Shared KEY_192() As Byte = {42, 16, 93, 156, 78, 4, 218, 32, _
            15, 167, 44, 80, 26, 250, 155, 112, _
            2, 94, 11, 204, 119, 35, 184, 197}
    Private Shared IV_192() As Byte = {55, 103, 246, 79, 36, 99, 167, 3, _
            42, 5, 62, 83, 184, 7, 209, 13, _
            145, 23, 200, 58, 173, 10, 121, 222}

    'Standard DES encryption
    Public Shared Function Encrypt(ByVal value As String) As String
        If value <> "" Then
            Dim cryptoProvider As DESCryptoServiceProvider = _
                New DESCryptoServiceProvider()
            Dim ms As MemoryStream = New MemoryStream()
            Dim cs As CryptoStream = _
                New CryptoStream(ms, cryptoProvider.CreateEncryptor(KEY_64, IV_64), _
                    CryptoStreamMode.Write)
            Dim sw As StreamWriter = New StreamWriter(cs)

            sw.Write(value)
            sw.Flush()
            cs.FlushFinalBlock()
            ms.Flush()

            'convert back to a string
            Return Convert.ToBase64String(ms.GetBuffer(), 0, ms.Length)
        End If
        Return Nothing
    End Function


    'Standard DES decryption
    Public Shared Function Decrypt(ByVal value As String) As String
        If value <> "" Then
            Dim cryptoProvider As DESCryptoServiceProvider = _
                New DESCryptoServiceProvider()

            'convert from string to byte array
            Dim buffer As Byte() = Convert.FromBase64String(value)
            Dim ms As MemoryStream = New MemoryStream(buffer)
            Dim cs As CryptoStream = _
                New CryptoStream(ms, cryptoProvider.CreateDecryptor(KEY_64, IV_64), _
                    CryptoStreamMode.Read)
            Dim sr As StreamReader = New StreamReader(cs)

            Return sr.ReadToEnd()
        End If
        Return Nothing
    End Function

    'TRIPLE DES encryption
    Public Shared Function EncryptTripleDES(ByVal value As String) As String
        If value <> "" Then
            Dim cryptoProvider As TripleDESCryptoServiceProvider = _
                New TripleDESCryptoServiceProvider()
            Dim ms As MemoryStream = New MemoryStream()
            Dim cs As CryptoStream = _
                New CryptoStream(ms, cryptoProvider.CreateEncryptor(KEY_192, IV_192), _
                    CryptoStreamMode.Write)
            Dim sw As StreamWriter = New StreamWriter(cs)

            sw.Write(value)
            sw.Flush()
            cs.FlushFinalBlock()
            ms.Flush()

            'convert back to a string
            Return Convert.ToBase64String(ms.GetBuffer(), 0, ms.Length)
        End If
        Return Nothing
    End Function


    'TRIPLE DES decryption
    Public Shared Function DecryptTripleDES(ByVal value As String) As String
        If value <> "" Then
            Dim cryptoProvider As TripleDESCryptoServiceProvider = _
                New TripleDESCryptoServiceProvider()

            'convert from string to byte array
            Dim buffer As Byte() = Convert.FromBase64String(value)
            Dim ms As MemoryStream = New MemoryStream(buffer)
            Dim cs As CryptoStream = _
                New CryptoStream(ms, cryptoProvider.CreateDecryptor(KEY_192, IV_192), _
                    CryptoStreamMode.Read)
            Dim sr As StreamReader = New StreamReader(cs)

            Return sr.ReadToEnd()
        End If
        Return Nothing
    End Function

End Class


Public Class clsCookieUtil

    'SET COOKIE FUNCTIONS *****************************************************

    'SetTripleDESEncryptedCookie - key & value only
    Public Shared Sub SetTripleDESEncryptedCookie(ByVal key As String, _
            ByVal value As String)
        'Convert parts
        key = clsCryptoUtil.EncryptTripleDES(key)
        value = clsCryptoUtil.EncryptTripleDES(value)

        SetCookie(key, value)
    End Sub

    'SetTripleDESEncryptedCookie - overloaded method with expires parameter
    Public Shared Sub SetTripleDESEncryptedCookie(ByVal key As String, _
            ByVal value As String, ByVal expires As Date)
        'Convert parts
        key = clsCryptoUtil.EncryptTripleDES(key)
        value = clsCryptoUtil.EncryptTripleDES(value)

        SetCookie(key, value, expires)
    End Sub


    'SetEncryptedCookie - key & value only
    Public Shared Sub SetEncryptedCookie(ByVal key As String, _
            ByVal value As String)
        'Convert parts
        key = clsCryptoUtil.Encrypt(key)
        value = clsCryptoUtil.Encrypt(value)

        SetCookie(key, value)
    End Sub

    'SetEncryptedCookie - overloaded method with expires parameter
    Public Shared Sub SetEncryptedCookie(ByVal key As String, _
            ByVal value As String, ByVal expires As Date)
        'Convert parts
        key = clsCryptoUtil.Encrypt(key)
        value = clsCryptoUtil.Encrypt(value)

        SetCookie(key, value, expires)
    End Sub


    'SetCookie - key & value only
    Public Shared Sub SetCookie(ByVal key As String, ByVal value As String)
        'Encode Part
        key = HttpContext.Current.Server.UrlEncode(key)
        value = HttpContext.Current.Server.UrlEncode(value)

        Dim cookie As HttpCookie
        cookie = New HttpCookie(key, value)
        SetCookie(cookie)
    End Sub

    'SetCookie - overloaded with expires parameter
    Public Shared Sub SetCookie(ByVal key As String, _
            ByVal value As String, ByVal expires As Date)
        'Encode Parts
        key = HttpContext.Current.Server.UrlEncode(key)
        value = HttpContext.Current.Server.UrlEncode(value)

        Dim cookie As HttpCookie
        cookie = New HttpCookie(key, value)
        cookie.Expires = expires
        SetCookie(cookie)
    End Sub

    'SetCookie - HttpCookie only
    'final step to set the cookie to the response clause
    Public Shared Sub SetCookie(ByVal cookie As HttpCookie)
        HttpContext.Current.Response.Cookies.Set(cookie)
    End Sub

    'GET COOKIE FUNCTIONS *****************************************************

    Public Shared Function GetTripleDESEncryptedCookieValue(ByVal key As String) As String
        'encrypt key only - encoding done in GetCookieValue
        key = clsCryptoUtil.EncryptTripleDES(key)

        'get value 
        Dim value As String
        value = GetCookieValue(key)
        'decrypt value
        value = clsCryptoUtil.DecryptTripleDES(value)
        Return value
    End Function

    Public Shared Function GetEncryptedCookieValue(ByVal key As String) As String
        'encrypt key only - encoding done in GetCookieValue
        key = clsCryptoUtil.Encrypt(key)

        'get value 
        Dim value As String
        value = GetCookieValue(key)
        'decrypt value
        value = clsCryptoUtil.Decrypt(value)
        Return value
    End Function

    Public Shared Function GetEncryptedCookie(ByVal key As String) As HttpCookie
        'encode key for retrieval
        key = clsCryptoUtil.Encrypt(key)
        Return GetCookie(key)
    End Function

    Public Shared Function GetCookie(ByVal key As String) As HttpCookie
        'encode key for retrieval
        key = HttpContext.Current.Server.UrlEncode(key)
        Return HttpContext.Current.Request.Cookies.Get(key)
    End Function

    Public Shared Function GetCookieValue(ByVal key As String) As String
        Try
            'don't encode key for retrieval here
            'done in the GetCookie function

            'get value 
            Dim value As String
            value = GetCookie(key).Value
            'decode stored value
            value = HttpContext.Current.Server.UrlDecode(value)
            Return value
        Catch
        End Try
        Return Nothing
    End Function

End Class

