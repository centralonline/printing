Imports System.Collections.Generic
Imports System.Data
Imports System.Net

Partial Class uc_ucConfirmOrder
	Inherits clsUCBase

	Private OrderIDValue As String
	Public Property OrderID() As String
		Get
			Return OrderIDValue
		End Get
		Set(ByVal value As String)
			OrderIDValue = value
		End Set
	End Property
	Private CardIDValue As String
	Public Property CardID() As String
		Get
			Return CardIDValue
		End Get
		Set(ByVal value As String)
			CardIDValue = value
		End Set
	End Property
	Private CardSizeValue As String
	Public Property CardSize() As String
		Get
			Return CardSizeValue
		End Get
		Set(ByVal value As String)
			CardSizeValue = value
		End Set
	End Property
	Private LogoUrlValue As String
	Public Property LogoUrl() As String
		Get
			Return LogoUrlValue
		End Get
		Set(ByVal value As String)
			LogoUrlValue = value
		End Set
	End Property
	Private SendMailValue As String
	Public Property SendMail() As String
		Get
			Return SendMailValue
		End Get
		Set(ByVal value As String)
			SendMailValue = value
		End Set
	End Property

	Private UserIDValue As String
	Public Property UserID() As String
		Get
			Return UserIDValue
		End Get
		Set(ByVal value As String)
			UserIDValue = value
		End Set
	End Property
	Private ImagePathValue As String
	Public Property ImagePath() As String
		Get
			ImagePathValue = Me.objAppVar.ImagePath
			Return ImagePathValue
		End Get
		Set(ByVal value As String)
			ImagePathValue = value
		End Set
	End Property
	'����������
	Private MailNameValue As String
	Public Property MailName() As String
		Get
			MailNameValue = Me.objAppVar.MailName
			Return MailNameValue
		End Get
		Set(ByVal value As String)
			MailNameValue = value
		End Set
	End Property

	'GIFT
	Private service As String
	Public Property serviceAdd() As String
		Get
			Return service
		End Get
		Set(ByVal value As String)
			service = value
		End Set
	End Property

	Private MailAdminValue As String
	Public Property MailAdmin() As String
		Get
			MailAdminValue = Me.objAppVar.MailAdmin
			Return MailAdminValue
		End Get
		Set(ByVal value As String)
			MailAdminValue = value
		End Set
	End Property

	Protected Overrides Sub OnInit(ByVal e As EventArgs)
		MyBase.OnInit(e)
	End Sub

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		If (Not IsPostBack) Then

			Dim clsOrder As New clsOrderEngine
			Dim listOrder As New List(Of clsOrder)

			Me.btnPrint.Attributes.Add("onclick", "window.print();")
			listOrder = clsOrder.GetInfoOrders(Me.OrderID)

			Dim ClsCardEn As New clsCardEngine
			Dim clsCards As New clsCard
			Dim listclsCard As New List(Of clsCardTextField)

			clsCards = ClsCardEn.GetCardByID(Me.CardID)
			listclsCard = ClsCardEn.GetListCardTextFields(Me.CardID)

			Me.dtTextFields.DataSource = listclsCard
			Me.dtTextFields.DataBind()

			If clsCards Is Nothing Then
				Exit Sub
			End If

			Dim strPaymentDesc As String = ""
			Select Case listOrder.Item(0).PaymentType
				Case "Transfer"
					strPaymentDesc = "�����¡���͹�Թ ��Һѭ�ո�Ҥ��"
				Case "Cash on Delivery"
					strPaymentDesc = "���Թ���·ҧ�¾�ѡ�ҹ�Ϳ������"
				Case "Credit Card EDC"
					strPaymentDesc = "�����Թ���ºѵ��ôԵ�Ѻ��ѡ�ҹ�Ϳ������"
			End Select

			Me.lbCardID.Text = listOrder.Item(0).CardID
			Me.lbPaperName.Text = listOrder.Item(0).PaperName
			Me.lbSize.Text = Me.CardSize
			Me.lbOrderID.Text = listOrder.Item(0).OrderID
			Me.lbDateOrder.Text = listOrder.Item(0).OrderDate.ToString("dd/MM/yyyy HH:mm:ss")
			Me.lbPayMentCode.Text = strPaymentDesc

			If listOrder.Item(0).Promotion = "0.00" Then
				Me.lbPrice.Text = listOrder.Item(0).PaperPrice
			Else
				Me.lbPrice.Text = listOrder.Item(0).Promotion
			End If

			Me.lbNumOrder.Text = listOrder.Item(0).Qty
			Me.lbTotal.Text = listOrder.Item(0).TotalAmt

			'GIFT ���� 14/06/2016
			Me.lbDelivery.Text = listOrder.Item(0).DeliveryAmt
			Me.lbSum.Text = listOrder.Item(0).TotAmt
			Me.lbPriceExcVat.Text = listOrder.Item(0).NetAmt
			Me.lbVat.Text = listOrder.Item(0).VatAmt
			Me.lbPaperID.Text = listOrder.Item(0).PaperID

			Me.lbAddress1.Text = listOrder.Item(0).ShippingAddr1
			Me.lbAddress2.Text = listOrder.Item(0).ShippingAddr2
			Me.lbAddress3.Text = listOrder.Item(0).ShippingAddr3
			Me.lbAddress4.Text = listOrder.Item(0).ShippingAddr4
			Me.lbDescription.Text = listOrder.Item(0).Description

			'GIFT ������¡���ʴ���������´
			'��ԡ�õѴ���
			lbOptionalPrice2.Text = listOrder.Item(0).OptionalPrice2
			lbOptionalDesc2.Text = listOrder.Item(0).OptionalDesc2
			'��ԡ�����ͺ
			lbOptionalPrice1.Text = listOrder.Item(0).OptionalPrice1
			lbOptionalDesc1.Text = listOrder.Item(0).OptionalDesc1

			Me.lbCustID.Text = listOrder.Item(0).CustID
			Me.lbCustName.Text = Me.objSessionVar.ModelUser.UserName
			Me.lbMobile.Text = Me.objSessionVar.ModelUser.Mobile

			Me.lbContactName.Text = Me.objSessionVar.ModelUser.Contact.ContactName
			Me.lbContactMobileNo.Text = Me.objSessionVar.ModelUser.Contact.ContactMobileNo
			Me.lbContactPhoneNo.Text = Me.objSessionVar.ModelUser.Contact.ContactPhoneNo
			Me.lbContactFaxNo.Text = Me.objSessionVar.ModelUser.Contact.ContactFaxNo
			Me.lbContactEmail.Text = Me.objSessionVar.ModelUser.Contact.ContactEmail.ContactEmail

			If (Me.SendMail = "") Then
				Mail2User(listOrder, listclsCard, Me.LogoUrl)
				Mail2Contact(listOrder, listclsCard, Me.LogoUrl)
			End If
			Me.Image2.ImageUrl = "../CardImages/FullSizes/" & Me.CardID & ".jpg"
		End If

	End Sub

	Private Sub Mail2User(ByVal listOrder As List(Of clsOrder), ByVal listclsCard As List(Of clsCardTextField), ByVal UrlLogo As String)
		Dim Message As New Net.Mail.MailMessage()

		Message.From = New Net.Mail.MailAddress(MailName)
        Message.To.Add(Trim(Me.objSessionVar.ModelUser.Contact.ContactEmail.ContactEmail))

		Message.Bcc.Add(New Net.Mail.MailAddress("surapon@officemate.co.th"))
		Message.Bcc.Add(New Net.Mail.MailAddress("waraporn_k@officemate.co.th"))
        Message.Bcc.Add(New Net.Mail.MailAddress("panupan.ja@officemate.co.th"))

		Message.Subject = "Officemate Printing Solution: Print & Delivery Service Center [Order: " & listOrder.Item(0).OrderID & "]"
		Message.IsBodyHtml = True

		Dim SqlStr As String = ""
		SqlStr += "<div align='left'><table align='center' border='0'><tr><td>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='0' cellpadding='0' style='width: 700px'><tr>" & vbNewLine
        SqlStr += "<td style='width: 260px; height: 70px;'><img src='https://printing.officemate.co.th/images/logo-trendyprint.png'></td>" & vbNewLine
		SqlStr += "<td style='width: 440px; height: 70px;'>" & vbNewLine
		SqlStr += "<p style='font-family: Tahoma; color: #666666; font-size: 11px;'>" & vbNewLine
		SqlStr += "<b>����ѷ ������� �ӡѴ (��Ҫ�)</b><br />" & vbNewLine
		SqlStr += "24 �.��͹�ت 66/1 �ǧ�ǹ��ǧ ࢵ�ǹ��ǧ ��ا෾� 10250<br />" & vbNewLine
		SqlStr += "�� : 02-739-5555(120 ���) �硫� : 02-763-5555(30 ���)<br />" & vbNewLine
		SqlStr += "Website : www.officemate.co.th, e-Mail : <a href='mailto:printing@officemate.co.th'>printing@officemate.co.th</a>" & vbNewLine
		SqlStr += "</p></td></tr></table>" & vbNewLine
		SqlStr += "<table width='680' border='0' cellspacing='0' cellpadding='2'>" & vbNewLine
		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px'><br />" & vbNewLine
		SqlStr += "<strong>�Ţ������觷Ӣͧ��ҹ��� </strong>" & listOrder.Item(0).OrderID & "</td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px'>�ѹ���ӡ����� :" & listOrder.Item(0).OrderDate & "<br /></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='1' cellpadding='5' style='width: 700px'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>�����١���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'><span class='hilight'>" & listOrder.Item(0).CustID & "</span></td>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>�����١���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbCustName.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������Ͷ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbMobile.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "</tr></table></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' style='font-family: Tahoma; color: #666666; font-size: 11px'>&nbsp;</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='1' cellpadding='5' style='width: 700px'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>ʶҹ���Ѵ��:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbAddress1.Text & "</td>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>����Ѻ�Թ���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbContactName.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbAddress2.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactEmail.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'><asp:Label ID='lbAddress3' runat='server' Text='lbAddress3'></asp:Label></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������Ͷ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactMobileNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbAddress4.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactPhoneNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>����ῡ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactFaxNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "</table></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td style='font-family: Tahoma; color: #666666; font-size: 11px'><br /><strong>��ػ��¡�÷����觷�:</strong></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellpadding='5' cellspacing='1' width='100%'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'><strong>��������´</strong></div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�Ҥҵ��˹���<br />(�ҷ)</div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�ӹǹ�����觷�<br />(�)</div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�ӹǹ�Թ<br />(�ҷ)</div></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>���ʹ���ѵ� :" & Me.lbCardID.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' width='100px'>" & Me.lbPrice.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbNumOrder.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbTotal.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#ffffff'> ��Ҵ����ѵ� :" & Me.lbSize.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px'>&nbsp;</td></tr>" & vbNewLine

		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#ffffff'>���ʡ�д�� :" & Me.lbPaperID.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px'>&nbsp;</td></tr>" & vbNewLine

		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��Դ��д�� :" & Me.lbPaperName.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px'>&nbsp;</td></tr>" & vbNewLine

		'Gift ������ԡ�õѴ���
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ԡ�õѴ���:" & Me.lbOptionalDesc2.Text & "</td>" & vbNewLine
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>��Һ�ԡ��:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbOptionalPrice2.Text & "</td></tr>" & vbNewLine
		'Gift ��ԡ�����ͺ
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ԡ�����ͺ:" & Me.lbOptionalDesc1.Text & "</td>" & vbNewLine
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>��Һ�ԡ��:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbOptionalPrice1.Text & "</td></tr>" & vbNewLine

		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ê����Թ :" & Me.lbPayMentCode.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right; height: 25px;'>�Ҥ��ط�� (��͹ vat) :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px; height: 25px;'>" & Me.lbPriceExcVat.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' style='text-align: right'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' colspan='2'>������Ť������ 7% :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbVat.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' colspan='2'>��ҨѴ�� :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbDelivery.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ededed' style='height: 24px;' colspan='3'>�ӹǹ�Թ��������� :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ededed' style='height: 24px; width: 100px;'>" & Me.lbSum.Text & "</td></tr>" & vbNewLine
		SqlStr += "</table></td></tr>"
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'><br /><strong>�ٻẺ����ѵ÷���觷� :</strong></td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'>" & vbNewLine
		SqlStr += "<table border='0' align='center' cellpadding='0' cellspacing='0' style='height: 32px;'>" & vbNewLine
		SqlStr += "<tr><td style='width: 14px'><img src='" & ImagePath & "/images/uc/b_gallery_top_left.gif' /></td>" & vbNewLine
		SqlStr += "<td style='background-image: url(" & ImagePath & "/images/uc/b_gallery_top.gif); width: 20px;'></td>" & vbNewLine
		SqlStr += "<td style='width: 14px'><img src='" & ImagePath & "/images/uc/b_gallery_top_right.gif' /></td></tr>" & vbNewLine
		SqlStr += "<tr><td style='width: 14px; background-image: url(" & ImagePath & "/images/uc/b_gallery_left.gif)'></td>" & vbNewLine
		SqlStr += "<td align='center'><img src='cid:Card'></td>" & vbNewLine
		SqlStr += "<td style='width: 14px; background-image: url(" & ImagePath & "/images/uc/b_gallery_right.gif); width: 17px;'></td></tr>" & vbNewLine
		SqlStr += "<tr><td style='width: 14px'><img src='" & ImagePath & "/images/uc/b_gallery_botomleft.gif' /></td>" & vbNewLine
		SqlStr += "<td style='background-image: url(" & ImagePath & "/images/uc/b_gallery_bottom.gif);'></td>" & vbNewLine
		SqlStr += "<td style='width: 14px'><img src='" & ImagePath & "/images/uc/b_gallery_bottom_right.gif' /></td></tr></table></td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'>" & vbNewLine
		SqlStr += "<strong>�����˵��١���: </strong>" & Me.lbDescription.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'>�����˵� : �Ҿ����ʴ�����բ�Ҵ��ҡѺ 1.5 ��Ңͧ��Ҵ��ԧ ���ͤ����Ѵਹ㹡���ʴ������Ź���ѵâͧ��ҹ</td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'>***����ͨѴ������բͧ����ѵ��Ҩ��ᵡ��ҧ�ҡ���</td></tr></table>" & vbNewLine
		SqlStr += "</td></tr></table>" & vbNewLine
		SqlStr += "</td></tr></table></div>" & vbNewLine

		Dim HtmlBody As Net.Mail.AlternateView
		HtmlBody = Net.Mail.AlternateView.CreateAlternateViewFromString(SqlStr, Nothing, "text/html")

		'Dim imgTopdesign As String = ImagePath & "/images/logo-trendyprint.png"
		'''Dim imgTopdesign As String = "http://demo.officemate.co.th/printingsolution/images/logo-trendyprint.png"

		'Dim client2 As New System.Net.WebClient()
		'Dim streamImage As IO.Stream
		'streamImage = client2.OpenRead(imgTopdesign)
		'Dim img1 As New Net.Mail.LinkedResource(streamImage, "image/gif")
		'img1.ContentId = "pic1"
		'HtmlBody.LinkedResources.Add(img1)

		'streamImage = client2.OpenRead(ImagePath & "/CardImages/FullSizes/" & Me.CardID & ".jpg")

		'Dim imgCard As New Net.Mail.LinkedResource(streamImage, "image/gif")
		'imgCard.ContentId = "Card"

		'HtmlBody.LinkedResources.Add(imgCard)

		'Message.AlternateViews.Add(HtmlBody)

		Dim req01 As WebRequest = HttpWebRequest.Create(ImagePath & "/images/logo-trendyprint.png")
		req01.Credentials = CredentialCache.DefaultCredentials
		CType(req01, HttpWebRequest).UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36"
		CType(req01, HttpWebRequest).KeepAlive = True
		CType(req01, HttpWebRequest).ProtocolVersion = HttpVersion.Version10
		Dim rep01 As WebResponse = req01.GetResponse()
		Dim img1 As New Net.Mail.LinkedResource(rep01.GetResponseStream(), "image/gif")
		img1.ContentId = "pic1"
		HtmlBody.LinkedResources.Add(img1)

		Dim req02 As WebRequest = HttpWebRequest.Create(ImagePath & "/CardImages/FullSizes/" & Me.CardID & ".jpg")
		req02.Credentials = CredentialCache.DefaultCredentials
		req02.UseDefaultCredentials = True
		req02.PreAuthenticate = True
		req02.Credentials = CredentialCache.DefaultCredentials
		CType(req02, HttpWebRequest).UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36"
		CType(req02, HttpWebRequest).KeepAlive = True
		CType(req02, HttpWebRequest).ProtocolVersion = HttpVersion.Version10
		Dim rep02 As WebResponse = req02.GetResponse()
		Dim imgCard As New Net.Mail.LinkedResource(rep02.GetResponseStream(), "image/jpeg")
		imgCard.ContentId = "Card"
		HtmlBody.LinkedResources.Add(imgCard)

		Message.AlternateViews.Add(HtmlBody)

		Try
			Dim MailClient As New Net.Mail.SmtpClient()
			MailClient.Host = ConfigurationManager.AppSettings("SmtpClientHost")
			''If (ConfigurationManager.AppSettings("SmtpClientUserName") = "") Then
			MailClient.Credentials = New Net.NetworkCredential(ConfigurationManager.AppSettings("SmtpClientUserName"), ConfigurationManager.AppSettings("SmtpClientPassword"))
			''End If

			MailClient.Send(Message)
		Catch ex As Exception

		End Try

	End Sub
	Private Sub Mail2Contact(ByVal listOrder As List(Of clsOrder), ByVal listclsCard As List(Of clsCardTextField), ByVal UrlLogo As String)
		Dim Message As New Net.Mail.MailMessage()

		Message.From = New Net.Mail.MailAddress(MailName)
		Message.To.Add(New Net.Mail.MailAddress(MailName))

		Message.Bcc.Add(New Net.Mail.MailAddress("printing@officemate.co.th"))
		Message.Bcc.Add(New Net.Mail.MailAddress("surapon@officemate.co.th"))
		''Message.Bcc.Add(New Net.Mail.MailAddress("surachart@officemate.co.th"))
		Message.Bcc.Add(New Net.Mail.MailAddress("waraporn_k@officemate.co.th"))
        Message.Bcc.Add(New Net.Mail.MailAddress("panupan.ja@officemate.co.th"))

		Message.Subject = "Officemate Printing Solution & Delivery Service Center For Admin [Order: " & listOrder.Item(0).OrderID & "]"
		Message.IsBodyHtml = True

		Dim SqlStr As String = ""
		SqlStr += "<div align='left'><table align='center' border='0'><tr><td>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='0' cellpadding='0' style='width: 700px'><tr>" & vbNewLine
        SqlStr += "<td style='width: 260px; height: 70px;'><img src='https://printing.officemate.co.th/images/logo-trendyprint.png'></td>" & vbNewLine
		SqlStr += "<td style='width: 440px; height: 70px;'>" & vbNewLine
		SqlStr += "<p style='font-family: Tahoma; color: #666666; font-size: 11px;'>" & vbNewLine
		SqlStr += "<b>����ѷ ������� �ӡѴ (��Ҫ�)</b><br />" & vbNewLine
		SqlStr += "24 �.��͹�ت 66/1 �ǧ�ǹ��ǧ ࢵ�ǹ��ǧ ��ا෾� 10250<br />" & vbNewLine
		SqlStr += "�� : 02-739-5555(120 ���) �硫� : 02-763-5555(30 ���)<br />" & vbNewLine
		SqlStr += "Website : www.officemate.co.th, e-Mail : <a href='mailto:printing@officemate.co.th'>printing@officemate.co.th</a>" & vbNewLine
		SqlStr += "</p></td></tr></table>" & vbNewLine
		SqlStr += "<table width='680' border='0' cellspacing='0' cellpadding='2'>" & vbNewLine
		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px'><br />" & vbNewLine
		SqlStr += "<strong>�Ţ������觷Ӣͧ��ҹ��� </strong>" & listOrder.Item(0).OrderID & "</td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family: Tahoma; color: #666666; font-size: 11px'>�ѹ���ӡ����� :" & listOrder.Item(0).OrderDate & "<br /></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='1' cellpadding='5' style='width: 700px'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>�����١���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'><span class='hilight'>" & listOrder.Item(0).CustID & "</span></td>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>�����١���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbCustName.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������Ͷ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbMobile.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "</tr></table></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF' style='font-family: Tahoma; color: #666666; font-size: 11px'>&nbsp;</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellspacing='1' cellpadding='5' style='width: 700px'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>ʶҹ���Ѵ��:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbAddress1.Text & "</td>" & vbNewLine
		SqlStr += "<td width='15%' bgcolor='#FFFFFF'>����Ѻ�Թ���:</td>" & vbNewLine
		SqlStr += "<td width='35%' bgcolor='#FFFFFF'>" & Me.lbContactName.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbAddress2.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactEmail.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'><asp:Label ID='lbAddress3' runat='server' Text='lbAddress3'></asp:Label></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������Ͷ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactMobileNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbAddress4.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>������:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactPhoneNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'></td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>����ῡ��:</td>" & vbNewLine
		SqlStr += "<td bgcolor='#FFFFFF'>" & Me.lbContactFaxNo.Text & "</td></tr>" & vbNewLine
		SqlStr += "</table></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td style='font-family: Tahoma; color: #666666; font-size: 11px'><br /><strong>��ػ��¡�÷����觷�:</strong></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#CCCCCC' style='font-family: Tahoma; color: #666666; font-size: 11px'>" & vbNewLine
		SqlStr += "<table border='0' cellpadding='5' cellspacing='1' width='100%'>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'><strong>��������´</strong></div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�Ҥҵ��˹���<br />(�ҷ)</div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�ӹǹ�����觷�<br />(�)</div></td>" & vbNewLine
		SqlStr += "<td bgcolor='#C9E4C9'><div align='center'>�ӹǹ�Թ<br />(�ҷ)</div></td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>���ʹ���ѵ� :" & Me.lbCardID.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' width='100px'>" & Me.lbPrice.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbNumOrder.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbTotal.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#ffffff'> ��Ҵ����ѵ� :" & Me.lbSize.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px'>&nbsp;</td></tr>" & vbNewLine

		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#ffffff'>���ʡ�д�� :" & Me.lbPaperID.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px'>&nbsp;</td></tr>" & vbNewLine

		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��Դ��д�� :" & Me.lbPaperName.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px'>&nbsp;</td></tr>" & vbNewLine

		'Gift ������ԡ�õѴ���
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ԡ�õѴ���:" & Me.lbOptionalDesc2.Text & "</td>" & vbNewLine
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>��Һ�ԡ��:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbOptionalPrice2.Text & "</td></tr>" & vbNewLine
		'Gift ��ԡ�����ͺ
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ԡ�����ͺ:" & Me.lbOptionalDesc1.Text & "</td>" & vbNewLine
		SqlStr += "<td colspan='2' bgcolor='#FFFFFF' align='right'>��Һ�ԡ��:</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF'>" & Me.lbOptionalPrice1.Text & "</td></tr>" & vbNewLine

		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ê����Թ :" & Me.lbPayMentCode.Text & "</td>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' colspan='2' style='text-align: right; height: 25px;'>�Ҥ��ط�� (��͹ vat) :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ffffff' style='width: 100px; height: 25px;'>" & Me.lbPriceExcVat.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td bgcolor='#ffffff' style='text-align: right'></td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' colspan='2'>������Ť������ 7% :</td>" & vbNewLine

		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbVat.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='left' bgcolor='#FFFFFF'>��ê����Թ :" & Me.lbPayMentCode.Text & "</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' colspan='2'>��ҨѴ�� :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#FFFFFF' style='width: 100px;'>" & Me.lbDelivery.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr style='font-size: 11px; color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ededed' style='height: 24px;' colspan='3'>�ӹǹ�Թ��������� :</td>" & vbNewLine
		SqlStr += "<td align='right' bgcolor='#ededed' style='height: 24px; width: 100px;'>" & Me.lbSum.Text & "</td></tr>" & vbNewLine
		SqlStr += "</table></td></tr>"
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'><br /><strong>�ٻẺ����ѵ÷���觷� :</strong></td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'>" & vbNewLine
		SqlStr += "<table border='0' align='center' cellpadding='0' cellspacing='0' style='height: 32px;'>" & vbNewLine
		SqlStr += "<tr><td style='width: 14px'><img src='" & ImagePath & "/images/uc/b_gallery_top_left.gif' /></td>" & vbNewLine
		SqlStr += "<td style='background-image: url(" & ImagePath & "/images/uc/b_gallery_top.gif); width: 20px;'></td>" & vbNewLine
		SqlStr += "<td style='width: 14px'><img src='" & ImagePath & "/images/uc/b_gallery_top_right.gif' /></td></tr>" & vbNewLine
		SqlStr += "<tr><td style='width: 14px; background-image: url(" & ImagePath & "/images/uc/b_gallery_left.gif)'></td>" & vbNewLine
		SqlStr += "<td align='center'><img src='cid:Card'></td>" & vbNewLine
		SqlStr += "<td style='width: 14px; background-image: url(" & ImagePath & "/images/uc/b_gallery_right.gif); width: 17px;'></td></tr>" & vbNewLine
		SqlStr += "<tr><td style='width: 14px'><img src='" & ImagePath & "/images/uc/b_gallery_botomleft.gif' /></td>" & vbNewLine
		SqlStr += "<td style='background-image: url(" & ImagePath & "/images/uc/b_gallery_bottom.gif);'></td>" & vbNewLine
		SqlStr += "<td style='width: 14px'><img src='" & ImagePath & "/images/uc/b_gallery_bottom_right.gif' /></td></tr></table></td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'>" & vbNewLine
		SqlStr += "<strong>�����˵��١���: </strong>" & Me.lbDescription.Text & "</td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'>�����˵� : �Ҿ����ʴ�����բ�Ҵ��ҡѺ 1.5 ��Ңͧ��Ҵ��ԧ ���ͤ����Ѵਹ㹡���ʴ������Ź���ѵâͧ��ҹ</td></tr>" & vbNewLine
		SqlStr += "<tr><td style='font-family:Tahoma; color:#666666; font-size:11px'>***����ͨѴ������բͧ����ѵ��Ҩ��ᵡ��ҧ�ҡ���</td></tr></table>" & vbNewLine
		SqlStr += "</td></tr></table>" & vbNewLine
		SqlStr += "<br /><strong><span style='font-size: 11 px; color: #666666; font-family:Tahoma;'> ��������´��ͤ���������ѵ� : </span><br />" & vbNewLine
		SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'>" & vbNewLine
		For Each obj As clsCardTextField In listclsCard
			If obj.TextValue.ToString <> "" Then
				SqlStr += "<tr><td align='left' bgcolor='#ffffff' style='font-size: 11px; width: 5px; color: #666666; font-family: Tahoma'>" & obj.Seq.ToString & "</td>" & vbNewLine
				SqlStr += "<td align='left' bgcolor='#ffffff' style='font-size: 11px; width: 300px; color: #666666; font-family: Tahoma'>" & obj.FieldName.ToString & "</td>" & vbNewLine
				SqlStr += "<td align='left' bgcolor='#ffffff' style='font-size: 11px; width: 300px; color: #666666; font-family: Tahoma'>" & obj.TextValue.ToString & "</td></tr>" & vbNewLine
			End If
		Next
		SqlStr += "</table>" & vbNewLine
		If UrlLogo <> "" Then
			SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'>" & vbNewLine
			SqlStr += "<tr><td align='left' bgcolor='#ffffff' style='font-size: 11px; color: #666666; font-family: Tahoma'>"
			SqlStr += "Link Attach File: <br />"
			SqlStr += "<a href=""" & "" & ImagePath & "/CardImages/UploadImage/Currents/" & UrlLogo & """>"
			SqlStr += "" & ImagePath & "/CardImages/UploadImage/Currents/" & UrlLogo & "</a>" & vbNewLine
			SqlStr += "</td></tr></table>" & vbNewLine
		End If
		SqlStr += "<table border='1' cellpadding='5' cellspacing='1' width='100%'>" & vbNewLine
		SqlStr += "<tr><td align='left' bgcolor='#ffffff' style='font-size: 11px;color: #666666; font-family: Tahoma'>" & vbNewLine
		SqlStr += "Link � RO : <br />"
		SqlStr += "<a href=""" & "" & ImagePath & "/ConfirmOrderPreview.aspx?Order=" & Me.lbOrderID.Text & "&User=" & Me.UserID & """>"
		SqlStr += "" & ImagePath & "/ConfirmOrderPreview.aspx?Order=" & lbOrderID.Text & " &User=" & Me.UserID & "</a>"
		SqlStr += "</td></tr></table>" & vbNewLine
		SqlStr += "</td></tr></table></div>" & vbNewLine

		Dim HtmlBody As Net.Mail.AlternateView
		HtmlBody = Net.Mail.AlternateView.CreateAlternateViewFromString(SqlStr, Nothing, "text/html")

		'Dim imgTopdesign As String = ImagePath & "/images/logo-trendyprint.png"
		'''Dim imgTopdesign As String = "http://demo.officemate.co.th/printingsolution/images/logo-trendyprint.png"

		'Dim client2 As New System.Net.WebClient()
		'Dim streamImage As IO.Stream
		'streamImage = client2.OpenRead(imgTopdesign) '#1
		'Dim img1 As New Net.Mail.LinkedResource(streamImage, "image/gif")
		'img1.ContentId = "pic1"
		'HtmlBody.LinkedResources.Add(img1)

		'streamImage = client2.OpenRead(ImagePath & "/CardImages/FullSizes/" & Me.CardID & ".jpg")
		'Dim imgCard As New Net.Mail.LinkedResource(streamImage, "image/gif")
		'imgCard.ContentId = "Card"
		'HtmlBody.LinkedResources.Add(imgCard)

		'Message.AlternateViews.Add(HtmlBody)

		Dim req01 As WebRequest = HttpWebRequest.Create(ImagePath & "/images/logo-trendyprint.png")
		req01.Credentials = CredentialCache.DefaultCredentials
		CType(req01, HttpWebRequest).UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36"
		CType(req01, HttpWebRequest).KeepAlive = True
		CType(req01, HttpWebRequest).ProtocolVersion = HttpVersion.Version10
		Dim rep01 As WebResponse = req01.GetResponse()
		Dim img1 As New Net.Mail.LinkedResource(rep01.GetResponseStream(), "image/gif")
		img1.ContentId = "pic1"
		HtmlBody.LinkedResources.Add(img1)

		Dim req02 As WebRequest = HttpWebRequest.Create(ImagePath & "/CardImages/FullSizes/" & Me.CardID & ".jpg")
		req02.Credentials = CredentialCache.DefaultCredentials
		req02.UseDefaultCredentials = True
		req02.PreAuthenticate = True
		req02.Credentials = CredentialCache.DefaultCredentials
		CType(req02, HttpWebRequest).UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36"
		CType(req02, HttpWebRequest).KeepAlive = True
		CType(req02, HttpWebRequest).ProtocolVersion = HttpVersion.Version10
		Dim rep02 As WebResponse = req02.GetResponse()
		Dim imgCard As New Net.Mail.LinkedResource(rep02.GetResponseStream(), "image/jpeg")
		imgCard.ContentId = "Card"
		HtmlBody.LinkedResources.Add(imgCard)

		Message.AlternateViews.Add(HtmlBody)

		Try
			Dim MailClient As New Net.Mail.SmtpClient()
			MailClient.Host = ConfigurationManager.AppSettings("SmtpClientHost")
			''If (ConfigurationManager.AppSettings("SmtpClientUserName") = "") Then
			MailClient.Credentials = New Net.NetworkCredential(ConfigurationManager.AppSettings("SmtpClientUserName"), ConfigurationManager.AppSettings("SmtpClientPassword"))
			''End If

			MailClient.Send(Message)
		Catch ex As Exception

		End Try
	End Sub


End Class

