
Partial Class uc_ucBar
    Inherits clsUCBase


    Private BarStepValue As Integer
    Public Property BarStep() As Integer
        Get
            Return BarStepValue
        End Get
        Set(ByVal value As Integer)
            BarStepValue = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim NonActiveColor As System.Drawing.Color = clsMyUtil.GetColorFromHtml("#cacaca") 'System.Drawing.ColorTranslator.FromHtml("#cacaca")
        Dim ActiveColor As System.Drawing.Color = clsMyUtil.GetColorFromHtml("#1b521b") 'System.Drawing.ColorTranslator.FromHtml("#1b521b")
        Select Case BarStep
            Case 1
                imgStep1.ImageUrl = "~/images/uc/num1.gif"
                imgStep2.ImageUrl = "~/images/uc/num21.gif"
                imgStep3.ImageUrl = "~/images/uc/num31.gif"
                imgStep4.ImageUrl = "~/images/uc/num41.gif"

                Label1.ForeColor = ActiveColor
                Label2.ForeColor = NonActiveColor
                Label3.ForeColor = NonActiveColor
                Label4.ForeColor = NonActiveColor
            Case 2
                imgStep1.ImageUrl = "~/images/uc/num11.gif"
                imgStep2.ImageUrl = "~/images/uc/num2.gif"
                imgStep3.ImageUrl = "~/images/uc/num31.gif"
                imgStep4.ImageUrl = "~/images/uc/num41.gif"

                Label1.ForeColor = NonActiveColor
                Label2.ForeColor = ActiveColor
                Label3.ForeColor = NonActiveColor
                Label4.ForeColor = NonActiveColor
            Case 3
                imgStep1.ImageUrl = "~/images/uc/num11.gif"
                imgStep2.ImageUrl = "~/images/uc/num21.gif"
                imgStep3.ImageUrl = "~/images/uc/num3.gif"
                imgStep4.ImageUrl = "~/images/uc/num41.gif"

                Label1.ForeColor = NonActiveColor
                Label2.ForeColor = NonActiveColor
                Label3.ForeColor = ActiveColor
                Label4.ForeColor = NonActiveColor
            Case 4
                imgStep1.ImageUrl = "~/images/uc/num11.gif"
                imgStep2.ImageUrl = "~/images/uc/num21.gif"
                imgStep3.ImageUrl = "~/images/uc/num31.gif"
                imgStep4.ImageUrl = "~/images/uc/num4.gif"

                Label1.ForeColor = NonActiveColor
                Label2.ForeColor = NonActiveColor
                Label3.ForeColor = NonActiveColor
                Label4.ForeColor = ActiveColor
            Case Else
                imgStep1.ImageUrl = "~/images/uc/num11.gif"
                imgStep2.ImageUrl = "~/images/uc/num21.gif"
                imgStep3.ImageUrl = "~/images/uc/num31.gif"
                imgStep4.ImageUrl = "~/images/uc/num41.gif"

                Label1.ForeColor = NonActiveColor
                Label2.ForeColor = NonActiveColor
                Label3.ForeColor = NonActiveColor
                Label4.ForeColor = NonActiveColor
        End Select
    End Sub
End Class
