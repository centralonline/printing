﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPageAdmin.master" AutoEventWireup="false"
	CodeFile="ViewOrder.aspx.vb" Inherits="Admin_ViewOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ctMasterCenter" runat="Server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {

			$("#txtFromDate").datepicker({ dateformat: 'yy-mm-dd' }); //การเซตวันที่ให้สามารถเลือกเป็นปฏิทินได้
			$("#txtToDate").datepicker({ dateformat: 'yy-mm-dd' });

			$("#btnSearch").click(function () {
				var OrderID = "";
				var FromDate = "";
				var ToDate = "";
				$("#radDate:checked").each(function () {
					$("#txtOrderID").val("");
					FromDate = $("#txtFromDate").val();
					ToDate = $("#txtToDate").val();
					if (FromDate == "" || ToDate == "") {
						alert("กรุณาระบุช่วงเวลาที่ต้องการค้นหาด้วยค่ะ");
						return false;
					}
				});
				$("#radOrderID:checked").each(function () {
					$("#txtFromDate").val("");
					$("#txtToDate").val("");
					OrderID = $("#txtOrderID").val();
					if (OrderID == "") {
						alert("กรุณาระบุเลขที่Order ที่ต้องการค้นหาด้วยค่ะ");
						return false;
					}
				});
				$.ajax({
					type: "POST",
					url: "ShowOrder.aspx",
					data: { "OrderID": OrderID, "FromDate": FromDate, "ToDate": ToDate },
					success: function (data) {
						if (data != "") {
							$('#dvShowOrder').html(data);
						}
					}
				});
			});
		});   
	</script>
	<fieldset style="text-align: left">
		<legend><b>กรุณาระบุข้อมูลที่ต้องการค้นหา</b></legend>
		<table>
			<tr>
				<td>
				</td>
				<td>
					<input id="radDate" type="radio" name="SearchBy" checked="checked" />
					ค้นหาด้วยวันที่สร้าง Order
				</td>
				<td>
					ตั้งแต่วันที่
					<input id="txtFromDate" type="text" style="width: 150px" />
					ถึง
					<input id="txtToDate" type="text" style="width: 150px" />
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<input id="radOrderID" type="radio" name="SearchBy" />
					ค้นหาด้วยเลขที่ Order
				</td>
				<td>
					<input id="txtOrderID" type="text" style="width: 150px" />
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td>
					<input id="btnSearch" type="button" value="Search" style="width: 80px" />
				</td>
			</tr>
		</table>
		<%--<table>
				<tr>
					<td>
					</td>
					<td>
						<input id="radDate" type="radio" name="SearchBy" checked="checked" />
						ค้นหาด้วยวันที่สร้าง Order
					</td>
					<td>
						ตั้งแต่วันที่
						<input id="txtFromDate" type="text" style="width: 150px" />
						ถึง
						<input id="txtToDate" type="text" style="width: 150px" />
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<input id="radOrderID" type="radio" name="SearchBy" />
						ค้นหาด้วยเลขที่ Order
					</td>
					<td>
						<input id="txtOrderID" type="text" style="width: 150px" />
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<input id="btnSearch" type="button" value="Search" style="width: 80px" />
					</td>
				</tr>
			</table>--%>
	</fieldset>
	<div id="dvShowOrder" align="center">
	</div>
</asp:Content>
