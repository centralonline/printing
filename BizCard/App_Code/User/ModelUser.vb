﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports ModelShipping
Imports ModelContact
Imports ModelInvoice
Public Class ModelUser
#Region "Create Property Fields"

	Private UserIDValue As String
	Public Property UserID() As String
		Get
			Return UserIDValue
		End Get
		Set(ByVal value As String)
			UserIDValue = value
		End Set
	End Property

	Private UserGuidValue As String
	Public Property UserGuid() As String
		Get
			Return UserGuidValue
		End Get
		Set(ByVal value As String)
			UserGuidValue = value
		End Set
	End Property

	Private PasswordValue As String
	Public Property Password() As String
		Get
			Return PasswordValue
		End Get
		Set(ByVal value As String)
			PasswordValue = value
		End Set
	End Property

	Private UserNameValue As String
	Public Property UserName() As String
		Get
			Return UserNameValue
		End Get
		Set(ByVal value As String)
			UserNameValue = value
		End Set
	End Property

	Private NickNameValue As String
	Public Property NickName() As String
		Get
			Return NickNameValue
		End Get
		Set(ByVal value As String)
			NickNameValue = value
		End Set
	End Property

    Private MobileValue As String
    Public Property Mobile() As String
        Get
            Return MobileValue
        End Get
        Set(ByVal value As String)
            MobileValue = value
        End Set
    End Property

	Private CustIDValue As String
	Public Property CustID() As String
		Get
			Return CustIDValue
		End Get
		Set(ByVal value As String)
			CustIDValue = value
		End Set
	End Property

    Private UserInfoValue As EnumTrendyPrint.eSystemType
    Public Property UserInfo() As EnumTrendyPrint.eSystemType
        Get
            Return UserInfoValue
        End Get
        Set(ByVal value As EnumTrendyPrint.eSystemType)
            UserInfoValue = value
        End Set
    End Property

    'แพร์เพิ่ม ใช้ check Upload FTP
	Private ActionUploadValue As Boolean
	Public Property ActionUpload() As Boolean
		Get
			Return ActionUploadValue
		End Get
		Set(ByVal value As Boolean)
			ActionUploadValue = value
		End Set
    End Property

    Private InvoiceValue As ModelInvoice
    Public Property Invoice() As ModelInvoice
        Get
            Return InvoiceValue
        End Get
        Set(ByVal value As ModelInvoice)
            InvoiceValue = value
        End Set
    End Property

    Private ContactValue As ModelContact
    Public Property Contact() As ModelContact
        Get
            Return ContactValue
        End Get
        Set(ByVal value As ModelContact)
            ContactValue = value
        End Set
    End Property

    Private ShippingValue As List(Of ModelShipping)
    Public Property Shipping() As List(Of ModelShipping)
        Get
            Return ShippingValue
        End Get
        Set(ByVal value As List(Of ModelShipping))
            ShippingValue = value
        End Set
	End Property

	Private StoreIdValue As String
	Public Property StoreId() As String
		Get
			Return StoreIdValue
		End Get
		Set(ByVal value As String)
			StoreIdValue = value
		End Set
	End Property

#End Region
End Class
