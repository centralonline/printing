﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ucDisplay.ascx.vb" Inherits="ucDisplay" %>
<script language="javascript" type="text/javascript">
// <![CDATA[


// ]]>
</script>
<style>
	.btn
	{
		background-color: #0054A6;
		padding: 4px 10px;
		font-size: 13px;
		color: #FFF;
		cursor: pointer;
		vertical-align: middle;
		text-decoration: none;
		border: 1px solid #003286;
		font-weight: bold;
	}
</style>
<asp:Panel ID="Panel1" runat="server" Width="920px">
	<asp:DataList ID="dlDisplay" runat="server" RepeatColumns="3" Width="350px" OnItemCommand="Item_Command">
		<ItemStyle VerticalAlign="Bottom" CssClass="topic1" />
		<ItemTemplate>
			<div style="padding: 10px;">
				<asp:ImageButton ID="Image1" runat="server" ImageUrl='<%# Eval("ImageURL") %>' BorderWidth="1px"
					BorderColor="silver" BorderStyle="Solid" />
				<br />
				<%--<asp:Image ID="Image2" ImageUrl="~/images/icondesign.gif" runat="server" />--%><span
					style="color: #c0282c;">
					<asp:Label ID="txtCatID" CssClass="des" runat="server" Text='<%# Eval("CategoryID") %>'></asp:Label>
					<asp:Label ID="txtCatName" CssClass="des" runat="server" Text='<%# Eval("CategoryName") %>'></asp:Label></span><br />
				<asp:Label ID="txtPaperID" CssClass="des" runat="server" Text='<%# Eval("PaperID") %>'></asp:Label>
			</div>
		</ItemTemplate>
	</asp:DataList>
</asp:Panel>
<asp:Panel ID="Panel3" runat="server" Width="900px">
	<table style="padding-left: 50px">
		<tr>
			<td style="border: 1px solid #DA9;">
				<div style="background-color: #fff;">
					<div style="float: left; padding: 20px; background-color: #fff; width: 800px">
						รหัสกระดาษ :
						<asp:TextBox ID="txtFieldValue" runat="server" Text='<%#Eval("TextValue") %>' Width="200"></asp:TextBox>
						<asp:Button ID="btnPurchase" runat="server" Text="Search" class="btn" />
						<asp:Label ID="lbError" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
					</div>
					<div style="padding-left: 260px;">
						<asp:Image ID="imgDisplay" runat="server" Visible="false" CssClass="BorderCard" BorderColor="Silver"
							BorderStyle="Solid" BorderWidth="1px" />
					</div>
					<div style="background-color: #fff; float: left; width: 660px; padding-left: 260px;">
						<asp:DataList ID="dlDetail" runat="server" Width="300px">
							<ItemStyle VerticalAlign="Bottom" CssClass="topic1" />
							<ItemTemplate>
								<div style="padding: 10px;">
									<asp:Image ID="Image2" ImageUrl="~/images/icondesign.gif" runat="server" />&nbsp;<span
										style="color: #c0282c;">
										<asp:Label ID="txtCatName" CssClass="des" runat="server" Text='<%# "ชื่อ : " & Eval("DisplayName") %>'></asp:Label></span><br />
									<asp:Label ID="txtCatID" CssClass="des" runat="server" Text='<%# "ขนาด : " & Eval("Size") %>'></asp:Label><br />
									<asp:Label ID="txtMinPrice" CssClass="des" runat="server" Text='<%# "ราคา : " & Eval("MinPrice") %>'></asp:Label>
									<asp:Label ID="txtMaxPrice" CssClass="des" runat="server" Text='<%# " - " & Eval("MaxPrice") & " บาท" %>'></asp:Label><br />
									<asp:HyperLink ID="HyperLinkData" Width="260px" NavigateUrl='<%# Eval("OrderURL") %>'
										runat="server" Text="สั่งซื้อ">
									</asp:HyperLink>
								</div>
							</ItemTemplate>
						</asp:DataList>
					</div>
				</div>
			</td>
		</tr>
	</table>
</asp:Panel>
<div style="background-color: #fff; margin: auto; clear: both; padding-top: 20px;">
	<div id="btn-card" align="left">
		<asp:LinkButton ID="LinkFile" runat="server" class="btn-face2"><< กลับไปหน้า Attach Files</asp:LinkButton>
	</div>
</div>
<br />
