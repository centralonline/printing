Imports Microsoft.VisualBasic

Public Class clsControl

    Private NumOfVisitorValue As Integer
    Public Property NumOfVisitor() As Integer
        Get
            Return NumOfVisitorValue
        End Get
        Set(ByVal value As Integer)
            NumOfVisitorValue = value
        End Set
    End Property

    Private CountUserStepValue As Integer
    Public Property CountUserStep() As Integer
        Get
            Return CountUserStepValue
        End Get
        Set(ByVal value As Integer)
            CountUserStepValue = value
        End Set
    End Property

    Private StartCountValue As Integer
    Public Property StartCount() As Integer
        Get
            Return StartCountValue
        End Get
        Set(ByVal value As Integer)
            StartCountValue = value
        End Set
    End Property

End Class
