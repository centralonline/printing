﻿Imports CoreBaseII
Imports System.Collections.Generic

Partial Class Admin_ajCardInformation
   Inherits clsHTTPBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim CardID As String = Request("CardID")

        'TBBCCards
        Dim CardName1 As String = Request("CardName1")
        Dim BGImage1 As String = Request("BGImage1")
        Dim BGColor1 As String = Request("BGColor1")
        Dim TemplateImage1 As String = Request("TemplateImage1")
        Dim Width1 As String = Request("Width1")
        Dim Height1 As String = Request("Height1")
        Dim Category1 As String = Request("Category1")

        Dim ObjectCard As New clsCard

        With ObjectCard
            .CardName = CardName1
            .BGImage = BGImage1
            .BGColor = BGColor1
            .templateImage = TemplateImage1
            .Width = Width1
            .Height = Height1
            .Category = Category1
        End With

        'TBBCImageFields
        Dim ImageUrl2 As String = Request("ImageUrl2")
        Dim PositionTop2 As String = Request("PositionTop2")
        Dim PositionLeft2 As String = Request("PositionLeft2")
        Dim zIndex2 As String = Request("zIndex2")
        Dim Width2 As String = Request("Width2")
        Dim Height2 As String = Request("Height2")
        Dim ImageSize2 As String = Request("ImageSize2")
        Dim PreviousName2 As String = Request("PreviousName2")

        Dim ObjectImageFields As New clsCardImageField

        With ObjectImageFields
            .imageUrl = ImageUrl2
            .PositionTop = PositionTop2
            .PositionLeft = PositionLeft2
            .zIndex = zIndex2
            .width = Width2
            .Height = Height2
            .ImageSize = ImageSize2
            .PreviousName = PreviousName2
        End With

        Dim CardInfo As New clsCardProvider()

        Dim Complete As Boolean = CardInfo.SaveTemplate(ObjectCard, ObjectImageFields, CardID)

        If Complete Then
            Response.Write("บันทึกข้อมูลเรียบร้อยแล้วค่ะ")
            objSessionVar.ThisCardID = ""
        Else
            Response.Write("Error!!! กรุณาลองใหม่อีกครั้งค่ะ")
        End If

    End Sub

End Class