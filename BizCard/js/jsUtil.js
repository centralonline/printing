﻿//========================  get  Object from Document  ==============================
//function getObj(idname)
//{
//	if (getObj)
//	{
//	    if (document.getElementById(idname)) return document.getElementById(idname);
//	    if (document.getElementById("ctl00_"+idname)) return document.getElementById("ctl00_"+idname);
//	    if (document.getElementById("ctl00_Top1_"+idname)) return document.getElementById("ctl00_Top1_"+idname);  	    
//	    if (document.getElementById("ctl00_ctCenter_"+idname)) return document.getElementById("ctl00_ctCenter_"+idname); 	    
//		return null;
//	}
//	else if (document.all)
//	{
//		return document.all[idname];
//	}
//	else if (document.layers)
//	{
//		return document.layers[idname];
//	}
//	else
//	{
//		return null;
//	}
//}

//================================  showPopup  and Dialog ===================================
function prvShowPopupRun(url,title,mywidth,myheight,scroll) {
    var prop = new String();
    if (mywidth == null) {
        prop = "center:yes;unadorned:no;resizable=no;scroll=no;status=no;edge:sunken";
    } else {
        prop = "center:yes;unadorned:no;resizable=no;scroll=no;status=no;edge:sunken;dialogWidth:"+mywidth+"px;dialogHeight:"+myheight+"px;";
    }
   
   var fullURL = new String();
   fullURL = "Popup.aspx?MyURL="+url;
   if (title != null) {
        fullURL += "&MyTitle="+title
   }
   if (scroll != null) {
        fullURL += "&IsScrolling="+scroll
   }
   
   window.showModalDialog(fullURL,"", prop);
    
    if (readCookie("RefreshPage") == "true") { 
        eraseCookie("RefreshPage"); 
        window.location.reload();
    }  
}

function showPopup(url,title,mywidth,myheight) {
    prvShowPopupRun(url,title,mywidth,myheight,null);
}

function showPopupWithScroll(url,title,mywidth,myheight) {
    prvShowPopupRun(url,title,mywidth,myheight,"yes");
}

function showPopupAdmin(url, title, width, height) {
    if (url != "") { 
        window.showModalDialog("PopupAdmin.aspx?MyURL="+url+"&MyTitle="+title,"", "center:yes;unadorned:no;resizable=no;scroll=no;status=no;edge:sunken;dialogWidth:"+width+"px;dialogHeight:"+height+"px;")
        window.location.reload();
    }
}

function showAsDialog(obj) {
    var scrW = screen.width;
    var scrH = screen.height;
    var objW = obj.style.width.replace('px','');
    var objH = obj.style.height.replace('px','');
    var intLeft =  ((scrW/2) - (objW/2));
    var intTop =  ((scrH/2) - (objH/2)-150); 
    obj.style.display = "inline";
    obj.style.position = "absolute";
    obj.style.left = intLeft;
    obj.style.top = intTop;
}


//============================ Cookies Module ==================================
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

//============================ Math Module ==================================
function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}

//============================ String Module ============================
function Trim(TRIM_VALUE){
    if(TRIM_VALUE.length < 1){
      return"";
    }
    TRIM_VALUE = RTrim(TRIM_VALUE);
    TRIM_VALUE = LTrim(TRIM_VALUE);
    if(TRIM_VALUE==""){
      return "";
    }
    else{
      return TRIM_VALUE;
    }
}

function RTrim(VALUE){
    var w_space = String.fromCharCode(32);
    var v_length = VALUE.length;
    var strTemp = "";
    if(v_length < 0){
        return"";
    }
    var iTemp = v_length -1;
    while(iTemp > -1){
          if(VALUE.charAt(iTemp) == w_space){
          }
          else{
            strTemp = VALUE.substring(0,iTemp +1);
            break;
          }
          iTemp = iTemp-1;
    } //End While
    return strTemp;
} //End Function

function LTrim(VALUE){
    var w_space = String.fromCharCode(32);
    if(v_length < 1){
        return"";
    }
    var v_length = VALUE.length;
    var strTemp = "";
    var iTemp = 0;
    while(iTemp < v_length){
          if(VALUE.charAt(iTemp) == w_space){
          }
          else{
            strTemp = VALUE.substring(iTemp,v_length);
            break;
          }
          iTemp = iTemp + 1;
    } //End While
    return strTemp;
} //End Function


//============================ Validate Date Format ============================
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : dd/mm/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

//============================ XXX ============================
