Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports CoreBaseII
Imports ModelUser

Public Class clsSessionVarWC

#Region "Properties"

	Private ThisModelUser As ModelUser
	Public Property ModelUser() As ModelUser
		Get
			Return ThisModelUser
		End Get
		Set(ByVal value As ModelUser)
			ThisModelUser = value
		End Set
    End Property

    '������� ���� Temp CardID �ͧ Admin 㹡�����ҧ��������
    Private ThisCardIDValue As String
    Public Property ThisCardID() As String
        Get
            Return ThisCardIDValue
        End Get
        Set(ByVal value As String)
            ThisCardIDValue = value
        End Set
    End Property

    Private ThisPaper As List(Of clsPaper)
    Public Property Paper() As List(Of clsPaper)
        Get
            Return ThisPaper
        End Get
        Set(ByVal value As List(Of clsPaper))
            ThisPaper = value
        End Set
    End Property

#End Region

#Region "Method"

    Private objAppVar As clsAppVarWC

    Public Sub New(ByVal objAppVar As clsAppVarWC)
        Me.objAppVar = objAppVar
    End Sub

#End Region

End Class
