﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChangePass.aspx.vb" Inherits="ChangePass" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Change Password - นามบัตร หัวจดหมาย การ์ด โปสเตอร์ ใบปลิว ราคางานพิมพ์</title>
	<style type="text/css">
		.style1
		{
			color: #FF0000;
		}
		.column-register
		{
			text-align: left;
		}
		.clear
		{
			clear: both;
		}
	</style>
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {

			$(function () {
				$("#btnSave").click(function () {

					if ($("#txtNewPass").val() == "" || $("#txtReNewPass").val() == "" || $("#txtOldPass").val() == "") {
						alert("กรุณากรอกข้อมูลให้ถูกต้องด้วยค่ะ");
						return false;
					}

					if ($("#txtNewPass").val() != $("#txtReNewPass").val()) {
						alert("กรุณายืนยันรหัสผ่านใหม่ให้ถูกต้องด้วยค่ะ");
					}
					else {

						var OldPass = $("#txtOldPass").val();
						var NewPass = $("#txtNewPass").val();

						$.ajax({
							type: 'POST',
							url: "ajChangePass.aspx",
							data: { "OldPass": OldPass, "NewPass": NewPass },
							success: function (msg) {
								alert(msg);
								if (msg == 'บันทึกข้อมูลเรียบร้อยแล้วค่ะ') {
									window.location.href = "MyAccount.aspx";
								}
							}
						});
					}
				});
			});

		});
	</script>
</head>
<body>
	<form id="ChangePassForm" runat="server">
	<div align="left" style="margin: auto; width: 700px;">
		<div id="ctl00_BodyCenter_panAccount" style="">
			<div class="column-register">
				<b><font color="#ff6600"><span id="ctl00_BodyCenter_Label1">แก้ไขรหัสผ่าน</span></font></b><br />
			</div>
			<br class="clear" />
			<div class="column-register">
				<b><span id="ctl00_BodyCenter_Label1">รหัสผ่านปัจจุบัน</span></b>
			</div>
			<div class="column-register">
				<input name="txtOldPass" id="txtOldPass" style="width: 200px;" type="password" maxlength="50" />&nbsp;<span
					class="style1">*</span>&nbsp;
			</div>
			<br class="clear" />
			<div class="column-register">
				<b><span id="ctl00_BodyCenter_Label2">สร้างรหัสผ่านใหม่</span></b>
			</div>
			<div class="column-register">
				<input name="txtNewPass" id="txtNewPass" style="width: 200px;" type="password" maxlength="50" />&nbsp;<span
					class="style1">*</span>&nbsp;
			</div>
			<br class="clear" />
			<div class="column-register">
				<b><span id="ctl00_BodyCenter_Label3">ยืนยันรหัสผ่านใหม่</span></b>
			</div>
			<div class="column-register">
				<input name="txtReNewPass" id="txtReNewPass" style="width: 200px;" type="password"
					maxlength="50" />&nbsp;<span class="style1">*</span>&nbsp;
			</div>
			<br />
			<br class="clear" />
			<div class="column-register" style="border-top: solid 1px #999999; padding-top: 20px;">
				<input id="btnSave" type="button" name="btnSave" value="Save" class="btn-face" /></div>
			<br class="clear" />
		</div>
	</div>
	</form>
</body>
</html>
